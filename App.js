import React, { Component } from 'react';
// import Setup from './js/Setup.js';
import AppNavigator from './js/navigation/AppNavigator';
import { Platform, AppState, AsyncStorage, PermissionsAndroid } from 'react-native';
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from 'react-native-fcm';
FCM.on(FCMEvent.Notification, async (notif) => {

  console.log('notif....', notif);

  // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
  if (notif.local_notification) {
    //this is a local notification
  }
  if (notif.opened_from_tray) {
    //iOS: app is open/resumed because user clicked banner
    //Android: app is open/resumed because user clicked banner or tapped app icon
  }
  // await someAsyncCall();

  if (Platform.OS === 'ios') {
    if (notif._actionIdentifier === 'com.preskil') {
      // handle notification action here
      // the text from user is in notif._userText if type of the action is NotificationActionType.TextInput
    }
    //optional
    //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application.
    //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
    //notif._notificationType is available for iOS platfrom
    switch (notif._notificationType) {
      case NotificationType.Remote:
        notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
        break;
      case NotificationType.NotificationResponse:
        notif.finish();
        break;
      case NotificationType.WillPresent:
        notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
        break;
    }
  } else { // Android

    FCM.createNotificationChannel({
      id: 'default',
      name: 'Default',
      description: 'used for example',
      priority: 'high'
    })

    console.log('FCM Data....', notif.fcm.body);

    FCM.presentLocalNotification({
      id: '1',                               // (optional for instant notification)
      title: notif.fcm.title,                     // as FCM payload
      body: notif.fcm.body,                    // as FCM payload (required)
      sound: "default",                                   // as FCM payload
      priority: "high",                                   // as FCM payload
      click_action: "com.preskil",               // as FCM payload - this is used as category identifier on iOS.
      badge: 0,                                          // as FCM payload IOS only, set 0 to clear badges
      // number: 10,                                         // Android only
      ticker: notif.fcm.title,                   // Android only
      auto_cancel: true,                                  // Android only (default true)
      // large_icon: "ic_launcher",                           // Android only
      // icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap
      big_text: notif.fcm.body,     // Android only
     // sub_text: notif.fcm.body,                      // Android only
     // color: "red",                                       // Android only
      vibrate: 300,                                       // Android only default: 300, no vibration if you pass 0
      wake_screen: true,                                  // Android only, wake up screen when notification arrives
      // group: "group",                                     // Android only
      // picture: "https://www.google.co.in/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",                      // Android only bigPicture style
      ongoing: false,                                      // Android only
      // my_custom_data: 'my_custom_field_value',             // extra data you want to throw
      lights: true,                                       // Android only, LED blinking (default false)
      show_in_foreground: true,                                  // notification when app is in foreground (local & remote)
      channel: 'default', 
      
    });
  }
});
import Keys from './js/constants/Keys';

export default class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      mapRegion: {
        latitude: null,
        longitude: null,
        // latitudeDelta: 1,
        // longitudeDelta: 1
      }
    }
  }

  componentDidMount() {
    FCM.requestPermissions()
      .then(() => console.log('granted'))
      .catch(() => console.log('notification permission rejected'));

    FCM.getFCMToken().then(token => {
      console.log("token...", token);
      this.setState({ fcm_token: token });
      //update your fcm token on server.
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      console.log('notificationListener notif....', notif);
    });

    
    FCM.getInitialNotification().then(notif => {
      console.log('getInitialNotification....', notif)
    });


    console.log('this.state.appState', this.state.appState);

    //this.loadMapDetails();
    //this.permissionandroid();


  }

  loadMapDetails = async () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log("wokeeey");
        console.log("wokeeey", position);
        // this.setState({
        //   latitude: position.coords.latitude,
        //   longitude: position.coords.longitude,
        //   error: null,
        // });
        this.setState({
          mapRegion: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 1,
            longitudeDelta: 1
          }
        });
        AsyncStorage.setItem(Keys.mapDetail, JSON.stringify(this.state.mapRegion));

      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    );



  }

  _loadrender = async () => {
    const mapdetails = await AsyncStorage.getItem(Keys.mapDetail);
    console.log("mapDetails", mapdetails)
  }

  // permissionandroid = async () =>{
  // 	try {
  // 		const granted = await PermissionsAndroid.request(
  // 		  PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
  // 		  {
  // 			'title': 'This App needs your Location Permission',
  // 			'message': 'This App needs access to your Location for finding the location'
  // 		  }
  // 		)
  // 		if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  // 		  console.log("You can use the location")
  // 		} else {
  // 		  console.log("Location permission denied")
  // 		}
  // 	  } catch (err) {
  // 		console.warn(err)
  // 	  }
  // }
  render() {
    this._loadrender();
    //console.log("wokeeey", this.state.mapRegion);
    return (
      <AppNavigator />
    );
  }
}

