package com.preskil;

import android.app.Application;

import com.arubanetworks.meridian.Meridian;
import com.arubanetworks.meridian.editor.EditorKey;
import com.facebook.react.ReactApplication;
import com.github.yamill.orientation.OrientationPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;
import com.reactnativecommunity.cameraroll.CameraRollPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.brentvatne.react.ReactVideoPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.clipsub.rnbottomsheet.RNBottomSheetPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.evollu.react.fcm.FIRMessagingPackage;
import io.invertase.firebase.app.ReactNativeFirebaseAppPackage;
import io.invertase.firebase.analytics.ReactNativeFirebaseAnalyticsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {


  public static final EditorKey APP_KEY = EditorKey.forApp("4748497067966464");
  public static final EditorKey MAP_KEY = EditorKey.forMap("4748497067966464", APP_KEY);

  // To build your own customized SDK based App, replace APP_KEY and MAP_KEY with your location's App and Map ID values:
  // public static final EditorKey APP_KEY = EditorKey.forApp("APP_KEY");
  // public static final EditorKey MAP_KEY = EditorKey.forMap("MAP_KEY", APP_KEY);

  // To build the default Sample SDK App for EU Servers, use the following:
  // NOTE: Even if you're geographically located in the EU, you probably won't need to do this.
  // public static final EditorKey APP_KEY = EditorKey.forApp("4856321132199936");
  // public static final EditorKey MAP_KEY = EditorKey.forMap("5752754626625536", APP_KEY);

  public static final String PLACEMARK_UID = "CASIO_UID"; // replace this with a unique id for one of your placemarks.
  public static final String CAMPAIGN_ID = ""; // unique id for one of your campaigns here.
  public static final String TAG_MAC = ""; // mac address of one of your tags here
  public static final String EDITOR_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0IjoxNTYwNzY4MzI1LCJ2YWx1ZSI6ImZjZDk2YmQ3MGI4NGU0ZDRlODMwNThlZjRlOTNlN2IxMjZmOGI4OTYifQ.4gX_2lS6pUSSh8xed_phYyO_HLJ4wROmhgjx8sruBiQ"; // your editor token here

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new OrientationPackage(),
            new FBSDKPackage(),
            new VectorIconsPackage(),
            new RNAndroidLocationEnablerPackage(),
            new CameraRollPackage(),
            new RNFetchBlobPackage(),
            new ReactNativeRestartPackage(),
            new MapsPackage(),
            new ReactVideoPackage(),
            new RNGoogleSigninPackage(),
            new RNI18nPackage(),
            new RNBottomSheetPackage(),
            new FIRMessagingPackage(),
              new ReactNativeFirebaseAppPackage(),
              new ReactNativeFirebaseAnalyticsPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    Meridian.configure(this);
    Meridian.getShared().setEditorToken(EDITOR_TOKEN);
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);

    // Create notification channel for Oreo
    CampaignReceiver.createNotificationChannel(this);
  }
}
