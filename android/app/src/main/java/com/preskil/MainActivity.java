package com.preskil;

import com.facebook.react.ReactActivity;
import android.content.Intent;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;


import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.arubanetworks.meridian.campaigns.CampaignsService;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    public void onNewIntent(Intent intent) {
                super.onNewIntent(intent);
                setIntent(intent);
            }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)  != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            else
                CampaignsService.resetAllCampaigns(getApplicationContext(),MainApplication.APP_KEY, null, null);
                CampaignsService.startMonitoring(getApplicationContext(), MainApplication.APP_KEY);
        }


    }
    @Override
    protected String getMainComponentName() {
        return "Preskil";
    }
}
