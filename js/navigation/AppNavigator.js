import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  AsyncStorage,
  Platform
} from 'react-native';
import { connect } from "react-redux";
import {
  Scene,
  Router,
  Actions,
  Reducer,
  Stack,
  Modal,
  Tabs,
} from "react-native-router-flux";
import Login from '../screens/Login';
import Language from '../screens/Language';
import GuestLogin from '../screens/GuestLogin';
import Hotel from '../screens/HotelSelection';
import Register from '../screens/Register';
import RegisterEmailOTP from '../screens/RegisterEmailOTP';
import MyProfile from '../screens/MyProfile';
import Colors from '../constants/Colors';
// import RestaurantBooking from '../screens/RestaurantBooking';
import GuestHome from '../screens/GuestHome';
import BookingsNewList from '../screens/BookingsNewList';
import DashboardDup from '../screens/DashboardDup';
import GuestCheckIn from '../screens/GuestCheckIn';
import GuestWelcome from '../screens/GuestWelcome';
import CheckInHome from '../screens/CheckInHome';
import CustomerHome from '../screens/CustomerHome';
import HotelInfo from '../screens/HotelInfo';
import UserWelcome from '../screens/UserWelcome';
import RestaurantListView from '../screens/RestaurantListView';
import RestaurantView from '../screens/RestaurantView';
import FoodDetailView from '../screens/FoodDetailView';
// import ListRestaurantBookings from '../screens/ListRestaurantBookings';
import CheckInDetails from '../screens/CheckInDetails';
import FlightDetails from '../screens/FlightDetails';
import PhoneDirectory from '../screens/PhoneDirectory';
import TVChannels from '../screens/TVChannels';
import DigitalKey from '../screens/DigitalKey';
import Bookings from '../screens/Bookings';
import BookingsList from '../screens/BookingsList';
import Services from '../screens/Services';
import FoodDetails from '../screens/FoodDetails';
import FoodMenus from '../screens/FoodMenus';
import Cart from '../screens/Cart';
import BookingCart from '../screens/BookingCart';
import Calendar from '../screens/Calendar';
import ChangePassword from '../screens/ChangePassword';
import DynamicMenuContent from '../screens/DynamicMenuContent';
import DynamicSubMenu from '../screens/DynamicSubMenu';
import DynamicWebpage from '../screens/DynamicWebpage';
import DynamicWebpage1 from '../screens/DynamicWebpage1';
import Maps from '../screens/Maps';
import ForgotPassword from '../screens/ForgotPassword';
import TextBoxWithLink from '../components/TextBoxWithLink';
import ForgotPasswordOTP from '../screens/ForgotPasswordOTP';
import SPADropdown from '../screens/SPADropdown';
import TermsandConditions from '../screens/TermsandConditions';
import BikeSafetyPolicy from '../screens/BikeSafetyPolicy';
import BeaconDetails from '../screens/BeaconDetails';
import GroupDisclaimer from '../screens/GroupDisclaimer';
import WeatherForecast from '../screens/WeatherForecast';
import AccuWeatherForecast from '../screens/AccuWeatherForecast';
import Amenities from '../screens/Amenities';
import AmenitiesList from '../screens/AmenitiesList';
import RestaurantList from '../screens/RestaurantList';

import GuestProfile from '../screens/GuestProfile';

import RestaurantBooking from '../screens/RestaurantBooking';
import SpaBooking from '../screens/SpaBooking';
import BikeBooking from '../screens/BikeBooking';
import BoatBooking from '../screens/BoatBooking';
import RoomServiceBooking from '../screens/RoomServiceBooking';

import CreateRestaurantBooking from '../screens/CreateRestaurantBooking';
import CreateSpaBooking from '../screens/CreateSpaBooking';
import CreateBikeBooking from '../screens/CreateBikeBooking';
import CreateBoatBooking from '../screens/CreateBoatBooking';
import CreateBoatBooking1 from '../screens/CreateBoatBooking1';
import CreateBoatBooking2 from '../screens/CreateBoatBooking2';
import BookingsRatings from '../screens/BookingsRatings';

import HeaderProfileRightMenu from '../components/HeaderProfileRightMenu';

import AppFont from '../constants/AppFont';
import Keys from '../constants/Keys';
import closeImage from '../../assets/images/close.png';
import logout from '../../assets/images/logout.png';
import I18n from '../constants/i18n';


import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
//const RouterWithRedux = connect()(Router);

export default class AppNavigator extends React.Component {

  constructor() {
    super();
  }

  render() {
    return (
      <Router>
        <Modal
          key="modal"
          hideNavBar>
          <Stack key="root">
            {/* <Scene key="RestaurantBooking" component={RestaurantBooking} title="RestaurantBooking" navigationBarStyle={styles.navBar} initial={true} headerTitleStyle={styles.headerText} /> */}
            <Scene key="Language" component={Language} title="Language" initial={true} hideNavBar />
            <Scene key="GuestLogin" component={GuestLogin} title="GuestLogin" hideNavBar />
            
            <Scene key="Login" component={Login} title="Login" hideNavBar />
            <Scene key="Hotel" component={Hotel} title="Hotel" hideNavBar />
            <Scene key="Register" component={Register} title="Register" navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene key="RegisterEmailOTP" component={RegisterEmailOTP} title="Email Verification" navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color} />
            <Scene key="ForgotPassword" component={ForgotPassword} title="Forgot Password" navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText} leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color} />
            <Scene key="TextBoxWithLink" component={TextBoxWithLink} title="TextBox With Link" navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color} />
            <Scene key="ForgotPasswordOTP" component={ForgotPasswordOTP} title="Forgot Password"
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color} />
            <Scene key="GuestHome" component={GuestHome} title="" navigationBarStyle={styles.navBar}
              // headerTitleStyle={styles.headerText}
              // leftButtonImage={closeImage}
              // navTransparent={1}
              // navBarButtonColor={Colors.Background_Color} 
              hideNavBar={true}
              />
            <Scene key="BookingsNewList" component={BookingsNewList} title="Bookings" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              rightButtonImage={logout}
                onRight={this.onSignOut}
                rightButtonTextStyle={{ marginLeft: 30 }}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene key="DashboardDup" component={DashboardDup} title="DashBoard" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              hideNavBar={true} 
              navBar
              ButtonColor={Colors.Background_Color}
            />
            <Scene key="RestaurantListView" component={RestaurantListView} title="Preskil Island Restaurants" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
            <Scene key="RestaurantView" component={RestaurantView} title="Preskil Island Restaurants" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
            <Scene key="FoodDetailView" component={FoodDetailView} title="Preskil Island Restaurants" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
              <Scene key="RestaurantBooking" component={RestaurantBooking} title="Restaurant Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="CreateRestaurantBooking" component={CreateRestaurantBooking} title="Create Restaurant Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="SpaBooking" component={SpaBooking} title="SPA Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="CreateSpaBooking" component={CreateSpaBooking} title="Create Spa Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="BikeBooking" component={BikeBooking} title="Bike Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="CreateBikeBooking" component={CreateBikeBooking} title="Create Bike Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="BoatBooking" component={BoatBooking} title="Boat Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="CreateBoatBooking" component={CreateBoatBooking} title="Boat List" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="CreateBoatBooking1" component={CreateBoatBooking1} title="Boat Details" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
              <Scene key="CreateBoatBooking2" component={CreateBoatBooking2} title="Create Boat Booking" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
             <Scene key="BookingsRatings" component={BookingsRatings} title="Ratings & Reviews" navigationBarStyle={styles.navBar1}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              leftButtonStyle={{ paddingLeft: 0 }}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene key="GuestCheckIn" component={GuestCheckIn} title="Check-In"
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              navBarButtonColor={Colors.Background_Color} />
              <Scene key="CheckInHome" component={CheckInHome} title="Check-In"
              navigationBarStyle={styles.navBar}
              hideNavBar={true} />
            <Scene key="GuestProfile" component={GuestProfile} title="My Profile"
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              navBarButtonColor={Colors.Background_Color}
              rightButtonImage={logout}
              onRight={this.onSignOut}
              rightButtonTextStyle={{ marginLeft: 30 }}
              tintColor="white" />
            <Scene key="GuestWelcome" component={GuestWelcome} title="Welcome"
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              navBarButtonColor={Colors.Background_Color} 
              hideNavBar={true}/>
            {/* <Scene key="MyProfile" component={MyProfile} title="My Profile" navigationBarStyle={styles.navBar} headerTitleStyle={styles.headerText} rightButtonImage={logout} /> */}
            {/* <Scene
              key="ListRestaurantBookings"
              title="Booking History"
              component={ListRestaurantBookings}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navTransparent={1}
              navBarButtonColor={Colors.Background_Color}
            /> */}
            <Scene
              key="BookingsList"
              title="Booking History"
              component={BookingsList}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navTransparent={1}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="CheckInDetails"
              title="Check-In"
              component={CheckInDetails}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="HotelInfo"
              component={HotelInfo}
              title="Southern Cross"
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              navTransparent={1}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="UserWelcome"
              component={UserWelcome}
              title="Southern Cross"
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              leftButtonImage={closeImage}
              navTransparent={1}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
            <Scene
              key="FlightDetails"
              title="Flight Details"
              component={FlightDetails}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="PhoneDirectory"
              title="Phone Directory"
              component={PhoneDirectory}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="Amenities"
              title="Amenities"
              component={Amenities}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="AmenitiesList"
              title="Booking List"
              component={AmenitiesList}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            {Platform.OS === 'ios' ?
              <Scene
                key="TVChannels"
                title="Live TV"
                component={TVChannels}
                // navTransparent={1}
                // hideNavBar={true} 
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                navBarButtonColor={Colors.Background_Color}
              /> :
              <Scene
                key="TVChannels"
                title="Live TV"
                component={TVChannels}
                // navTransparent={1}
                hideNavBar={true}
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                navBarButtonColor={Colors.Background_Color}
              />
            }
            <Scene
                key="DigitalKey"
                title="Digital Key"
                component={DigitalKey}
                // navTransparent={1}
                hideNavBar={false}
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                navBarButtonColor={Colors.Background_Color}
              />
            <Scene
              key="Maps"
              title="Maps"
              component={Maps}
              navigationBarStyle={styles.navBar}
              //   titleStyle={{fontFamily:AppFont.SemiBold,fontSize: 20,
              //     color: Colors.Background_Color,fontWeight:"normal"
              // }}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="ChangePassword"
              title={I18n.t('title_change_pwd')}
              component={ChangePassword}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="DynamicMenuContent"
              title="Southern Cross"
              component={DynamicMenuContent}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="DynamicSubMenu"
              title="Southern Cross"
              component={DynamicSubMenu}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="DynamicWebpage"
              component={DynamicWebpage}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
            <Scene
              key="DynamicWebpage1"
              component={DynamicWebpage1}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
            <Scene
              key="SPADropdown"
              title="Spa Package"
              component={SPADropdown}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="TermsandConditions"
              title="Terms and Condtions"
              component={TermsandConditions}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="BikeSafetyPolicy"
              title="Disclaimer"
              component={BikeSafetyPolicy}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="GroupDisclaimer"
              title="Group Disclaimer"
              component={GroupDisclaimer}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="BeaconDetails"
              title="Beacon Details"
              component={BeaconDetails}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="WeatherForecast"
              title="Weather"
              component={WeatherForecast}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="AccuWeatherForecast"
              title="Weather"
              component={AccuWeatherForecast}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />

            {/* <Scene
              key="Services"
              title="Services"
              component={Services}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
              tabBarIcon={(props) =>
                (
                  <View style={styles.tabiconContainer}>
                   
                    <Image source={{ uri: 'service_2d' }} style={{ width: 26, height: 26, marginTop: 3 }}></Image>
                    <Text style={[{ color: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }, styles.tabLabel]}>Service</Text>
                  </View>
                )}
            /> */}
            <Scene
              key="Services"
              title="Services"
              component={Services}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="FoodDetails"
              title="FoodDetails"
              component={FoodDetails}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="FoodMenus"
              title="F & B"
              component={FoodMenus}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
              hideNavBar={true}
            />
            <Scene
              key="Cart"
              title="My Cart"
              component={Cart}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Scene
              key="BookingCart"
              title="My Cart"
              component={BookingCart}
              navigationBarStyle={styles.navBar}
              headerTitleStyle={styles.headerText}
              navBarButtonColor={Colors.Background_Color}
            />
            <Tabs
              key="tabbar"
              tabBarPosition='bottom'
              // inactiveBackgroundColor={Colors.FooterMenu_Bg}
              // activeBackgroundColor={Colors.FooterMenu_Bg}
              tabBarStyle={{ backgroundColor: Colors.FooterMenu_Bg }}
              // tabBarOnPress={(obj)=>{
              //   console.log('tabBarOnPress',obj)
              // }}
              inactiveTintColor={Colors.FooterMenu_Text}
              activeTintColor={Colors.App_Font}
              showLabel={false}
              hideNavBar
            >
              <Scene
                key="Bookings"
                component={BookingsNewList}
                title={I18n.t('bookings')}
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                hideNavBar={true}
                // onEnter={() => { this.verifyCheckInDetails(); }}
                tabBarIcon={(props) =>
                  (
                    <View style={styles.tabiconContainer}>
                      {/* <Image source={{ uri: 'bookings' }} style={{ width: 24, height: 25, marginTop: 3, tintColor: props.focused ? props.tintColor : Colors.FooterMenu_Text }}></Image> */}
                      <Image source={{ uri: 'bookings_2d' }} style={{ width: 24, height: 25, marginTop: 3, }}></Image>
                      <Text style={[{ color: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }, styles.tabLabel]}>{I18n.t('bookings')}</Text>
                    </View>
                  )}
              />
              <Scene
                key="Calendar"
                title={I18n.t('calendar')}
                component={Calendar}
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                tabBarIcon={(props) =>
                  (
                    <View style={styles.tabiconContainer}>
                      {/* <Image source={{ uri: 'calendar' }} style={{ width: 24, height: 25, marginTop: 3, tintColor: props.focused ? props.tintColor : Colors.FooterMenu_Text }}></Image> */}
                      <Image source={{ uri: 'calendar_2d' }} style={{ width: 24, height: 25, marginTop: 3, }}></Image>
                      <Text style={[{ color: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }, styles.tabLabel]}>{I18n.t('calendar')}</Text>
                    </View>
                  )}
              />

              <Scene
                key="CustomerHome"
                title="CustomerHome"
                initial={true}
                component={CustomerHome}
                // navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                onExit={() => console.log('onExit')}
                tabBarIcon={(props) =>
                  (
                    <View style={styles.tabiconContainer}>
                      {/* <Image source={{ uri: 'home' }} style={{ width: 24, height: 25, marginTop: 3, tintColor: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }}></Image> */}
                      <Image source={{ uri: 'home_2d' }} style={{ width: 24, height: 25, marginTop: 3, }}></Image>
                      <Text style={[{ color: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }, styles.tabLabel]}>{I18n.t('home')}</Text>
                    </View>
                  )}
                hideNavBar
              />
              <Scene
                key="RoomServiceBooking"
                title={I18n.t('room_service')}
                component={RoomServiceBooking}
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                tabBarIcon={(props) =>
                  (
                    <View style={styles.tabiconContainer}>
                      {/* <Image source={{ uri: 'service' }} style={{ width: 26, height: 26, marginTop: 3, tintColor: props.focused ? props.tintColor : Colors.FooterMenu_Text }}></Image> */}
                      {/* <Image source={{ uri: 'service_2d' }} style={{ width: 26, height: 26, marginTop: 3 }}></Image> */}
                      <Image source={{ uri: 'roomservices_2d' }} style={{ width: 26, height: 26, marginTop: 3 }}></Image>
                      <Text style={[{ color: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }, styles.tabLabel]}>{I18n.t('room_service')}</Text>
                    </View>
                  )}
              />

              <Scene
                key="MyProfile"
                title={I18n.t('profile_tab')}
                component={MyProfile}
                navigationBarStyle={styles.navBar}
                headerTitleStyle={styles.headerText}
                rightButtonImage={logout}
                onRight={this.onSignOut}
                rightButtonTextStyle={{ marginLeft: 30 }}
                tintColor="white"
                //renderRightButton={HeaderProfileRightMenu}
                tabBarIcon={(props) =>
                  (
                    <View style={styles.tabiconContainer}>
                      {/* <Image source={{ uri: 'profile' }} style={{ width: 23, height: 25, marginTop: 3, tintColor: props.focused ? props.tintColor : Colors.FooterMenu_Text }}></Image> */}
                      <Image source={{ uri: 'profile_2d' }} style={{ width: 23, height: 25, marginTop: 3, }}></Image>
                      <Text style={[{ color: props.focused ? props.activeTintColor : Colors.FooterMenu_Text }, styles.tabLabel]}>{I18n.t('profile_tab')}</Text>
                    </View>
                  )}
              />
              


            </Tabs>
          </Stack>
        </Modal>
      </Router>
    );
  }


  // verifyCheckInDetails = async () => {
  //   console.log("Appnavigator verifyCheckInDetails")
  //   await this.setState({ loading: true });
  //   const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
  //   let overalldetails = {
  //     "CheckInId": checkInVal,
  //   }
  //   console.log("overalldetails", overalldetails);
  //   await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
  // }

  // postCheckInDetailsResponse = async (response) => {
  //     await this.setState({ loading: false });
  //     var postCheckInDetailResponse = response.ResponseData;
  //     console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
  //     if (postCheckInDetailResponse.InCustomer == "False") {
  //         console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
  //         Alert.alert(I18n.t('alertnotcheckin'));
  //         AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
  //         AsyncStorage.removeItem(Keys.UserDetail);
  //         AsyncStorage.removeItem(Keys.roomNo);
  //         AsyncStorage.removeItem(Keys.inCustomer);
  //         AsyncStorage.removeItem(Keys.checkInId);
  //         Actions.GuestLogin();
  //     }
  // }

  onSignOut = () => {
    Alert.alert(I18n.t('signout'), '',
      [
        {
          text: I18n.t('cancel'),
          onPress: () => console.log('Cancel Pressed'),
          //style: 'cancel'
        },
        {
          text: I18n.t('yes'),
          onPress: () => this.onSignOutAction(),
          //style: 'destructive'
        },
      ],
      {
        cancelable: false
      }
    )
  };

  onSignOutAction = async () => {

    AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    AsyncStorage.removeItem(Keys.UserDetail);
    AsyncStorage.removeItem(Keys.roomNo);
    AsyncStorage.removeItem(Keys.inCustomer);
    AsyncStorage.removeItem(Keys.checkInId);

    //setTimeout(() => {

    //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
    Actions.GuestLogin({ type: 'replace' });
    // }, 500);

    // Actions.pop(); Actions.pop(); delete_item()

    // Actions.GuestLogin({type:'reset'});delete_item()


    //Actions.popTo('GuestLogin');
  }
  //Logout method ends

}

const styles = StyleSheet.create({
  headerText: {
    fontFamily: AppFont.SemiBold,
    fontSize: 20,
    color: Colors.Background_Color,
    fontWeight: 'normal',

  },
  navBar: {
    // height:40,
    backgroundColor: Colors.App_Font
  },
  navBar1: {
    backgroundColor: Colors.App_Font,
    marginTop:0
  },
  tabiconContainer: {
    height: 45,
    alignItems: 'center',

  },
  tabicon: {
    fontSize: 18
  },
  tabLabel: {
    paddingTop: 3,
    paddingBottom: 5,
    fontSize: 11,
    fontFamily: AppFont.Regular,
    width: "100%"
  }

})