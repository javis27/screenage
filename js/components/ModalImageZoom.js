import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Modal,
    Dimensions,
    Animated,
    TouchableOpacity,
    Text,
    TouchableHighlight,
    Image,
} from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class ModalImageZoom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: this.props.modalVisible,
        };
        console.log('imagezoom', this.props)
    }

    setModalVisible(visible) {
        this.props.callbackModal(visible,this.props.imageProps)
    }
    static dimen = Dimensions.get('window');
    render() {
        const full_size = {
            width: ModalImageZoom.dimen.width,
            height: ModalImageZoom.dimen.height,
        }
        return (
            <Modal
                transparent={true}
                animationType={'none'}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    console.log("hide modal");
                    this.setModalVisible(!this.state.modalVisible);
                }}>
                <View style={styles.modalBackground}>
                    <View style={{
                        position: 'absolute',
                        top: 0, left: 0
                    }}>
                        <ImageZoom cropWidth={Dimensions.get('window').width}
                            cropHeight={Dimensions.get('window').height}
                            imageWidth={Dimensions.get('window').width}
                            imageHeight={Dimensions.get('window').height}>
                            <Image style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
                               source={{ uri: this.props.imageProps }} />
                        </ImageZoom></View>
                    <View style={styles.rightContainer}>
                        {console.log("width", Dimensions.get('window').width)}
                        {console.log("width", Dimensions.get('window').height)}
                        <TouchableOpacity onPress={() => {
                            console.log("hide modal");
                            this.setModalVisible(!this.state.modalVisible);
                        }}>
                            <Image source={{ uri: 'sclose' }} style={{ width: 30, height: 30 }} />
                        </TouchableOpacity>
                    </View>


                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#000000',
    },
    closeButton: {
        fontSize: 35,
        color: 'white',
        lineHeight: 70,
        width: 40,
        textAlign: 'center',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 1.5,
        shadowColor: 'black',
        shadowOpacity: 0.8,
    },
    rightContainer: {
        position: 'absolute',
        right: 25,
        top: SCREEN_HEIGHT * 0.12,
    },
});

