import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Platform

} from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';

class FlightListCard extends Component {
    constructor(props) {
        super(props);
        console.log("props", this.props);
        // this.state = {
        //     flightData
        // }
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                    <View style={styles.FlightDetailsOuter}>
                        <View style={styles.FlightDetails}>
                            <View style={{ flex: 0.4 }}>
                                <Text style={styles.PlaceLeftText}>{this.props.detail.departureAirport}</Text>
                                <Text style={styles.TimeLeftText}>{this.props.detail.ScheduledDepartureTime}</Text>
                                <Text style={styles.DetailsLeftText}>{I18n.t('fschedtime')}</Text>
                            </View>
                            <View style={{ flex: 0.4 }}>
                                <Text style={styles.FlightTitle}>{this.props.detail.airlineName}</Text>
                                <Image source={require('../../assets/images/flight.png')} style={{ width: '100%' }} />
                                <Text style={styles.FlightNo}>{this.props.detail.FlightNumber}</Text>
                            </View>
                            <View style={{ flex: 0.4 }}>
                                <Text style={styles.PlaceRightText}>{this.props.detail.arrivalAirport}</Text>
                                <Text style={styles.TimeRightText}>{this.props.detail.arrivalTime}</Text>
                                <Text style={styles.DetailsRightText}>Arrival Time</Text>
                            </View>
                        </View>
                        <View style={styles.FlightDetailsActive}>
                            <View style={{ flex: 0.1, alignItems: "flex-start" }}>
                                <Image source={{ uri: 'arrival' }} style={{ width: 24, height: 24, paddingRight: 10, marginTop: 0 }} />
                            </View>
                            <View style={{ flex: 0.9, alignItems: "flex-start" }}>
                                <Text style={styles.flighttitle}>{this.props.detail.arrivalAirportName}</Text>
                            </View>
                        </View>
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        // height: '100%'
    },
    FlightDetails: {
        flex: 1,
        flexDirection: "row",
        borderBottomColor: Colors.Gray,
        borderBottomWidth: 0.5,
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    FlightDetailsActive: {
        flex: 1,
        flexDirection: "row",
        borderBottomColor: Colors.Gray,
        borderBottomWidth: 0.5,
        backgroundColor: Colors.ShadowGray,
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    FlightDetailsOuter: {
        borderColor: Colors.Gray,
        borderWidth: 1,
        flex: 1,
        //flexDirection: "row",
        backgroundColor: Colors.Background_Color,
        marginBottom: 12,
        borderRadius: 5,
        ...Platform.select({
            ios: {
                shadowOffset: { width: 2, height: 2, },
                shadowColor: Colors.LightGray,
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },
    FlightTitle: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Bold,
        paddingVertical: 5,
        paddingTop: 15,
        textAlign: 'center'
    },
    TimeLeftText: {
        color: Colors.Black,
        fontSize: 30,
        fontFamily: AppFont.Bold,
        textAlign: 'left'
    },
    DetailsLeftText: {
        color: Colors.DarkGray,
        fontSize: 10,
        fontFamily: AppFont.Regular,
        paddingVertical: 5,
        flexWrap: 'wrap',
        textAlign: 'left'
    },
    FlightNo: {
        color: Colors.DarkGray,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        paddingVertical: 5,
        textAlign: 'center'
    },
    PlaceLeftText: {
        color: Colors.App_Font,
        fontSize: 14,
        fontFamily: AppFont.Bold,
        paddingVertical: 5,
        textAlign: 'left',
    },
    TimeRightText: {
        color: Colors.Black,
        fontSize: 30,
        fontFamily: AppFont.Bold,
        textAlign: 'right'
    },
    DetailsRightText: {
        color: Colors.DarkGray,
        fontSize: 10,
        fontFamily: AppFont.Regular,
        paddingVertical: 5,
        flexWrap: 'wrap',
        textAlign: 'right'
    },
    PlaceRightText: {
        color: Colors.App_Font,
        fontSize: 14,
        fontFamily: AppFont.Bold,
        paddingVertical: 5,
        textAlign: 'right',
    },
    flighttitle: {
        color: Colors.Black,
        fontFamily: AppFont.Bold,
        fontSize: 14,
    }

})

module.exports = FlightListCard;