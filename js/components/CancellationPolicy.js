import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Dimensions,
    ScrollView,
    Image,
    Alert
} from 'react-native';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';

import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import I18n from '../constants/i18n';

const SCREEN_HEIGHT = Dimensions.get('window').height;


export default class CancellationPolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            policyVisible: this.props.policyVisibility,
            typeId: this.props.policyType,
            bookingTypeId: this.props.bookingType,
            listId: this.props.listId,
            policy: {}
        }
        console.log('props', this.props.policyVisibility)
        console.log('props', this.props);


        this.closeDialog = this.closeDialog.bind(this);
    }
    componentWillMount() {
        this._onLoadPolicy();
    }
    async closeDialog() {
        console.log('hit close')
        await this.setState({ policyVisible: false });
        this.props.callbackPolicy(this.state.policyVisible);
       // Actions.refresh({ key: Math.random() * 1000000, BookingListIndex: this.props.bookListIndex })
    }

    okDialog = async () => {
        console.log('hit Ok');
        console.log('bookingType',this.props.bookingType);
        console.log('Id',this.props.listId);
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        await this.setState({ userId: jsonValue.UserId });
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        if(this.props.bookingType==1){
            let params = {
                "ApprovalStatusId": "4",
                "ModifiedBy": this.state.userId,
                "RestarurantBookingId": this.props.listId,
                "HotelId":HotelId
            }
            await apiCallWithUrl(APIConstants.GetRestaurantCancelAPI, "POST", params, this.postPolicyResponse)
        }
        else if(this.props.bookingType==2){
            let params = {
                "ApprovalStatusId": "4",
                "ModifiedBy": this.state.userId,
                "SpaBookingId": this.props.listId,
                "HotelId":HotelId
            }
            await apiCallWithUrl(APIConstants.GetSpaCancelAPI, "POST", params, this.postPolicyResponse)
        }
        else if(this.props.bookingType==3){
            let params = {
                "ApprovalStatusId": "4",
                "ModifiedBy": this.state.userId,
                "BikeBookingId": this.props.listId,
                "HotelId":HotelId
            }
            await apiCallWithUrl(APIConstants.GetBikeCancelAPI, "POST", params, this.postPolicyResponse)
        }
        else if(this.props.bookingType==4){
            let params = {
                "ApprovalStatusId": "4",
                "ModifiedBy": this.state.userId,
                "BoatBookingId": this.props.listId,
                "HotelId":HotelId
            }
            await apiCallWithUrl(APIConstants.GetBoatCancelAPI, "POST", params, this.postPolicyResponse)
        }
        else if(this.props.bookingType==7){
            let params = {
                "ApprovalStatusId": "4",
                "ModifiedBy": this.state.userId,
                "RoomServiceBookingId": this.props.listId,
                "HotelId":HotelId
            }
            await apiCallWithUrl(APIConstants.GetRoomServiceCancelAPI, "POST", params, this.postPolicyResponse)
        }

        //console.log(params);

        

    }


    postPolicyResponse = async (response) => {
        console.log('policy response', response);
        this.setState({
            loading: false,
        });

        if (response.ResponseData != false) {
            var postPolicyResponses = response.ResponseData;
            console.log("booklistIndex", this.state.bookingIndex)
            var bookIndex = this.props.bookListIndex;

            console.log(postPolicyResponses);

            setTimeout(function () {
                console.log("bookIndex", bookIndex)
                //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
                // Alert.alert("Policy Successfully.");

                if (bookIndex == 0) {
                    Alert.alert(I18n.t("alertrestaurantcancelled"));
                } else if (bookIndex == 1) {
                    Alert.alert(I18n.t("alertspacancelled"));
                } else if (bookIndex == 2) {
                    Alert.alert(I18n.t("alertbikecancelled"));
                }else if (bookIndex == 3) {
                    Alert.alert('Boat Booking Cancelled');
                }else {
                    Alert.alert('Room Service Booking Cancelled');
                }
            }, 1000);

            this.setState({ policyVisible: false });
            this.props.callbackPolicy(this.state.policyVisible, this.state.listId);

            Actions.refresh({ key: Math.random() * 1000000, BookingListIndex: this.props.bookListIndex })
        }

    }


    render() {
        return (
            <Dialog
                visible={this.state.policyVisible}
                overlayBackgroundColor='#000'
                dialogStyle={{
                    backgroundColor: 'transparent',
                    height: 'auto',
                    width: '100%',
                    display: 'flex',
                }}
                dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                onTouchOutside={this.closeDialog}
            >
                <DialogContent>
                    <View>
                        {this.state.policy.Contents != "" ?

                            <View style={{ marginTop: '5%', padding: '0%', borderRadius: 10, backgroundColor: 'white', height: 'auto' }}>
                                <View style={styles.CloseBtn}>
                                    <TouchableOpacity onPress={this.closeDialog}>
                                        <Image source={{ uri: 'sclose' }} style={{ width: 28, height: 28, right: 0 }} />
                                    </TouchableOpacity>
                                </View>
                                <ScrollView>
                                    <View style={styles.PopHeader}>
                                        <HTML
                                            html={this.state.policy.PolicyTitle}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing", "display"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                            style={{ color: "#FFFFFF", fontFamily: AppFont.Regular, fontSize: 20, textAlign: 'center', textTransform: 'uppercase' }} />
                                    </View>
                                    <View style={styles.PopDescription}>
                                        <HTML
                                            html={this.state.policy.Contents}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                            style={{ paddingBottom: 20, fontFamily: AppFont.Light, color: '#717171' }} />
                                    </View>
                                    <View style={{ alignItems: 'center', paddingHorizontal: 25, paddingBottom: 25, flex: 1, flexDirection: 'row' }}>
                                        <View style={{ flex: 0.5, alignItems: 'center' }}>
                                            <View style={{ width: "90%" }}>
                                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.okDialog}>
                                                    <Text style={styles.btntext}>{I18n.t('agree')}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={{ flex: 0.5, alignItems: 'center' }}>
                                            <View style={{ width: "90%" }}>
                                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.closeDialog}>
                                                    <Text style={styles.btntext}>{I18n.t('cancel')}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                            : null
                        }

                    </View>
                </DialogContent>
            </Dialog>
        );
    }

    _onLoadPolicy = async () => {
        const langId = await AsyncStorage.getItem(Keys.langId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId":langId,
            "PolicyTypeId":this.state.typeId
          }
          await apiCallWithUrl(APIConstants.GetPolicyAPI, 'POST', overalldetails, (callback) => { this.getPolicyResponse(callback) });
        //apiCallWithUrl(APIConstants.GetPolicyAPI + "?LanguageId=" + langId + "&&PolicyTypeId=" + this.state.typeId, 'GET', "", this.getPolicyResponse)
    }

    getPolicyResponse = async (response) => {
        if (response.IsException == null) {
            var policyResponse = response.ResponseData;
            console.log('policy response', policyResponse[0].Contents)

            await this.setState({
                policy: policyResponse === null ? '' : policyResponse[0]
            })

            console.log('state disclaimer', this.state.policy)
        }
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        borderRadius: 5,
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
        textAlign:'center',
    },
    PopHeader: {
        backgroundColor: Colors.App_Font,
        alignItems: 'center',
        paddingVertical: 20,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    PopDescription: {
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    CloseBtn: {
        position: 'absolute',
        right: -10,
        top: -10,
        zIndex: 1
    },
})