import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Dimensions,
    ScrollView,
    Image
} from 'react-native';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';

import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import I18n from '../constants/i18n';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const SCREEN_HEIGHT = Dimensions.get('window').height;


export default class Policy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            eventVisible: this.props.eventVisibility,
            policy: this.props.eventTotalDetails
        }
        console.log('props', this.props.eventVisibility)
        this.closeDialog = this.closeDialog.bind(this);
    }
    componentWillMount() {
        //this._onLoadPolicy();
    }
    async closeDialog() {
        console.log('hit close')
        await this.setState({ eventVisible: false });
        this.props.callbackPolicy(this.state.eventVisible);

       // Actions.refresh({ key: Math.random() * 1000000,BookingIndex: this.props.bookPageIndex  })
    }
    render() {
        return (
            <Dialog
                visible={this.state.eventVisible}
                overlayBackgroundColor='#000'
                dialogStyle={{
                    backgroundColor: 'transparent',
                    height: 'auto',
                    width: '100%',
                    display:'flex',
                }}
                dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                onTouchOutside={this.closeDialog}
            >
                <DialogContent>
                    <View>
                        {this.state.policy.Content != "" ?

                            <View style={{ marginTop: '5%', padding: '0%', borderRadius: 10, backgroundColor: 'white', height: 'auto' }}>
                                    <View style={styles.CloseBtn}>
                                        <TouchableOpacity onPress={this.closeDialog}>
                                            <Image source={{ uri: 'sclose' }} style={{ width: 28, height: 28, right: 0 }} />
                                        </TouchableOpacity>
                                    </View>
                                <ScrollView>
                                    <View style={styles.PopHeader}>
                                            <HTML
                                            html={this.state.policy.EventName}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                            style={{ color: "#FFFFFF", fontFamily: AppFont.Regular, fontSize: 18, textAlign: 'center', textTransform: 'uppercase' }} />
                                    </View>
                                    {/* <View style={styles.PopDescription}>
                                        <HTML
                                            html={this.state.policy.Content}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                            style={{ paddingBottom: 20, fontFamily: AppFont.Light, color: '#717171' }} />
                                    </View> */}
                                    {this.state.policy.RestaurantName!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Restaurant Name</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.RestaurantName}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.BikeName!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Bike Name</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.BikeName}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.BoatHouseActivityName!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Boat Activity Name</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.BoatHouseActivityName}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.SPAPackageName!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Spa Package Name</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.SPAPackageName}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.EventDate!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Event Date</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.EventDate}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.EventTime!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Event Time</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.EventTime}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.Amount!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Amount</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.Amount}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.BoatBookingAmount!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Booking Amount</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.BoatBookingAmount}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.NoOfPeoples>0 ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>No of Peoples</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.NoOfPeoples}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.SoloOrDuo!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Solo/Duo</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.SoloOrDuo}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.TherapistName!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Therapist</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.TherapistName}</Text></View>
                                    </View>
                                    : null}
                                    {this.state.policy.StatusName!='' ?
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        <View style={styles.Descrow1}><Text style={styles.Desc1}>Status</Text></View>
                                        <View style={styles.Descrow2}><Text style={styles.Desc1}>:</Text></View>
                                        <View style={styles.Descrow3}><Text style={styles.Desc2}>{this.state.policy.StatusName}</Text></View>
                                    </View>
                                    : null}
                                    
                                    <View style={{ alignItems: 'center',  paddingBottom: 25 }}>
                                        <View style={{ width: "35%" }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.closeDialog}>
                                                <Text style={styles.btntext}>{I18n.t('ok')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                            : null
                        }

                    </View>
                </DialogContent>
            </Dialog>
        );
    }

    // _onLoadPolicy = async () => {
    //     const langId = await AsyncStorage.getItem(Keys.langId);
    //     var HotelId = await AsyncStorage.getItem('HotelId');
    //     console.log("HotelId...",HotelId)
    //     let overalldetails = {
    //         "HotelId": HotelId,
    //         "LanguageId":langId,
    //         "PolicyTypeId":this.state.typeId
    //       }
    //       await apiCallWithUrl(APIConstants.GetPolicyAPI, 'POST', overalldetails, (callback) => { this.getPolicyResponse(callback) });
    //     // apiCallWithUrl(APIConstants.GetPolicyAPI + "?LanguageId=" + langId + "&&PolicyTypeId=" + this.state.typeId, 'GET', "", this.getPolicyResponse)
    // }

    // getPolicyResponse = async (response) => {
    //     if (response.IsException == null) {
    //         var policyResponse = response.ResponseData;
    //         console.log('policy response', policyResponse[0].Contents)

    //         await this.setState({
    //             policy: policyResponse === null ? '' : policyResponse[0]
    //         })

    //         console.log('state disclaimer', this.state.policy)
    //     }
    // }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        borderRadius:5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    PopHeader: {
        backgroundColor: Colors.App_Font,
        alignItems: 'center',
        paddingVertical: 20,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    PopDescription: {
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    CloseBtn: {
        position: 'absolute',
        right: -10,
        top: -10,
        zIndex: 1
    },
    Desc1:{ marginLeft: 20,marginTop:20,marginBottom:10,marginRight:5, fontFamily: AppFont.Regular, color: Colors.App_Font, fontSize:15 },
    Desc2:{ marginLeft: 20,marginTop:20,marginBottom:10,marginRight:5, fontFamily: AppFont.Regular, color: '#000000' },
    Descrow1:{ flex:0.35 },
    Descrow2:{ flex:0.05 },
    Descrow3:{ flex:0.60 }
})