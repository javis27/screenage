import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    RefreshControl,
    Dimensions,
    Platform,
    Alert
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Status from '../constants/Status';
import CancellationPolicy from '../components/CancellationPolicy';
import Loader from '../components/Loader';
//import Policy from '../components/Policy';
import Moment from 'moment';
import { apiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';

class Booking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bikeListArray: [],
            bikeCancelPolicyVisible: false,
            listId: 0,
            bikeTime: '',
            isbikeRefreshing: false,
            loading: false,
        }

        // this.setCancelPolicyVisibility = this.setCancelPolicyVisibility.bind(this);
    }

    componentDidMount() {
        this._loadBikeBookings();
        // this.verifyCheckInDetails();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {

    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({ loading: false }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this._loadBikeBookings();
    //         }
    //     }
    // }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0, marginBottom: 35 }} refreshControl={
                    <RefreshControl
                        refreshing={this.state.isbikeRefreshing}
                        onRefresh={this._onRefreshBike}
                    />
                }>
                    <Loader loading={this.state.loading} />
                    {this.state.bikeListArray.length > 0 ?
                        this.state.bikeListArray.map((item, index) => {
                            return <View key={index} style={styles.boxWithShadow} >
                                <View style={styles.Restaurant}>
                                    <View style={{ alignItems: "flex-start", flex: 0.7 }}>
                                        <Text style={styles.IconReqText}>{item.GuestName}</Text>
                                    </View>
                                    <View style={{ alignItems: "flex-end", flex: 0.3 }}>
                                        <Text style={[styles.IconReqTextColor, { color: this.StatusColors(item.ApprovalStatus) }]}>{item.ApprovalStatus}</Text>
                                    </View>
                                </View>


                                <View style={styles.DescriptionList}>
                                    <View style={styles.BookingListIcon}>
                                        <Image source={{ uri: 'bike_icon' }} style={{ width: 48, height: 48 }} />
                                    </View>
                                    <View style={[styles.BookingListDes, { borderLeftWidth: 0 }]}>
                                        <Text style={styles.ListDesText}>{item.BikeName}</Text>
                                        <Text style={styles.DesText}>{I18n.t('mur')} {item.Amount}</Text>
                                    </View>
                                </View>

                                <View style={[styles.Booking, { borderTopWidth: 0 }]}>
                                    <View style={styles.BookingList}>
                                        <Text style={styles.IconText}>{Moment(item.BookingDateTime).format('lll')}</Text>
                                        <Text style={styles.IconSubText}>{I18n.t('date')}</Text>
                                    </View>
                                    <View style={styles.BookingList}>
                                        {
                                            item.ApprovalStatus == Status.BookingStatusTypes.requested.name  || item.ApprovalStatus == Status.BookingStatusTypes.confirmed.name ?
                                                <View>
                                                    {/* <TouchableOpacity activeOpacity={.5} onPress={() => this.handleBikeCancelPolicy(item,item.BikeBookingId)} style={{ backgroundColor: Colors.App_Font, borderRadius: 20, padding: 6, paddingRight: 10, paddingLeft: 25 }}>
                                                        <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                                        <Text style={styles.CloseText}> Cancel</Text>
                                                    </TouchableOpacity> */}
                                                    <TouchableOpacity activeOpacity={.5} onPress={() => this.handleBikeCancelPolicy(item,item.BikeBookingId)}>
                                                    <Image source={require('../../assets/images/icon_cancel.png') } style={{width: 90, height: 30}} />
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                <View>
                                                    {/* <TouchableOpacity activeOpacity={.5} onPress={() => this.handleCannotCancelPolicy(item.ApprovalStatus)} style={{ backgroundColor: Colors.App_Font, borderRadius: 20, padding: 6, paddingRight: 10, paddingLeft: 25 }}>
                                                        <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                                        <Text style={styles.CloseText}> Cancel</Text>
                                                    </TouchableOpacity> */}
                                                </View>
                                        }
                                    </View>
                                </View>
                                {item.ApprovalStatus == 'Completed'  ?
                                <View style={[styles.DescriptionList, { borderTopWidth: 0.5,borderColor: Colors.Background_Gray }]}>
                                {item.IsGivenReview == 0 ?
                                    <View style={{ alignItems: 'center', flex: 1}}>
                                    <TouchableOpacity activeOpacity={.5}  onPress={() => this.feedbackRate(item.BikeBookingId)}>
                                        <Text style={styles.IconReqText}>{I18n.t('please_rate_your_experience')}</Text>
                                    </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={{ alignItems: 'center', flex: 1}}>
                                        <Text style={styles.IconReqText}>{I18n.t('thanks_for_rating')}</Text>
                                    </View>
                                    }
                                </View>
                                :
                                <View></View>
                        }
                            </View>
                        })

                        :
                        <View style={{ flex: 1, backgroundColor: "#FFFFFF", }}>
                            <View style={{ flex: 1, textAlign: 'center', alignItems: 'center', justifyContent: 'center', width: Dimensions.get('window').width, height: Dimensions.get('window').height - 180, }}>
                                <Text style={{ fontFamily: AppFont.Regular }}>{I18n.t('to_make_booking_please_click_plus')}</Text>
                            </View>
                        </View>
                    }
                </ScrollView>
                {/* {
                    this.state.cancelPolicyVisible ?
                        <CancellationPolicy cancelPolicyVisibility={this.state.cancelPolicyVisible} callbackCancelPolicyVisibility={this.setCancelPolicyVisibility} />
                        : null
                } */}

                {/* {
                    this.state.bikeCancelPolicyVisible ?
                        <Policy policyVisibility={this.state.bikeCancelPolicyVisible} policyType={Status.policyTypes.bikeCancellationPolicy.typeId} callbackPolicy={this.setBikeCancelPolicyVisibility} />
                        : null
                } */}

                {
                    this.state.bikeCancelPolicyVisible ?
                        <CancellationPolicy policyVisibility={this.state.bikeCancelPolicyVisible}
                            bookingType={Status.bookingTypes.bike.typeId}
                            policyType={Status.policyTypes.bikeCancellationPolicy.typeId}
                            listId={this.state.listId}
                            bookListIndex={2}
                            callbackPolicy={this.setBikeCancelPolicyVisibility} />
                        : null
                }
            </View>
        )
    }

    _onRefreshBike = () => {
        this.setState({ isbikeRefreshing: true })
        this._loadBikeBookings();
    }

    StatusColors(item) {
        var x = item;
        var textbgColor = "";
        if (x == Status.BookingStatusTypes.requested.name) {
            textbgColor = Status.BookingStatusTypes.requested.textColor;
        }
        else if (x == Status.BookingStatusTypes.confirmed.name) {
            textbgColor = Status.BookingStatusTypes.confirmed.textColor;
        }
        else if (x == Status.BookingStatusTypes.rejected.name) {
            textbgColor = Status.BookingStatusTypes.rejected.textColor;
        }

        return textbgColor;
    }
    feedbackRate = (itemId) => {
        console.log("itemid",itemId);
        Actions.BookingsRatings({"ModuleId":3,"OrderId":itemId});
    }
    handleBikeCancelPolicy = async (item,itemId) => {
        console.log("Item:", item);
        console.log("Index no:::", itemId);
        var now = Moment(new Date()); //todays date
        var end = Moment(item.BookingDateTime); // another date
        var duration = Moment.duration(end.diff(now));
        var days = duration.asMinutes();
        console.log('diff...',days)
        if(days>120){
            await this.setState({ bikeCancelPolicyVisible: true, listId: itemId }, () => console.log("bikeCancelPolicyVisible", this.state.bikeCancelPolicyVisible))
        }else{
            Alert.alert(I18n.t('cancellation_is_allowed_only_before'));
        }
        //await this.setState({ bikeCancelPolicyVisible: true, listId: itemId }, () => console.log("bikeCancelPolicyVisible", this.state.bikeCancelPolicyVisible))
    }

    handleCannotCancelPolicy = async (statusName) => {
        console.log("Index no:::", statusName);
        Alert.alert(statusName + I18n.t('alertrequestnotcancel'));
    }

    setBikeCancelPolicyVisibility = async (data) => {
        console.log('setAboutVisibility', data)
        await this.setState({ bikeCancelPolicyVisible: data });
    }

    _loadBikeBookings = async () => {
        this.setState({ loading: true })
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        await this.setState({ userId: userJson.UserId });
        console.log("HotelId...",HotelId)
        //await apiCallWithUrl(APIConstants.GetBikeBookingList + "?UserId=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        let overalldetails = {
            "HotelId": HotelId,
            "UserId": this.state.userId
          }
          await apiCallWithUrl(APIConstants.GetBikeBookingList, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
        
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getBikeBookingsResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }
    formatDate (date){
        var date = new Date(date);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return date.getDate() + "/" + (date.getMonth()+1)  + "/" + date.getFullYear() + " " + strTime;
      }
      
    getBikeBookingsResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response != null) {
            var getbikeListResponse = response.ResponseData;
            console.log('getbikeResponse', getbikeListResponse);

            await this.setState({ bikeListArray: getbikeListResponse, isbikeRefreshing: false });

            console.log('getbikeResponse', this.state.bikeListArray.length);

        }
    }

    _onPressButton = () => {
        Actions.Booking();
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    Booking: {
        flex: 1,
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: Colors.LightBlue,
    },
    BookingListIcon: {
        width: "33.3333333333%",
        // height: 60,
        padding:10,
        borderColor: Colors.App_Font,
        alignItems: 'center',
        borderRightWidth: 0.5,
        justifyContent: 'center'
    },
    BookingList: {
        width: "50%",
        height: 60,
        borderColor: Colors.App_Font,
        padding:10,
        alignItems: 'center',
        borderWidth: 0.5,
        justifyContent: 'center'
    },
    BookingListDes: {
        width: "66.6666666667%",
        // height: 60,
        borderColor: Colors.App_Font,
        padding:10,
        justifyContent: 'center'
    },
    IconText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Bold,
        textTransform: "uppercase",
        textAlign: 'center'
    },
    IconSubText: {
        color: Colors.DarkGray,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        textTransform: "uppercase",
        textAlign: 'center',
        marginTop: 5
    },
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        padding:10,
        paddingTop: 10
    },
    IconReqText: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        paddingVertical: 3
    },
    IconReqTextColor: {
        color: Colors.Orange,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        textTransform: "uppercase",
        paddingVertical: 3
    },
    DescriptionList: {
        flex: 1,
        flexDirection: "row",
        borderColor: Colors.App_Font,
        borderTopWidth:0.5
    },
    DesText: {
        color: Colors.DarkGray,
        fontSize: 13,
        fontFamily: AppFont.Regular
    },
    ListDesText: {
        color: Colors.Black,
        fontSize: 13,
        marginBottom: 4,
        fontFamily: AppFont.SemiBold
    },
    CloseText: {
        fontSize: 10,
        color: Colors.Background_Color,
        alignItems: 'center',
        justifyContent: "center",
        textTransform: "uppercase",
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: "#ffffff",
        marginHorizontal: 12,
        marginTop: 14,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.4,
    },

})

module.exports = Booking;