import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    TextInput,
    Alert
} from 'react-native';
import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';

import { Actions } from 'react-native-router-flux';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';

class AddRoomService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            selectedCategory: 'Choose Service Request',
            categoryId: '',
            categoryArray: [],
            serviceRequest: '',
            userId: '',
        }
        console.log("room service add component", this.props)
    }

    componentDidMount() {
        this._loadDropdowns();
    }


    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <TableView>
                        <Loader loading={this.state.loading} />
                        <Section sectionPaddingTop={0} sectionPaddingBottom={0} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                            <Cell title="Service Request" placeholder="Service Request" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label} >
                                            Service Request For
                                        </Text>
                                        <View>
                                            <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetCategory}>
                                                <Text style={styles.picker}>{this.state.selectedCategory}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />

                            <Cell title="Description" placeholder="Description" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            Additional Remarks
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                placeholder="Any requests? Write them here...."
                                                onChangeText={(serviceRequest) => this.setState({ serviceRequest })}
                                                value={this.state.serviceRequest} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Book" placeholder="Book" id="" cellContentView={
                                <View style={{ flex: 1, marginTop: 40, justifyContent: 'center' }}>
                                    <View style={{ width: "90%", marginLeft: 20 }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressServiceRequest}>
                                                <Text style={styles.btntext}>{I18n.t('btnSubmit')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            } />

                            <Cell title="View My Bookings" hideSeparator={true} placeholder="View My Bookings" id="7" cellContentView={
                                <View style={{ flex: 1, height: 70, justifyContent: "center", alignItems: "center", marginBottom: 25 }}>
                                    <TouchableOpacity activeOpacity={.5} onPress={this._onPressRoomServiceList}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>View my requests</Text>
                                    </TouchableOpacity>
                                </View>
                            } />
                        </Section>
                    </TableView>
                </ScrollView>
            </View>
        );
    }

    _onPressServiceRequest = async () => {

        if (this.state.categoryId == "") {
            Alert.alert(I18n.t("servicerequestchoose"));
        } else {
            this.setState({ loading: true })
            let overalldetails = {
                "ServiceRequestId": this.state.categoryId,
                "Description": this.state.serviceRequest,
                "RequestedById": this.state.userId,
            }
            console.log('overalldetails', overalldetails)
            await apiCallWithUrl(APIConstants.PostServiceRequest, 'POST', overalldetails, this.postServiceRequestResponse);
        }
    }

    postServiceRequestResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response.IsException == null) {
            var postRestaurantResponse = response.ResponseData;
            console.log('postUserResponse', postRestaurantResponse);
            setTimeout(function () {

                //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
                Alert.alert("Your request registered successfully.")

            }, 1000);
            // Actions.BookingsList();
            this._onPressRoomServiceList();

        }

    }

    _onBtnSheetCategory = async () => {
        console.log("this.state.myArray", this.state.categoryArray);
        console.log("this.state.totalData", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            options: this.state.categoryArray,
            dark: false,
            cancelButtonIndex: 12,
        }, (value) => {
            console.log("this.state.totalData.value", this.state.totalData[value].value);
            this.setState({
                "selectedCategory": this.state.totalData[value].value,
                "categoryId": this.state.totalData[value].id
            })
            console.log("categoryId", this.state.categoryId);
            console.log("selectedCategory", this.state.selectedCategory);
        });
    }

    _onPressRoomServiceList = async () => {
        // let callbackdetails = {
        //     "selectedRoomIndex": 1,
        //     "IsFoodMenu": false,
        // }
        // console.log("back room service", callbackdetails)
        this.props.callbackRoomServiceList();
    }

    _loadDropdowns = async () => {
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        await this.setState({ userId: jsonValue.UserId });

        await apiCallWithUrl(APIConstants.GetRoomCategoryMasters, 'GET', '', this.getDropdownsResponse);
    }

    getDropdownsResponse = async (response) => {
        this.setState({ totalData: response.ResponseData })

        console.log("totalResponse", response.ResponseData);
        console.log("haaa...", response.ResponseData);
        var list1 = [];
        response.ResponseData.map((categories, index) => {
            console.log("loop", index, categories.value);
            list1.push(categories.value);

        });
        this.setState({ categoryArray: list1 })


        console.log("restaurantArray", this.state.categoryArray);
    }
}

const styles = StyleSheet.create({
    container: {
        //flex: 1,
        backgroundColor: "#FFFFFF",
        // flexDirection: 'column'
    },
    label: {
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    picker: {
        color: Colors.PlaceholderText,
        marginLeft: 8,
        height: 28,
        padding: 0,
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        fontSize: 15,
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius:5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },

});

module.exports = AddRoomService;
