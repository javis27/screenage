import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TouchableNativeFeedback } from 'react-native';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import I18n from '../constants/i18n';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import EventDetails from '../components/EventDetails';

class CalendarListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventDetailsVisible: false
          }
    }

    render() {
        return (
            <View>
                {this.props.calendar.EventTypeId == 1 ?
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                        <TouchableOpacity activeOpacity={.5} onPress={this.handleEventContent}>
                            <Text style={styles.RightText}>{this.props.calendar.Content}</Text>
                        </TouchableOpacity>
                            <Text style={{ position: "absolute", right: -6, top: 30 }}>
                                <Image source={{ uri: 'ellipse1' }} style={{ width: 10, height: 10 }} />
                            </Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.LeftText}></Text>
                        </View>
                    </View>
                    :
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                        {/* <TouchableOpacity activeOpacity={.5} onPress={this.callbackEvent}> */}
                            
                            <Text style={styles.LeftText}>{this.props.calendar.Content}</Text>
                            {/* </TouchableOpacity> */}
                            <Text style={{ position: "absolute", left: -5, top: 20 }}>
                                <Image source={{ uri: 'ellipse2' }} style={{ width: 10, height: 10, justifyContent: "center" }} />
                            </Text>
                        </View>
                    </View>
                }
                {
                        this.state.eventDetailsVisible ?
                            <EventDetails eventVisibility={this.state.eventDetailsVisible}
                                eventTotalDetails={this.props.calendar}
                                //policyType={Status.policyTypes.restaurantCancellationPolicy.typeId}
                                bookPageIndex={1}
                                callbackPolicy={this.setEventVisibility} />
                            : null
                    }
            </View>
        )
    }
    callbackEvent = async () =>{
        console.log("Entered")
    }
    handleEventContent = async () => {
        await this.setState({ eventDetailsVisible: true }, () => console.log("eventDetailsVisible", this.state.eventDetailsVisible))
    }
    
    setEventVisibility = async (data) => {
        console.log('setAboutVisibility', data)
        await this.setState({ eventDetailsVisible: data });
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        //flex: 1,
        flexDirection: 'row',
    },
    menuWrapper: {
        width: "50%",
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 10
    },
    RightText: {
        textAlign: "right",
        fontSize: 14,
        fontFamily: AppFont.Regular,
    },
    LeftText: {
        textAlign: "left",
        fontSize: 14,
        fontFamily: AppFont.Regular,
    }

})
export default CalendarListItem