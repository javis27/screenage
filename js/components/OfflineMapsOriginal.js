import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    FlatList,
    Platform,
    Alert,
    PermissionsAndroid,
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'react-native-fetch-blob'

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import groupBy from '../constants/groupBy';
import Status from '../constants/Status';
import Swiper from 'react-native-swiper';
import Loader from '../components/Loader';
import I18n from '../constants/i18n';
import { Dimensions } from 'react-native';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const ScreenHeight = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class OfflineMaps extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            OfflineMapsArray: [],
            selectedMapsTypeArray: [],
            isOfflineMaps: false,
            headerType: '',
            activeRestaurant: true,
            activeRoutes: false,
            activeViewPoints: false,
            imgWidth: Dimensions.get('window').width,
            imgHeight: Dimensions.get('window').height,
        };
        console.log("width..", this.state.imgWidth);
        console.log("height..", this.state.imgHeight);
    }
    componentDidMount() {
        this._onLoadOfflineMaps();
    }
    renderMaps(mapData) {
        var base64Icon = '';
        if (Platform.OS == 'ios') {
            console.log("ios", mapData)
            // base64Icon = "data:image/png;base64," + mapData.IOSImage;
            base64Icon = mapData.IOSImagePath
        } else {
            console.log("android", mapData)
            // base64Icon = "data:image/png;base64," + mapData.AndroidImage;
            base64Icon = mapData.AndroidImagePath
        }
        if (mapData.AndroidImagePath != "") {
            return (
                <Image source={{ uri: base64Icon }} style={{ width: '100%', height: '100%', borderRadius: 10 }} />
            )
        }
        else
            return null

    }


    find_dimesions(layout) {
        const { x, y, width, height } = layout;

        this.setState({
            imgHeight: height * .85
        })
    }

    saveToCameraRoll = (image) => {
        if (Platform.OS === 'android') {
            this.requestAndroidCameraRollPermission(image)
        } else {
            CameraRoll.saveToCameraRoll(image)
                .then(Alert.alert('Success', 'Photo added to gallery!'))
        }
    }

    requestAndroidCameraRollPermission = async (imagePath) => {
        let dirs = RNFetchBlob.fs.dirs;
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Preskil Gallery Permission',
                    message:
                        'Allow Preskil to access your photos', 
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the camera');
                RNFetchBlob
                    .config({
                        fileCache: true,
                        appendExt: 'jpg,',
                        path: dirs.DownloadDir + "1.png",
                    })
                    .fetch('GET', imagePath)
                    .then((res) => {
                        console.log("res", res)
                        console.log("res", res.path())
                        console.log("res", res.data)
                        res.data = 'file://' + res.data;
                        console.log("res", res.data)

                        CameraRoll.saveToCameraRoll(res.data)
                            .then(Alert.alert('Success', 'Photo added to gallery!'))
                            .catch(err => console.log('err:', err))
                    })
            } else {
                console.log('Camera permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>
                <Loader loading={this.state.loading} />

                <View onLayout={(event) => { this.find_dimesions(event.nativeEvent.layout) }} style={{ flex: 0.85, justifyContent: "flex-end" }}>
                    <ScrollView contentContainerStyle={{ alignItems: 'center', paddingHorizontal: 10, paddingVertical: 15, position: "relative", backgroundColor: Colors.Background_Color }}>
                        {
                            this.state.isOfflineMaps ?
                                <TableView style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>

                                    <View style={{ flex: 1, flexDirection: "row" }}>
                                        <View style={{ flex: 0.7 }}><Text style={styles.guestyou}>{this.state.headerType}</Text></View>
                                        <TouchableOpacity activeOpacity={0.5} onPress={() => this._onPressDownload(this.state.selectedMapsTypeArray)}>
                                            <View style={{ flex: 0.3 }}><Image source={{ uri: 'download' }} style={{ width: 22, height: 22 }} /></View>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ flex: 6, justifyContent: "flex-end" }}>
                                        <View style={{ padding: 15, borderRadius: 10, height: this.state.imgHeight, width: this.state.imgWidth }}>
                                            <Swiper autoplay={false} dotColor="rgba(255,255,255,.2)" activeDotColor="#FFFFFF" showsButtons={true} loop={false}>
                                                {this.state.selectedMapsTypeArray.data.map((mapItem, index) => {
                                                    return <View key={index} style={styles.slide1}>
                                                        {this.renderMaps(mapItem)}
                                                        {/* <Image source={require('../../assets/images/map.jpg')} style={{ width: '100%', height: "100%", borderRadius: 10 }} /> */}
                                                    </View>
                                                })}
                                            </Swiper>
                                        </View>
                                    </View>
                                </TableView>
                                :
                                <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>
                                    <View style={{ flex: 1, justifyContent: "flex-end" }}>
                                        <View style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', height: this.state.imgHeight, width: this.state.imgWidth }}>
                                        <Text style={{ fontFamily: AppFont.Regular }}>{I18n.t('nodata')}</Text>
                                        </View>
                                    </View>
                                </View>

                        }
                    </ScrollView>
                </View>

                <View style={{ flex: 0.15, justifyContent: "flex-end" }}>
                    <View style={styles.Booking}>
                        <View style={styles.BookingList}>
                            <TouchableOpacity activeOpacity={0.5} onPress={this._onPressRestaurants}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center", padding: 5 }}>
                                    <Image source={{ uri: 'restaurant' }} style={{ width: 30, height: 30, tintColor: this.state.activeRestaurant ? Colors.App_Font : Colors.FooterMenu_Text }} />
                                </View>
                                <Text style={[styles.IconText, { color: this.state.activeRestaurant ? Colors.App_Font : Colors.FooterMenu_Text }]}>{I18n.t('offlinemaprestaurant')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.BookingList}>
                            <TouchableOpacity activeOpacity={0.5} onPress={this._onPressRoutes}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center", padding: 5 }}>
                                    <Image source={{ uri: 'routes' }} style={{ width: 30, height: 30, tintColor: this.state.activeRoutes ? Colors.App_Font : Colors.FooterMenu_Text }} />
                                </View>
                                <Text style={[styles.IconText, { color: this.state.activeRoutes ? Colors.App_Font : Colors.FooterMenu_Text }]}>{I18n.t('offlinemaproutes')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.BookingList}>
                            <TouchableOpacity activeOpacity={0.5} onPress={this._onPressViewPoints}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center", padding: 5 }}>
                                    <Image source={{ uri: 'viewpoints' }} style={{ width: 30, height: 30, tintColor: this.state.activeViewPoints ? Colors.App_Font : Colors.FooterMenu_Text }} />
                                </View>
                                <Text style={[styles.IconText, { color: this.state.activeViewPoints ? Colors.App_Font : Colors.FooterMenu_Text }]}>{I18n.t('offlinemaviewpoints')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    _onPressRestaurants = async () => {
        this.state.OfflineMapsArray.map((item) => {
            if (item.title == Status.MapTypes.restaurants.typeId) {
                console.log("item", item)
                this.setState({ selectedMapsTypeArray: item, headerType: item.data[0].Type, activeRestaurant: true, activeRoutes: false, activeViewPoints: false })
            }
        })
        //  this.setState({ OfflineMapsArray: arrayOfData, selectedMapsTypeArray: item })
    }

    _onPressRoutes = async () => {
        console.log("this.state.OfflineMapsArray", this.state.OfflineMapsArray)
        this.state.OfflineMapsArray.map((item) => {
            if (item.title == Status.MapTypes.routes.typeId) {
                console.log("item", item)
                this.setState({ selectedMapsTypeArray: item, headerType: item.data[0].Type, activeRestaurant: false, activeRoutes: true, activeViewPoints: false })
            }
        })
    }

    _onPressViewPoints = async () => {
        this.state.OfflineMapsArray.map((item) => {
            if (item.title == Status.MapTypes.viewPoints.typeId) {
                console.log("item", item)
                this.setState({ selectedMapsTypeArray: item, headerType: item.data[0].Type, activeRestaurant: false, activeRoutes: false, activeViewPoints: true })
            }
        })
    }

    _onPressDownload = async (offlineData) => {
        var mapImages = '';
        var imagesArr = [];
        console.log("data", offlineData.data)
        await offlineData.data.forEach(item => {
            console.log('log item', item)

            if (Platform.OS == 'ios') {
                console.log("ios")
                mapImages = item.IOSImagePath;
            } else {
                console.log("android")
                mapImages = item.AndroidImagePath;
            }

            console.log('mapImages', mapImages)

            this.saveToCameraRoll(mapImages)

        })
        console.log("imagesArr", imagesArr)

    }

    _onLoadOfflineMaps = async () => {
        this.setState({
            loading: true,
        });
        apiCallWithUrl(APIConstants.GetOfflineMapsAPI, 'GET', "", this.getOfflineMapsResponse);
    }

    getOfflineMapsResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response.IsException == null) {
            var offlineMapResponse = response.ResponseData;
            console.log('offline maps response', offlineMapResponse)

            await this.setState({
                OfflineMapsArray: offlineMapResponse === null ? '' : offlineMapResponse
            })

            console.log('offline maps', this.state.OfflineMapsArray)

            var groupByMapsTypeId = groupBy(offlineMapResponse, 'TypeId');
            var arrayOfData = [];
            Object.keys(groupByMapsTypeId).forEach(element => {
                arrayOfData.push({
                    title: element,
                    data: groupByMapsTypeId[element]
                });
            });
            console.log('arrayOfData', arrayOfData)

            arrayOfData.map((item) => {
                if (item.title == Status.MapTypes.restaurants.typeId) {
                    console.log("item", item.data[0].Type)
                    this.setState({ OfflineMapsArray: arrayOfData, selectedMapsTypeArray: item, isOfflineMaps: true, headerType: item.data[0].Type })
                }
            })

            console.log('arrayOfData...', arrayOfData);
            this.setState({ OfflineMapsArray: arrayOfData })
        }
    }

}

const styles = StyleSheet.create({
    Booking: {
        flex: 1,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        textAlign: "center",
        position: 'absolute',
        bottom: 0,
        marginBottom: 10
    },
    BookingList: {
        width: "33.3333333333%",
        height: 'auto',
        alignItems: 'center',
        marginBottom: 10
    },
    guestyou: {
        color: Colors.Black,
        fontSize: 18,
        fontFamily: AppFont.Regular,
        textAlign: "center",
        justifyContent: 'center',
    },
    IconText: {
        fontFamily: AppFont.Regular,
    }
});

module.exports = OfflineMaps;