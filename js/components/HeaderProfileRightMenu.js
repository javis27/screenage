import React, { Component } from 'react';

import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

import { Actions } from "react-native-router-flux";
import Colors from '../constants/Colors';


const HeaderProfileRightMenu = () => (
    <TouchableOpacity onPress={() => Actions.settings()}>

        {/* <TouchableOpacity onPress={() => Actions.refresh({clear:true})}> */}
        <View style={styles.dots}>
            <Image source={require('../../assets/images/logout.png')} style={{ width: 25, height: 25, tintColor:Colors.Background_Color }} />
        </View>
    </TouchableOpacity>
);

export default HeaderProfileRightMenu;

const styles = StyleSheet.create({
    dots: {
         paddingRight: 10,
    },
})

