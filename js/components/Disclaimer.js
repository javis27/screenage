import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Dimensions,
    Image

} from 'react-native';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import HTML from 'react-native-render-html';
import { Actions } from 'react-native-router-flux';

import I18n from '../constants/i18n';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { ScrollView } from 'react-native';


export default class Disclaimer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.disclaimerVisibility,
            typeId: this.props.disclaimerType,
            disclaimer: {}
        }
        console.log('props', this.props.disclaimerVisibility)
        this.closeDialog = this.closeDialog.bind(this);
    }

    componentWillMount() {
        this._onLoadInitialState();
    }

    async closeDialog() {
        console.log('hit close')
        await this.setState({ visible: false });
        this.props.callbackDisclaimerVisibility(this.state.visible)
        Actions.refresh({ key: Math.random() * 1000000 })
    }
    render() {
        return (
            <Dialog
                visible={this.state.visible}
                overlayBackgroundColor='#000'
                dialogStyle={{
                    backgroundColor: 'transparent',
                    height: 'auto',
                    width: '100%',
                    display: 'flex',
                }}
                dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                onTouchOutside={this.closeDialog}
            >
                <DialogContent>
                    <View>
                        <ScrollView>
                        {this.state.disclaimer.Contents != "" ?
                            <View style={{ marginVertical: '5%', padding: '0%', borderRadius: 10, backgroundColor: 'white', height: 'auto' }}>
                                <View style={styles.PopHeader}>
                                    <Text style={{ color: "#FFFFFF", fontFamily: AppFont.Regular, fontSize: 18, textAlign: 'center', textTransform: 'uppercase' }}>{this.state.disclaimer.DisclaimerName}</Text>
                                </View>
                                <View style={styles.CloseBtn}>
                                    <Image source={{ uri: 'sclose' }} style={{ width: 28, height: 28, right: 0 }} />
                                </View>
                                <View style={styles.PopDescription}>
                                    <HTML
                                        html={this.state.disclaimer.Contents}
                                        tagsStyles={{ fontFamily: AppFont.Regular, }}
                                        baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                        ignoredStyles={["font-family", "letter-spacing"]}
                                        imagesMaxWidth={Dimensions.get('window').width}
                                    //style={{ paddingBottom: 20, fontFamily: AppFont.Light, color: '#717171' }} />
                                    />
                                    {/* <HTML
                                        html={this.state.pageMenuData.Content}
                                        tagsStyles={{ fontFamily: AppFont.Regular, }}
                                        baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                        ignoredStyles={["font-family", "letter-spacing"]}
                                        imagesMaxWidth={Dimensions.get('window').width}
                                    //style={{ paddingBottom: 20, fontFamily: AppFont.Regular, color: '#717171' }} 
                                    /> */}
                                </View>
                                <View style={{ alignItems: 'center', paddingBottom: 25 }}>
                                    <View style={{ width: "25%" }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.closeDialog}>
                                            <Text style={styles.btntext}>{I18n.t('ok')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            : null
                        }
                    </ScrollView>
                    </View>
                </DialogContent>
            </Dialog>
        );
    }

    _onLoadInitialState = async () => {
        const langId = await AsyncStorage.getItem(Keys.langId)
        let overalldetails = {
            "DisclaimerTypeId": this.state.typeId,
            "LanguageId": langId
          }
          await apiCallWithUrl(APIConstants.GetDisclaimerAPI, 'POST', overalldetails, (callback) => { this.getDisclaimerResponse(callback) });
        //await apiCallWithUrl(APIConstants.GetDisclaimerAPI + "?LanguageId=" + langId + "&&DisclaimerTypeId=" + this.state.typeId, 'POST', "", this.getDisclaimerResponse)
    }

    getDisclaimerResponse = async (response) => {
        if (response.IsException == null) {
            var disclaimerResponse = response.ResponseData;
            console.log('disclaimer response', disclaimerResponse[0].Contents)

            await this.setState({
                disclaimer: disclaimerResponse === null ? '' : disclaimerResponse[0]
            })

            console.log('state disclaimer', this.state.disclaimer)
        }
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    PopHeader: {
        backgroundColor: Colors.App_Font,
        alignItems: 'center',
        paddingVertical: 20,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    PopDescription: {
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    CloseBtn: {
        position: 'absolute',
        right: -10,
        top: -10,
        zIndex: 1
    }
})