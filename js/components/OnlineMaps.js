import React, { Component } from "react";
import { View, Text, AsyncStorage, StyleSheet, Image, TouchableOpacity, Alert } from "react-native";
import Keys from '../constants/Keys';
import MapView, { PROVIDER_GOOGLE, Polyline } from 'react-native-maps';
import { dynamicApiCallWithUrl } from '../api/APIHandler';
import Colors from '../constants/Colors';
import I18n from '../constants/i18n';
import AppFont from '../constants/AppFont';
const polyline = require('@mapbox/polyline');


export default class Geolocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapRegion: this.props.mapData,
      nearByPlacesArrayResponse: [],
      isShowNearBy: false,
      isShowDirections: false,
      coords: [],
      activeCurrentLocation: true,
    }
    console.log("map Data", this.props.mapData)
  }

  componentWillMount() {

    // Chennai nearby location link
    // dynamicApiCallWithUrl("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=12.9503215,80.1458512&type=tourist attractions in Chennai&key=AIzaSyDaUFEHWj6I8WW6enVTPJ7gHdUj_jRgE8w&rankby=distance&name=Tourist attraction", 'GET', '', this.touristAttractionResponse);

    //Mauritius link
    dynamicApiCallWithUrl("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-20.421796, 57.721973&type=tourist attractions near preskil island resort&key=AIzaSyDaUFEHWj6I8WW6enVTPJ7gHdUj_jRgE8w&rankby=distance&name=Tourist attraction", 'GET', '', this.touristAttractionResponse);

    // Initial route
    // this.onPressFetchDirection();
  }

  touristAttractionResponse = (response) => {
    console.log("touristAttractionResponse", response.results)
    console.log("latlag", response.results[0].geometry.location.lat)
    this.setState({ nearByPlacesArrayResponse: response.results })
  }

  // this methods shows Directions

  onPressFetchDirection = async () => {
   // const startLoc = "-20.415198, 57.710989";  // Mahebourg Waterfront

    const startLoc = this.state.mapRegion.latitude + "," + this.state.mapRegion.longitude;
    const endLoc = "-20.421796, 57.721973"; // Preskil island resort latlog

    try {
      // fetch direction from google (chennai location)
      // const resp = await fetch("https://maps.googleapis.com/maps/api/directions/json?origin=12.968760,80.150196&destination=13.039818,80.232954&key=AIzaSyDaUFEHWj6I8WW6enVTPJ7gHdUj_jRgE8w");

      const resp = await fetch("https://maps.googleapis.com/maps/api/directions/json?origin=" + startLoc + "&destination=" + endLoc + "&key=AIzaSyDaUFEHWj6I8WW6enVTPJ7gHdUj_jRgE8w");

      //get the json from response
      const respJson = await resp.json();
      console.log('respJson', respJson)
      if (respJson.status != "ZERO_RESULTS") {
        // decode polyne data
        let points = polyline.decode(respJson.routes[0].overview_polyline.points);

        // convert each decoded polyne item into an array of objects
        let coords = points.map((point, index) => {
          console.log('point', point);
          return { latitude: point[0], longitude: point[1] }
        })

        this.setState({ coords: coords, activeCurrentLocation: false, isShowDirections: !this.state.isShowDirections })
        _mapView.animateToCoordinate({
          latitude: respJson.routes[0].legs[0].start_location.lat,
          longitude: respJson.routes[0].legs[0].start_location.lng
        }, 1000)
      }
      else {
       // Alert.alert(I18n.t("alertonlinewrongroutes"));
      // Alert.alert("alertonlinewrongroutes");
       
       Alert.alert(I18n.t('alertonlinewrongroutes'));
      }

    }
    catch (error) {
      console.log("error online maps", error);
    }
  }


  // this methods shows nearby places in mauritius
  onPressNearByPlaces = () => {
    this.setState({ isShowNearBy: !this.state.isShowNearBy, activeCurrentLocation: false })
  }

  // this methods shows live location
  onPressLiveLocation = () => {
    _mapView.animateToCoordinate({
      latitude: this.state.mapRegion.latitude,
      longitude: this.state.mapRegion.longitude
    }, 1000)
    this.setState({ activeCurrentLocation: true })
  }

  render() {
    console.log("country", !!this.state.mapRegion)
    return (
      <View style={{ flex: 1 }}>
        {!!this.state.mapRegion ?
          <View style={{ flex: 1, backgroundColor: '#f3f3f3', flexDirection: "column", justifyContent: "space-between", }}>
            <MapView
              style={{ flex: 0.85, justifyContent: "flex-end", position: 'relative', }}
              ref={(mapView) => { _mapView = mapView; }}
              initialRegion={this.state.mapRegion} showsUserLocation
              camera={{ center: this.state.mapRegion, pitch: 12, heading: 14, altitude: 10, zoom: 16 }}>


              {this.state.nearByPlacesArrayResponse.length > 0 && this.state.isShowNearBy ?
                this.state.nearByPlacesArrayResponse.map((item, index) => {
                  return <MapView.Marker
                    coordinate={{ "latitude": item.geometry.location.lat, "longitude": item.geometry.location.lng }}
                    title={item.name}
                  />
                })
                : null}
              {this.state.isShowDirections ?
                <Polyline
                  coordinates={this.state.coords}
                  strokeColor="#256ed1" // fallback for when `strokeColors` is not supported by the map-provider
                  strokeWidth={6}
                />
                : null}

            </MapView>
            <View style={{ flex: 0.15, justifyContent: "flex-end" }}>
              <View style={styles.Booking}>
                <View style={styles.BookingList}>
                  <TouchableOpacity activeOpacity={0.5} onPress={this.onPressLiveLocation}>
                    <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center", padding: 5 }}>
                      <Image source={{ uri: 'target' }} style={{ width: 30, height: 30, tintColor: this.state.activeCurrentLocation ? Colors.App_Font : Colors.FooterMenu_Text }} />
                    </View>
                    <Text style={[styles.IconText, { color: this.state.activeCurrentLocation ? Colors.App_Font : Colors.FooterMenu_Text }]}>{I18n.t('recenter')}</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.BookingList}>
                  <TouchableOpacity activeOpacity={0.5} onPress={this.onPressFetchDirection}>
                    <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center", padding: 5 }}>
                      <Image source={{ uri: 'routes' }} style={{ width: 30, height: 30, tintColor: this.state.isShowDirections ? Colors.App_Font : Colors.FooterMenu_Text }} />
                    </View>
                    <Text style={[styles.IconText, { color: this.state.isShowDirections ? Colors.App_Font : Colors.FooterMenu_Text }]}>{I18n.t('directions')}</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.BookingList}>
                  <TouchableOpacity activeOpacity={0.5} onPress={this.onPressNearByPlaces}>
                    <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center", padding: 5 }}>
                      <Image source={{ uri: 'nearby' }} style={{ width: 30, height: 30, tintColor: this.state.isShowNearBy ? Colors.App_Font : Colors.FooterMenu_Text }} />
                    </View>
                    <Text style={[styles.IconText, { color: this.state.isShowNearBy ? Colors.App_Font : Colors.FooterMenu_Text }]}>{I18n.t('nearby')}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          :
          <View><Text style={{ color: 'black' }}>
            {I18n.t('locationoffmsg')}
        </Text></View>
        }
      </View>

    )
  }
}

const styles = StyleSheet.create({
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 0.85
  },
  Booking: {
    // flex: 1,
    // flexDirection: "row",
    // justifyContent: "center",
    // position: 'relative',
    // bottom: 0,
    // backgroundColor: '#f5f5f0',
    // marginTop:15

    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    position: 'absolute',
    bottom: 0,
    marginBottom: 10,
    backgroundColor: '#f5f5f0',
  },
  BookingList: {
    flex: 1,
    height: 'auto',
    alignItems: 'center',
    
  },
  IconText: {
    fontFamily: AppFont.Regular,
  }
});


// directionRoutesResponse = (response) => {
//   console.log("response", response);
//   // decode polyne data
//   if (response != null) {
//     let points = polyline.decode(response.routes[0].overview_polyline.points);
//     console.log("points", points);
//   }
// }

{/* <View
              style={{
                position: 'absolute',//use absolute position to show button on top of the map
                bottom: 5, //for center align
                alignSelf: 'flex-end', //for align to right
                right: 5
              }}
            >
              <TouchableOpacity 
           onPress = {() => _mapView.animateToCoordinate({
            latitude: this.state.mapRegion.latitude,
            longitude: this.state.mapRegion.longitude
          }, 1000)}>
          <Text>Tap</Text>
        </TouchableOpacity>
              <TouchableOpacity onPress={this.onPressNearByPlaces}>
                <Image source={{ uri: 'nearby' }} style={{ width: 40, height: 40 }} />
              </TouchableOpacity>
            </View> */}
{/* 
            <MapView.Marker
              coordinate={{ "latitude": -20.415198, "longitude": 57.710989 }}
              title={"Mahebourg Museum"}
            >
              <Image source={require('../../assets/images/pin.png')} style={{ width: 15, height: 40 }} />
            </MapView.Marker>

            <MapView.Marker
              coordinate={{ "latitude": -20.416319, "longitude": 57.703330 }}
              title={"Mahebourg Museum"}
            >
              <Image source={require('../../assets/images/pin.png')} style={{ width: 15, height: 40 }} />
            </MapView.Marker>

            <Polyline
              coordinates={[
                { latitude: this.state.mapRegion.latitude, longitude: this.state.mapRegion.longitude },
                { latitude: -20.421796, longitude: 57.721973 },
                // { latitude: 13.002318, longitude: 80.256853 },

              ]}
              strokeColor="#256ed1" // fallback for when `strokeColors` is not supported by the map-provider
              strokeColors={[
                '#7F0000',
                '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                '#B24112',
                '#E5845C',
                '#238C23',
                '#7F0000'
              ]}
              strokeWidth={6}
            /> */}

{/* <MapView.Marker
                coordinate={{ "latitude": this.state.mapRegion.latitude, "longitude": this.state.mapRegion.longitude }}
                title={"Your Location"}
              /> */}


{/* <Polyline
                coordinates={[
                  { latitude: 12.9095971, longitude: 80.2264763 },
                  { latitude: 12.9095758, longitude: 80.22613419999999 },
                  { latitude: 12.9100825, longitude: 80.22629719999999 },
                  { latitude: 12.9090884, longitude: 80.228439 },
                  { latitude: 12.9117682, longitude: 80.2288422 },
                  { latitude: 12.9707126, longitude: 80.24947759999999 },
                  { latitude: 12.9943674, longitude: 80.2496609 },
                  { latitude: 12.9950543, longitude: 80.2523575 },
                  { latitude: 12.999485, longitude: 80.2519693 },
                  { latitude: 13.0013385, longitude: 80.2519741 },
                  { latitude: 13.0012693, longitude: 80.2564261 },
                ]}
                strokeColor="#000" // fallback for when `strokeColors` is not supported by the map-provider
                strokeColors={[
                  '#7F0000',
                  '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                  '#B24112',
                  '#E5845C',
                  '#238C23',
                  '#7F0000'
                ]}
                strokeWidth={6}
              />
            
             <Polyline
              coordinates={[
                { latitude: 12.9098236, longitude: 80.22621389999999 },
                { latitude: 12.9100825, longitude: 80.22629719999999 },
                { latitude: 12.9090884, longitude: 80.228439 },
                { latitude: 12.9117682, longitude: 80.2288422 },
                { latitude: 12.9707126, longitude: 80.24947759999999 },
                { latitude: 12.9943674, longitude: 80.2496609 },
                { latitude: 12.9950543, longitude: 80.2523575 },
                { latitude: 12.999485, longitude: 80.2519693 },
                { latitude: 13.0013385, longitude: 80.2519741 },
                { latitude: 13.0012693, longitude: 80.2564261 },
              ]}
              strokeColor="#000" // fallback for when `strokeColors` is not supported by the map-provider
              strokeColors={[
                '#7F0000',
                '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                '#B24112',
                '#E5845C',
                '#238C23',
                '#7F0000'
              ]}
              strokeWidth={6}
            />
            
            
            */}