import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Dimensions,
    Image

} from 'react-native';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import HTML from 'react-native-render-html';
import { Actions } from 'react-native-router-flux';

import I18n from '../constants/i18n';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class Rating extends Component {
	constructor(props) {
        super(props);
        this.state = {
            ambitious: false,
            sad: false,
            smile: false,
            surprised: false,
            worried: false,
            Default_Rating: 0,
            Max_Rating: 5,
        };
        this.Star = 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png';
        this.Star_With_Border = 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png';
	}
	UpdateRating(key) {
        this.setState({ Default_Rating: key });
        this.props.onselectedItems(this.props.details,key)
        //Keeping the Rating Selected in state
      }
	render() {
        var ambitious = this.state.ambitious
        ? require('../../assets/images/Smile_2_On.png')
        : require('../../assets/images/Smile_2_Over.png');
        var sad = this.state.sad
        ? require('../../assets/images/Smile_3_On.png')
        : require('../../assets/images/Smile_3_Over.png');
        var smile = this.state.smile
        ? require('../../assets/images/Smile_4_On.png')
        : require('../../assets/images/Smile_4_Over.png');
        var surprised = this.state.surprised
        ? require('../../assets/images/Smile_5_On.png')
        : require('../../assets/images/Smile_5_Over.png');
        var worried = this.state.worried
        ? require('../../assets/images/Smile_1_On.png')
        : require('../../assets/images/Smile_1_Over.png');

        let React_Native_Rating_Bar = [];
    //Array to hold the filled or empty Stars
    for (var i = 1; i <= this.state.Max_Rating; i++) {
      React_Native_Rating_Bar.push(
        <TouchableOpacity
          activeOpacity={0.7}
          key={i}
          onPress={this.UpdateRating.bind(this, i)}>
          <Image
            style={styles.StarImage}
            source={
              i <= this.state.Default_Rating
                ? { uri: this.Star }
                : { uri: this.Star_With_Border }
            }
          />
        </TouchableOpacity>
      );
    }
		return (
			<View style={{ borderBottomColor:'grey', borderBottomWidth: 0.5 }}>
				<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15 }}>
					<Text style={{
						fontSize: 16,
						marginLeft: 16,
                        includeFontPadding: false,
                        fontFamily:AppFont.Regular
					}}>
						{this.props.details.RatingQuestion}
                       
                    </Text>
                    </View>
                    {this.props.details.RatingQuestionType == 1 ?
					<View style={{
                        flexDirection: 'row',
                        height:70,
                        justifyContent:'center'
					}}>

						<TouchableOpacity style={{ width: 30, height: 30, marginLeft: 25, marginTop: 15 }} onPress={this.onChangeStatus.bind(this,'1')}>
							<Image style={{ width: 35, height: 35 }} source={worried} />
						</TouchableOpacity>
                        <TouchableOpacity style={{ width: 30, height: 30, marginLeft: 10, marginTop: 15 }} onPress={this.onChangeStatus.bind(this,'2')}>
							<Image style={{ width: 35, height: 35 }} source={sad} />
						</TouchableOpacity>
                        <TouchableOpacity style={{ width: 30, height: 30, marginLeft: 10, marginTop: 15 }} onPress={this.onChangeStatus.bind(this,'3')}>
							<Image style={{ width: 35, height: 35 }} source={ambitious} />
						</TouchableOpacity>
                        <TouchableOpacity style={{ width: 30, height: 30, marginLeft: 10, marginTop: 15 }} onPress={this.onChangeStatus.bind(this,'4')}>
							<Image style={{ width: 35, height: 35 }} source={smile} />
						</TouchableOpacity>
                        <TouchableOpacity style={{ width: 30, height: 30, marginLeft: 10, marginTop: 15 }} onPress={this.onChangeStatus.bind(this,'5')}>
							<Image style={{ width: 35, height: 35 }} source={surprised} />
						</TouchableOpacity>
                        
						
                    </View>	
                    :
                    <View style={styles.childView}>{React_Native_Rating_Bar}</View>
                }	
			</View>
        )
        
    }
    onChangeStatus = (textl) => {
        if(textl==1){
            this.setState({
                worried:true,
                sad: false,
                ambitious: false,
                smile: false,
                surprised: false,
            })
        }else if(textl==2){
            this.setState({
                worried:false,
                sad: true,
                ambitious: false,
                smile: false,
                surprised: false,
            })
        }else if(textl==3){
            this.setState({
                worried:false,
                sad: false,
                ambitious: true,
                smile: false,
                surprised: false,
            })
        }else if(textl==4){
            this.setState({
                worried:false,
                sad: false,
                ambitious: false,
                smile: true,
                surprised: false,
            })
        }else if(textl==5){
            this.setState({
                worried:false,
                sad: false,
                ambitious: false,
                smile: false,
                surprised: true,
            })
        }
        this.props.onselectedItems(this.props.details,textl)
    }
}
const styles = StyleSheet.create({
    
    StarImage: {
      width: 40,
      height: 40,
      resizeMode: 'cover',
    },
    childView: {
        flexDirection: 'row',
        marginTop: 10,
        height:70,
        marginLeft: 16,
        justifyContent:'center'
      },
  });

module.exports = Rating;