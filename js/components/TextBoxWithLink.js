import React, { Component } from 'react';
import { Actions, Reducer } from 'react-native-router-flux';
import AppFont from '../constants/AppFont';

import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    AsyncStorage,
    Alert,
    Image,
    KeyboardAvoidingView,
    ImageBackground,
    Dimensions,
    Platform

} from 'react-native';
import Keys from '../constants/Keys';
import Colors from '../constants/Colors';
import I18n from '../constants/i18n';

class TextBoxWithLink extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentStep: 1,
            textContent: "",
        }
    }

    setStep(num) {
        this.setState({ currentStep: num });
    }

    updateForgotEmail(email) {
         console.log("email", email)
        // console.log("email length", email.length)
        this.setState({ textContent: email.trim() },()=>console.log("email", this.state.textContent));
    }

    render() {
        console.log("textboxwithlink", this.state)
        console.log("props", this.props)
        return (
            <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <View style={styles.inputIconContainer}>
                        <View style={{ width: '100%', height: 80, alignItems: 'flex-start' }}>
                            <Text
                                allowFontScaling
                                numberOfLines={1}
                                style={styles.label}>
                                {I18n.t('emailid')}
                            </Text>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    placeholder={this.props.placeholder}
                                    placeholderTextColor={Colors.PlaceholderText}
                                    style={styles.textInput}
                                    // onChangeText={(text) => {this.setState({textContent: text})}}
                                    onChangeText={this.updateForgotEmail.bind(this)}
                                    value={this.state.textContent}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    editable={this.props.textboxDisableStatus}
                                />
                                {this.state.currentStep == 1 ?
                                    <TouchableOpacity onPress={() => { this.props.linkHandler(this.state.textContent) }}>
                                        <Text style={styles.verifyLink}>
                                            {this.props.verifybtnText}
                                        </Text>
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignContent: 'center',
        paddingHorizontal: 20,
        marginTop: 30
    },
    inputContainer: {
        flexDirection: 'column',
    },
    inputIconContainer: {
        flexDirection: 'row',
    },
    textInput: {
        marginLeft: 0,
        height: '100%',
        fontSize: 15,
        fontFamily: AppFont.Regular,
        marginTop: 0,
        ...Platform.select({
            ios: {
                width: '80%',
            },
            android: {
                width: '83%',
            },
        }),
    },
    verifyLink: {
        color: Colors.AppFont,
        marginTop: 4,
        paddingHorizontal: 14,
        paddingVertical: 4,
        width: "100%",
        borderWidth: 1,
        borderColor: Colors.App_Font,
        borderRadius: 12,
        fontSize: 12,
        zIndex: 1,
        alignContent: 'center',
        
    },
    SectionStyle: {
        height: 50,
        width: '100%',
        borderBottomWidth: 1,
        borderColor: Colors.App_Font,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
        marginBottom: 10
    },
    label: {
        alignItems: "flex-start",
        width: '85%',
        fontFamily: AppFont.Regular,
        color: Colors.Black,
        ...Platform.select({
            ios: {
                marginLeft: 0,
            },
            android: {
                marginLeft: 3,
            },
        }),
    },
});

export default TextBoxWithLink;