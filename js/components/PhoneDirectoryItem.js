import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TouchableNativeFeedback } from 'react-native';
//import More from '../screens/More';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import I18n from '../constants/i18n';


class PhoneDirectoryItem extends React.Component {
    constructor(props) {
        super(props);
        console.log("props",this.props)
    }

    render() {
        return (
            <View style={styles.FlightDetails}>
                <View style={{ flex: 0.6 }}>
                    <Text style={styles.HeadText}>{this.props.phonedirectory.Name}</Text>
                </View>
                <View style={{ flex: 0.4, borderLeftColor: Colors.Gray, borderLeftWidth: 0.5 }}>
                    <Text style={styles.DetailsText}>{this.props.phonedirectory.Number}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    FlightDetails: {
        flex: 1,
        flexDirection: "row",
        borderBottomColor: Colors.Gray,
        borderBottomWidth: 0.5,
    },
    HeadText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        padding: 13
    },
    DetailsText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        padding: 13,
        flexWrap: 'wrap',
    },

});

export default PhoneDirectoryItem