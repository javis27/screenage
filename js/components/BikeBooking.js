import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    Platform
} from 'react-native';
import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';
import { Actions } from 'react-native-router-flux';
import BottomSheet from 'react-native-bottomsheet';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Fontisto';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Status from '../constants/Status';
// import CancellationPolicy from '../components/CancellationPolicy';
import Policy from '../components/Policy';
import Loader from '../components/Loader';
import Moment from 'moment';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import DatePickerAttributes from '../constants/DatePickerAttributes';

class BikeBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bikeBookingDate: '',
            selectedBike: I18n.t('bikechoose'),
            selectedTimeSlot: 'Choose Time Slot',
            // selectedVisitor: I18n.t('chooseguest'),
            selectedVisitor: '',
            bikeArray: [],
            bikeTimeslotArray: [],
            bikeVisitorArray: [],
            bikeId: '',
            bikeTimeslotId: '',
            bikeVisitorId: '',
            preferenceNote: '',
            userId: '',
            bikeCancelPolicyVisible: false,
            loading: false,
            isSelectedIndividualDisclaimer: false,
            isSelectedGroupDisclaimer: false,
            bikeTime: '',
            minBookingDate: '',
            maxBookingDate: '',
            isSelectedBikeDisclaimer: false
        }
    }
    componentDidMount() {
        this._loadDropdowns();
        // this.verifyCheckInDetails();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     console.log("postCheckInResponse Language", response);

    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this.setState({ minBookingDate: postCheckInDetailResponse.currentDate, maxBookingDate: postCheckInDetailResponse.checkOutDate })
    //             // this._loadDropdowns();
    //         }
    //     }
    // }


    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <TableView>
                        <Loader loading={this.state.loading} />
                        <Section sectionPaddingTop={0} sectionPaddingBottom={0} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                            <Cell title="Date" placeholder="Date" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label} >
                                           {I18n.t('date')}
                                        </Text>
                                        <View>
                                            <DatePicker
                                                style={{ width: null, marginLeft: 8, }}
                                                date={this.state.bikeBookingDate}
                                                mode="date"
                                                placeholder="Date"
                                                // format="LL LT"
                                                format="DD/MM/YYYY"
                                                minDate={new Date(this.state.minBookingDate)}
                                                maxDate={new Date(this.state.maxBookingDate)}
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                iconSource={{ uri: 'date' }}
                                                customStyles={{
                                                    dateInput: DatePickerAttributes.dateInput,
                                                    dateIcon: DatePickerAttributes.dateIcon,
                                                    placeholderText: DatePickerAttributes.placeholderText,
                                                    dateText: DatePickerAttributes.dateText,
                                                    btnTextCancel: DatePickerAttributes.btnTextCancel,
                                                    btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                                                }}
                                                onDateChange={(date) => {
                                                    this.setState({
                                                        bikeBookingDate: date,
                                                    })
                                                }}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Bike" placeholder="Bike" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('bike')}
                                        </Text>
                                        <View>
                                            <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheetBike}>
                                                <Text style={styles.picker}>{this.state.selectedBike}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />

                            <Cell title="Bike Time" placeholder="Bike Time" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label} >
                                            {I18n.t('time')}
                                        </Text>
                                        <View>
                                            {/* <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5}>
                        <Text style={styles.picker}>{this.state.bookingDate}</Text>
                      </TouchableOpacity> */}
                                            <DatePicker
                                                style={{ width: null, marginLeft: 8, }}
                                                date={this.state.bikeTime}
                                                mode="time"
                                                placeholder={I18n.t('time')}
                                                // format="LL LT"
                                                format="HH:mm"
                                                minDate="10:30"
                                                maxDate="19:30"
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                iconSource={{ uri: 'date' }}
                                                is24Hour={false}
                                                androidMode="spinner"
                                                customStyles={{
                                                    dateInput: DatePickerAttributes.dateInput,
                                                    dateIcon: DatePickerAttributes.dateIcon,
                                                    placeholderText: DatePickerAttributes.placeholderText,
                                                    dateText: DatePickerAttributes.dateText,
                                                    btnTextCancel: DatePickerAttributes.btnTextCancel,
                                                    btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                                                }}
                                                onDateChange={(time) => {
                                                    this.setState({
                                                        bikeTime: time,
                                                    })
                                                }}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            {/* <Cell title="Time Slot" placeholder="Time Slot" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('timeslot')}
                                        </Text>
                                        <View>
                                            <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheetBikeTimeSlot}>
                                                <Text style={styles.picker}>{this.state.selectedTimeSlot}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } /> */}

                            <Cell title="Guest Name" placeholder="Guest Name" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('guestname')}
                                        </Text>
                                        <View>
                                            {/* <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheetBikeVisitor}>
                                                <Text style={styles.picker}>{this.state.selectedVisitor}</Text>
                                            </TouchableOpacity> */}
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                placeholder={I18n.t('guestname')}
                                                onChangeText={(selectedVisitor) => this.setState({ selectedVisitor })}
                                                value={this.state.selectedVisitor} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Preference" placeholder="Preference" id="" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label} >
                                            {I18n.t('preference')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                placeholder={I18n.t('preference')}
                                                onChangeText={(preferenceNote) => this.setState({ preferenceNote })}
                                                value={this.state.preferenceNote} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />

                            <Cell title="Disclaimer" hideSeparator={true} placeholder="Disclaimer" id="6" cellContentView={
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingVertical: 20, }}>
                                    {/* <View style={{ flex: 1, alignItems: "flex-start" }}> */}
                                    <View>
                                        <TouchableOpacity onPress={() => this.onPressAgree()} style={{ paddingHorizontal: 10 }}>
                                            <Icon name={this.state.isSelectedBikeDisclaimer ? "checkbox-active" : "checkbox-passive"} size={18} color={Colors.Black} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexWrap: 'wrap' }}>
                                        <Text style={styles.AgreeText}>{I18n.t('agreebikedisclaimer')}</Text>
                                    </View>
                                    {/* <TouchableOpacity activeOpacity={.5} style={{ paddingHorizontal: 5 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeIndividualDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('individualDisclaimer')}</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.AgreeText}> or </Text>
                                    <TouchableOpacity activeOpacity={.5} style={{ paddingLeft: 40, paddingVertical:10 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeGroupDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('groupbikeDisclaimer')}</Text>
                                    </TouchableOpacity> */}
                                </View>
                            } />
                            <Cell title="Cancellation Policy" hideSeparator={true} placeholder="Cancellation Policy" id="7" cellContentView={
                                <View style={{ flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center", paddingHorizontal: 10, paddingVertical: 10 }}>
                                    <View style={{ flex: 1, alignItems: "center" }}>
                                        <TouchableOpacity activeOpacity={.5} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeIndividualDisclaimer.typeId)}>
                                            <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('individualDisclaimer')}</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ flex: 1, alignItems: "center" }}>
                                        <TouchableOpacity activeOpacity={.5} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeGroupDisclaimer.typeId)}>
                                            <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('groupbikeDisclaimer')}</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ flex: 1, alignItems: "center" }}>
                                        <TouchableOpacity activeOpacity={.5} onPress={this.handleBikeCancelPolicy}>
                                            <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('cancellaitionpolicy')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            } />
                            <Cell title="Book" placeholder="Book" id="" cellContentView={
                                <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                    <View style={{ width: "90%", marginLeft: 15 }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressBikeBooking}>
                                                <Text style={styles.btntext}>{I18n.t('bookbtn')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            } />
                            {/* <Cell title="View My Bookings" hideSeparator={true} placeholder="View My Bookings" id="7" cellContentView={
                                <View style={{ flex: 1, height: 70, justifyContent: "center", alignItems: "center", marginBottom: 25 }}>
                                    <TouchableOpacity activeOpacity={.5} onPress={this._onPressBikeBookingList}>
                                        <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('viewbookings')}</Text>
                                    </TouchableOpacity>
                                </View>
                            } /> */}
                        </Section>
                    </TableView>
                </ScrollView>

                {
                    this.state.bikeCancelPolicyVisible ?
                        <Policy policyVisibility={this.state.bikeCancelPolicyVisible}
                            policyType={Status.policyTypes.bikeCancellationPolicy.typeId}
                            bookPageIndex={2}
                            callbackPolicy={this.setBikeCancelPolicyVisiblity} />
                        : null
                }

            </View>
        );
    }

    onPressAgree = () => {
        console.log("isSelected", this.state.isSelectedBikeDisclaimer)
        this.setState({ isSelectedBikeDisclaimer: !this.state.isSelectedBikeDisclaimer })
    }

    handleBikeCancelPolicy = async () => {
        await this.setState({ bikeCancelPolicyVisible: true }, () => console.log("bikeCancelPolicyVisible", this.state.bikeCancelPolicyVisible))
    }

    setBikeCancelPolicyVisiblity = async (data) => {
        console.log('setAboutVisibility', data);
        await this.setState({ bikeCancelPolicyVisible: data }, () => console.log("Visibility", this.state.bikeCancelPolicyVisible));

    }


    _onPressBikeBookingList = async () => {
        // Actions.ListRestaurantBookings();
        Actions.BookingsList({ BookingListIndex: 2 });
    }

    _onPressSafetyPolicy = async (typeId) => {
        console.log("typeId", typeId)
        Actions.BikeSafetyPolicy({
            disclaimerType: typeId,
            // callbackBikeDisclaimerFunc: this.getcallbackBikeDisclaimerFunc
        });
    }

    // getcallbackBikeDisclaimerFunc = async (data) => {
    //     console.log("selecteddata..", data);
    //     if (data.typeId == Status.disclaimerTypes.bikeIndividualDisclaimer.typeId) {
    //         this.setState({ isSelectedIndividualDisclaimer: data.isSelected })
    //     }
    //     else if (data.typeId == Status.disclaimerTypes.bikeGroupDisclaimer.typeId) {
    //         this.setState({ isSelectedGroupDisclaimer: data.isSelected })
    //     }

    //     console.log("isSelectedGroupDisclaimer..", this.state);
    // }

    _onPressGroupDisclaimer = async () => {
        Actions.GroupDisclaimer();
    }

    _onBtmSheetBike = async () => {
        console.log("this.state.myArray", this.state.bikeArray);
        console.log("this.state.totalData", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            // title: "Choose Restaurant",
            options: this.state.bikeArray,
            dark: false,
            cancelButtonIndex: 50,
            // destructiveButtonIndex: 1,
        }, (value) => {
            console.log("selectedBike", this.state.totalData[value].BikeName);
            this.setState({
                "selectedBike": this.state.totalData[value].BikeName,
                "bikeId": this.state.totalData[value].BikeId
            })
            console.log("bikeId", this.state.id);
            console.log("selectedBike", this.state.selectedBike);
        });
    }

    _onBtmSheetBikeTimeSlot = async () => {
        console.log("this.state.myArray", this.state.bikeTimeslotArray);
        console.log("this.state.totalData", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            //  title: "Choose Restaurant",
            options: this.state.bikeTimeslotArray,
            dark: false,
            cancelButtonIndex: 12,
            // destructiveButtonIndex: 1,
        }, (value) => {
            // if (value >= this.state.totalData.length) {  
            //   console.log("array",this.state.myArray)
            //   return;
            // }
            // else {
            console.log("value", value);
            console.log("this.state.totalData.value", this.state.totalData[1][value].value);
            console.log("this.state.totalData.value", this.state.totalData[1][value].id);
            this.setState({
                "selectedTimeSlot": this.state.totalData[1][value].value,
                "bikeTimeslotId": this.state.totalData[1][value].id
            })
            console.log("bikeTimeslotId", this.state.bikeTimeslotId);
            console.log("selectedTimeSlot", this.state.selectedTimeSlot);
        });
    }

    _onBtmSheetBikeVisitor = async () => {
        console.log("this.state.myArray", this.state.bikeVisitorArray);
        console.log("this.state.totalData", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            //  title: "Choose Restaurant",
            options: this.state.bikeVisitorArray,
            dark: false,
            cancelButtonIndex: 12,
            // destructiveButtonIndex: 1,
        }, (value) => {
            // if (value >= this.state.totalData.length) {  
            //   console.log("array",this.state.myArray)
            //   return;
            // }
            // else {
            console.log("value", value);
            console.log("this.state.totalData.value", this.state.totalData[2][value].value);
            console.log("this.state.totalData.value", this.state.totalData[2][value].id);
            this.setState({
                "selectedVisitor": this.state.totalData[2][value].value,
                "bikeVisitorId": this.state.totalData[2][value].id
            })
            console.log("bikeVisitorId", this.state.bikeVisitorId);
            console.log("selectedVisitor", this.state.selectedVisitor);
        });
    }

    _loadDropdowns = async () => {
        await this.setState({ loading: true });
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        await this.setState({ userId: jsonValue.UserId });
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        
        let overalldetails = {
            "HotelId": HotelId
          }
          await apiCallWithUrl(APIConstants.GetBikesMastersAPI, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });

        // await apiCallWithUrl(APIConstants.GetBikesMastersAPI + "?FullName=" + jsonValue.Fullname + "&&CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }
        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        var currentdate=today.toISOString();
        this.getDropdownsResponse(response);
        this.setState({ minBookingDate: currentdate, maxBookingDate: Moment().add(60, 'days').calendar() })
        console.log("postCheckInResponse", response.InCustomer);
        // if (response.InCustomer == "False") {
        //     console.log("postCheckInResponse Language", response.InCustomer);
        //     if (Platform.OS === 'ios') {
        //         console.log("ios postCheckInResponse")
        //         this.setState({ loading: false }, () => {
        //             setTimeout(() => this.funcCheckOutResponse(response), 1000);
        //         });
        //     } else {
        //         console.log("android postCheckInResponse")
        //         this.setState({
        //             loading: false,
        //         }, () => this.funcCheckOutResponse(response));
        //     }

        // }
        //  else {
        //     this.getDropdownsResponse(response);
        // }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getDropdownsResponse = async (response) => {
        await this.setState({ loading: false, totalData: response.ResponseData })

        console.log("totalResponse", response.ResponseData);
        console.log("haaa...", response.ResponseData[0]);
        var list1 = [];
        var list2 = [];
        var list3 = [];
        response.ResponseData.map((bike, index) => {
            console.log("loop", index, bike.BikeName);
            list1.push(bike.BikeName);
        });
        this.setState({ bikeArray: list1 })

        // response.ResponseData[1].map((timeslot, index) => {
        //     console.log("loop", index, timeslot.value);
        //     list2.push(timeslot.value);
        // });
        // this.setState({ bikeTimeslotArray: list2 })

        // response.ResponseData[2].map((visitor, index) => {
        //     console.log("loop", index, visitor.value);
        //     list3.push(visitor.value);
        // });
        // this.setState({ bikeVisitorArray: list3 })

        console.log("bikeArray", this.state.bikeArray);
        // console.log("timeslotArray", this.state.bikeTimeslotArray);
        // console.log("visitorArray", this.state.bikeVisitorArray);
    }

    _onPressBikeBooking = async () => {
        console.log("Bike Time", this.state.bikeTime);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        const InCustomer = await AsyncStorage.getItem(Keys.inCustomer);
        console.log("InCustomer", InCustomer);
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        console.log("roomNo", roomNo);
        if (InCustomer == "True") {
            const AssignedRoomNo=roomNo
          } else {
            const AssignedRoomNo=""
          }
        if (this.state.bikeBookingDate == "") {
            Alert.alert(I18n.t('chooseDate'));
        } else {
            if (this.state.bikeId == "") {
                Alert.alert(I18n.t('bikechoose'));
            } else {
                if (this.state.bikeTime == "") {
                    Alert.alert(I18n.t('selectTime'));
                } else {
                    if (this.state.selectedVisitor == "") {
                        Alert.alert(I18n.t('chooseguest'));
                    } else {
                        // if (!this.state.isSelectedIndividualDisclaimer) {
                        //     if (!this.state.isSelectedGroupDisclaimer) {
                        //         Alert.alert(I18n.t('agreedisclaimer'));
                        //         return false;
                        //     }
                        // }

                        if (!this.state.isSelectedBikeDisclaimer) {
                            Alert.alert(I18n.t('agreedisclaimer'));
                            return false;
                        }
                        var res = this.state.bikeBookingDate.split("/");
                        var bookingdt=res[2]+'-'+res[1]+'-'+res[0];
                        var bookingdatetime=bookingdt+' '+this.state.bikeTime;
                        this.setState({ loading: true })
                        let overalldetails = {
                            "BikeId": this.state.bikeId,
                            "BookingDateTime": bookingdatetime,
                            "GuestName": this.state.selectedVisitor,
                            "Preference": this.state.preferenceNote,
                            "UserId": this.state.userId,
                            "CreatedBy": this.state.userId,
                            "ApprovalStatusId": "1",
                            "HotelId": HotelId,
                            "UserType": "0",
                            "RoomNo": roomNo
                        }
                        
                        console.log('overalldetails', overalldetails)
                        await apiCallWithUrl(APIConstants.PostBikeBookingAPI, 'POST', overalldetails, this.postBikeResponse);
                    }
                }
            }
        }
    }

    postBikeResponse = async (response) => {
        if (Platform.OS === 'ios') {
            this.setState({
                loading: false,
            }, () => {
                setTimeout(() => this.funcBikeResponse(response), 1000);

            });
        } else {
            this.setState({
                loading: false,
            }, () => this.funcBikeResponse(response));
        }

        // if (response.IsException == null) {
        //     if (response.ResponseData.length > 0) {
        //         var postBikeResponse = response.ResponseData;
        //         console.log('postBikeResponse', postBikeResponse);

        //         setTimeout(function () {

        //             //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
        //             Alert.alert(I18n.t('alertbikesuccess'));
        //         }, 1000);
        //         Actions.BookingsList({ BookingListIndex: 2 });
        //     }
        // }

    }

    funcBikeResponse = (response) => {
        if (response.IsException == null) {
            // if (response.ResponseData.length > 0) {
                var postBikeResponse = response.ResponseData;
                console.log('postBikeResponse', postBikeResponse);
                Alert.alert(I18n.t('alertbikesuccess'));
                Actions.pop({ refresh: true });
        // Actions.refresh({ key: Math.random() * 1000000 })
        setTimeout(() => {
            Actions.refresh({ key: Math.random() * 1000000 })
      }, 5)
            // }
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        // flexDirection: 'column'
    },
    label: {
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    picker: {
        color: Colors.PlaceholderText,
        marginLeft: 8,
        height: 28,
        padding: 0,
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        fontSize: 15,
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    AgreeText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular
    },

});

module.exports = BikeBooking;