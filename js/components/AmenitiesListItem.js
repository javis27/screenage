import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,Image,Alert,AsyncStorage
} from 'react-native';

import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Icon from 'react-native-vector-icons/Feather';
import Status from '../constants/Status';
import { Actions } from 'react-native-router-flux';
import Keys from '../constants/Keys';
import I18n from '../constants/i18n';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
class AmenitiesListitem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amenityListItem: this.props.amenityDetails
        }
    }
    render() {
        return (
            <View style={styles.listcontainer}>
                <View style={styles.Amenity}>
                    <Text style={styles.ReqText}>
                        {this.props.amenityDetails.Amenity}
                    </Text>
                </View>
                <View style={styles.Amenity}>
                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <Icon name={'clock'} color={'#a3a3a3'}></Icon>
                        <Text style={styles.StatusText}>{this.props.amenityDetails.BookingDateTime}</Text>
                    </View>
                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', }}>
                        <Icon size={13} name={'check-circle'} color={this.StatusColors(this.props.amenityDetails.ApprovalStatusId)}></Icon>
                        <Text style={[styles.StatusText, { color: this.StatusColors(this.props.amenityDetails.ApprovalStatusId) }]}>{this.props.amenityDetails.ApprovalStatus}</Text>
                    </View>
                </View>
                <View style={styles.Amenity}>
                {this.props.amenityDetails.ApprovalStatus == 'Completed'?
                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                    {this.props.amenityDetails.IsGivenReview == 0 ?
                    <View>
                        {/* <Text style={styles.StatusText}>Please rate your experience!</Text> */}
                        <TouchableOpacity activeOpacity={.5}  onPress={() => this.feedbackRate(this.props.amenityDetails.AmenityBookingId)}>
                        <Text style={styles.StatusText}>{I18n.t('please_rate_your_experience')}</Text>
                                    </TouchableOpacity>
                    </View>
                    :
                    <View>
                        {/* <Text style={styles.StatusText}>Please rate your experience!</Text> */}
                        
                        <Text style={styles.StatusText}>{I18n.t('thanks_for_rating')}</Text>
                    </View>
                    }
                    </View>
                    :
                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}></View>
                }

                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                    {
                        this.props.amenityDetails.ApprovalStatusId == Status.amenityRequestStatus.requested.id ?
                            <View>
                                {/* <TouchableOpacity activeOpacity={.5} onPress={() => this.handleAmenetiesCancel(this.props.amenityDetails.AmenityBookingId)} style={{ backgroundColor: Colors.App_Font, borderRadius: 20, padding: 6, paddingRight: 10, paddingLeft: 25 }}>
                                    <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                    <Text style={styles.CloseText}> Cancel</Text>
                                </TouchableOpacity> */}
                                <TouchableOpacity activeOpacity={.5} onPress={() => this.handleAmenetiesCancel(this.props.amenityDetails.AmenityBookingId)}>
                                <Image source={require('../../assets/images/icon_cancel.png') } style={{width: 90, height: 30}} />
                                </TouchableOpacity>
                            </View>
                            :
                            <View>
                                {/* <TouchableOpacity activeOpacity={.5} style={{ backgroundColor: Colors.App_Font, borderRadius: 20, padding: 6, paddingRight: 10, paddingLeft: 25 }}>
                                    <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                    <Text style={styles.CloseText}> Cancel</Text>
                                </TouchableOpacity> */}
                            </View>
                    }
                        
                    </View>
                </View>
            </View >
        );
    }
    feedbackRate = (itemId) => {
        console.log("itemid",itemId);
        Actions.BookingsRatings({"ModuleId":4,"OrderId":itemId});
    }
    handleAmenetiesCancel =(AmenityBookingId)=>{
        console.log("AmenityBookingId",AmenityBookingId);
        Alert.alert(I18n.t('are_you_sure_cancel'), '',
            [
                {
                    text: I18n.t('no_dialog'),
                    onPress: () => console.log('Cancel Pressed'),
                    //style: 'cancel'
                },
                {
                    text: I18n.t('yes_dialog'),
                    onPress: () => this.okDialog(AmenityBookingId),
                    //style: 'destructive'
                },
            ],
            {
                cancelable: false
            }
        )
    }
    StatusColors(item) {
        var x = item;
        var textbgColor = "";
        if (x == Status.amenityRequestStatus.requested.id) {
            textbgColor = Status.amenityRequestStatus.requested.textColor;
        }
        else if (x == Status.amenityRequestStatus.completed.id) {
            textbgColor = Status.amenityRequestStatus.completed.textColor;
        }
        else if (x == Status.amenityRequestStatus.pending.id) {
            textbgColor = Status.amenityRequestStatus.pending.textColor;
        }
        else if (x == Status.amenityRequestStatus.notavailable.id) {
            textbgColor = Status.amenityRequestStatus.notavailable.textColor;
        }

        return textbgColor;
    }
    okDialog = async (AmenityBookingId) => {
        console.log('hit Ok');
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        await this.setState({ userId: jsonValue.UserId });
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        console.log("AmenityBookingId...",AmenityBookingId)
        
            let params = {
                "CreatedBy": this.state.userId,
                "AmenityBookingId": AmenityBookingId,
                "HotelId":HotelId
            }
            await apiCallWithUrl(APIConstants.GetAmenityCancelAPI, "POST", params, this.postPolicyResponse)
    }


    postPolicyResponse = async (response) => {
        console.log('cancel response', response);
        this.setState({
            loading: false,
        });

        if (response.ResponseData != false) {
            var postPolicyResponses = response.ResponseData;

            console.log(postPolicyResponses);

            setTimeout(function () {
                Alert.alert(I18n.t('booking_cancelled'));
            }, 1000);

            Actions.refresh({ key: Math.random() * 1000000})
        }

    }
}

const styles = StyleSheet.create({
    listcontainer: {
        flex: 1,
        flexDirection: 'column',
    },
    Amenity: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 5,
        paddingVertical: 5,
    },
    ReqText: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
    },
    StatusText: {
        color: '#a3a3a3',
        fontSize: 13,
        paddingLeft: 6,
        fontFamily: AppFont.Regular
    },
    CloseText: {
        fontSize: 10,
        color: Colors.Background_Color,
        alignItems: 'center',
        justifyContent: "center",
        textTransform: "uppercase",
    },
});

module.exports = AmenitiesListitem;