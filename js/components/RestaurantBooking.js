import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  Linking
} from 'react-native';

import {
  Cell,
  TableView,
  Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/Fontisto';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

import BottomSheet from 'react-native-bottomsheet';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import NumericInput from 'react-native-numeric-input';
import Status from '../constants/Status';
// import CancellationPolicy from '../components/CancellationPolicy';
import Policy from '../components/Policy';
class RestaurantBooking extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedRestaurant: I18n.t('restaurantchoose'),
      bookingDate: '',
      selectedTimeSlot: I18n.t('choose_time_slot'),
      numberofPeople: I18n.t('choose_no_of_people'),
      restaurantArray: [],
      timeslotArray: [],
      // noofPeopleArray: ["<Select>", "1", "2", "3", "4", "5", "6"],
      internalGuests: 0,
      externalGuests: 0,
      restaurantId: '',
      // timeslotId: '',
      preferenceNote: '',
      allergyNote: '',
      userId: '',
      loading: false,
      bookingTime: '',
      minBookingDate: '',
      maxBookingDate: '',
      menupath:''
    }
  }

  static onEnter() {
    console.log("Restaurant booking");
  }

  componentDidMount() {
    console.log("Date", new Date())
    this._loadDropdowns();
    // this.verifyCheckInDetails();
  }

  // verifyCheckInDetails = async () => {
  //   await this.setState({ loading: true });
  //   const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
  //   let overalldetails = {
  //     "CheckInId": checkInVal,
  //   }
  //   console.log("overalldetails", overalldetails);
  //   await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
  // }

  // postCheckInDetailsResponse = async (response) => {
  //   console.log("postCheckInResponse Language", response);

  //   if (Platform.OS === 'ios') {
  //     this.setState({ loading: false }, () => {
  //       setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
  //     });
  //   } else {
  //     this.setState({
  //       loading: false,
  //     }, () => this.funcCheckInDetailsResponse(response));
  //   }
  // }

 
  render() {
    return (
      <View style={styles.container}>

        <ScrollView>
          <TableView>
            {/* <Loader loading={this.state.loading} /> */}
            <Section sectionPaddingTop={0} sectionPaddingBottom={0} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
              <Cell title="Restaurant" placeholder="Restaurant" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label} >
                      {I18n.t('restaurant')}
                    </Text>
                    <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetRestaurant}>
                        <Text style={styles.picker}>{this.state.selectedRestaurant}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } />
              <Cell title="Restaurant" placeholder="Restaurant" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%",alignItems:'center' }}>
                      <TouchableOpacity style={{ paddingTop: 7,alignItems:'center' }} activeOpacity={.5} onPress={this.viewMenu}>
                        <Text style={styles.picker}>{I18n.t('click_to_view_menu')}</Text>
                      </TouchableOpacity>
                    </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } />

              <Cell title="Date" placeholder="Date" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label} >
                      {I18n.t('date_alone')}
                    </Text>
                    <View>
                      {/* <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5}>
                        <Text style={styles.picker}>{this.state.bookingDate}</Text>
                      </TouchableOpacity> */}
                      <DatePicker
                        style={{ width: null, marginLeft: 8, }}
                        date={this.state.bookingDate}
                        mode="date"
                        placeholder="Date"
                        // format="LL LT"
                        format="DD/MM/YYYY"
                        minDate={new Date(this.state.minBookingDate)}
                        maxDate={new Date(this.state.maxBookingDate)}
                        // maxDate={this.state.maximumDate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={{ uri: 'date' }}
                        customStyles={{
                          dateInput: DatePickerAttributes.dateInput,
                          dateIcon: DatePickerAttributes.dateIcon,
                          placeholderText: DatePickerAttributes.placeholderText,
                          dateText: DatePickerAttributes.dateText,
                          btnTextCancel: DatePickerAttributes.btnTextCancel,
                          btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                        }}
                        onDateChange={(date) => {
                          this.setState({
                            bookingDate: date,
                          })
                        }}
                      />
                    </View>
                  </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } />

              <Cell title="Time" placeholder="Time" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label} >
                      {I18n.t('time')}
                    </Text>
                    <View>
                      {/* <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5}>
                        <Text style={styles.picker}>{this.state.bookingDate}</Text>
                      </TouchableOpacity> */}
                      <DatePicker
                        style={{ width: null, marginLeft: 8, }}
                        date={this.state.bookingTime}
                        mode="time"
                        placeholder={I18n.t('time')}
                        // format="LL LT"
                        format="HH:mm"
                        minDate="10:30"
                        maxDate="19:30"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={{ uri: 'date' }}
                        is24Hour={false}
                        androidMode="spinner"
                        customStyles={{
                          dateInput: DatePickerAttributes.dateInput,
                          dateIcon: DatePickerAttributes.dateIcon,
                          placeholderText: DatePickerAttributes.placeholderText,
                          dateText: DatePickerAttributes.dateText,
                          btnTextCancel: DatePickerAttributes.btnTextCancel,
                          btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                        }}
                        onDateChange={(time) => {
                          this.setState({
                            bookingTime: time,
                          })
                        }}
                      />
                    </View>
                  </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } />

              {/* <Cell title="Time Slot" placeholder="Time Slot" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      {I18n.t('timeslot')}
                    </Text>
                    <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetTimeSlot}>
                        <Text style={styles.picker}>{this.state.selectedTimeSlot}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } /> */}

              <Cell title="Number of People" placeholder="Number of People" id="" cellContentView={
                <View style={{ flex: 1,flexDirection:'row', paddingTop: 10, paddingBottom: 10 }}>
                  <View style={{flex:1}}> 
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      {I18n.t('no_of_guests')}
                    </Text>
                    {/* <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetNoofPeople}>
                        <Text style={styles.picker}>{this.state.numberofPeople}</Text>
                      </TouchableOpacity>
                    </View> */}
                    <View style={{ marginTop: 10, marginLeft: 7 }}>
                      <NumericInput
                        value={this.state.internalGuests}
                        onChange={internalGuests => this.setState({ internalGuests })}
                        onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                        minValue={0}
                        maxValue={10}
                        totalWidth={120}
                        totalHeight={40}
                        iconSize={16}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#000000'
                        borderColor={Colors.App_Font}
                        iconStyle={{ color: 'white' }}
                        rightButtonBackgroundColor={Colors.App_Font}
                        leftButtonBackgroundColor={Colors.App_Font} />
                    </View>
                  </View>
                  </View>
                  {/* <View style={{flex:0.5}}> 
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      External Guests
                    </Text>
                    
                    <View style={{ marginTop: 10, marginLeft: 7 }}>
                      <NumericInput
                        value={this.state.externalGuests}
                        onChange={externalGuests => this.setState({ externalGuests })}
                        onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                        minValue={0}
                        maxValue={10}
                        totalWidth={120}
                        totalHeight={40}
                        iconSize={16}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#000000'
                        borderColor={Colors.App_Font}
                        iconStyle={{ color: 'white' }}
                        rightButtonBackgroundColor={Colors.App_Font}
                        leftButtonBackgroundColor={Colors.App_Font} />
                    </View>
                  </View>
                  </View> */}
                  
                  {/* <View style={styles.textBorderLine}></View> */}

                </View>
              } />

              <Cell title="Preference" placeholder="Preference" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label} >
                      {I18n.t('preference')}
                    </Text>
                    <View>
                      <TextInput
                        underlineColorAndroid="transparent"
                        autoCapitalize='none'
                        placeholderTextColor={Colors.PlaceholderText}
                        style={styles.textInput}
                        placeholder={I18n.t('preference')}
                        onChangeText={(preferenceNote) => this.setState({ preferenceNote })}
                        value={this.state.preferenceNote} />
                    </View>
                  </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } />

              <Cell title="Allergies" placeholder="Allergies" id="" cellContentView={
                <View style={{ flex: 1, paddingTop: 10 }}>
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      {I18n.t('allergies')}
                    </Text>
                    <View>
                      <TextInput
                        underlineColorAndroid="transparent"
                        autoCapitalize='none'
                        placeholderTextColor={Colors.PlaceholderText}
                        style={styles.textInput}
                        placeholder={I18n.t('pcspecifyallergies')}
                        onChangeText={(allergyNote) => this.setState({ allergyNote })}
                        value={this.state.allergyNote} />
                    </View>
                  </View>
                  <View style={styles.textBorderLine}></View>
                </View>
              } />
<Cell title="Disclaimer" hideSeparator={true} placeholder="Disclaimer" id="" cellContentView={
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingVertical: 20, }}>
                                    {/* <View style={{ flex: 1, alignItems: "flex-start" }}> */}
                                    <View>
                                        <TouchableOpacity onPress={() => this.onPressAgree()} style={{ paddingHorizontal: 10 }}>
                                            <Icon name={this.state.isSelectedSpaDisclaimer ? "checkbox-active" : "checkbox-passive"} size={18} color={Colors.Black} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexWrap: 'wrap' }}>
                                        <Text style={styles.AgreeText}>{I18n.t('agreebikedisclaimer')}</Text>
                                    </View>
                                    {/* <TouchableOpacity activeOpacity={.5} style={{ paddingHorizontal: 5 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeIndividualDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('individualDisclaimer')}</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.AgreeText}> or </Text>
                                    <TouchableOpacity activeOpacity={.5} style={{ paddingLeft: 40, paddingVertical:10 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeGroupDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('groupbikeDisclaimer')}</Text>
                                    </TouchableOpacity> */}
                                </View>
                            } />
                        <Cell title="See Cancellation Policy" hideSeparator={true} placeholder="See Cancellation Policy" id="7" cellContentView={
                            <View style={{ flex: 1, height: 50, justifyContent: "center", alignItems: "center" }}>
                                <TouchableOpacity activeOpacity={.5} onPress={this.handleSpaCancelPolicy}>
                                    <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('cancellaitionpolicy')}</Text>
                                </TouchableOpacity>
                            </View>
                        } />
              <Cell title="Book" placeholder="Book" id="" cellContentView={
                <View style={{ flex: 1, marginTop: 40, justifyContent: 'center' }}>
                  <View style={{ width: "90%", marginLeft: 20 }}>
                    <View style={{ alignItems: 'center' }}>
                      <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressRestaurantBooking}>
                        <Text style={styles.btntext}>{I18n.t('bookbtn')}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              } />
              {/* <Cell title="Book" placeholder="Book" id="" cellContentView={
                            <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressAddToCart}>
                                            <Text style={styles.btntext}>Add to Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } />
                        <Cell title="Book" placeholder="Book" id="" cellContentView={
                            <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button1} onPress={this._onPressViewCart}>
                                            <Text style={styles.btntext}>View Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } /> */}


              {/* <Cell title="View My Bookings" hideSeparator={true} placeholder="View My Bookings" id="7" cellContentView={
                <View style={{ flex: 1, height: 70, justifyContent: "center", alignItems: "center", marginBottom: 25 }}>
                  <TouchableOpacity activeOpacity={.5} onPress={this._onPressBookingList}>
                    <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('viewbookings')}</Text>
                  </TouchableOpacity>
                </View>
              } /> */}
            </Section>
            {
                        this.state.spaCancelPolicyVisible ?
                            <Policy policyVisibility={this.state.spaCancelPolicyVisible}
                                policyType={Status.policyTypes.restaurantCancellationPolicy.typeId}
                                bookPageIndex={1}
                                callbackPolicy={this.setSpaCancelPolicyVisibility} />
                            : null
                    }
          </TableView>
        </ScrollView>
      </View>

    );
  }
  viewMenu = async() =>{
    console.log('menupath', this.state.menupath)
    //Actions.FoodMenus();
    if(this.state.menupath==''){
      Alert.alert('Please Choose Any Restaurant!');
    }else{
      await Linking.openURL(this.state.menupath);
    }
  }
  onPressAgree = () => {
    console.log("isSelected", this.state.isSelectedSpaDisclaimer)
    this.setState({ isSelectedSpaDisclaimer: !this.state.isSelectedSpaDisclaimer })
}
handleSpaCancelPolicy = async () => {
    await this.setState({ spaCancelPolicyVisible: true }, () => console.log("spacancelPolicyVisible", this.state.spaCancelPolicyVisible))
}

setSpaCancelPolicyVisibility = async (data) => {
    console.log('setAboutVisibility', data)
    await this.setState({ spaCancelPolicyVisible: data });
}
_onPressAddToCart = async () => {
  Alert.alert('Package Added to cart')
}
_onPressViewCart = async () => {
  let passingDetails=[{
      // spaBookingDate: this.state.spaBookingDate,
      // selectedSpaPackage: this.state.selectedSpaPackage,
      // selectedTimeSlot: this.state.selectedTimeSlot,
      // selectedVisitor: this.state.selectedVisitor,
      // selectedSpaCategory: this.state.selectedSpaCategory,
      // spaPackageId: this.state.spaPackageId,
      // spaVisitorId: this.state.spaVisitorId,
      // spaCategoryId: this.state.spaCategoryId,
      // preferenceNote: this.state.preferenceNote,
      // userId: this.state.userId,
      // selectthepackage: this.state.selectthepackage,
      // Amount: this.state.Amount,
      // spaTime: this.state.spaTime,
      // selectedPackageType: this.state.selectedPackageType,
      // selectedDuration: this.state.selectedDuration,
      // therapist: this.state.therapist,
      // internalGuests:this.state.internalGuests,
      // externalGuests:this.state.externalGuests
  }]
  Actions.BookingCart({'PassingDetails':passingDetails});
}
  _onBtnSheetRestaurant = async () => {
    console.log("this.state.myArray", this.state.restaurantArray);
    console.log("this.state.totalData", this.state.totalData);

    // var restaurantArrayLen = this.state.restaurantArray.length;
    BottomSheet.showBottomSheetWithOptions({
      // title: "Choose Restaurant",
      options: this.state.restaurantArray,
      dark: false,
      cancelButtonIndex: 20,
      // destructiveButtonIndex: 1,
    }, (value) => {
      console.log("value==", value);
      console.log("this.state.totalData.value", this.state.totalData);
      this.setState({
        "selectedRestaurant": this.state.totalData[value].RestaurantName,
        "restaurantId": this.state.totalData[value].RestaurantId,
        "menupath": this.state.totalData[value].MenuPath
      })
      console.log("restaurantId", this.state.restaurantId);
      console.log("selectedRestaurant", this.state.selectedRestaurant);
      console.log("menupath", this.state.menupath);
    });
  }

  // _onBtnSheetTimeSlot = async () => {
  //   console.log("this.state.myArray", this.state.timeslotArray);
  //   console.log("this.state.totalData", this.state.totalData);

  //   // var restaurantArrayLen = this.state.restaurantArray.length;
  //   BottomSheet.showBottomSheetWithOptions({
  //     //  title: "Choose Restaurant",
  //     options: this.state.timeslotArray,
  //     dark: false,
  //     cancelButtonIndex: 12,
  //     // destructiveButtonIndex: 1,
  //   }, (value) => {
  //     // if (value >= this.state.totalData.length) {  
  //     //   console.log("array",this.state.myArray)
  //     //   return;
  //     // }
  //     // else {
  //     console.log("value", value);
  //     console.log("this.state.totalData.value", this.state.totalData[1][value].value);
  //     console.log("this.state.totalData.value", this.state.totalData[1][value].id);
  //     this.setState({
  //       "selectedTimeSlot": this.state.totalData[1][value].value,
  //       "timeslotId": this.state.totalData[1][value].id
  //     })
  //     console.log("timeslotId", this.state.timeslotId);
  //     console.log("selectedTimeSlot", this.state.selectedTimeSlot);
  //   });
  // }

  // _onBtnSheetNoofPeople = async () => {
  //   console.log("this.state.myArray", this.state.noofPeopleArray);

  //   // var restaurantArrayLen = this.state.restaurantArray.length;
  //   BottomSheet.showBottomSheetWithOptions({
  //     // title: "Choose Restaurant",
  //     options: this.state.noofPeopleArray,
  //     dark: false,
  //     cancelButtonIndex: 12,
  //     // destructiveButtonIndex: 1,
  //   }, (value) => {
  //     // if (value >= this.state.totalData.length) {  
  //     //   console.log("array",this.state.myArray)
  //     //   return;
  //     // }
  //     // else {
  //     this.setState({
  //       "numberofPeople": value
  //     })
  //     console.log("value", value);

  //   });
  // }

  _onPressBookingList = async () => {
    // Actions.ListRestaurantBookings();
    Actions.BookingsList({ BookingListIndex: 0 });
  }

  _onPressRestaurantBooking = async () => {
    //this.setState({ loading: false }
    console.log("Booking Date", this.state.bookingDate);
    console.log("Booking Time", this.state.bookingTime);
    console.log("No of People", this.state.noofPeople);
    var HotelId = await AsyncStorage.getItem('HotelId');
    console.log("HotelId...",HotelId)
    const InCustomer = await AsyncStorage.getItem(Keys.inCustomer);
        console.log("InCustomer", InCustomer);
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        console.log("roomNo", roomNo);
        if (InCustomer == "True") {
            let RoomNo=roomNo
          } else {
            let RoomNo=""
          }
    if (this.state.restaurantId == "") {
      Alert.alert(I18n.t("restaurantchoose"));
    } else {
      if (this.state.bookingDate == "") {
        Alert.alert(I18n.t('chooseDate'));
      } else {
        if (this.state.bookingTime == "") {
          Alert.alert(I18n.t('selectTime'));
        } else {
          if (this.state.internalGuests == "") {
            Alert.alert(I18n.t('enternoofpeople'));
          } else {
            
            if (!this.state.isSelectedSpaDisclaimer) {
              Alert.alert(I18n.t('agreedisclaimer'));
              return false;
          }
            var res = this.state.bookingDate.split("/");
            var bookingdt=res[2]+'-'+res[1]+'-'+res[0];
            var bookingdatetime=bookingdt+' '+this.state.bookingTime;
            //var passingdatetime=Moment(bookingdatetime).format('YYYY-MM-DD HH:mm')
            console.log("bookingdatetime", bookingdatetime);
            //console.log("passingdatetime", passingdatetime);
            this.setState({ loading: true })

          var noofpeops = this.state.internalGuests + this.state.externalGuests
            let overalldetails = {
              "RestaurantId": this.state.restaurantId,
              "BookingDateTime": bookingdatetime,
              "NumberOfPeople": noofpeops,
              "InternalGuest":this.state.internalGuests,
              "Externalguest": 0,
              "Preference": this.state.preferenceNote,
              "AnyAllergies": this.state.allergyNote,
              "UserId": this.state.userId,
              "CreatedBy":this.state.userId,
              "ApprovalStatusId":"1",  
              "Remarks":"", 
              "UserType":"0",
              "HotelId":HotelId,
              "RoomNo":roomNo
            }
            console.log('overalldetails', overalldetails)
            await apiCallWithUrl(APIConstants.PostRestaurantBooking, 'POST', overalldetails, this.postRestaurantResponse);
          }
        }
      }
    }
  }

  postRestaurantResponse = async (response) => {
    if (Platform.OS === 'ios') {
      this.setState({ loading: false }, () => {
        setTimeout(() => this.funcRestaurantResponse(response), 1000);
      });
    } else {
      this.setState({
        loading: false,
      }, () => this.funcRestaurantResponse(response));
    }

  }

  funcRestaurantResponse = (response) => {
    console.log("response..",response)
    if (response.IsException == null) {
      console.log("entered1")
      // if (response.ResponseData.length > 0) {
        console.log("entered2")
        var postRestaurantResponse = response.ResponseData;
        console.log('postUserResponse', postRestaurantResponse);

        //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
        Alert.alert(I18n.t('alertrestaurantsuccess'))
        // Actions.BookingsList();
        Actions.pop();
        setTimeout(() => {
          Actions.refresh({ key: Math.random() * 1000000 })
    }, 5)
        // Actions.refresh({ key: Math.random() * 1000000 })
      // }

    }
  }


  _loadDropdowns = async () => {
    await this.setState({ loading: true });
    const details = await AsyncStorage.getItem(Keys.UserDetail);
    var jsonValue = JSON.parse(details);
    const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    await this.setState({ userId: jsonValue.UserId });
    var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        

        //await apiCallWithUrl(APIConstants.GetRestaurantBookingsList + "?id=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        let overalldetails = {
            "HotelId": HotelId
          }
          await apiCallWithUrl(APIConstants.GetRestaurantMasters, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });

    //await apiCallWithUrl(APIConstants.GetRestaurantMasters + "?CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
    // await apiCallWithUrl(APIConstants.GetRestaurantMasters + "?CheckInId=" + checkInVal, 'GET', '', this.getDropdownsResponse);
  }

  postCheckInDetailsResponse = async (response) => {
    // var postCheckInDetailResponse = response.ResponseData;
    // if (response.IsException == "True") {
    //   return Alert.alert(response.ExceptionMessage);
    // }
    const timeElapsed = Date.now();
    const today = new Date(timeElapsed);
    var currentdate=today.toISOString();
    this.getDropdownsResponse(response);
    
    //this.setState({ minBookingDate: response.currentDate, maxBookingDate: response.checkOutDate })
    this.setState({ minBookingDate: currentdate, maxBookingDate: Moment().add(60, 'days').calendar()  })
    console.log("postCheckInResponse", response.InCustomer);
    // if (response.InCustomer == "False") {
    //   console.log("postCheckInResponse Language", response.InCustomer);
    //   if (Platform.OS === 'ios') {
    //     console.log("ios postCheckInResponse")
    //     this.setState({ loading: false }, () => {
    //       setTimeout(() => this.funcCheckOutResponse(response), 1000);
    //     });
    //   } else {
    //     console.log("android postCheckInResponse")
    //     this.setState({
    //       loading: false,
    //     }, () => this.funcCheckOutResponse(response));
    //   }

    // } else {
    //   console.log("currentDate", response.currentDate, "checkOutDate:", response.checkOutDate)
    //   //this.setState({ minBookingDate: response.currentDate, maxBookingDate: response.checkOutDate })
    // }
  }

  funcCheckOutResponse = async (response) => {
    Alert.alert(I18n.t('alertnotcheckin'));
    AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    AsyncStorage.removeItem(Keys.UserDetail);
    AsyncStorage.removeItem(Keys.roomNo);
    AsyncStorage.removeItem(Keys.inCustomer);
    AsyncStorage.removeItem(Keys.checkInId);
    Actions.GuestLogin();
  }


  getDropdownsResponse = async (response) => {
    // this.setState({ totalData: response.ResponseData, "selctedLanguage": response.ResponseData[0].NativeName, })
    await this.setState({ loading: false, totalData: response.ResponseData })

    console.log("totalResponse", response.ResponseData);
    console.log("haaa...", response.ResponseData[0]);
    var list1 = [];
    response.ResponseData.map((restaurants, index) => {
      console.log("loop", index, restaurants.RestaurantName);
      list1.push(restaurants.RestaurantName);

    });
    this.setState({ restaurantArray: list1 })

    console.log("restaurantArray", this.state.restaurantArray);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    // flexDirection: 'column'
  },
  label: {
    marginTop: 10,
    marginLeft: 8,
    marginRight: 8,
    fontSize: 13,
    fontFamily: AppFont.Regular,
    color: Colors.Black
  },
  textInput: {
    textAlign: 'left',
    fontFamily: AppFont.Regular,
    height: 35,
    fontSize: 15,
    padding: 0,
    marginLeft: 8,
    includeFontPadding: false,
  },
  textBorderLine: {
    marginLeft: 8,
    marginRight: 8,
    borderBottomColor: Colors.App_Font,
    borderBottomWidth: 0.8
  },
  picker: {
    color: Colors.PlaceholderText,
    marginLeft: 8,
    height: 28,
    padding: 0,
    textAlign: 'left',
    fontFamily: AppFont.Regular,
    fontSize: 15,
  },
  button: {
    alignItems: 'center',
    backgroundColor: Colors.App_Font,
    padding: 10,
    width: "100%",
    marginLeft: 8,
    marginRight: 8,
    borderRadius: 5
},
button1: {
    alignItems: 'center',
    backgroundColor: Colors.ButtonBlue,
    padding: 10,
    width: "100%",
    marginLeft: 8,
    marginRight: 8,
    borderRadius: 5
},
  btntext: {
    color: "#FFF",
    fontSize: 18,
    letterSpacing: 1,
    fontFamily: AppFont.Regular,
  },
});

module.exports = RestaurantBooking;