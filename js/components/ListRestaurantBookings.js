import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    Alert,
    RefreshControl,
    Dimensions,
    Platform,
    StatusBar
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Status from '../constants/Status';
import CancellationPolicy from '../components/CancellationPolicy';
import Loader from '../components/Loader';
//import Policy from '../components/Policy';
import Moment from 'moment';
import { apiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';

class Booking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurantListArray: [],
            cancelPolicyVisible: false,
            listId: 0,
            bookingTime: '',
            isRefreshing: false,
            loading: false,
        }
    }

    componentDidMount() {
        this._loadRestaurantBookings();
        // this.verifyCheckInDetails();
    }
    

    render() {
        return (
            <View>
                
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0, marginBottom: 35 }} refreshControl={
                    <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={this._onRefreshRestaurant}
                    />
                }>
                    <Loader loading={this.state.loading} />
                    {this.state.restaurantListArray.length > 0 ?
                        this.state.restaurantListArray.map((item, index) => {
                            return <View key={index} style={styles.boxWithShadow} >
                                <View style={styles.Restaurant}>
                                    <View style={{ alignItems: "flex-start", flex: 0.7, }}>
                                        <Text style={styles.IconReqText}>{item.RestaurantName}</Text>
                                    </View>
                                    <View style={{ alignItems: "flex-end", flex: 0.3, }}>
                                        <Text style={[styles.IconReqTextColor, { color: this.StatusColors(item.ApprovalStatusName) }]}>{item.ApprovalStatusName}</Text>
                                    </View>
                                </View>

                                <View style={styles.Booking}>
                                    <View style={[styles.BookingList, { borderLeftWidth: 0, borderRightWidth: 0, }]}>
                                        <Text style={styles.IconText}>{Moment(item.BookingDateTime).format('lll')}</Text>
                                        <Text style={styles.IconSubText}>{I18n.t('date')}</Text>
                                    </View>
                                    <View style={styles.BookingList}>
                                        <Text style={styles.IconText}>{item.NumberOfPeople}</Text>
                                        <Text style={styles.IconSubText}>{I18n.t('guests')}</Text>
                                    </View>
                                </View>

                                <View style={styles.DescriptionResList}>
                                    <View style={{ flex: 0.2 }}>
                                        <Image source={{ uri: 'table' }} style={{ width: 48, height: 48 }} />
                                    </View>
                                    <View style={{ flex: 0.7 }}>
                                        <View style={{ flex: 1, flexDirection: "row" }}>
                                            <View style={{ flex: 0.1 }}><Image source={{ uri: 'tick' }} style={{ width: 15, height: 15 }} /></View>
                                            <View style={{ flex: 0.9 }}>
                                                {item.Preference != "" ? <Text style={styles.DesText}>{item.Preference}</Text>
                                                    : <Text style={styles.DesText}>{I18n.t('nil')}</Text>}
                                            </View>
                                        </View>

                                        <View style={{ flex: 1, flexDirection: "row" }}>
                                            <View style={{ flex: 0.1 }}><Image source={{ uri: 'tick' }} style={{ width: 15, height: 15 }} /></View>
                                            <View style={{ flex: 0.9 }}>
                                                {item.AnyAllergies != "" ?
                                                    <Text style={styles.DesText}>{item.AnyAllergies}</Text>
                                                    : <Text style={styles.DesText}>{I18n.t('nil')}</Text>}
                                            </View>
                                        </View>
                                    </View>
                                    {
                                        item.ApprovalStatusName == Status.BookingStatusTypes.requested.name  || item.ApprovalStatusName == Status.BookingStatusTypes.confirmed.name ?
                                            <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
                                                <View >
                                                    {/* <TouchableOpacity activeOpacity={.5} onPress={() => this.handleCancelPolicy(item,item.RestarurantBookingId)} style={{ backgroundColor: Colors.App_Font, borderRadius: 20, padding: 6, paddingRight: 10, paddingLeft: 25 }}>
                                                        {/* <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                                        <Text style={styles.CloseText}> Cancel</Text>
                                                        <Image source={require('../../assets/images/icon_cancel.png') } style={{width: 60, height: 20}} />
                                                    </TouchableOpacity> */}
                                                    <TouchableOpacity activeOpacity={.5} onPress={() => this.handleCancelPolicy(item,item.RestarurantBookingId)}>
                                                        {/* <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                                        <Text style={styles.CloseText}> Cancel</Text> */}
                                                        <Image source={require('../../assets/images/icon_cancel.png') } style={{width: 90, height: 30}} />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            :
                                            <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
                                                {/* <View >
                                                    <TouchableOpacity activeOpacity={.5} onPress={() => this.handleCannotCancelPolicy(item.ApprovalStatusName)} style={{ backgroundColor: Colors.App_Font, borderRadius: 20, padding: 6, paddingRight: 10, paddingLeft: 25 }}>
                                                        <Image source={{ uri: 'close' }} style={{ width: 20, height: 20, backgroundColor: Colors.Background_Color, borderRadius: 10, top: 2, left: 2, position: "absolute" }} />
                                                        <Text style={styles.CloseText}> Cancel</Text>
                                                    </TouchableOpacity>
                                                </View> */}
                                            </View>
                                    }
                                
                                </View>
                                {item.ApprovalStatusName == 'Completed' ?
                                <View style={[styles.DescriptionResList, { borderTopWidth: 0.5,borderColor: Colors.Background_Gray }]}>
                                {item.IsGivenReview == 0 ?
                                    <View style={{ alignItems: 'center', flex: 1}}>
                                    <TouchableOpacity activeOpacity={.5}  onPress={() => this.feedbackRate(item.RestarurantBookingId)}>
                                        <Text style={styles.IconReqText}>{I18n.t('please_rate_your_experience')}</Text>
                                    </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={{ alignItems: 'center', flex: 1}}>
                                        <Text style={styles.IconReqText}>{I18n.t('thanks_for_rating')}</Text>
                                    </View>
                        }
                                </View> :
                                <View></View>
                    }
                            </View>
                        })

                        :
                        <View style={{ flex: 1, backgroundColor: "#FFFFFF", }}>
                            <View style={{ flex: 1, textAlign: 'center', alignItems: 'center', justifyContent: 'center', width: Dimensions.get('window').width, height: Dimensions.get('window').height - 180, }}>
                                <Text style={{ fontFamily: AppFont.Regular }}>{I18n.t('to_make_booking_please_click_plus')}</Text>
                            </View>
                        </View>
                    }
                </ScrollView>
                {/* {
                    this.state.cancelPolicyVisible ?
                        <CancellationPolicy cancelPolicyVisibility={this.state.cancelPolicyVisible} callbackCancelPolicyVisibility={this.setCancelPolicyVisibility} />
                        : null
                } */}
                {/* {
                    this.state.cancelPolicyVisible ?
                        <Policy policyVisibility={this.state.cancelPolicyVisible} policyType={Status.policyTypes.restaurantCancellationPolicy.typeId} listId = {this.state.listId} callbackPolicy={this.setCancelPolicyVisibility} />
                        : null
                } */}
                {
                    this.state.cancelPolicyVisible ?
                        <CancellationPolicy policyVisibility={this.state.cancelPolicyVisible}
                            bookingType={Status.bookingTypes.restaurant.typeId}
                            policyType={Status.policyTypes.restaurantCancellationPolicy.typeId}
                            listId={this.state.listId}
                            bookListIndex={0}
                            callbackPolicy={this.setCancelPolicyVisibility} />
                        : null
                }
                        
 
            </View>
        )
    }

    _onRefreshRestaurant = () => {
        this.setState({ isRefreshing: true })
        this._loadRestaurantBookings();
    }

    StatusColors(item) {
        var x = item;
        var textbgColor = "";
        if (x == Status.BookingStatusTypes.requested.name) {
            textbgColor = Status.BookingStatusTypes.requested.textColor;
        }
        else if (x == Status.BookingStatusTypes.confirmed.name) {
            textbgColor = Status.BookingStatusTypes.confirmed.textColor;
        }
        else if (x == Status.BookingStatusTypes.rejected.name) {
            textbgColor = Status.BookingStatusTypes.rejected.textColor;
        }

        return textbgColor;
    }

    // handleCancelPolicy = async () => {
    //     await this.setState({ cancelPolicyVisible: true }, () => console.log("cancelPolicyVisible", this.state.cancelPolicyVisible))
    // }

    feedbackRate = (itemId) => {
        console.log("itemid",itemId);
        Actions.BookingsRatings({"ModuleId":1,"OrderId":itemId});
    }

    handleCancelPolicy = async (item,itemId) => {
        console.log("Item:", item);
        console.log("Index no:::", itemId);
        var now = Moment(new Date()); //todays date
        var end = Moment(item.BookingDateTime); // another date
        var duration = Moment.duration(end.diff(now));
        var days = duration.asMinutes();
        console.log('diff...',days)
        if(days>120){
            await this.setState({ cancelPolicyVisible: true, listId: itemId });
        }else{
            Alert.alert(I18n.t('cancellation_is_allowed_only_before'));
        }
        //await this.setState({ cancelPolicyVisible: true, listId: itemId });

        //await this.setState({ });
    }

    handleCannotCancelPolicy = async (statusName) => {

        console.log("Index no:::", statusName);
        Alert.alert(statusName + I18n.t('alertrequestnotcancel'));
    }

    setCancelPolicyVisibility = async (data) => {
        console.log('setAboutVisibility', data)
        await this.setState({ cancelPolicyVisible: data });
    }

    _loadRestaurantBookings = async () => {
        this.setState({ loading: true })
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        await this.setState({ userId: userJson.UserId });

        //await apiCallWithUrl(APIConstants.GetRestaurantBookingsList + "?id=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        let overalldetails = {
            "HotelId": HotelId,
            "UserId": this.state.userId,
            "LanguageId": this.state.languageId
          }
          await apiCallWithUrl(APIConstants.GetRestaurantBookingsList, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getRestaurantBookingsResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }
    
     formatDate (date){
        var date = new Date(date);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return date.getDate() + "/" + (date.getMonth()+1)  + "/" + date.getFullYear() + " " + strTime;
      }
      
    //   var d = new Date();
    //   var e = formatDate(d);
      
    //   alert(e);
    getRestaurantBookingsResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response != null) {
            var getrestaurantListResponse = response.ResponseData;

            console.log('getuserResponse', getrestaurantListResponse);

            await this.setState({ restaurantListArray: getrestaurantListResponse, isRefreshing: false });

            console.log('getuserResponse', this.state.restaurantListArray.length);

        }
    }

}

const styles = StyleSheet.create({

    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    Booking: {
        flex: 1,
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: Colors.LightBlue,
    },
    BookingList: {
        width: "50%",
        height: 60,
        borderColor: Colors.App_Font,
        padding:10,
        alignItems: 'center',
        borderWidth: 0.5,
        justifyContent: 'center'
    },
    IconText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Bold,
        textTransform: "uppercase",
        textAlign: 'center',
    },
    IconSubText: {
        color: Colors.DarkGray,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        textTransform: "uppercase",
        textAlign: 'center',
        marginTop: 5
    },
    Restaurant: {
        flexDirection: "row",
        padding: 10
    },
    IconReqText: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        paddingVertical: 3
    },
    IconReqTextColor: {
        color: Colors.Orange,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        textTransform: "uppercase",
        paddingVertical: 3
    },
    DescriptionResList: {
        flexDirection: "row",
        padding:10
    },
    DesText: {
        color: Colors.DarkGray,
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    CloseText: {
        fontSize: 9,
        color: Colors.Background_Color,
        alignItems: 'center',
        justifyContent: "center",
        textTransform: "uppercase",
        fontFamily: AppFont.Regular,
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: "#ffffff",
        marginHorizontal: 12,
        marginTop: 14,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.4,
    },
    
})

module.exports = Booking;