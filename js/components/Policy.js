import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Dimensions,
    ScrollView,
    Image
} from 'react-native';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';

import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import I18n from '../constants/i18n';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const SCREEN_HEIGHT = Dimensions.get('window').height;


export default class Policy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            policyVisible: this.props.policyVisibility,
            typeId: this.props.policyType,
            policy: {}
        }
        console.log('props', this.props.policyVisibility)
        this.closeDialog = this.closeDialog.bind(this);
    }
    componentWillMount() {
        this._onLoadPolicy();
    }
    async closeDialog() {
        console.log('hit close')
        await this.setState({ policyVisible: false });
        this.props.callbackPolicy(this.state.policyVisible);

       // Actions.refresh({ key: Math.random() * 1000000,BookingIndex: this.props.bookPageIndex  })
    }
    render() {
        return (
            <Dialog
                visible={this.state.policyVisible}
                overlayBackgroundColor='#000'
                dialogStyle={{
                    backgroundColor: 'transparent',
                    height: 'auto',
                    width: '100%',
                    display:'flex',
                }}
                dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                onTouchOutside={this.closeDialog}
            >
                <DialogContent>
                    <View>
                        {this.state.policy.Contents != "" ?

                            <View style={{ marginTop: '5%', padding: '0%', borderRadius: 10, backgroundColor: 'white', height: 'auto' }}>
                                    <View style={styles.CloseBtn}>
                                        <TouchableOpacity onPress={this.closeDialog}>
                                            <Image source={{ uri: 'sclose' }} style={{ width: 28, height: 28, right: 0 }} />
                                        </TouchableOpacity>
                                    </View>
                                <ScrollView>
                                    <View style={styles.PopHeader}>
                                            <HTML
                                            html={this.state.policy.PolicyTitle}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                            style={{ color: "#FFFFFF", fontFamily: AppFont.Regular, fontSize: 18, textAlign: 'center', textTransform: 'uppercase' }} />
                                    </View>
                                    <View style={styles.PopDescription}>
                                        <HTML
                                            html={this.state.policy.Contents}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                            style={{ paddingBottom: 20, fontFamily: AppFont.Light, color: '#717171' }} />
                                    </View>
                                    <View style={{ alignItems: 'center',  paddingBottom: 25 }}>
                                        <View style={{ width: "35%" }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.closeDialog}>
                                                <Text style={styles.btntext}>{I18n.t('ok')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                            : null
                        }

                    </View>
                </DialogContent>
            </Dialog>
        );
    }

    _onLoadPolicy = async () => {
        const langId = await AsyncStorage.getItem(Keys.langId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId":langId,
            "PolicyTypeId":this.state.typeId
          }
          await apiCallWithUrl(APIConstants.GetPolicyAPI, 'POST', overalldetails, (callback) => { this.getPolicyResponse(callback) });
        // apiCallWithUrl(APIConstants.GetPolicyAPI + "?LanguageId=" + langId + "&&PolicyTypeId=" + this.state.typeId, 'GET', "", this.getPolicyResponse)
    }

    getPolicyResponse = async (response) => {
        if (response.IsException == null) {
            var policyResponse = response.ResponseData;
            console.log('policy response', policyResponse[0].Contents)

            await this.setState({
                policy: policyResponse === null ? '' : policyResponse[0]
            })

            console.log('state disclaimer', this.state.policy)
        }
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        borderRadius:5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    PopHeader: {
        backgroundColor: Colors.App_Font,
        alignItems: 'center',
        paddingVertical: 20,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    PopDescription: {
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    CloseBtn: {
        position: 'absolute',
        right: -10,
        top: -10,
        zIndex: 1
    },
})