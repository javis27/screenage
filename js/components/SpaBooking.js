import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    RefreshControl,
    Platform
} from 'react-native';

import {
    Cell,
    TableView,
    Section

} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import BottomSheet from 'react-native-bottomsheet';
import Moment from 'moment';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import Icon from 'react-native-vector-icons/Fontisto';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import Status from '../constants/Status';

import NumericInput from 'react-native-numeric-input';
// import CancellationPolicy from '../components/CancellationPolicy';
import Policy from '../components/Policy';

import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';
class SpaBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            spaBookingDate: '',
            selectedSpaPackage: I18n.t('choosespa'),
            selectedTimeSlot: I18n.t('choosetimeslot'),
            // selectedVisitor: I18n.t('chooseguest'),
            selectedVisitor: '',
            selectedSpaCategory: I18n.t('choosespacategory'),
            spaPackageArray: [],
            spaTimeslotArray: [],
            spaVisitorArray: [],
            spaCategoryArray: [],
            spaPackageId: '',
            // spaTimeslotId: '',
            spaVisitorId: 1,
            spaCategoryId: '',
            preferenceNote: '',
            userId: '',
            selectthepackage: 'Select the Package',
            displayAmountState: false,
            selectedColor1: Colors.App_Font,
            selectedColor2: Colors.FullGray,
            selectedFontColor1: "#FFFFFF",
            selectedFontColor2: Colors.Black,
            Amount: '',
            spaloading: false,
            spaCancelPolicyVisible: false,
            spaTime: '',
            refreshing: false,
            minBookingDate: '',
            maxBookingDate: '',
            selectedPackageType: 0,
            selectedDuration: '',
            therapist: '',
            isSelectedSpaDisclaimer:false,
            internalGuests:0,
            externalGuests:0
        };
    }

    async componentDidMount() {
        await firebase.app();
     analytics().logScreenView({
        screen_name: 'SpaBookingcartScreen',
        screen_class: 'SpaBookingcartScreen'
    });
        this._loadDropdowns();
        // this.verifyCheckInDetails();
        console.log("seleee..", this.props.selectedVals);
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ spaloading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     console.log("postCheckInResponse Language", response);

    //     if (Platform.OS === 'ios') {
    //         this.setState({ spaloading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             spaloading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this.setState({ minBookingDate: postCheckInDetailResponse.currentDate, maxBookingDate: postCheckInDetailResponse.checkOutDate })
    //             // this._loadDropdowns();
    //         }
    //     }
    // }

    render() {
        return (
            <ScrollView style={styles.container} refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onSpaRefresh}
                />
            }>
                <TableView>
                    <Loader loading={this.state.spaloading} />
                    <Section sectionPaddingTop={0} sectionPaddingBottom={0} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                        <Cell title="Date" placeholder="Date" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label} >
                                        {I18n.t('date')}
                                    </Text>
                                    <View>
                                        <DatePicker
                                            style={{ width: null, marginLeft: 8, }}
                                            date={this.state.spaBookingDate}
                                            mode="date"
                                            placeholder={I18n.t('date')}
                                            // height={20}
                                            // format="LL LT"
                                            format="DD/MM/YYYY"
                                            minDate={new Date(this.state.minBookingDate)}
                                            maxDate={new Date(this.state.maxBookingDate)}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            iconSource={{ uri: 'date' }}
                                            customStyles={{
                                                dateInput: DatePickerAttributes.dateInput,
                                                dateIcon: DatePickerAttributes.dateIcon,
                                                placeholderText: DatePickerAttributes.placeholderText,
                                                dateText: DatePickerAttributes.dateText,
                                                btnTextCancel: DatePickerAttributes.btnTextCancel,
                                                btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                                            }}
                                            onDateChange={(date) => {
                                                this.setState({
                                                    spaBookingDate: date,
                                                })
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="Spa Category" placeholder="Spa Category" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('spacategory')}
                                    </Text>
                                    <View>
                                        <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheetSpaCategory}>
                                            <Text style={styles.picker}>{this.state.selectedSpaCategory}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="SPA Package" placeholder="SPA Package" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('spapackage')}
                                    </Text>

                                    <View>
                                        <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onPressSpaDropdown}>
                                            <Text style={styles.picker}>{this.state.selectedSpaPackage}</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>
                                <View style={styles.textBorderLine}></View>
                                {this.state.selectedAmount1 || this.state.selectedAmount2 ?
                                    <View style={{ flex: 1, paddingHorizontal: 10 }}><Text style={{ color: Colors.Black, marginTop: 15, fontSize: 13, fontFamily: AppFont.Regular }}>{I18n.t('selectpackage')}</Text></View>
                                    : null}

                                {(() => {
                                    if (this.state.selectedAmount1 && this.state.selectedAmount2) {
                                        return <View style={styles.DescriptionServiceList}>
                                            <View style={{ flex: 0.5, paddingVertical: 7, paddingRight: 5 }}>
                                                <View style={{ flex: 1, flexDirection: "row" }}>
                                                    <TouchableOpacity style={{
                                                        backgroundColor: this.state.selectedColor1,
                                                        borderRadius: 5,
                                                        alignItems: "center",
                                                        justifyContent: "center",
                                                    }} activeOpacity={.5} onPress={this.displayAmount.bind(this, this.state.selectedAmount1)}>
                                                        <Text style={{ paddingHorizontal: 15, paddingVertical: 7, color: this.state.selectedFontColor1, fontSize: 13, fontFamily: AppFont.Regular }}>Solo - MUR {this.state.selectedAmount1}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>

                                            <View style={{ flex: 0.5, paddingVertical: 7, paddingRight: 5 }}>
                                                <View style={{ flex: 1, flexDirection: "row" }}>
                                                    <TouchableOpacity style={{
                                                        backgroundColor: this.state.selectedColor2,
                                                        borderRadius: 5,
                                                        alignItems: "center",
                                                        justifyContent: "center",
                                                    }} activeOpacity={.5} onPress={this.displayAmount1.bind(this, this.state.selectedAmount2)}>
                                                        <Text style={{ paddingHorizontal: 15, paddingVertical: 7, color: this.state.selectedFontColor2, fontSize: 13, fontFamily: AppFont.Regular }}>Duo - MUR {this.state.selectedAmount2}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    }
                                    else if (this.state.selectedAmount1) {
                                        return <View style={styles.DescriptionServiceList}>
                                            <View style={{ flex: 0.5, paddingVertical: 7, paddingRight: 5 }}>
                                                <View style={{ flex: 1, flexDirection: "row" }}>
                                                    <TouchableOpacity style={{
                                                        backgroundColor: Colors.App_Font,
                                                        borderRadius: 5,
                                                        alignItems: "center",
                                                        justifyContent: "center",
                                                    }} activeOpacity={.5} onPress={this.displayAmount.bind(this, this.state.selectedAmount1)}>
                                                        <Text style={{ paddingHorizontal: 15, paddingVertical: 7, color: Colors.Background_Color, fontSize: 13, fontFamily: AppFont.Regular }}>Single - MUR {this.state.selectedAmount1}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    }
                                    else if (this.state.selectedAmount2) {
                                        return <View style={{ flex: 0.5, paddingVertical: 7, paddingRight: 5 }}>
                                            <View style={{ flex: 1, flexDirection: "row" }}>
                                                <TouchableOpacity style={{
                                                    backgroundColor: Colors.App_Font,
                                                    borderRadius: 5,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                }} activeOpacity={.5} onPress={this.displayAmount1.bind(this, this.state.selectedAmount2)}>
                                                    <Text style={{ paddingHorizontal: 15, paddingVertical: 7, color: Colors.Background_Color, fontSize: 13, fontFamily: AppFont.Regular }}>Duo - MUR {this.state.selectedAmount2}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    }
                                })()}

                                {/* <View style={styles.DescriptionServiceList}>
                                    {this.state.selectedAmount1 ?
                                        <View style={{ flex: 0.5, paddingVertical: 7, paddingRight: 5 }}>
                                            <View style={{ flex: 1, flexDirection: "row" }}>
                                                <TouchableOpacity style={{
                                                    backgroundColor: this.state.selectedColor1,
                                                    borderRadius: 5,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                }} activeOpacity={.5} onPress={this.displayAmount.bind(this, this.state.selectedAmount1)}>
                                                    <Text style={{ paddingHorizontal: 15, paddingVertical: 7, color: this.state.selectedFontColor1, fontSize: 13, fontFamily: AppFont.Regular }}>Single - MUR {this.state.selectedAmount1}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        : null}

                                    {this.state.selectedAmount2 ?
                                        <View style={{ flex: 0.5, paddingVertical: 7, paddingRight: 5 }}>
                                            <View style={{ flex: 1, flexDirection: "row" }}>
                                                <TouchableOpacity style={{
                                                    backgroundColor: this.state.selectedColor2,
                                                    borderRadius: 5,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                }} activeOpacity={.5} onPress={this.displayAmount1.bind(this, this.state.selectedAmount2)}>
                                                    <Text style={{ paddingHorizontal: 15, paddingVertical: 7, color: this.state.selectedFontColor2, fontSize: 13, fontFamily: AppFont.Regular }}>Duo - MUR {this.state.selectedAmount2}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        : null}

                                </View> */}
                            </View>
                        } />

                        < Cell title="SPA Time" placeholder="SPA Time" id="" cellContentView={
                            < View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label} >
                                        {I18n.t('time')}
                                    </Text>
                                    <View>
                                        {/* <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5}>
                        <Text style={styles.picker}>{this.state.bookingDate}</Text>
                      </TouchableOpacity> */}
                                        <DatePicker
                                            style={{ width: null, marginLeft: 8, }}
                                            date={this.state.spaTime}
                                            mode="time"
                                            placeholder={I18n.t('time')}
                                            // format="LL LT"
                                            format="HH:mm"
                                            // minDate="10:30"
                                            // maxDate="19:30"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            iconSource={{ uri: 'date' }}
                                            is24Hour={false}
                                            androidMode="spinner"
                                            customStyles={{
                                                dateInput: DatePickerAttributes.dateInput,
                                                dateIcon: DatePickerAttributes.dateIcon,
                                                placeholderText: DatePickerAttributes.placeholderText,
                                                dateText: DatePickerAttributes.dateText,
                                                btnTextCancel: DatePickerAttributes.btnTextCancel,
                                                btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                                            }}
                                            onDateChange={(time) => {
                                                this.setState({
                                                    spaTime: time,
                                                })
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
            <Cell title="Number of People" placeholder="Number of People" id="" cellContentView={
                <View style={{ flex: 1,flexDirection:'row', paddingTop: 10, paddingBottom: 10 }}>
                  <View style={{flex:0.5}}> 
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      {I18n.t('internal_guest_alone')}
                    </Text>
                    {/* <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetNoofPeople}>
                        <Text style={styles.picker}>{this.state.numberofPeople}</Text>
                      </TouchableOpacity>
                    </View> */}
                    <View style={{ marginTop: 10, marginLeft: 7 }}>
                      <NumericInput
                        value={this.state.internalGuests}
                        onChange={internalGuests => this.setState({ internalGuests })}
                        onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                        minValue={0}
                        maxValue={10}
                        totalWidth={120}
                        totalHeight={40}
                        iconSize={16}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#000000'
                        borderColor={Colors.App_Font}
                        iconStyle={{ color: 'white' }}
                        rightButtonBackgroundColor={Colors.App_Font}
                        leftButtonBackgroundColor={Colors.App_Font} />
                    </View>
                  </View>
                  </View>
                  <View style={{flex:0.5}}> 
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      {I18n.t('external_guest_alone')}
                    </Text>
                    {/* <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetNoofPeople}>
                        <Text style={styles.picker}>{this.state.numberofPeople}</Text>
                      </TouchableOpacity>
                    </View> */}
                    <View style={{ marginTop: 10, marginLeft: 7 }}>
                      <NumericInput
                        value={this.state.externalGuests}
                        onChange={externalGuests => this.setState({ externalGuests })}
                        onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                        minValue={0}
                        maxValue={10}
                        totalWidth={120}
                        totalHeight={40}
                        iconSize={16}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#000000'
                        borderColor={Colors.App_Font}
                        iconStyle={{ color: 'white' }}
                        rightButtonBackgroundColor={Colors.App_Font}
                        leftButtonBackgroundColor={Colors.App_Font} />
                    </View>
                  </View>
                  </View>
                  
                  {/* <View style={styles.textBorderLine}></View> */}

                </View>
              } />

                        {/* <Cell title="Time Slot" placeholder="Time Slot" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('timeslot')}
                                    </Text>
                                    <View>
                                        <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheetTimeSlot}>
                                            <Text style={styles.picker}>{this.state.selectedTimeSlot}</Text>
                                        </TouchableOpacity>
                                    </View>
                                <View style={styles.textBorderLine}></View>
                                </View>
                            </View>
                        } /> */}

                        <Cell title="Guest Name" placeholder="Guest Name" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('guestname')}
                                    </Text>
                                    <View>
                                        {/* <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheetVisitor}>
                                            <Text style={styles.picker}>{this.state.selectedVisitor}</Text>
                                        </TouchableOpacity> */}
                                        <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            placeholder={I18n.t('guestname')}
                                            // placeholder={I18n.t('preference')}
                                            onChangeText={(selectedVisitor) => this.setState({ selectedVisitor })}
                                            value={this.state.selectedVisitor} />
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="Preference" placeholder="Preference" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label} >
                                        {I18n.t('preference')}
                                    </Text>
                                    <View>
                                        <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            placeholder={I18n.t('preference')}
                                            onChangeText={(preferenceNote) => this.setState({ preferenceNote })}
                                            value={this.state.preferenceNote} />
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        {/* <Cell title="Notes" placeholder="Notes" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label} >
                                        Preferred Spa Therapist
                                    </Text>
                                    <View>
                                        <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            placeholder='Enter Preferred SPA Therapist'
                                            // placeholder={I18n.t('preference')}
                                            onChangeText={(therapist) => this.setState({ therapist })}
                                            value={this.state.therapist} />
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } /> */}
                        <Cell title="Disclaimer" hideSeparator={true} placeholder="Disclaimer" id="6" cellContentView={
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingVertical: 20, }}>
                                    {/* <View style={{ flex: 1, alignItems: "flex-start" }}> */}
                                    <View>
                                        <TouchableOpacity onPress={() => this.onPressAgree()} style={{ paddingHorizontal: 10 }}>
                                            <Icon name={this.state.isSelectedSpaDisclaimer ? "checkbox-active" : "checkbox-passive"} size={18} color={Colors.Black} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexWrap: 'wrap' }}>
                                        <Text style={styles.AgreeText}>{I18n.t('agreebikedisclaimer')}</Text>
                                    </View>
                                    {/* <TouchableOpacity activeOpacity={.5} style={{ paddingHorizontal: 5 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeIndividualDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('individualDisclaimer')}</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.AgreeText}> or </Text>
                                    <TouchableOpacity activeOpacity={.5} style={{ paddingLeft: 40, paddingVertical:10 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeGroupDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('groupbikeDisclaimer')}</Text>
                                    </TouchableOpacity> */}
                                </View>
                            } />
                        <Cell title="See Cancellation Policy" hideSeparator={true} placeholder="See Cancellation Policy" id="7" cellContentView={
                            <View style={{ flex: 1, height: 50, justifyContent: "center", alignItems: "center" }}>
                                <TouchableOpacity activeOpacity={.5} onPress={this.handleSpaCancelPolicy}>
                                    <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('cancellaitionpolicy')}</Text>
                                </TouchableOpacity>
                            </View>
                        } />
                        <Cell title="Book" placeholder="Book" id="" cellContentView={
                            <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressSpaBooking}>
                                            <Text style={styles.btntext}>{I18n.t('bookbtn')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } />
                        {/* <Cell title="Book" placeholder="Book" id="" cellContentView={
                            <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressAddToCart}>
                                            <Text style={styles.btntext}>Add to Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } />
                        <Cell title="Book" placeholder="Book" id="" cellContentView={
                            <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button1} onPress={this._onPressViewCart}>
                                            <Text style={styles.btntext}>View Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } /> */}
                        
                    </Section>
                    {
                        this.state.spaCancelPolicyVisible ?
                            <Policy policyVisibility={this.state.spaCancelPolicyVisible}
                                policyType={Status.policyTypes.spaCancellationPolicy.typeId}
                                bookPageIndex={1}
                                callbackPolicy={this.setSpaCancelPolicyVisibility} />
                            : null
                    }
                </TableView>
            </ScrollView>


        );
    }

    _onSpaRefresh = () => {
        this.setState({ refreshing: true })
        this._loadDropdowns();
    }
    onPressAgree = () => {
        console.log("isSelected", this.state.isSelectedSpaDisclaimer)
        this.setState({ isSelectedSpaDisclaimer: !this.state.isSelectedSpaDisclaimer })
    }
    _onPressSpaBookingList = async () => {
        // Actions.ListRestaurantBookings();
        Actions.BookingsList({ BookingListIndex: 1 });
    }
    _onPressAddToCart = async () => {
        Alert.alert('Package Added to cart')
    }
    _onPressViewCart = async () => {
        let passingDetails=[{
            spaBookingDate: this.state.spaBookingDate,
            selectedSpaPackage: this.state.selectedSpaPackage,
            selectedTimeSlot: this.state.selectedTimeSlot,
            selectedVisitor: this.state.selectedVisitor,
            selectedSpaCategory: this.state.selectedSpaCategory,
            spaPackageId: this.state.spaPackageId,
            spaVisitorId: this.state.spaVisitorId,
            spaCategoryId: this.state.spaCategoryId,
            preferenceNote: this.state.preferenceNote,
            userId: this.state.userId,
            selectthepackage: this.state.selectthepackage,
            Amount: this.state.Amount,
            spaTime: this.state.spaTime,
            selectedPackageType: this.state.selectedPackageType,
            selectedDuration: this.state.selectedDuration,
            therapist: this.state.therapist,
            internalGuests:this.state.internalGuests,
            externalGuests:this.state.externalGuests
        }]
        Actions.BookingCart({'PassingDetails':passingDetails});
    }

    handleSpaCancelPolicy = async () => {
        await this.setState({ spaCancelPolicyVisible: true }, () => console.log("spacancelPolicyVisible", this.state.spaCancelPolicyVisible))
    }

    setSpaCancelPolicyVisibility = async (data) => {
        console.log('setAboutVisibility', data)
        await this.setState({ spaCancelPolicyVisible: data });
    }

    // _onBtmSheetSpaPackage = async () => {
    //     console.log("this.state.myArray", this.state.spaPackageArray);
    //     console.log("this.state.totalData", this.state.totalData);

    //     BottomSheet.showBottomSheetWithOptions({
    //         // title: "Choose Restaurant",
    //         options: this.state.spaPackageArray,
    //         dark: false,
    //         cancelButtonIndex: 20,
    //         // destructiveButtonIndex: 1,
    //     }, (value) => {
    //         console.log("selectedSpaPackage", this.state.totalData[0][value].SPAPackageName);
    //         this.setState({
    //             "selectedSpaPackage": this.state.totalData[0][value].SPAPackageName,
    //             "spaPackageId": this.state.totalData[0][value].SPAPackageId
    //         })
    //         console.log("spaPackageId", this.state.spaPackageId);
    //         console.log("selectedSpaPackage", this.state.selectedSpaPackage);
    //     });
    // }

    _onBtmSheetSpaCategory = async () => {
        console.log("this.state.myArray", this.state.spaCategoryArray);
        console.log("this.state.totalData", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            // title: "Choose Restaurant",
            options: this.state.spaCategoryArray,
            dark: false,
            cancelButtonIndex: 60,
            // destructiveButtonIndex: 1,
        }, (value) => {
            console.log("selectedSpaPackage", this.state.totalData[value].SpaCategoryName);
            this.setState({
                "selectedSpaCategory": this.state.totalData[value].SpaCategoryName,
                "spaCategoryId": this.state.totalData[value].SpaCategoryId
            })
            if (this.state.totalData[value].SpaCategoryId !== "" && this.state.totalData[value].SpaCategoryId !== "0") {
                this.loadSpaPackages();
            }
            console.log("spaCategoryId", this.state.spaCategoryId);
            console.log("selectedSpaCategory", this.state.selectedSpaCategory);
        });
    }

    _onBtmSheetTimeSlot = async () => {
        console.log("this.state.myArray", this.state.timeslotArray);
        console.log("this.state.totalData", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            //  title: "Choose Restaurant",
            options: this.state.spaTimeslotArray,
            dark: false,
            cancelButtonIndex: 12,
            // destructiveButtonIndex: 1,
        }, (value) => {
            // if (value >= this.state.totalData.length) {  
            //   console.log("array",this.state.myArray)
            //   return;
            // }
            // else {
            console.log("value", value);
            console.log("this.state.totalData.value", this.state.totalData[1][value].value);
            console.log("this.state.totalData.value", this.state.totalData[1][value].id);
            this.setState({
                "selectedTimeSlot": this.state.totalData[1][value].value,
                "spaTimeslotId": this.state.totalData[1][value].id
            })
            console.log("timeslotId", this.state.timeslotId);
            console.log("selectedTimeSlot", this.state.selectedTimeSlot);
        });
    }

    // _onBtmSheetVisitor = async () => {
    //     console.log("this.state.myArray", this.state.spaVisitorArray);
    //     console.log("this.state.totalData", this.state.totalData);

    //     BottomSheet.showBottomSheetWithOptions({
    //         //  title: "Choose Restaurant",
    //         options: this.state.spaVisitorArray,
    //         dark: false,
    //         cancelButtonIndex: 12,
    //         // destructiveButtonIndex: 1,
    //     }, (value) => {
    //         // if (value >= this.state.totalData.length) {  
    //         //   console.log("array",this.state.myArray)
    //         //   return;
    //         // }
    //         // else {
    //         console.log("value", value);
    //         console.log("this.state.totalData.value", this.state.totalData[2][value].value);
    //         console.log("this.state.totalData.value", this.state.totalData[2][value].id);
    //         this.setState({
    //             "selectedVisitor": this.state.totalData[2][value].value,
    //             "spaVisitorId": this.state.totalData[2][value].id
    //         })
    //         console.log("spaVisitorId", this.state.spaVisitorId);
    //         console.log("selectedVisitor", this.state.selectedVisitor);
    //     });
    // }

    // _loadDropdowns = async () => {
    //     const details = await AsyncStorage.getItem(Keys.UserDetail);
    //     var jsonValue = JSON.parse(details);
    //     console.log("json", jsonValue)
    //     await this.setState({ userId: jsonValue.UserId });
    //     var languageId = await AsyncStorage.getItem(Keys.langId);

    //     await apiCallWithUrl(APIConstants.GetSpaMasterAPI + "?FullName=" + jsonValue.Fullname + "&&LanguageId=" + languageId, 'GET', '', this.getDropdownsResponse);
    // }

    // getDropdownsResponse = async (response) => {
    //     this.setState({ totalData: response.ResponseData, refreshing: false })

    //     console.log("totalResponse", response.ResponseData);
    //     console.log("haaa...", response.ResponseData[0]);
    //     var list1 = [];
    //     var list2 = [];
    //     var list3 = [];
    //     response.ResponseData[0].map((spapackage, index) => {
    //         console.log("loop", index, spapackage.SPAPackageName);
    //         list1.push(spapackage.SPAPackageName);
    //     });
    //     this.setState({ spaPackageArray: list1 })

    //     response.ResponseData[1].map((timeslot, index) => {
    //         console.log("loop", index, timeslot.value);
    //         list2.push(timeslot.value);
    //     });
    //     this.setState({ spaTimeslotArray: list2 })

    //     response.ResponseData[2].map((visitor, index) => {
    //         console.log("loop", index, visitor.value);
    //         list3.push(visitor.value);
    //     });
    //     this.setState({ spaVisitorArray: list3 })

    //     console.log("spaPackageArray", this.state.spaPackageArray);
    //     console.log("timeslotArray", this.state.spaTimeslotArray);
    //     console.log("visitorArray", this.state.spaVisitorArray);
    // }

    _loadDropdowns = async () => {
        await this.setState({ spaloading: true });
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        console.log("json", jsonValue)
        await this.setState({ userId: jsonValue.UserId });
        var languageId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        
        let overalldetails = {
            "HotelId": HotelId
          }
          await apiCallWithUrl(APIConstants.GetSpaMastersAPI, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
        //await apiCallWithUrl(APIConstants.GetSpaMastersAPI + "?FullName=" + jsonValue.Fullname + "&&LanguageId=" + languageId + "&&CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }
        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        var currentdate=today.toISOString();
        this.getDropdownsResponse(response);
        this.setState({ minBookingDate: currentdate, maxBookingDate: Moment().add(60, 'days').calendar()  })
        console.log("postCheckInResponse", response.InCustomer);
        // if (response.InCustomer == "False") {
        //     console.log("postCheckInResponse Language", response.InCustomer);
        //     if (Platform.OS === 'ios') {
        //         console.log("ios postCheckInResponse")
        //         this.setState({ loading: false }, () => {
        //             setTimeout(() => this.funcCheckOutResponse(response), 1000);
        //         });
        //     } else {
        //         console.log("android postCheckInResponse")
        //         this.setState({
        //             loading: false,
        //         }, () => this.funcCheckOutResponse(response));
        //     }

        // }
        //  else {
        //     this.getDropdownsResponse(response);
        // }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getDropdownsResponse = async (response) => {
        console.log("response.ResponseData.length", response.ResponseData.length)
        await this.setState({ spaloading: false, })
        if (response.ResponseData.length > 0) {
            this.setState({ totalData: response.ResponseData, refreshing: false })

            console.log("totalResponse", response.ResponseData);
            console.log("haaa...", response.ResponseData[0]);
            var list1 = [];
            var list2 = [];
            var list3 = [];
            response.ResponseData.map((spacategory, index) => {
                console.log("loop", index, spacategory.SpaCategoryName);
                list1.push(spacategory.SpaCategoryName);
            });
            this.setState({ spaCategoryArray: list1 })

            // response.ResponseData[1].map((timeslot, index) => {
            //     console.log("loop", index, timeslot.value);
            //     list2.push(timeslot.value);
            // });
            // this.setState({ spaTimeslotArray: list2 })

            // response.ResponseData[2].map((visitor, index) => {
            //     console.log("loop", index, visitor.value);
            //     list3.push(visitor.value);
            // });
            // this.setState({ spaVisitorArray: list3 })

            console.log("spaCategoryArray", this.state.spaCategoryArray);
            // console.log("timeslotArray", this.state.spaTimeslotArray);
            console.log("visitorArray", this.state.spaVisitorArray);
        }
    }

    loadSpaPackages = async () => {
        var languageId = await AsyncStorage.getItem(Keys.langId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId":languageId,
            "CategoryId":this.state.spaCategoryId
          }
          await apiCallWithUrl(APIConstants.GetSpaPackagesAPI, 'POST', overalldetails, (callback) => { this.getSpaPackagesResponse(callback) });
        // await apiCallWithUrl(APIConstants.GetSpaPackagesAPI + "?LanguageId=" + languageId + "&&CategoryId=" + this.state.spaCategoryId, 'GET', '', this.getSpaPackagesResponse);
    }

    getSpaPackagesResponse = async (response) => {
        console.log("totalSpaPackagesData", response.ResponseData);
        this.setState({ totalSpaPackagesData: response.ResponseData, refreshing: false, spaPackageId: '', selectedSpaPackage: I18n.t('choosespa'), selectedAmount1: '', selectedAmount2: '' })

        // if (response.ResponseData.length > 0) {
        //     this.setState({ totalSpaPackagesData: response.ResponseData, refreshing: false, spaPackageId: '', selectedSpaPackage: I18n.t('choosespa') })

        //     console.log("totalSpaPackagesData", response.ResponseData);
        // } else {
        //     this.setState({ totalSpaPackagesData: response.ResponseData, refreshing: false, spaPackageId: '', selectedSpaPackage: I18n.t('choosespa') })
        // }
    }


    _onPressSpaBooking = async () => {
        console.log("SPA Time", this.state.spaTime);
        console.log("spaCategoryId", this.state.spaCategoryId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        const InCustomer = await AsyncStorage.getItem(Keys.inCustomer);
        console.log("InCustomer", InCustomer);
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        console.log("roomNo", roomNo);
        if (InCustomer == "True") {
            let RoomNo=roomNo
          } else {
            let RoomNo=""
          }
        if (this.state.spaBookingDate == "") {
            Alert.alert(I18n.t('chooseDate'));
        } else {
            if (this.state.spaCategoryId == "" || this.state.spaCategoryId == 0) {
                Alert.alert(I18n.t('choosespacategory'));
            } else {
                if (this.state.spaPackageId == "") {
                    Alert.alert(I18n.t('choosespa'));
                } else {
                    if (this.state.spaTime == "") {
                        Alert.alert(I18n.t('selectTime'));
                    } else {
                        if (this.state.spaVisitorId == "") {
                            Alert.alert(I18n.t('chooseguest'));
                        } else {
                            if (!this.state.isSelectedSpaDisclaimer) {
                                Alert.alert(I18n.t('agreedisclaimer'));
                                return false;
                            }

                            var res = this.state.spaBookingDate.split("/");
                            var bookingdt=res[2]+'-'+res[1]+'-'+res[0];
                            var bookingdatetime=bookingdt+' '+this.state.spaTime;
                            this.setState({ spaloading: true })
                            let overalldetails = {
                                "SpaPackageId": this.state.spaPackageId,
                                "BookingDateTime": bookingdatetime,
                                "GuestName": this.state.selectedVisitor,
                                "Preference": this.state.preferenceNote,
                                "UserId": this.state.userId,
                                "CreatedBy": this.state.userId,
                                "Amount": this.state.selectedAmount,
                                "PackageType": this.state.selectedPackageType,
                                "Notes":this.state.therapist,         
                                "HotelId":HotelId,
                                "MobileNo":"",
                                "UserType":"0",
                                "RoomNo":roomNo,
                                "TherapistId":"",
                                "InternalGuest":this.state.internalGuests,
                                "ExternalGuest":this.state.externalGuests,
                                "AmountType":this.state.AmountType
                            }
                            await analytics().logEvent('SpaBooking_Cart', {
                                id: 1,
                                ClickedOption: 'SpaBooking Cart',
                                Details:overalldetails
                              })
                            console.log('overalldetails', overalldetails)
                            await apiCallWithUrl(APIConstants.PostSpaBookingAPI, 'POST', overalldetails, this.postSpaResponse);
                        }
                    }
                }
            }
        }
    }

    _onPressSpaDropdown = async () => {
        console.log("package data", this.state.totalSpaPackagesData);
        if (this.state.totalSpaPackagesData !== undefined) {
            if (this.state.totalSpaPackagesData.length > 0) {
                Actions.SPADropdown({
                    //  totalStateData: this.state.totalData[0],
                    totalStateData: this.state.totalSpaPackagesData,
                    callbackSpaFunc: this.getcallbackspafunc
                });
            } else {
                Alert.alert(I18n.t('alertnopackageavailable'));
            }

        } else {
            Alert.alert(I18n.t('choosespacategory'));
        }

    }



    getcallbackspafunc = async (data) => {
        console.log("selecteddata..", data);
        // this.setState({
        //     "selectedSpaPackage": data.SPAPackageName,
        //     "spaPackageId": data.SPAPackageId,
        //     "selectedAmount1": data.Amount,
        //     "selectedAmount2": data.Amount
        // })
        if (data.Amount != "" && data.DuoAmount != "" && data.SingleOfferAmt == "" && data.DuoOfferAmt == "") {

            this.setState({
                "selectedSpaPackage": data.SpaPackageName,
                "spaPackageId": data.SpaPackageId,
                "selectedAmount1": data.Amount,
                "selectedAmount2": data.DuoAmount,
                "Amount": data.Amount,
                "selectedAmount": data.Amount,
                "selectedPackageType": 1,
                "selectedDuration": data.Duration,
                "AmountType":1
            })
        } else if (data.SingleOfferAmt != "" && data.DuoOfferAmt != "") {
            
            this.setState({
                "selectedSpaPackage": data.SpaPackageName,
                "spaPackageId": data.SpaPackageId,
                "selectedAmount1": data.SingleOfferAmt,
                "selectedAmount2": data.DuoOfferAmt,
                "Amount": data.SingleOfferAmt,
                "selectedAmount": data.SingleOfferAmt,
                "selectedPackageType": 1,
                "selectedDuration": data.Duration,
                "AmountType":1
            })
        } else if (data.SingleOfferAmt != "") {
            this.setState({
                "selectedSpaPackage": data.SpaPackageName,
                "spaPackageId": data.SpaPackageId,
                "selectedAmount1": data.SingleOfferAmt,
                "selectedAmount2": data.DuoOfferAmt,
                "Amount": data.SingleOfferAmt,
                "selectedAmount": data.SingleOfferAmt,
                "selectedPackageType": 1,
                "AmountType":1
            })
        } else if (data.DuoOfferAmt != "") {
            this.setState({
                "selectedSpaPackage": data.SpaPackageName,
                "spaPackageId": data.SpaPackageId,
                "selectedAmount1": data.SingleOfferAmt,
                "selectedAmount2": data.DuoOfferAmt,
                "Amount": data.DuoOfferAmt,
                "selectedAmount": data.DuoOfferAmt,
                "selectedPackageType": 2,
                "AmountType":2
            })
        }
        // this.setState({
        //     "selectedSpaPackage": data.SPAPackageName,
        //     "spaPackageId": data.SPAPackageId,
        //     "selectedAmount1": data.Amount,
        //     "selectedAmount2": data.DuoAmount,
        //     "Amount": data.Amount,
        //     "selectedAmount": data.Amount
        // })
    }
    displayAmount = async (val) => {
        this.setState({
            selectedColor1: Colors.App_Font,
            selectedColor2: Colors.FullGray,
            selectedFontColor1: "#FFFFFF",
            selectedFontColor2: Colors.Black,
            selectedAmount: val,
            selectedPackageType: 1
        })
    }
    displayAmount1 = async (val) => {
        this.setState({
            selectedColor1: Colors.FullGray,
            selectedColor2: Colors.App_Font,
            selectedFontColor1: Colors.Black,
            selectedFontColor2: "#FFFFFF",
            selectedAmount: val,
            selectedPackageType: 2
        })
    }
    postSpaResponse = async (response) => {
        if (Platform.OS === 'ios') {
            await this.setState({
                spaloading: false,
            }, () => {
                setTimeout(() => this.funcSpaResponse(response), 1000);
            });
        } else {
            this.setState({
                spaloading: false,
            }, () => this.funcSpaResponse(response));
        }


    }

    funcSpaResponse = (response) => {
        if (response.IsException == null) {
            // if (response.ResponseData.length > 0) {
                var postSpaResponse = response.ResponseData;
                console.log('postSpaResponse', postSpaResponse);
                Alert.alert(I18n.t('alertspasuccess'));
                Actions.pop({ refresh: true });
        // Actions.refresh({ key: Math.random() * 1000000 })
        setTimeout(() => {
            Actions.refresh({ key: Math.random() * 1000000 })
      }, 5)
            // }
        }

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        // flexDirection: 'column'
    },
    label: {
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    picker: {
        color: Colors.PlaceholderText,
        marginLeft: 8,
        // height: 28,
        padding: 0,
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        fontSize: 15,
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    button1: {
        alignItems: 'center',
        backgroundColor: Colors.ButtonBlue,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 10
    },

});

module.exports = SpaBooking;

