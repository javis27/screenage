import React, {Component} from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont'
// import { Text, Card, Divider } from 'react-native-elements';


export default class ForecastCard extends Component {

	render() {
		let time;

		// Create a new date from the passed date time
		var date = new Date(this.props.detail.dt*1000);

		// Hours part from the timestamp
		var hours = date.getHours();
		
		// Minutes part from the timestamp
		var minutes = "0" + date.getMinutes();

		time = hours + ':' + minutes.substr(-2);

		return (
			<View containerStyle={styles.card}>
				<View style={styles.boxWithShadow}>
					<View style={{ flex: 0.40, alignItems: "center" }}>
						<View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
							<Image style={{width:95, height:95}} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
						</View>				
					</View>

					{/* <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:20}} /> */}
					<View style={{ flex: 0.60, alignItems: "flex-start" }}>
						<View style={{flexDirection:'column', justifyContent:'space-between'}}>
							<Text style={styles.temp}>{time} / {Math.round( this.props.detail.main.temp * 10) / 10 }&#8451;</Text>
							<Text style={styles.notes}>{this.props.location}</Text>
							<Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	card:{
		backgroundColor:'rgba(56, 172, 236, 1)',
		borderWidth:0,
		borderRadius:20,
	},
	temp: {
		fontSize: 18,
		color: Colors.Black,
		textTransform:'capitalize',
		marginBottom: 5,
		marginTop: 15,
		fontFamily: AppFont.Regular,
	},
	notes: {
		fontSize: 14,
		textTransform:'capitalize',
		color: '#aaaaaa',
		marginBottom: 4,
		fontFamily: AppFont.Regular,
	},
	boxWithShadow: {
        flex: 1,
		flexDirection: 'row',
		borderBottomColor: Colors.Gray,
		borderBottomWidth: 1,
    },
});