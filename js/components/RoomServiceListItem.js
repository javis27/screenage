import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';
import I18n from '../constants/i18n';
import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Status from '../constants/Status';
import Icon from 'react-native-vector-icons/Feather';

class RoomServiceListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roomServicelistItem: this.props.roomServices,
        }
        console.log("room props", this.props.roomServices)
    }
    render() {
        return (
            <View>
                <TableView style={{ marginBottom: 35 }}>
                    <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                        <View style={styles.boxWithShadow}>
                            {/* <View style={{ flex: 0.23, alignItems: "center", justifyContent: 'center', backgroundColor: Colors.App_Font }}>
                                <Text style={{ color: Colors.Background_Color, fontFamily: AppFont.Regular, fontSize: 15 }}>16</Text>
                                <Text style={{ color: Colors.Background_Color, fontFamily: AppFont.Regular, fontSize: 30 }}>Oct</Text>
                                <Text style={{ color: Colors.Background_Color, fontFamily: AppFont.Regular, fontSize: 15 }}>2019</Text>
                            </View> */}
                            <View style={styles.Restaurant}>
                                <Text style={styles.IconReqText}>
                                    {this.props.roomServices.ServiceRequest}
                                </Text>
                            </View>
                            {this.props.roomServices.Description != "" ?
                                <View style={styles.Restaurant}>
                                    <Text style={styles.IconReqText}>
                                        {this.props.roomServices.Description}
                                    </Text>
                                </View>
                                : null}

                            <View style={styles.DescriptionServiceList}>
                                <View style={{ flex: 0.5 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                                        <Icon name={'clock'} color={'#a3a3a3'}></Icon>
                                        <Text style={styles.ServiceText}>{this.props.roomServices.RequestedDate}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 0.5 }}>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'flex-end',
                                    }}>
                                        <Icon size={13} name={'check-circle'} color={this.StatusColors(this.props.roomServices.ServiceStatusId)}></Icon>
                                        <Text style={[styles.ServiceText, { color: this.StatusColors(this.props.roomServices.ServiceStatusId) }]}>{this.props.roomServices.ServiceStatus}</Text></View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </TableView>
            </View>
        );
    }

    // StatusColors(item) {
    //     var x = item;
    //     var textbgColor = "";
    //     console.log()
    //     if (x == "Requested") {
    //         // textbgColor = Status.serviceAvailability.Available.color;
    //         textbgColor = '#f8bd08';
    //     }
    //     else if (x == "Pending") {
    //         textbgColor = '#f14242';
    //     }
    //     else if (x == "Completed") {
    //         textbgColor = '#74ce32';
    //     }
    //     else if (x == "Not Available") {
    //         textbgColor = 'red';
    //     }
    //     return textbgColor;
    // }

    StatusColors(item) {
        console.log("item",item)
        var x = item;
        var textbgColor = "";
        if (x == Status.RoomServiceStatusType.requested.id) {
            textbgColor = Status.RoomServiceStatusType.requested.textColor;
        }
        else if (x == Status.RoomServiceStatusType.completed.id) {
            textbgColor = Status.RoomServiceStatusType.completed.textColor;
        }
        else if (x == Status.RoomServiceStatusType.pending.id) {
            textbgColor = Status.RoomServiceStatusType.pending.textColor;
        }
        else if (x == Status.RoomServiceStatusType.notavailable.id) {
            textbgColor = Status.RoomServiceStatusType.notavailable.textColor;
        }

        return textbgColor;
    }
}

const styles = StyleSheet.create({
    // TouchableOpacityStyle: {
    //     position: 'absolute',
    //     width: 60,
    //     height: 60,
    //     // alignItems: 'center',
    //     // justifyContent: 'center',
    //     right: 20,
    //     bottom: 20,
    //     backgroundColor: '#fff',
    // },
    // FloatingButtonStyle: {
    //     resizeMode: 'contain',
    //     width: 60,
    //     height: 60,
    // },
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        //  paddingBottom: 10,
        paddingHorizontal: 5,
        paddingVertical: 5,
        // padding: 15 ,
        // borderWidth:1,
        // borderColor:'blue'
    },
    IconReq: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
    },
    IconReqText: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 5,
        paddingVertical: 5,
    },
    ServiceText: {
        color: '#a3a3a3',
        fontSize: 13,
        paddingLeft: 6,
        // marginLeft: 7,
        fontFamily: AppFont.Regular
    },
    NearServiceText: {
        color: Colors.DarkGray,
        fontSize: 12,
        paddingHorizontal: 5,
        fontFamily: AppFont.Regular
    },
    boxWithShadow: {
        flex: 1,
        flexDirection: 'column'
    },
    AvailableText: {
        fontSize: 13,
        fontFamily: AppFont.Regular
    },
    NotAvailableText: {
        color: Colors.NotavailableColor,
        fontSize: 13,
        fontFamily: AppFont.Regular
    }

})

module.exports = RoomServiceListItem;