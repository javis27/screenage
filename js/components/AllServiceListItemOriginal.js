import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';
import I18n from '../constants/i18n';
import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Status from '../constants/Status';

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {
            servicelistItem: this.props.services,
            responsiveItemTypeImageURI: Status.serviceItemType.Veg.image,
            responsiveSpiceLevelImageURI: Status.SpiceLevelStatus.High.image,
            resSpiceImgWidth: Status.SpiceLevelStatus.Low.imgWidth,
            resSpiceImgHeight: Status.SpiceLevelStatus.Low.imgHeight,
            resChiliImgFlex: Status.SpiceLevelStatus.Low.imgFlex,
            resChiliTextFlex: Status.SpiceLevelStatus.Low.textFlex,
        }
        console.log("props", this.props.services.length);
        console.log("servicelistItem", this.state);
    }
    componentWillMount() {
        var ItemTypeImage = null;
        var spiceLevelImage = null;
        var spiceImgWidth = null;
        var spiceImgHeight = null;
        var chiliImgFlex = null;
        var chiliTextFlex = null;

        if (this.props.services.ItemType[0].toLowerCase() == Status.serviceItemType.Veg.type) {
            ItemTypeImage = Status.serviceItemType.Veg.image
            console.log("spicelevel", this.props.services.SpicyLevel)
        } else if (this.props.services.ItemType[0].toLowerCase() == Status.serviceItemType.Nonveg.type) {
            ItemTypeImage = Status.serviceItemType.Nonveg.image
            console.log("spicelevel", this.props.services.SpicyLevel)
        }

        switch (this.props.services.SpicyLevel) {
            case Status.SpiceLevelStatus.Low.spiceLevel:
                spiceLevelImage = Status.SpiceLevelStatus.Low.image
                spiceImgWidth = Status.SpiceLevelStatus.Low.imgWidth
                spiceImgHeight = Status.SpiceLevelStatus.Low.imgHeight
                chiliImgFlex = Status.SpiceLevelStatus.Low.imgFlex
                chiliTextFlex = Status.SpiceLevelStatus.Low.textFlex
                console.log("spiceLevelImage Low", spiceLevelImage);
                break
            case Status.SpiceLevelStatus.Medium.spiceLevel:
                spiceLevelImage = Status.SpiceLevelStatus.Medium.image
                spiceImgWidth = Status.SpiceLevelStatus.Medium.imgWidth
                spiceImgHeight = Status.SpiceLevelStatus.Low.imgHeight
                chiliImgFlex = Status.SpiceLevelStatus.Medium.imgFlex
                chiliTextFlex = Status.SpiceLevelStatus.Medium.textFlex
                console.log("spiceLevelImage Low", spiceLevelImage);
                break
            case Status.SpiceLevelStatus.High.spiceLevel:
                spiceLevelImage = Status.SpiceLevelStatus.High.image
                spiceImgWidth = Status.SpiceLevelStatus.High.imgWidth
                spiceImgHeight = Status.SpiceLevelStatus.Low.imgHeight
                chiliImgFlex = Status.SpiceLevelStatus.High.imgFlex
                chiliTextFlex = Status.SpiceLevelStatus.High.textFlex
                console.log("spiceLevelImage Low", spiceLevelImage);
                break
        }


        this.setState({ responsiveItemTypeImageURI: ItemTypeImage, responsiveSpiceLevelImageURI: spiceLevelImage, resSpiceImgWidth: spiceImgWidth, resChiliImgFlex: chiliImgFlex, resChiliTextFlex: chiliTextFlex })
        console.log("whole state", this.state);
    }
    render() {
        console.log("render servicelistItem", this.state.servicelistItem);
        return (
            <TableView style={{ marginBottom: 35 }}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                    <View style={styles.boxWithShadow}>
                        <View style={{ flex: 0.23, alignItems: "flex-start" }}>
                            <Image source={{ uri: this.state.servicelistItem.ItemImagePath }} style={{ width: 80, height: 80, borderRadius: 5 }} />
                        </View>                        
                        <View style={{ flex: 0.77, alignItems: "flex-end", padding: 15 }}>
                            <View style={styles.Restaurant}>
                                <View style={{ flex: 0.09 }}>
                                    <Image source={{ uri: this.state.responsiveItemTypeImageURI }} style={{ width: 12, height: 12, marginTop: 3, zIndex: 1 }} />
                                </View>
                                <View style={{ alignItems: "flex-start", flex: 0.7, flexDirection: 'row' }}>
                                    <Text style={styles.IconReqText}>{this.props.services.ItemName}</Text><Text style={styles.NearServiceText}>({this.props.services.ServiceType})</Text>
                                </View>
                                <View style={{ alignItems: "flex-end", flex: 0.3 }}>
                                <Text style={styles.IconReq}>{Status.Currency_ISO_Code.Mauritius.code}{this.props.services.Price}</Text>
                                </View>
                            </View>

                            <View style={styles.DescriptionServiceList}>
                                <View style={{ flex: 0.5 }}>
                                    <View style={{ flex: 1, flexDirection: "row" }}>
                                        <View style={{ flex: this.state.resChiliImgFlex }}><Image source={{ uri: this.state.responsiveSpiceLevelImageURI }} style={{ width: this.state.resSpiceImgWidth, height: 18 }} /></View>
                                        <View style={{ flex: this.state.resChiliTextFlex }}><Text style={styles.ServiceText}>{this.props.services.SpicyLevel}</Text></View>
                                    </View>
                                </View>
                                <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <View>
                                        <Text style={[styles.AvailableText,{ color: this.StatusColors(this.props.services.AvailabilityId) }]}>{this.props.services.Availability}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </TableView>
        );
    }

    StatusColors(item) {
        var x = item;
        var textbgColor = "";
        if (x == Status.serviceAvailability.Available.id) {
            textbgColor = Status.serviceAvailability.Available.color;
        }
        else if (x == Status.serviceAvailability.NotAvailable.id) {
            textbgColor = Status.serviceAvailability.NotAvailable.color;
        }
        return textbgColor;
    }
}

const styles = StyleSheet.create({
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        paddingBottom: 10,
        paddingHorizontal: 5,
    },
    IconReq: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
    },
    IconReqText: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 5
    },
    ServiceText: {
        color: Colors.Black,
        fontSize: 13,
        marginLeft: 7,
        fontFamily: AppFont.Regular
    },
    NearServiceText: {
        color: Colors.DarkGray,
        fontSize: 12,
        paddingHorizontal: 5,
        fontFamily: AppFont.Regular
    },
    boxWithShadow: {
        flex: 1,
        flexDirection: 'row'
    },
    AvailableText: {
        fontSize: 13,
        fontFamily: AppFont.Regular
    },
    NotAvailableText: {
        color: Colors.NotavailableColor,
        fontSize: 13,
        fontFamily: AppFont.Regular
    }

})

module.exports = Services;