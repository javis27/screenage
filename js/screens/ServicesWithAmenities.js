import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    FlatList,
    StatusBar
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import ServiceListItem from '../components/ServiceListItem';
import AddRoomService from '../components/AddRoomService';
import AllServiceListItem from '../components/AllServiceListItem';
import RoomServiceListItem from '../components/RoomServiceListItem';
import SegmentedControlTab from "react-native-segmented-control-tab";
import Loader from '../components/Loader';

import { apiCallWithUrl, dynamicApiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';
import ActionButton from 'react-native-action-button';

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            serviceListArray: [],
            IsfetchResponse: false,
            selectedIndex: 0,
            menuIndex: 0,
            IsFoodMenu: false,
            selectedRoomIndex: 0,
            IsRoomServiceResponse: false,
            roomServiceArray: [],
            isRefreshing: false,
        };
    }

    componentDidMount() {
        console.log("servicelistindex", this.props)
        // if (this.props.ServiceListIndex !== undefined) {
        //     this.setState({
        //         selectedIndex: this.props.ServiceListIndex
        //     });
        //     console.log("props", this.props.ServiceListIndex)
        // }
        //this._loadServices();
        this._loadRoomServices();
    }

    handleSubMenuIndexChange = async (childindex) => {
        await this.setState({
            ...this.state,
            selectedIndex: childindex,
            IsfetchResponse: false,
            loading: true,

        });

        const typeId = this.state.selectedIndex;

        console.log("service type", typeId);
        await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=1&&ServiceTypeId=" + typeId, 'GET', "", this.getServiceListResponse)
    };

    handleMainMenuIndexChange = async (menuindex) => {
        await this.setState({
            ...this.state,
            menuIndex: menuindex,
            IsfetchResponse: false,
            // loading: true,
            IsFoodMenu: !this.state.IsFoodMenu
        });

        const typeId = this.state.menuIndex;

        console.log("service type", typeId);
        if (typeId == 0) {
            this._loadRoomServices();
        }
        else if (typeId == 1) {
            this._loadServices();
        }
        // await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=1&&ServiceTypeId=" + typeId, 'GET', "", this.getServiceListResponse)
    };


    updateTabBar = () => {
        //let bgListColors = [Colors.Background_Color, Colors.ShadowGray];

        if (this.state.selectedIndex == 0) {
            return (<View style={{ marginBottom: 150 }}>{this.state.IsfetchResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshServices}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><AllServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 1) {
            return (<View style={{ marginBottom: 150 }}>{this.state.IsfetchResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshServices}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 2) {
            return (<View style={{ marginBottom: 150 }}>{this.state.IsfetchResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshServices}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        //<View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 3) {
            return (<View style={{ marginBottom: 150 }}>{this.state.IsfetchResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshServices}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
    }

    updateRoomTabBar = () => {
        console.log("updateRoomTabBar", this.state.selectedRoomIndex);
        if (this.state.selectedRoomIndex == 0) {
            console.log("helloworld room list ")
            // return (<View><RoomServiceListItem /></View>);

            return (<View>{this.state.IsRoomServiceResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshRoomServices}
                data={this.state.roomServiceArray}
                keyExtractor={(x, i) => i}
                ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        <View style={styles.boxWithShadow}><RoomServiceListItem roomServices={item} /></View>
                    )
                }}
            /> : null}</View>)
        } else if (this.state.selectedRoomIndex == "1") {
            console.log("helloworld room request")
            return (<View><AddRoomService callbackRoomServiceList={(data) => {
                this.setRoomListVisible(data);
            }} /></View>)
        }
    }

    displayActionButton = () => {
        console.log("displayActionButton")
        if (!this.state.IsFoodMenu) {
            if (this.state.selectedRoomIndex == 0) {
                return (<ActionButton
                    buttonColor={Colors.App_Font}
                    onPress={this.openNewRoomRequest}
                />)
            }
        }
    }

    setRoomListVisible = async () => {
        // console.log('setRoomListVisible', data);
        await this.setState({ selectedRoomIndex: 0, IsFoodMenu: false });
        this._loadRoomServices();
    }

    openNewRoomRequest = async () => {
        await this.setState({
            selectedRoomIndex: 1
        });
        // this.updateRoomTabBar();
        console.log("onpress openNewRoomrequest")
        // return (<View><AddRoomService /></View>)
    }


    render() {
        return (
            <View style={styles.container}>
                {/* <View>
                    <Image source={require('../../assets/images/AboutUs.jpg')} style={{ width: '100%', height: 200 }} />
                </View> */}
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader
                    loading={this.state.loading} />

                <View>
                    <SegmentedControlTab
                        tabsContainerStyle={styles.tabsContainerStyle}
                        tabStyle={styles.tabStyle}
                        firstTabStyle={styles.firstTabStyle}
                        lastTabStyle={styles.lastTabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                        values={["Amenities", "Food"]}
                        selectedIndex={this.state.menuIndex}
                        onTabPress={this.handleMainMenuIndexChange}
                    />
                </View>

                {this.state.IsFoodMenu ?
                    <View>
                        <SegmentedControlTab
                            tabsContainerStyle={styles.tabsContainerStyle}
                            tabStyle={styles.tabStyle}
                            firstTabStyle={styles.firstTabStyle}
                            lastTabStyle={styles.lastTabStyle}
                            tabTextStyle={styles.tabTextStyle}
                            activeTabStyle={styles.activeTabStyle}
                            activeTabTextStyle={styles.activeTabTextStyle}
                            values={["All", "Breakfast", "Lunch", "Dinner"]}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={this.handleSubMenuIndexChange}
                        />
                        {this.updateTabBar()}
                    </View>

                    :
                    <View>{this.updateRoomTabBar()}</View>
                }
                {this.displayActionButton()}
                {/* {this.state.IsFoodMenu ?
                    null
                    :
                    <ActionButton
                        buttonColor="rgba(231,76,60,1)"
                        onPress={this.openNewRoomRequest}
                    />
                } */}

            </View>
        )
    }

    _loadServices = async () => {
        this.setState({
            loading: true,
        });
        const typeId = this.state.selectedIndex;

        console.log("service type", typeId);

        await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=1&&ServiceTypeId=" + this.state.selectedIndex, 'GET', "", this.getServiceListResponse)
    }

    _onRefreshServices = () => {
        this.setState({ isRefreshing: true });
        this._loadServices();
    }

    _onRefreshRoomServices = () => {
        this.setState({ isRefreshing: true });
        this._loadRoomServices();
    }

    getServiceListResponse = async (response) => {
        if (response.IsException == null) {
            var getserviceListResponse = response.ResponseData;
            console.log('get services', getserviceListResponse);

            await this.setState({ loading: false, serviceListArray: getserviceListResponse, IsfetchResponse: true, isRefreshing: false });

            console.log('get service length', this.state.serviceListArray.length);

        }
        this.setState({ loading: false })
    }

    _loadRoomServices = async () => {
        this.setState({
            loading: true
        })
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);

        console.log("userId", jsonValue.UserId);

        await apiCallWithUrl(APIConstants.GetRoomServiceList + "?UserId=" + jsonValue.UserId, 'GET', "", this.getRoomServiceListResponse)
        // await dynamicApiCallWithUrl("http://192.168.5.123:8082/api/RoomServiceAPI/ListRoomService?UserId=1", 'GET', "", this.getRoomServiceListResponse)

        // + jsonValue.UserId
    }

    getRoomServiceListResponse = async (response) => {
        console.log("get room service response", response);
        if (response.ResponseData != null) {
            console.log("get room service response", response.ResponseData);
            var serviceRequestList = response.ResponseData;
            await this.setState({ roomServiceArray: serviceRequestList, IsRoomServiceResponse: true, isRefreshing: false })
        }
        this.setState({ loading: false })
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    tabStyle: {
        backgroundColor: "#FFFFFF",
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabStyle: {
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    AvailableText: {
        color: Colors.AvailableColor,
        fontSize: 13
    },
    NotAvailableText: {
        color: Colors.NotavailableColor,
        fontSize: 13
    }

})

module.exports = Services;