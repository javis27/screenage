import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Moment from 'moment';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import Swiper from 'react-native-swiper';
import CartListItem from '../components/CartListItem';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class BookingCart extends Component {
    constructor(props) {
        super(props);

        
    }

    componentWillMount() {
        console.log("enteringcart",this.props.PassingDetails)
        
    }

    


    render() {
        
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'/>
                
                <View style={{flexDirection:"column",flex:1}}>
                    <ScrollView>
                        <View style={{flex:0.75}}>
                        <FlatList
                        data={this.props.PassingDetails}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxWithShadow}>
                                    {/* <View style={{alignItems:'center',justifyContent:'center',margin:10}}>
                                            <Text style={styles.welcomeText}>Hi {item.selectedVisitor} Your Booking Details</Text>
                                        </View> */}
                                    <View style={{flex:1,flexDirection:'row'}}>
                                        
                                       
                                        <View style={{flex:0.25,margin:10}}>
                                            <Image source={require('../../assets/images/sugarbeach_img11.png')} style={{width:'90%',height:80,resizeMode:'cover',borderRadius:10}}/>
                                        </View>
                                        <View style={{flex:0.75,marginLeft:-5,marginTop:5}}>
                                            <Text style={styles.wText1}>{item.selectedSpaCategory}</Text>
                                            <Text style={styles.wText2}>Package : {item.selectedSpaPackage}</Text>
                                            <Text style={styles.wText2}>On {item.spaBookingDate} at {item.spaTime} </Text>
                                        </View>
                                    </View>
                                    <View style={{flex:1,flexDirection:'row',margin:5,alignItems:'center',justifyContent:'center'}}>
                                        <View style={{flex:0.10}}></View>
                                        <View style={{flex:0.60}}><Text style={styles.wText2}>Total Persons - {item.externalGuests}</Text></View>
                                        <View style={{flex:0.20}}><Text style={styles.wText21}>{item.Amount}</Text></View>
                                        <View style={{flex:0.10}}></View>
                                    </View>
                                    <View style={{flex:1,flexDirection:'row',margin:5,alignItems:'center',justifyContent:'center'}}>
                                        <View style={{flex:0.10}}></View>
                                        <View style={{flex:0.60,}}><Text style={styles.wText3}>Total</Text></View>
                                        <View style={{flex:0.20,}}><Text style={styles.wText31}>{item.Amount}</Text></View>
                                        <View style={{flex:0.10}}></View>
                                    </View>
                                    
                                </View>
                            )
                        }}
                       /> 
                        </View>
                        
                        </ScrollView>
                        <View style={{flex:0.25,marginTop:20,backgroundColor:'#FFFFFF',borderRadius:15,alignItems:'center',justifyContent:'center'}}>
                            
                        <View style={{ width: "80%"}}>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressConfirm}>
                                <Text style={styles.btntext}>Proceed to Pay </Text>
                                </TouchableOpacity>
                            </View>
                         </View>
                         </View>
                         
                         </View>

            </View>
        )
    }
    _onPressConfirm = async () => {
        
    }
    
    
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: Colors.Background_Gray, flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.ButtonBlue,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 15
      },
      btntext: {
        color: "#FFF",
        fontSize: 16,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
      },
      welcomeText: {
        color: "#000",
        fontSize: 16,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
      },
      wText1: {
        color: "#000",
        fontSize: 14,
        letterSpacing: 1,
        fontFamily: AppFont.Bold,
      },
      wText2: {
        color: "#000",
        fontSize: 12,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
        marginTop:5
      },
      wText21: {
        color: "#000",
        fontSize: 12,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
        marginTop:5,
        textAlign:'right'
      },
      wText3: {
        color: "#000",
        fontSize: 16,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
        marginTop:5
      },
      wText31: {
        color: "#000",
        fontSize: 16,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
        marginTop:5,
        textAlign:'right'
      },
      textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 17,
        height:100,
        padding: 0,
        includeFontPadding: false,
        borderWidth:0.5,
        borderColor:Colors.LightGray,
        marginLeft:10,
        marginRight:10
      },
})

module.exports = BookingCart;