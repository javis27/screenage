import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


class GuestHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            languageId: '',
            hasroomNo: false,
            hasLoggedIn: false,
            dynamicMenus: [],
            loadingMenu: false,
            bannerArray: [],
            IsFetchBanners: false,
            bannerheight: Dimensions.get('window').height / 4,
            bannerwidth: Dimensions.get('window').width,
        }
    }

    componentWillMount() {
        StatusBar.setHidden(true);
        // this.verifyCheckInDetails();
        console.log("props", this.props)
        this._loadInitialState();
        this.handleBackAddEventListener();
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        if (Actions.currentScene == "GuestHome") {
            BackHandler.exitApp();
            return true;
        }
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }


    // verifyCheckInDetails = async () => {
    //     await this.setState({ loadingMenu: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     console.log("postCheckInResponse Language", response);
    //     await this.setState({ loadingMenu: false });
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.Login();
    //         } else {
    //             console.log("postCheckInResponse Calendar else", response);
    //         }
    //     }
    // }

    renderBannerImages(bannerData) {
        var base64Icon = '';
        if (Platform.OS == 'ios') {
            console.log("ios")
            // base64Icon = "data:image/png;base64," + bannerData.IOSImage;
            base64Icon = bannerData.IOSImagePath;
        } else {

            if (bannerData.AndroidImagePath.length != 0) {
                console.log("android")
                //base64Icon = "data:image/png;base64," + bannerData.AndroidImage;
                base64Icon = bannerData.AndroidImagePath;
            }
        }
        //  if (bannerData.AndroidImage != "") {
        return (
            <Image source={{ uri: base64Icon }} style={{ width: this.state.bannerwidth, height: this.state.bannerheight }} />
        )
        // }
        // else
        //     return null

    }
    navigateBackOption=()=>{
        Actions.pop();
    }

    render() {
        //const lid = this.props.userid;
        console.log('hasroomNo', this.state.hasroomNo)
        console.log('hasLoggedIn', this.state.hasLoggedIn)
        return (
            <View style={styles.container}>
                {/* <StatusBar
                    translucent
                    backgroundColor='transparent'
                    barStyle='default'
                /> */}
                <Loader loading={this.state.loadingMenu} />
                <View style={{ flex: 1,flexDirection:'column'}}>
                    <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
                    <ImageBackground source ={require('../../assets/images/SouthernGross_img24.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
                        <View style={{flex:1,flexDirection:'column'}}>
                            <View style={{flex:0.4,justifyContent:'flex-end'}}>
                                <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                                    <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:0.6}}>
                            <Image
                                source={require('../../assets/images/sugarbeach.png')}
                                style={{
                                    width:'60%',
                                    height:80,
                                    position: 'absolute', // child
                                    bottom: 60, // position where you want
                                    // left: 30,
                                    marginRight:10,
                                }}
                                />
                            </View>
                        </View>       
                    </ImageBackground>
                    </View>
                    <View style={{flex:0.75,flexDirection:'column',backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                    <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                        {this.state.dynamicMenus.length > 0 ?
                            this.state.dynamicMenus.map((menus, index) => {
                                return <View><View><TouchableOpacity key={index} activeOpacity={.5} onPress={this._onPressDynamicMenu.bind(this, menus.PageId)}>
                                    <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                        {/* <View style={{ width: "82%" }}>
                                            <Text style={styles.mainmenu}>{menus.Title}</Text>
                                        </View>
                                        <View style={{ width: "18%", padding: 12 }}>
                                            <Cell accessory="DisclosureIndicator" />
                                        </View> */}
                                        <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                        <View style={{ width: "70%" }}>
                                            <Text style={styles.mainmenu}>{menus.Title}</Text>
                                        </View>
                                        <View style={{ width: "18%", padding: 12 }}>
                                            <Cell accessory="DisclosureIndicator" />
                                        </View>
                                    </View>

                                </TouchableOpacity>
                                </View>
                                {/* <View>
                                    <TouchableOpacity><Text style={styles.mainmenu}>Bookings</Text></TouchableOpacity>
                                </View> */}
                                </View>
                            })
                            :
                            null
                        }
                        <TouchableOpacity activeOpacity={.5} onPress={this._onGuestRestaurant}>
                                <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                    <View style={{ width: "70%" }}>
                                        <Text style={styles.mainmenu}>{I18n.t('offlinemaprestaurant')}</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        {!this.state.hasroomNo && !this.state.hasLoggedIn ?
                            <TouchableOpacity activeOpacity={.5} onPress={this._onGuestCheckInButton1}>
                                <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                    <View style={{ width: "70%" }}>
                                        <Text style={styles.mainmenu}>{I18n.t('checkin')}</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : null}
                            {!this.state.hasroomNo && this.state.hasLoggedIn ?
                            <TouchableOpacity activeOpacity={.5} onPress={this._onGuestCheckInButton}>
                                <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                    <View style={{ width: "70%" }}>
                                        <Text style={styles.mainmenu}>{I18n.t('checkin')}</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : null}
                        {this.state.hasLoggedIn ?
                            <TouchableOpacity activeOpacity={.5} onPress={this._onGuestProfileButtonClick}>
                                <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                    <View style={{ width: "70%" }}>
                                        <Text style={styles.mainmenu}>{I18n.t('profile')}</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : null}
                        {this.state.hasLoggedIn ?
                        <View>
                                <View>
                                    <TouchableOpacity onPress={this._onBookingsNewButton}>
                                    <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                    <View style={{ width: "70%" }}>
                                        <Text style={styles.mainmenu}>Bookings</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                                    </TouchableOpacity>
                                </View>
                                
                                <View>
                                    <TouchableOpacity onPress={this.onSignOut}>
                                    <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                    <View style={{ width: "70%" }}>
                                        <Text style={styles.mainmenu}>Sign Out</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                                    </TouchableOpacity>
                                </View>
                                </View>
                                : null}
                        {/* <TouchableOpacity activeOpacity={.5} onPress={this._onGuestMaps}>
                            <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                <View style={{ width: "82%" }}>
                                    <Text style={styles.mainmenu}>Map Reference</Text>
                                </View>
                                <View style={{ width: "18%", padding: 12 }}>
                                    <Cell accessory="DisclosureIndicator" />
                                </View>
                            </View>
                        </TouchableOpacity> */}

                    </ScrollView>
                            
                    </View>
            </View>

            </View>
        )
    }

    // API call while clicking on dynamic Menus 
    _onPressDynamicMenu = async (pageId) => {
        console.log("id", pageId);
        this.setState({
            loadingMenu: true
        })
        let overalldetails = {
            "HotelId": this.state.HotelId,
            "PageId": pageId,
            "LanguageId": this.state.languageId
          }
          await apiCallWithUrl(APIConstants.GetSubMenuAPI, 'POST', overalldetails, (callback) => { this.subMenuAPIResponse(callback, pageId) });
        //await apiCallWithUrl(APIConstants.GetSubMenuAPI + "?LanguageId=" + this.state.languageId + "&&PageId=" + pageId, 'GET', "", (callback) => { this.subMenuAPIResponse(callback, pageId) });
    }
    _onBookingsNewButton =  () => {
        Actions.BookingsNewList()
        }
        _onBookingsNewButton1 =  () => {
            Actions.DashboardDup()
            }

    // Response for dynamic menus api call

    subMenuAPIResponse = async (response, selectedPageId) => {
        console.log("res", response)
        console.log("selectedPageId", selectedPageId)
        if (response.ResponseData != null) {
            var submenuApiResponse = response.ResponseData;
            var submenuResponselength = submenuApiResponse.length;
            console.log('submenu response', submenuApiResponse.length)
            console.log('submenu response', selectedPageId)
            if (submenuResponselength == 0) {
                await this.setState({
                    loadingMenu: true
                })
                let overalldetails = {
                    "HotelId": this.state.HotelId,
                    "PageId": selectedPageId,
                    "LanguageId": this.state.languageId
                  }
                await apiCallWithUrl(APIConstants.GetPagesContentAPI, 'POST', overalldetails, (callback) => { this.menuContentAPIResponse(callback) });
                //await apiCallWithUrl(APIConstants.GetPagesContentAPI + "?LanguageId=" + this.state.languageId + "&&PageId=" + selectedPageId, 'GET', "", this.menuContentAPIResponse);
            } else {
                await this.setState({ loadingMenu: false })
                Actions.DynamicSubMenu({ subMenus: submenuApiResponse })
            }
        }else{
            let overalldetails = {
                "HotelId": this.state.HotelId,
                "PageId": selectedPageId,
                "LanguageId": this.state.languageId
              }
            await apiCallWithUrl(APIConstants.GetPagesContentAPI, 'POST', overalldetails, (callback) => { this.menuContentAPIResponse(callback) });
        }

    }

    menuContentAPIResponse = async (response) => {
        await this.setState({ loadingMenu: false })
        console.log("content", response);
        if (response.ResponseData != null) {
            console.log('content response', response.ResponseData[0])
            Actions.DynamicMenuContent({ menuContent: response.ResponseData[0] })
        }


    }

    // Page load API call
    _loadInitialState = async () => {
        this.setState({
            loadingMenu: true
        })
        var langId = await AsyncStorage.getItem(Keys.langId);
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var HotelId = await AsyncStorage.getItem('HotelId');
        AsyncStorage.getItem(Keys.UserDetail)
            .then((userdetails) => {
                var userJson = JSON.parse(userdetails);
                console.log('AsyncStorage userJson', userJson)
                console.log('AsyncStorage userJson', userJson.UserId)
                this.setState({ hasLoggedIn: userJson.UserId ? true : false })
            });
        // var roomNumber = await AsyncStorage.getItem(Keys.roomNo);
        console.log('details', langId)
        AsyncStorage.getItem(Keys.roomNo)
            .then((roomNumber) => {
                console.log(' AsyncStorage guesthome', roomNumber)
                this.setState({ hasroomNo: roomNumber ? true : false })
            });
        await this.setState({
            languageId: langId,
            HotelId:HotelId
        })

        console.log("state", this.state)
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId": this.state.languageId
          }
          await apiCallWithUrl(APIConstants.MobilePagesAPI, 'POST', overalldetails, this.menuApiResponse);
        //await apiCallWithUrl(APIConstants.MobilePagesAPI + "?LanguageId=" + this.state.languageId, 'GET', "", this.menuApiResponse);
        // await apiCallWithUrl(APIConstants.GetBannerAPI, 'GET', "", this.getBannerResponse)
        await apiCallWithUrl(APIConstants.GetBannerAPI, 'POST', overalldetails, (callback) => { this.getBannerResponse(callback) });

    }

    // Page load API call response

    menuApiResponse = async (response) => {
        if (response.ResponseData != null) {
            var menuApiResponse = response.ResponseData;
            console.log('dynamic response', menuApiResponse)
            console.log('dynamic response', menuApiResponse.Title)

            this.setState({
                dynamicMenus: menuApiResponse === null ? [] : menuApiResponse
            })
        }
         this.setState({ loadingMenu: false })
    }

    getBannerResponse = async (response) => {
        if (response.ResponseData.length > 0) {
            var bannersList = response.ResponseData;
            console.log('bannerArray', bannersList);
            await this.setState({ bannerArray: bannersList, IsFetchBanners: true, loadingMenu: false })
            console.log('bannerArray', this.state.bannerArray);
        }
        this.setState({ loadingMenu: false })
    }

    // _onAboutButton = () => {
    //     Actions.About();
    // }

    _onGuestCheckInButton = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        console.log("userdetails", userdetails);
        if (userdetails == null) {
            // If user is not logged in then redirect to login 
            Actions.Login();
            // var userJson = JSON.parse(userdetails);
            // Actions.GuestCheckIn();
        }
        else {
            Actions.GuestCheckIn();

        }
    }
    _onGuestCheckInButton1 = async () => {
        //guest checkin
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        console.log("userdetails", userdetails);
        
            Actions.CheckInHome();

    }
    _onGuestRestaurant= async () => {
        
            Actions.RestaurantListView();
           
    }

    _onGuestProfileButtonClick = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        console.log("userdetails", userdetails);
        if (userdetails == null) {
            // If user is not logged in then redirect to login 
            Actions.Login();
            // var userJson = JSON.parse(userdetails);
            // Actions.GuestCheckIn();
        }
        else {
            Actions.GuestProfile();
        }
    }
    onSignOut = () => {
        Alert.alert(I18n.t('signout'), '',
          [
            {
              text: I18n.t('cancel'),
              onPress: () => console.log('Cancel Pressed'),
              //style: 'cancel'
            },
            {
              text: I18n.t('yes'),
              onPress: () => this.onSignOutAction(),
              //style: 'destructive'
            },
          ],
          {
            cancelable: false
          }
        )
      };
    
      onSignOutAction = async () => {
    
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
    
        //setTimeout(() => {
    
        //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
        Actions.GuestLogin({ type: 'replace' });
        // }, 500);
    
        // Actions.pop(); Actions.pop(); delete_item()
    
        // Actions.GuestLogin({type:'reset'});delete_item()
    
    
        //Actions.popTo('GuestLogin');
      }

    // _onGuestMaps = () => {
    //     Actions.Maps();
    // }

    // _onGuestHomeButton = () => {
    //     Actions.GuestHome();
    // }
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },

})

module.exports = GuestHome;