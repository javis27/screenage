import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';
import Ratings from '../components/Ratings';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class BookingsRatings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questionsListArray: [],
            selectedans:[],
            comments:""
        };
    }

    componentDidMount() {
        this._loadRatingQuestions();
    }

    render() {
        
        return (
            <View style={styles.container}>
                <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
                />
                {/* <Loader loading={this.state.loadingMenu} /> */}

               
                    <View style={{marginTop:30}}></View>
                    <View style={{flexDirection:"column",flex:1}}>
                        <View style={{flex:0.70,justifyContent:'center'}}>
                            <View style={{flex:1,justifyContent:'center',alignItems: 'stretch'}}>
                                <FlatList
                                    data={this.state.questionsListArray}
                                    keyExtractor={item => item.Id}
                                    renderItem={({ item }) => {
                                        return (<Ratings details={item} onselectedItems={this.getSelectedItems}/>)
                                    }}
                                    />
                            </View>
                        </View>
                        <View style={{flex:0.15}}>
                        <TextInput
                            multiline={true}
                            underlineColorAndroid="transparent"
                            style={styles.textInput}
                            numberOfLines={4}
                            placeholder={I18n.t('comments')}
                            onChangeText={(text) => this.setState({comments:text})}
                            value={this.state.comments}/>
                        </View>
                        <View style={{flex:0.15}}>
                        <View style={{ width: "90%", marginBottom: 35, marginLeft: 20}}>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressRating}>
                                    <Text style={styles.btntext}>{I18n.t('rate')}</Text>
                                </TouchableOpacity>
                            </View>
                         </View>
                         </View>
                         </View>

            </View>
        )
    }
    getSelectedItems = async (details, selectedRate) => {
        console.log("details..",details)
        console.log("selectedRate..",selectedRate)
        const seldata= {
            "RatingQuestionId":details.RatingQuestionId,
            "QuestionRating":selectedRate
        }
        console.log("seldata..",seldata)
        var joined1 = this.state.selectedans.concat(seldata);
        await this.setState({ selectedans: joined1 })
        console.log("selectedans..",this.state.selectedans)
        

    }
    _onPressRating = async () => {
        console.log("ModuleId..",this.props.ModuleId)
        console.log("OrderId..",this.props.OrderId)
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        console.log("roomNo", roomNo);
        let overalldetails = {
            "HotelId": this.state.HotelId,
            "ModuleId":this.props.ModuleId,
            "OrderId":this.props.OrderId,
            "CreatedBy":this.state.userId,
            "Comments":this.state.comments,
            "RoomNo":roomNo,
            "UserType":"0",
            "ReviewRatingList":this.state.selectedans
          }
          await apiCallWithUrl(APIConstants.CreateRatingAPI, 'POST', overalldetails, (callback) => { this.getafterRatingResponse(callback) });
    }
    _loadRatingQuestions = async () => {
        this.setState({ loading: true })
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        await this.setState({ userId: userJson.UserId,HotelId:HotelId });
        console.log("HotelId...",HotelId)
        //await apiCallWithUrl(APIConstants.GetBikeBookingList + "?UserId=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        let overalldetails = {
            "HotelId": HotelId,
            "ModuleId": this.props.ModuleId
          }
          await apiCallWithUrl(APIConstants.GetRatingQuestionAPI, 'POST', overalldetails, (callback) => { this.getRatingResponse(callback) });
        
    }

    getRatingResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response != null) {
            var getResponse = response.ResponseData;
            console.log('getResponse', getResponse);

            await this.setState({ questionsListArray: getResponse, isbikeRefreshing: false });

            console.log('getListResponse', this.state.questionsListArray.length);

        }
    }

    getafterRatingResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response != null) {
            Alert.alert(I18n.t('thanks_for_rating'));
            Actions.pop();
            setTimeout(() => {
                Actions.refresh({ key: Math.random() * 1000000 })
          }, 5)
        }
    }
    
}


const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
      },
      btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
      },
      textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 17,
        height:100,
        padding: 0,
        includeFontPadding: false,
        borderWidth:0.5,
        borderColor:Colors.LightGray,
        marginLeft:20,
        marginRight:20
      },
})

module.exports = BookingsRatings;