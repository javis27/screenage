import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import { Actions } from 'react-native-router-flux';

export default class PrivacyPolicy extends Component{
    constructor(props){
        super(props);
        this.state={
            privacyVisible:this.props.privacyPolicyVisibility
        }
        console.log('props', this.props.privacyPolicyVisibility)
        this.closeDialog = this.closeDialog.bind(this);
    }
    async closeDialog() {
        console.log('hit close')
        await this.setState({ privacyVisible: false });
        this.props.callbackPrivacyPolicy(this.state.privacyVisible)
        Actions.refresh({ key: Math.random() * 1000000 })
        // Actions.refresh({ refresh: { Clear: Math.random() * 1000000 } })
    }
    render(){
        return(
            <Dialog
            visible={this.state.privacyVisible}
            overlayBackgroundColor='#000'
            dialogStyle={{
                backgroundColor: 'transparent'
            }}
            dialogAnimation={new SlideAnimation({
                slideFrom: 'bottom',
            })}
            onTouchOutside={this.closeDialog}
        >
            <DialogContent>
                <View style={{ margin: '5%', padding: '5%', borderRadius: 10, backgroundColor: 'white' }}>
                    {/* <View style={styles.rightContainer}>
                            <TouchableOpacity onPress={this.closeDialog}>
                                <Image source={{ uri: 'sclose' }} style={{ width: 30, height: 30 }} />
                            </TouchableOpacity>
                        </View> */}
                    <View>
                        <Text style={{ color: Colors.Black, fontFamily: AppFont.Regular, fontSize: 18, paddingBottom: 20, textAlign: 'center' }}>Privacy Policy</Text>
                    </View>
                    <View>
                        <Text style={{ paddingBottom: 20, fontFamily: AppFont.Light, color: '#717171' }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                            <View style={{ width: "40%" }}>
                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.closeDialog}>
                                    <Text style={styles.btntext}>OK</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                </View>
            </DialogContent>
        </Dialog>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
})