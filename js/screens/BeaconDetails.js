import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    SectionList,
    StatusBar
} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import groupBy from '../constants/groupBy';
import BeaconItem from '../components/BeaconItem';
//import PhoneDirectoryItem from '../components/PhoneDirectoryItem';

import APIConstants from '../api/APIConstants';
import { beaconApiCallWithUrl } from '../api/APIHandler';

class BeaconDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            beaconArray: [],
            loading: false,
            isRefreshing: false,
            isBeacon: false,
        }
    }
    componentWillMount() {
        this._onLoadBeaconDetails();
    }

    renderEmptyComponent = () => {
        if (this.state.beaconArray.length != 0) return null;
        return (
            // <View style={{flex:1, borderColor:'red', borderWidth: 1.0}}>
            <Text style={{ fontSize: 18, alignSelf: 'center', fontFamily: AppFont.Light, color: 'black', textAlign: 'center' }}>{I18n.t('nodata')}</Text>
            // </View>
        );
    };
    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.isBeacon ?
                        <ScrollView contentContainerStyle={{ padding: 25 }}>
                            <StatusBar
                                translucent={false}
                                backgroundColor={Colors.App_Font}
                                barStyle='light-content'
                            />
                            <Loader
                                loading={this.state.loading} />
                            <View style={styles.FlightDetailsOuter}>
                                <SectionList
                                    stickySectionHeadersEnabled
                                    refreshing={this.state.isRefreshing}
                                    onRefresh={this._onLoadBeaconDetails}
                                    ListEmptyComponent={this.renderEmptyComponent}
                                    keyExtractor={(x, i) => i}
                                    sections={this.state.beaconArray}
                                    renderItem={({ item }) => { return <BeaconItem beacon={item} /> }}
                                    renderSectionHeader={({ section }) => <View style={styles.FlightDetailsOuterActive}><Text style={styles.title}>{section.title}</Text></View>}
                                />
                            </View>

                        </ScrollView>
                        : null
                }
            </View>
        )
    }

    _onLoadBeaconDetails = () => {
        this.setState({
            loading: true,
            isRefreshing: true
        });
        let location_id = "4748497067966464";
        beaconApiCallWithUrl("locations/" + location_id + "/beacons", 'GET', "", this.getBeaconResponse);
      //  beaconApiCallWithUrl("locations/" + location_id + "/campaigns", 'GET', "", this.getBeaconResponse);
    }

    getBeaconResponse = async (response) => {
        // console.log("locations/" + location_id + "/beacons")
        console.log("beacon response", response.results)
        var groupByLocationId = groupBy(response.results, 'location')
        var arrayOfData = [];
        Object.keys(groupByLocationId).forEach(element => {
            arrayOfData.push({
                title: element,
                data: groupByLocationId[element]
            });
        });

        console.log('arrayOfData...', arrayOfData);
        this.setState({ loading: false, isRefreshing: false, beaconArray: arrayOfData, isBeacon: true });
    }

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    title: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        padding: 13,
    },
    innerText: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30
    },
    FlightDetails: {
        flex: 1,
        flexDirection: "row",
        borderBottomColor: Colors.Gray,
        borderBottomWidth: 0.5,
    },
    FlightDetailsOuter: {
        borderColor: Colors.Gray,
        borderWidth: 1,
        flex: 1,
        //flexDirection: "row",
    },
    FlightDetailsOuterActive: {
        backgroundColor: Colors.FullGray,
        borderColor: Colors.Gray,
        borderWidth: 0.5,
        flex: 1,
        //flexDirection: "row",
    },
    HeadText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        padding: 13
    },
    DetailsText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        padding: 13,
        flexWrap: 'wrap',
    },

})

module.exports = BeaconDetails;