
import React from 'react';
import {
    Platform,
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    StatusBar
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import RestaurantBooking from '../components/RestaurantBooking';
import SpaBooking from '../components/SpaBooking';
import BikeBooking from '../components/BikeBooking';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

import BottomSheet from 'react-native-bottomsheet';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import SegmentedControlTab from "react-native-segmented-control-tab";

class Bookings extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedRestaurant: 'Choose Restaurant',
            bookingDate: '',
            selectedTimeSlot: 'Choose Time Slot',
            numberofPeople: 'Choose Number of People',
            restaurantArray: [],
            timeslotArray: [],
            noofPeopleArray: ["<Select>", "1", "2", "3", "4", "5", "6"],
            restaurantId: '',
            timeslotId: '',
            preferenceNote: '',
            allergyNote: '',
            userId: '',
            selectedIndex: 0,
            loading: false
        };
    }

    componentDidMount() {
        // this.verifyCheckInDetails();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     await this.setState({ loading: false });
    //     var postCheckInDetailResponse = response.ResponseData;
    //     console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //     if (postCheckInDetailResponse.InCustomer == "False") {
    //         console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //         Alert.alert(I18n.t('alertnotcheckin'));
    //         AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //         AsyncStorage.removeItem(Keys.UserDetail);
    //         AsyncStorage.removeItem(Keys.roomNo);
    //         AsyncStorage.removeItem(Keys.inCustomer);
    //         AsyncStorage.removeItem(Keys.checkInId);
    //         Actions.GuestLogin();
    //     }

    //     // else {
    //     //     this._loadDropdowns();
    //     // }
    // }

    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };

    updateTabBar = () => {

        if (this.state.selectedIndex == 0) {
            return (<RestaurantBooking />);
        }
        else if (this.state.selectedIndex == 1) {
            return (<SpaBooking />);
        }
        else if (this.state.selectedIndex == 2) {
            return (<BikeBooking />);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading} />
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle="light-content"
                />
                <View>
                    <SegmentedControlTab
                        tabsContainerStyle={styles.tabsContainerStyle}
                        tabStyle={styles.tabStyle}
                        firstTabStyle={styles.firstTabStyle}
                        lastTabStyle={styles.lastTabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                        values={[I18n.t('restaurant'), I18n.t('spa'), I18n.t('bike')]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />
                </View>

                {/* Restaurant Start */}

                {this.updateTabBar()}

                {/* Restaurant End */}

                {/* SPA Start */}

                {/* SPA Start */}

            </View>

        );
    }

    _onBtnSheetRestaurant = async () => {
        console.log("this.state.myArray", this.state.restaurantArray);
        console.log("this.state.totalData", this.state.totalData);

        // var restaurantArrayLen = this.state.restaurantArray.length;
        BottomSheet.showBottomSheetWithOptions({
            // title: "Choose Restaurant",
            options: this.state.restaurantArray,
            dark: false,
            cancelButtonIndex: 12,
            // destructiveButtonIndex: 1,
        }, (value) => {
            // if (value >= this.state.totalData.length) {  
            //   console.log("array",this.state.myArray)
            //   return;
            // }
            // else {
            console.log("this.state.totalData.value", this.state.totalData[0][value].value);
            this.setState({
                "selectedRestaurant": this.state.totalData[0][value].value,
                "restaurantId": this.state.totalData[0][value].id
            })
            console.log("restaurantId", this.state.restaurantId);
            console.log("selectedRestaurant", this.state.selectedRestaurant);
        });
    }

    _onBtnSheetTimeSlot = async () => {
        console.log("this.state.myArray", this.state.timeslotArray);
        console.log("this.state.totalData", this.state.totalData);

        // var restaurantArrayLen = this.state.restaurantArray.length;
        BottomSheet.showBottomSheetWithOptions({
            //  title: "Choose Restaurant",
            options: this.state.timeslotArray,
            dark: false,
            cancelButtonIndex: 12,
            // destructiveButtonIndex: 1,
        }, (value) => {
            // if (value >= this.state.totalData.length) {  
            //   console.log("array",this.state.myArray)
            //   return;
            // }
            // else {
            console.log("value", value);
            console.log("this.state.totalData.value", this.state.totalData[1][value].value);
            console.log("this.state.totalData.value", this.state.totalData[1][value].id);
            this.setState({
                "selectedTimeSlot": this.state.totalData[1][value].value,
                "timeslotId": this.state.totalData[1][value].id
            })
            console.log("timeslotId", this.state.timeslotId);
            console.log("selectedTimeSlot", this.state.selectedTimeSlot);
        });
    }

    _onBtnSheetNoofPeople = async () => {
        console.log("this.state.myArray", this.state.noofPeopleArray);

        // var restaurantArrayLen = this.state.restaurantArray.length;
        BottomSheet.showBottomSheetWithOptions({
            // title: "Choose Restaurant",
            options: this.state.noofPeopleArray,
            dark: false,
            cancelButtonIndex: 12,
            // destructiveButtonIndex: 1,
        }, (value) => {
            // if (value >= this.state.totalData.length) {  
            //   console.log("array",this.state.myArray)
            //   return;
            // }
            // else {
            this.setState({
                "numberofPeople": value
            })
            console.log("value", value);

        });
    }

    _onPressBookingList = async () => {
        Actions.BookingsList();
    }

    // _onPressRestaurantBooking = async () => {
    //     if (this.state.restaurantId == "") {
    //         Alert.alert('Choose restaurant');
    //     } else {
    //         if (this.state.bookingDate == "") {
    //             Alert.alert('Enter date ');
    //         } else {
    //             if (this.state.timeslotId == "") {
    //                 Alert.alert('Enter time slot');
    //             } else {
    //                 if (this.state.numberofPeople == "") {
    //                     Alert.alert('Enter number of people');
    //                 } else {
    //                     let overalldetails = {
    //                         "RestaurantId": this.state.restaurantId,
    //                         "Date": this.state.bookingDate,
    //                         "TimeSlotId": this.state.timeslotId,
    //                         "NoofPeople": this.state.numberofPeople,
    //                         "PreferenceNote": this.state.preferenceNote,
    //                         "AllergyNote": this.state.allergyNote,
    //                         "UserId": this.state.userId
    //                     }
    //                     console.log('overalldetails', overalldetails)
    //                     await apiCallWithUrl(APIConstants.PostRestaurantBooking, 'POST', overalldetails, this.postRestaurantResponse);
    //                 }
    //             }
    //         }
    //     }
    // }

    // postRestaurantResponse = async (response) => {
    //     if (response.IsException == null) {
    //         var postRestaurantResponse = response.ResponseData;
    //         console.log('postUserResponse', postRestaurantResponse);
    //         //Actions.ListRestaurantBookings();
    //         Actions.BookingsList();

    //         // Alert.alert('Booked Succesfully');
    //     }
    // }


    _loadDropdowns = async () => {
        if (this.props.BookingIndex !== undefined) {
            this.setState({
                loading: true,
                selectedIndex: this.props.BookingIndex
            });
            console.log("props", this.props.BookingIndex)
        }

        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        await this.setState({ userId: jsonValue.UserId });

        await apiCallWithUrl(APIConstants.GetRestaurantMasters, 'GET', '', this.getDropdownsResponse);
    }

    getDropdownsResponse = async (response) => {
        // this.setState({ totalData: response.ResponseData, "selctedLanguage": response.ResponseData[0].NativeName, })
        this.setState({ totalData: response.ResponseData })

        console.log("totalResponse", response.ResponseData);
        console.log("haaa...", response.ResponseData[0]);
        var list1 = [];
        var list2 = [];
        response.ResponseData[0].map((restaurants, index) => {
            console.log("loop", index, restaurants.value);
            list1.push(restaurants.value);

        });
        this.setState({ restaurantArray: list1 })

        response.ResponseData[1].map((timeslot, index) => {
            console.log("loop", index, timeslot.value);
            list2.push(timeslot.value);

        });
        this.setState({ timeslotArray: list2 })

        console.log("restaurantArray", this.state.restaurantArray);
        console.log("timeslotArray", this.state.timeslotArray);
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        // flexDirection: 'column'
    },
    tabStyle: {
        //backgroundColor: "#FFFFFF",
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabStyle: {
        //backgroundColor: Colors.TabActive,
        backgroundColor: Colors.App_Font,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabTextStyle: {
        //color: Colors.Black,
        color: Colors.Background_Color,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },

});

module.exports = Bookings;