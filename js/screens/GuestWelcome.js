import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    CheckBox,
    AsyncStorage,
    Image,
    ImageBackground
} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import { Actions } from 'react-native-router-flux';

class GuestWelcome extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        // this.onLoadIntialState();
    }

    // onLoadIntialState = async () => {
    //     const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
    //     var userJson = JSON.parse(userdetails);
    //     guestName = userJson.Fullname;
    //     guestEmail = userJson.EmailId;
    //     console.log("userdetails guest welcome", userdetails, guestName, guestEmail);
    // }

    render() {
        // console.log("guestName,guestEmail", guestName, guestEmail)
        return (
            // <View style={styles.container}>
            //     <View style={styles.modelbox}>
            //         <Text style={styles.titlename}>Mr./ Ms./ Mrs. {this.props.checkedInGuestDetails.primaryGuestName}</Text>
            //         <Text style={styles.title}>{I18n.t('preskilapp')}</Text>
            //         <Text style={styles.innerText}>{I18n.t('modeldesc1')}{this.props.checkedInGuestDetails.email}{I18n.t('modeldesc2')}</Text>

            //         {/* <View style={{ padding: 10 }}>
            //             <Text style={styles.innerText}>{I18n.t('load')}</Text>
            //         </View> */}

            //         <View style={styles.guestbtn}>
            //             <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onGuestCheckInButton}>
            //                 <Text style={styles.btntext}>{I18n.t('ok')}</Text>
            //             </TouchableOpacity>
            //         </View>
            //     </View>
            // </View>
            <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>
            <ImageBackground source={require('../../assets/images/SouthernGross_img0.png')} style={{flex: 1,resizeMode: 'cover',justifyContent: "center",flexDirection: 'column'}}>

            <View style={styles.modelbox}>
                    <Text style={styles.titlename}>{this.props.checkedInGuestDetails.primaryGuestName}</Text>
                    <Text style={styles.title}>{I18n.t('preskilapp')}</Text>
                    <Text style={styles.innerText}>{I18n.t('modeldesc1')}{this.props.checkedInGuestDetails.email}{I18n.t('modeldesc2')}</Text>

                    {/* <View style={{ padding: 10 }}>
                        <Text style={styles.innerText}>{I18n.t('load')}</Text>
                    </View> */}

                    <View style={styles.guestbtn}>
                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onGuestCheckInButton}>
                            <Text style={styles.btntext}>{I18n.t('ok')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
            </View>
        )
    }

    _onGuestCheckInButton = async () => {
        // let userdetails = {
        //     Fullname: "Administrator",
        //     Password: null,
        //     UserId: "1",
        //     Username: "admin"
        // }
        // AsyncStorage.setItem(Keys.UserDetail, JSON.stringify(userdetails));
         Actions.CustomerHome();
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    title: {
        color: Colors.Background_Color,
        fontSize: 22,
        fontFamily: AppFont.Regular,
        paddingVertical: 15,
        textAlign: "center",
    },
    titlename: {
        color: Colors.Background_Color,
        fontSize: 16,
        fontFamily: AppFont.Bold,
        paddingVertical: 15,
        textAlign: "center"
    },
    innerText: {
        color: Colors.Background_Color,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        textAlign: 'center',
        lineHeight: 30,
    },
    modelbox: {
        paddingHorizontal: 30,
        paddingVertical: 30,
        borderWidth: 0.5,
        margin: 20,
        marginTop: 50,
        borderRadius: 5,
        borderColor: Colors.ModelView,
        padding: 20,
        backgroundColor: '#00000070',
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    guestbtn: {
        width: "100%",
        paddingTop: 25,
        alignItems: "center"
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.ButtonBlue,
        padding: 10,
        width: "100%",
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 25
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },

})

module.exports = GuestWelcome;