import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    TextInput
} from 'react-native';
import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import I18n from '../constants/i18n';
import { Actions } from 'react-native-router-flux';
import Moment from 'moment';
class CheckInDetails extends Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     bookingReference: 'BO-9807-9485-0380',
        //     fullname: 'Kishan',
        //     checkInDateTime: 'Saturday 20-Apr-2019, 12:45 PM',
        //     checkOutDateTime: 'Wednesday 24-Apr-2019, 12:00 PM',
        //     numberofDays: '4',
        //     totalNoofGuests: '3',
        //     emailId: 'kisham@gmail.com',
        //     phonenumber: '+230 5 123 4567',
        //     country: 'Mauritius'
        // }
        // console.log(this.props.checkInDetails);

        this.state = {
            bookingReference: this.props.checkInDetails.bookingId,
            fullname: this.props.checkInDetails.primaryGuestName,
            checkInDateTime: Moment(this.props.checkInDetails.CheckInDateTime).format('lll'),
            checkOutDateTime: Moment(this.props.checkInDetails.CheckOutDateTime).format('lll'),
            numberofDays: this.props.checkInDetails.numberOfDays,
            totalNoofGuests: this.props.checkInDetails.numberOfGuest,
            emailId: this.props.checkInDetails.Email,
            phonenumber: this.props.checkInDetails.contactnumber,
            country: this.props.checkInDetails.CountryName
        }
    }
    render() {
        return (
            <ScrollView>
                <TableView contentContainerStyle={styles.container}>
                    <Section sectionPaddingTop={0} sectionPaddingBottom={100} sectionTintColor={Colors.Background_Color} separatorTintColor={Colors.Background_Color}>
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('bookingreference')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.bookingReference}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ bookingReference: text })}
                                            value={this.state.bookingReference}
                                            placeholder="Booking Reference" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('fullname')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.fullname}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.fullname}
                                            placeholder="Your Name" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('checkindatetime')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.checkInDateTime}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.checkInDateTime}
                                            placeholder="Check-In Date" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('checkoutdatetime')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.checkOutDateTime}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.checkOutDateTime}
                                            placeholder="Check-Out Date" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('numberOfDays')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.numberofDays}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.numberofDays}
                                            placeholder="Number of days" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('totalguests')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.totalNoofGuests}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.totalNoofGuests}
                                            placeholder="Total number of guest" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('emailid')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.emailId}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.emailId}
                                            placeholder="Email" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('phonenumber')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.phonenumber}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.phonenumber}
                                            placeholder="Phone Number" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="" placeholder="" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        {I18n.t('country')}
                                    </Text>
                                    <View style={styles.cellrow}>
                                        <Text style={styles.textDisplay}>{this.state.country}</Text>
                                        {/* <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            onChangeText={(text) => this.setState({ fullname: text })}
                                            value={this.state.country}
                                            placeholder="Country" /> */}
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="Confirm" placeholder="Confirm" id="6" cellContentView={
                            <View style={{ flex: 1, marginTop: 40, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 20 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressCheckInDetails}>
                                            <Text style={styles.btntext}>{I18n.t('confirmbtn')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } />
                    </Section>
                </TableView>
            </ScrollView >
        );
    }

    _onPressCheckInDetails = () => {
        //Actions.GuestWelcome({ checkedInGuestDetails: this.props.checkInDetails });
        Actions.CustomerHome();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background_Color
    },
    label: {
        //flex: 1,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        // marginRight:8,
        includeFontPadding: false,
    },
    cellrow: {
        marginTop: 12,
        marginBottom: 7,
        width: null
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    textDisplay: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        // height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        // marginRight:8,
        includeFontPadding: false,
    },
});

export default CheckInDetails;