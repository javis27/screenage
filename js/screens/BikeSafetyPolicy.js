import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Image,
    AsyncStorage,
    Dimensions,
    Alert
} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
// import Status from '../constants/Status';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class BikeSafetyPolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            typeId: this.props.disclaimerType,
            disclaimer: {},
            isSelected: false,
            bikeSafetyloading: false,
            IsfetchData: false,
        }
    }

    componentWillMount() {
        this.onLoadDisclaimer();
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.bikeSafetyloading} />
                {this.state.IsfetchData ?
                    <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                        <View>
                            <View style={styles.Restaurant}>
                                <View style={{ alignItems: "flex-start", flex: 1 }}>
                                    {this.state.disclaimer.DisclaimerTitle != "" ?
                                        <HTML
                                            html={this.state.disclaimer.DisclaimerTitle}
                                            tagsStyles={{ fontFamily: AppFont.Regular }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, fontSize: 18, alignItems: "center", color: Colors.App_Font }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                        // style={{ color: "#FFFFFF", fontFamily: AppFont.Regular, fontSize: 26, textAlign: 'center', textTransform: 'uppercase' }}
                                        />
                                        : null}
                                    {this.state.disclaimer.Contents != "" ?
                                        <HTML
                                            html={this.state.disclaimer.Contents}
                                            tagsStyles={{ fontFamily: AppFont.Regular, }}
                                            baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                            ignoredStyles={["font-family", "letter-spacing"]}
                                            imagesMaxWidth={Dimensions.get('window').width}
                                        />
                                        : null}
                                </View>
                            </View>

                            {/* <View>
                                <TouchableOpacity onPress={() => this.onPressAgree()} style={{ backgroundColor: 'white', margin: 10, flex: 0.5, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image style={{
                                        resizeMode: 'contain',
                                        width: 16,
                                        height: 16,
                                        marginHorizontal: 5,
                                    }} source={{ uri: this.state.isSelected ? 'check_on' : 'check_off' }} />
                                    <Text style={styles.AgreeText}> I agree terms and conditions apply here</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.DescriptionServiceList}>
                                <View style={{ flex: 0.5, paddingTop: 7, paddingBottom: 15 }}>
                                    <View style={{ flex: 1, alignItems: 'center', fontFamily: AppFont.Regular }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.ServiceTextActive} onPress={this.onSubmit}>
                                            <Text style={{ fontFamily: AppFont.Regular, color: "#FFFFFF" }}>{I18n.t('btnProceed')}</Text></TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ flex: 0.5, paddingTop: 7, paddingBottom: 15 }}>
                                    <View style={{ flex: 1, alignItems: 'center', fontFamily: AppFont.Regular }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.ServiceText} onPress={this.onBack}>
                                            <Text style={{ fontFamily: AppFont.Regular }}>{I18n.t('btnBack')}</Text></TouchableOpacity>
                                    </View>
                                </View>
                            </View> */}
                        </View>
                    </ScrollView>
                    :
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", height: Dimensions.get('window').height, width: Dimensions.get('window').width, }}>
                        {this.state.bikeSafetyloading ? null
                            : <Text style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', fontFamily: AppFont.Regular }}>{I18n.t('nodata')}</Text>}

                    </View>
                }

            </View >
        )
    }

    onPressAgree = () => {
        console.log("isSelected", this.state.isSelected)
        this.setState({ isSelected: !this.state.isSelected })
    }
    onSubmit = () => {
        console.log("isSelected", this.state.isSelected)
        if (this.state.isSelected) {
            // alert('Your Booking Successfully.',this.state);
            this.props.callbackBikeDisclaimerFunc(this.state);
            Actions.pop();
            //Actions.pop({ refresh:{disclaimerTypeId:this.state.typeId,agreeTerms:this.state.isSelected } })
            //Actions.pop({ refresh: { Clear: Math.random() * 1000000, disclaimerTypeId:this.state.typeId,agreeTerms:this.state.isSelected } })
        }
        else {
            alert('You must agree to the terms first.');
            return false;
        }
    }

    onBack = () => {
        Actions.pop();
    }

    onLoadDisclaimer = async () => {
        this.setState({ bikeSafetyloading: true })
        const languageId = await AsyncStorage.getItem(Keys.langId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId":languageId,
            "DisclaimerTypeId":this.state.typeId
          }
          await apiCallWithUrl(APIConstants.GetDisclaimerAPI, 'POST', overalldetails, (callback) => { this.getDisclaimerResponse(callback) });
        //apiCallWithUrl(APIConstants.GetDisclaimerAPI + "?LanguageId=" + languageId + "&&DisclaimerTypeId=" + this.state.typeId, 'GET', "", this.getDisclaimerResponse)
    }

    getDisclaimerResponse = async (response) => {
        await this.setState({
            bikeSafetyloading: false,
        });

        if (response.ResponseData.length != 0) {
            var disclaimerResponse = response.ResponseData;
            console.log('disclaimer response', disclaimerResponse[0].Contents)

            await this.setState({
                disclaimer: disclaimerResponse === null ? '' : disclaimerResponse[0],
                IsfetchData: true
            })
            console.log('state disclaimer', this.state.disclaimer)
            console.log("length", response.ResponseData.length);
        }

    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%',
    },
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        paddingVertical: 8,
        paddingHorizontal: 15,
        marginTop: 10
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 15,
        marginTop: 10,
        marginBottom: 25
    },
    AgreeText: {
        color: Colors.Black,
        fontSize: 15,
        fontFamily: AppFont.Regular
    },
    ServiceText: {
        color: Colors.Black,
        fontSize: 15,
        backgroundColor: Colors.FullGray,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        width: '75%',
        fontFamily: AppFont.Regular,
        borderRadius: 5
    },
    ServiceTextActive: {
        color: "#FFFFFF",
        fontSize: 15,
        backgroundColor: Colors.App_Font,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        width: '75%',
        fontFamily: AppFont.Regular,
        borderRadius: 5
    },

})

module.exports = BikeSafetyPolicy;