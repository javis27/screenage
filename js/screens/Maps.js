import React from 'react';
import {
    Platform,
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    StatusBar
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import OfflineMaps from '../components/OfflineMaps';
import OnlineMaps from '../components/OnlineMaps';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

import BottomSheet from 'react-native-bottomsheet';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import SegmentedControlTab from "react-native-segmented-control-tab";


class Maps extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: 0,
            mapRegion: {
                latitude: null,
                longitude: null,
                // latitudeDelta: 1,
                // longitudeDelta: 1
              }
        };
    }

    componentWillMount() {
        this.loadMapDetails();
        this.onLoadGeoLocationDetails();
    }

    loadMapDetails = async () => {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            console.log("wokeeey");
            console.log("wokeeey", position);
            // this.setState({
            //   latitude: position.coords.latitude,
            //   longitude: position.coords.longitude,
            //   error: null,
            // });
            this.setState({
              mapRegion: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 1,
                longitudeDelta: 1
              }
            });
           // AsyncStorage.setItem(Keys.mapDetail, JSON.stringify(this.state.mapRegion));
    
          },
          (error) => this.setState({ error: error.message }),
          { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    
    
    
      }

    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };

    updateTabBar = () => {

        if (this.state.selectedIndex == 0) {
            return (<OfflineMaps />);
        }
        else if (this.state.selectedIndex == 1) {
            return (<OnlineMaps mapData={this.state.mapRegion} />
            );
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <View>
                    <SegmentedControlTab
                        tabsContainerStyle={styles.tabsContainerStyle}
                        tabStyle={styles.tabStyle}
                        firstTabStyle={styles.firstTabStyle}
                        lastTabStyle={styles.lastTabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                        values={[I18n.t('offlinemaps'), I18n.t('onlinemaps')]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />
                </View>

                {/* Restaurant Start */}

                {this.updateTabBar()}

                {/* Restaurant End */}

                {/* SPA Start */}

                {/* SPA Start */}

            </View>

        );
    }

    onLoadGeoLocationDetails = async () => {
        this.getLocationDetails();
        var langId = await AsyncStorage.getItem(Keys.langId);
        //await apiCallWithUrl(APIConstants.GeoLocationAPI + "?LanguageId=" + langId, 'GET', '', this.getGeoLocationResponse);
    }

    getGeoLocationResponse = (response) => {
        console.log('getGeoLocationResponse', response)
    }

    getLocationDetails = async () => {
        const mapDetails = await AsyncStorage.getItem(Keys.mapDetail);
        var jsonMapValues = JSON.parse(mapDetails);
        console.log("map", jsonMapValues);
        await this.setState({
          mapRegion: jsonMapValues
        })
      }

}

const styles = StyleSheet.create({
    tabStyle: {
        backgroundColor: "#FFFFFF",
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabStyle: {
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
});

module.exports = Maps;