import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Image,
    AsyncStorage,
    Dimensions
} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';


import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class TermsandConditions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            typeId: this.props.disclaimerType,
            disclaimer: {},
            isSelected: false,
            loading: false,
        }
    }

    componentWillMount() {
        this.onLoadDisclaimer();
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                <Loader loading={this.state.loading} />
                    <View style={styles.Restaurant}>
                        <View style={{ alignItems: "flex-start", flex: 1 }}>
                            <HTML
                                html={this.state.disclaimer.DisclaimerTitle}
                                tagsStyles={{ fontFamily: AppFont.Regular }}
                                baseFontStyle={{ fontFamily: AppFont.Regular, fontSize: 18, alignItems: "center", color: Colors.App_Font }}
                                ignoredStyles={["font-family", "letter-spacing"]}
                                imagesMaxWidth={Dimensions.get('window').width}
                            />
                            <HTML
                                html={this.state.disclaimer.Contents}
                                tagsStyles={{ fontFamily: AppFont.Regular, }}
                                baseFontStyle={{ fontFamily: AppFont.Regular, }}
                                ignoredStyles={["font-family", "letter-spacing"]}
                                imagesMaxWidth={Dimensions.get('window').width}
                            />
                        </View>
                    </View>

                    {/* <View>
                        <TouchableOpacity onPress={() => this.onPressAgree()} style={{ backgroundColor: 'white', margin: 10, flex: 0.5, flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{
                                resizeMode: 'contain',
                                width: 16,
                                height: 16,
                                marginHorizontal: 5,
                            }} source={{ uri: this.state.isSelected ? 'check_on' : 'check_off' }} />
                            <Text style={styles.AgreeText}> * I agree terms and conditions apply here </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.DescriptionServiceList}>
                        <View style={{ flex: 0.5, paddingTop: 7, paddingBottom: 15 }}>
                            <View style={{ flex: 1, alignItems: 'center', fontFamily: AppFont.Regular }}>
                                <TouchableOpacity activeOpacity={.5} style={styles.ServiceTextActive} onPress={this.onSubmit}>
                                    <Text style={{ fontFamily: AppFont.Regular, color: "#FFFFFF" }}>Proceed</Text></TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.5, paddingTop: 7, paddingBottom: 15 }}>
                            <View style={{ flex: 1, alignItems: 'center', fontFamily: AppFont.Regular }}>
                                <TouchableOpacity activeOpacity={.5} style={styles.ServiceText} onPress={this.onBack}>
                                    <Text style={{ fontFamily: AppFont.Regular }}>Back</Text></TouchableOpacity>
                            </View>
                        </View>
                    </View> */}
                </ScrollView>
            </View>
        )
    }

    onPressAgree = () => {
        console.log("isSelected", this.state.isSelected)
        this.setState({ isSelected: !this.state.isSelected })
    }
    onSubmit = () => {
        console.log("isSelected", this.state.isSelected)
        if (this.state.isSelected) {
            // alert('Your Booking Successfully.',this.state);
            this.props.callbackBikeDisclaimerFunc(this.state);
            Actions.pop();
            //Actions.pop({ refresh:{disclaimerTypeId:this.state.typeId,agreeTerms:this.state.isSelected } })
            //Actions.pop({ refresh: { Clear: Math.random() * 1000000, disclaimerTypeId:this.state.typeId,agreeTerms:this.state.isSelected } })
        }
        else {
            alert('You must agree to the terms first.');
            return false;
        }
    }

    onBack = () => {
        Actions.pop();
    }

    onLoadDisclaimer = async () => {
        this.setState({ loading: true })
        const languageId = await AsyncStorage.getItem(Keys.langId);
        apiCallWithUrl(APIConstants.GetDisclaimerAPI + "?LanguageId=" + languageId + "&&DisclaimerTypeId=" + this.state.typeId, 'GET', "", this.getDisclaimerResponse)
    }

    getDisclaimerResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response.IsException == null) {
            var disclaimerResponse = response.ResponseData;
            console.log('disclaimer response', disclaimerResponse[0].Contents)

            await this.setState({
                disclaimer: disclaimerResponse === null ? '' : disclaimerResponse[0]
            })
            console.log('state disclaimer', this.state.disclaimer)
        }
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        paddingVertical: 8,
        paddingHorizontal: 15,
        marginTop: 10
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 15,
        marginTop: 10,
        marginBottom: 25
    },
    AgreeText: {
        color: Colors.Black,
        fontSize: 15,
        fontFamily: AppFont.Regular
    },
    ServiceText: {
        color: Colors.Black,
        fontSize: 15,
        backgroundColor: Colors.FullGray,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        width: '75%',
        fontFamily: AppFont.Regular
    },
    ServiceTextActive: {
        color: "#FFFFFF",
        fontSize: 15,
        backgroundColor: Colors.App_Font,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        width: '75%',
        fontFamily: AppFont.Regular
    },

})

module.exports = TermsandConditions;