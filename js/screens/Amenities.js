import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
    StatusBar,
    Platform,
    Alert,
    FlatList
} from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Status from '../constants/Status';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { Actions } from 'react-native-router-flux';

class Amenities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            roomNo: '',
            loading: false
        }
    }

    componentWillMount() {
        this.loadAmenities();
        this.verifyCheckInDetails();
    }

    loadAmenities = async () => {
        await this.setState({ loading: true });
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);

        var HotelId = await AsyncStorage.getItem('HotelId');
       
     
        let overalldetails = {
            "HotelId": HotelId,
        }
        console.log("overalldetails", overalldetails);
        await apiCallWithUrl(APIConstants.GetAmenitiesMasterAPI, 'POST', overalldetails, this.getAmenitiesListResponse);
    }
    getAmenitiesListResponse = async (response) => {
        console.log("get amenities response", response);
        if (response.ResponseData.length > 0) {
            console.log("get amenities response", response.ResponseData);
            var amenitiesList = response.ResponseData;
            await this.setState({ amenitiesArray: amenitiesList, IsAmenitiesResponse: true, isRefreshing: false })
        }
        this.setState({ loading: false })
    }
    verifyCheckInDetails = async () => {
        await this.setState({ loading: true });
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        let overalldetails = {
            "CheckInId": checkInVal,
        }
        console.log("overalldetails", overalldetails);
        await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = (response) => {
        if (Platform.OS === 'ios') {
            this.setState({ loading: false }, () => {
                setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
            });
        } else {
            this.setState({
                loading: false,
            }, () => this.funcCheckInDetailsResponse(response));
        }

        // await this.setState({ loading: false });
        // var postCheckInDetailResponse = response.ResponseData;
        // console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
        // if (response.IsException == "True") {
        //     return Alert.alert(response.ExceptionMessage);
        // }

        // if (postCheckInDetailResponse.InCustomer == "False") {
        //     console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
        //     Alert.alert(I18n.t('alertnotcheckin'));
        //     AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        //     AsyncStorage.removeItem(Keys.UserDetail);
        //     AsyncStorage.removeItem(Keys.roomNo);
        //     AsyncStorage.removeItem(Keys.inCustomer);
        //     AsyncStorage.removeItem(Keys.checkInId);
        //     Actions.Login();
        // }
    }

    funcCheckInDetailsResponse = async (response) => {
        var postCheckInDetailResponse = response.ResponseData;
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        if (postCheckInDetailResponse !== null) {
            console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
            if (postCheckInDetailResponse.InCustomer == "False") {
                console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
                Alert.alert(I18n.t('alertnotcheckin'));
                AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
                AsyncStorage.removeItem(Keys.UserDetail);
                AsyncStorage.removeItem(Keys.roomNo);
                AsyncStorage.removeItem(Keys.inCustomer);
                AsyncStorage.removeItem(Keys.checkInId);
                Actions.GuestLogin();
            }
        }
    }

    render() {
        console.log("waterbottle", I18n.t('waterbottleid'))
        const renderItem = ({ item }) => (
            <View style={styles.menuWrapper}>
                            <TouchableOpacity activeOpacity={.5} onPress={this.onPressAmenities.bind(this, item)}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                                    <Image source={{ uri: item.ImagePath }} style={{ width: 80, height: 80 }} />
                                </View>
                                <Text style={styles.IconText}>{item.Amenity}</Text>
                            </TouchableOpacity>
                        </View>
          );
        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading} />
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <ScrollView contentContainerStyle={styles.scrollViewContainer}>
                    <View>
                        {/* <View style={styles.menuWrapper}>
                            <TouchableOpacity activeOpacity={.5} onPress={this.onPressAmenities.bind(this, I18n.t('waterbottleid'))}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                                    <Image source={{ uri: 'bottle' }} style={{ width: 45, height: 45 }} />
                                </View>
                                <Text style={styles.IconText}>{I18n.t('waterbottle')}</Text>
                            </TouchableOpacity>
                        </View> */}
                        <FlatList
                        data={this.state.amenitiesArray}
                        renderItem={renderItem}
                        numColumns={3}
                        keyExtractor={item => item.Id}
                    />
                        {/* <View style={styles.menuWrapper}>
                            <TouchableOpacity activeOpacity={.5} onPress={this.onPressAmenities.bind(this, I18n.t('towelid'))}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                                    <Image source={{ uri: 'towel' }} style={{ width: 45, height: 45 }} />
                                </View>
                                <Text style={styles.IconText}>{I18n.t('towel')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.menuWrapper}>
                            <TouchableOpacity activeOpacity={.5} onPress={this.onPressAmenities.bind(this, I18n.t('soapid'))}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                                    <Image source={{ uri: 'soap' }} style={{ width: 45, height: 45 }} />
                                </View>
                                <Text style={styles.IconText}>{I18n.t('soap')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.menuWrapper}>
                            <TouchableOpacity activeOpacity={.5} onPress={this.onPressAmenities.bind(this, I18n.t('minibarid'))}>
                                <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                                    <Image source={{ uri: 'minibar' }} style={{ width: 45, height: 45 }} />
                                </View>
                                <Text style={styles.IconText}>{I18n.t('minibar')}</Text>
                            </TouchableOpacity>
                        </View> */}
                        

                    </View>
                    {/* <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center", marginBottom: 25 }}>
                        <TouchableOpacity activeOpacity={.5} onPress={this._onPressAmenitiesList}>
                            <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('requesthistory')}</Text>
                        </TouchableOpacity>
                    </View> */}
                </ScrollView>
            </View>
        )
    }

    _onPressAmenitiesList = async () => {
        Actions.AmenitiesList();
    }

    onPressAmenities = (item) => {
        console.log("item", item);
        var currentResponseName = null;
        // switch (id) {
        //     case Status.amenitiesTypes.water.id:
        //         currentResponseName = Status.amenitiesTypes.water.name
        //         break
        //     case Status.amenitiesTypes.towel.id:
        //         currentResponseName = Status.amenitiesTypes.towel.name
        //         break
        //     case Status.amenitiesTypes.soap.id:
        //         currentResponseName = Status.amenitiesTypes.soap.name
        //         break
        //     case Status.amenitiesTypes.minibar.id:
        //         currentResponseName = Status.amenitiesTypes.minibar.name
        //         break
        //     case Status.amenitiesTypes.waterFrench.id:
        //         currentResponseName = Status.amenitiesTypes.waterFrench.name
        //         break
        //     case Status.amenitiesTypes.towelFrench.id:
        //         currentResponseName = Status.amenitiesTypes.towelFrench.name
        //         break
        //     case Status.amenitiesTypes.soapFrench.id:
        //         currentResponseName = Status.amenitiesTypes.soapFrench.name
        //         break
        //     case Status.amenitiesTypes.minibarFrench.id:
        //         currentResponseName = Status.amenitiesTypes.minibarFrench.name
        //         break
        // }
        Alert.alert(I18n.t('alertconfirmrequest') + ' ' +item.Amenity +'?', '',
            [
                {
                    text: I18n.t('no_dialog'),
                    onPress: () => console.log('Cancel Pressed'),
                    //style: 'cancel'
                },
                {
                    text: I18n.t('yes_dialog'),
                    onPress: () => this.onRequestAmenities(item.AmenityId),
                    //style: 'destructive'
                },
            ],
            {
                cancelable: false
            }
        )
    }

    onRequestAmenities = async (amenityId) => {
        var today = new Date();
        var todayDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var todayTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var requestedDateTime = todayDate + ' ' + todayTime;

        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        const checkInId = await AsyncStorage.getItem(Keys.checkInId);
        await this.setState({ userId: jsonValue.UserId, roomNo: roomNo });
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("amenities", this.state.userId, this.state.roomNo, checkInId)

        if (this.state.categoryId == "") {
            Alert.alert(I18n.t("servicerequestchoose"));
        } else {
            this.setState({ loading: true })
            
            let overalldetails = {
                "AmenityId": amenityId,
                "CreatedBy": this.state.userId,
                "RoomNo": this.state.roomNo,
                "CheckInId": checkInId,
                "ApprovalStatusId": 1,
                "HotelId": HotelId,
                "BookingDateTime":requestedDateTime
            }
            console.log('overalldetails', overalldetails)
            await apiCallWithUrl(APIConstants.PostAmenities, 'POST', overalldetails, this.postAmenitiesResponse);
        }
    }

    postAmenitiesResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response.IsException == null) {
            var postAmenitiesResponse = response.ResponseData;
            console.log('postAmenitiesResponse', postAmenitiesResponse);
            setTimeout(function () {

                //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
                Alert.alert(I18n.t('alertrequest'));

            }, 1000);
            Actions.pop({ refresh: true });
            // Actions.refresh({ key: Math.random() * 1000000 })
            setTimeout(() => {
                Actions.refresh({ key: Math.random() * 1000000 })
          }, 5)
        }

    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background_Color,
    },
    scrollViewContainer: {
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 15,
        flexDirection: 'column',
        flex: 1
        // position: "relative"
    },
    rowContainer: {
        // flexDirection: 'row',
        // flexWrap: "wrap"
    },
    menuWrapper: {
        //width: "33.3333333333%",
        // height: 120,
        alignItems: 'center',
        width:120,
        marginTop:15
    },
    IconText: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        paddingTop: 10,
        textAlign: 'center'
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
});

module.exports = Amenities;