import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    FlatList,
    AsyncStorage,
    Dimensions,
    TouchableOpacity,
    Platform,
    Alert
} from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Loader from '../components/Loader';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { Actions } from 'react-native-router-flux';

class RestaurantList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isRefreshing: false,
            restaurantsArray: [],
            isRestuarantsResponse: false,
        }
    }
    componentWillMount() {
        this.loadRestaurants();
        //  this.verifyCheckInDetails();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = (response) => {
    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this.loadRestaurants();
    //         }
    //     }
    // }

    renderEmptyComponent = () => {
        if (this.state.restaurantsArray.length != 0) return null;
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center", height: Dimensions.get('window').height - 150, width: Dimensions.get('window').width, }}>
                <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', fontFamily: AppFont.Regular }}>{I18n.t('nodata')}</Text>
            </View>
        );
    };
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader
                    loading={this.state.loading} />
                <View>
                    <FlatList
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefreshRestaurants}
                        data={this.state.restaurantsArray}
                        keyExtractor={(x, i) => i}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxWithShadow}><View style={styles.listcontainer}>
                                    <TouchableOpacity activeOpacity={.5} onPress={() => this.onPressRestaurantServices(item)}>
                                        <View style={styles.restaurantName}>
                                            <Text style={styles.ReqText}>
                                                {item.RestaurantName}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View ></View>
                            )
                        }}
                    />
                </View>
            </View>
        );
    }

    onRefreshRestaurants = () => {
        this.setState({ isRefreshing: true });
        this.loadRestaurants();
    }

    onPressRestaurantServices = (item) => {
        console.log("onpressrestaurantServices", item);
        Actions.Services({ restaurantDetails: item });
    }

    loadRestaurants = async () => {
        this.setState({ loading: true });
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        
        let overalldetails = {
            "HotelId": HotelId
          }
          await apiCallWithUrl(APIConstants.GetRestaurantMasters, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
        //await apiCallWithUrl(APIConstants.GetRestaurantListAPI + "?CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } 
        else {
            this.getRestaurantResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getRestaurantResponse = async (response) => {
        console.log("get Restaurant response", response);
        if (response.ResponseData.length > 0) {
            console.log("get Restaurant response", response.ResponseData);
            var restaurantsList = response.ResponseData;
            await this.setState({ restaurantsArray: restaurantsList, isRestuarantsResponse: true, isRefreshing: false })
        }
        this.setState({ loading: false })
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    listcontainer: {
        flex: 1,
        flexDirection: 'column',
    },
    restaurantName: {
        paddingHorizontal: 15,
        paddingVertical: 15,
    },
    ReqText: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
    },
})

module.exports = RestaurantList;
