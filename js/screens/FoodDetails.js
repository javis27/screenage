import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground,
  ScrollView
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import NumericInput from 'react-native-numeric-input';
import Status from '../constants/Status';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';
import SpaBooking from '../components/SpaBooking';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      responsiveItemTypeImageURI: Status.serviceItemType.Veg.image,
      responsiveSpiceLevelImageURI: Status.SpiceLevelStatus.High.image,
      resSpiceImgWidth: Status.SpiceLevelStatus.Low.imgWidth,
      resChiliImgFlex: Status.SpiceLevelStatus.Low.imgFlex,
      resChiliTextFlex: Status.SpiceLevelStatus.Low.textFlex,
      serviceListArray:[],
      qty:1
  }
  }
  componentDidMount() {
    this._loadServices();
}
  componentWillMount() {
    var ItemTypeImage = null;
    var spiceLevelImage = null;
    var spiceImgWidth = null;
    var chiliImgFlex = null;
    var chiliTextFlex = null;

    if (this.props.Details.ItemType.toLowerCase() == Status.serviceItemType.Veg.type) {
        ItemTypeImage = Status.serviceItemType.Veg.image
        console.log("spicelevel", this.props.Details.SpicyLevel)
    } else if (this.props.Details.ItemType.toLowerCase() == Status.serviceItemType.Nonveg.type) {
        ItemTypeImage = Status.serviceItemType.Nonveg.image
        console.log("spicelevel", this.props.Details.SpicyLevel)
        
    }

    switch (this.props.Details.SpicyLevel) {
        case Status.SpiceLevelStatus.Low.spiceLevel:
            spiceLevelImage = Status.SpiceLevelStatus.Low.image
            spiceImgWidth = Status.SpiceLevelStatus.Low.imgWidth
            chiliImgFlex = Status.SpiceLevelStatus.Low.imgFlex
            chiliTextFlex = Status.SpiceLevelStatus.Low.textFlex
            console.log("spiceLevelImage Low", spiceLevelImage);
            break
        case Status.SpiceLevelStatus.Medium.spiceLevel:
            spiceLevelImage = Status.SpiceLevelStatus.Medium.image
            spiceImgWidth = Status.SpiceLevelStatus.Medium.imgWidth
            chiliImgFlex = Status.SpiceLevelStatus.Medium.imgFlex
            chiliTextFlex = Status.SpiceLevelStatus.Medium.textFlex
            console.log("spiceLevelImage Low", spiceLevelImage);
            break
        case Status.SpiceLevelStatus.High.spiceLevel:
            spiceLevelImage = Status.SpiceLevelStatus.High.image
            spiceImgWidth = Status.SpiceLevelStatus.High.imgWidth
            chiliImgFlex = Status.SpiceLevelStatus.High.imgFlex
            chiliTextFlex = Status.SpiceLevelStatus.High.textFlex
            console.log("spiceLevelImage Low", spiceLevelImage);
            break
    }


    this.setState({ responsiveItemTypeImageURI: ItemTypeImage, responsiveSpiceLevelImageURI: spiceLevelImage, resSpiceImgWidth: spiceImgWidth, resChiliImgFlex: chiliImgFlex, resChiliTextFlex: chiliTextFlex })
    console.log("whole state", this.state);
}
  render() {
    
    return (
      <ScrollView style={styles.container} >
      <View style={styles.container}>
        
        <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
        />
        <View style={{ flex: 1}}>
          <View style={{height:180}}>
            {
              this.state.serviceListArray.ItemImagePath!="" ?
            
            <ImageBackground source = {{uri: this.state.serviceListArray.ItemImagePath}} style = {{ width: '100%', height: 180}}/> :
            <ImageBackground source = {require('../../assets/images/food.jpg')} style = {{ width: '100%', height: 180}}/>
            }
          </View>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:1}}>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>{this.state.serviceListArray.ItemName}</Text>
            </View>
            {/* <View style={{flex:0.3}}>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>{this.state.serviceListArray.Amount}</Text>
            </View>
             */}
          </View>
          {/* <View>
            <Text style={{fontSize:14,fontFamily: AppFont.Regular,padding:10}}>{this.state.serviceListArray.Location}</Text>
          </View> */}
          <View style={{borderTopWidth:0.5, height:10,padding:10}}></View>
          <View>
            <Text style={{fontSize:14,fontFamily: AppFont.Regular,padding:10}}>{this.state.serviceListArray.Remarks}</Text>
          </View>
          {/* <View style={{borderTopWidth:0.5, height:10,padding:10}}></View>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:0.33,alignItems:'center'}}><Text>Spicy Level</Text></View>
            <View style={{flex:0.33}}>
            <View style={{ flex: this.state.resChiliImgFlex }}><Image source={{ uri: this.state.responsiveSpiceLevelImageURI }} style={{ width: this.state.resSpiceImgWidth, height: 18 }} /></View>
            </View>
            <View style={{flex:0.33}}>
            <Text style={styles.IconReq}>{Status.Currency_ISO_Code.Mauritius.code} {this.state.serviceListArray.Price}</Text>
            </View>
          </View>
          <View style={{flexDirection:'row', marginTop:10}}>
            <View style={{flex:0.33,alignItems:'center'}}>
            {this.state.serviceListArray.AvailabilityId != "0" ?
                                (() => {
                                    if (this.state.serviceListArray.AvailabilityId == "1") {
                                        return <View style={{ flex: 1, flexDirection: "row", paddingBottom: 10, }}>
                                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                                <View>
                                                    <Text style={[styles.AvailableText, { color: this.StatusColors(this.state.serviceListArray.AvailabilityId) }]}>{I18n.t('available')}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    }
                                    else if (this.state.serviceListArray.AvailabilityId == "2") {
                                        return <View style={{ flex: 1, flexDirection: "row", paddingBottom: 10, }}>
                                            <View style={{justifyContent: 'center', alignItems: 'flex-end' }}>
                                                <View>
                                                    <Text style={[styles.AvailableText, { color: this.StatusColors(this.state.serviceListArray.AvailabilityId) }]}>{I18n.t('notavailable')}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    }
                                })()
                                : null}
            </View>
            <View style={{flex:0.33}}>
            
            </View>
            <View style={{flex:0.33}}>
            <NumericInput
                                    value={this.state.qty}
                                    onChange={qty => this.setState({ qty })}
                                    onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                                    minValue={1}
                                    maxValue={50}
                                    totalWidth={80}
                                    totalHeight={30}
                                    iconSize={10}
                                    step={1}
                                    valueType='real'
                                    rounded
                                    textColor='#000000'
                                    borderColor={Colors.App_Font}
                                    iconStyle={{ color: 'white' }}
                                    rightButtonBackgroundColor={Colors.App_Font}
                                    leftButtonBackgroundColor={Colors.App_Font} />
            </View>
          </View> */}
          
          {/* <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                    <View style={{ width: "90%", marginLeft: 15 }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressBoatBooking}>
                                                <Text style={styles.btntext}>View Cart</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
          </View> */}
        </View>
        
      </View>
      </ScrollView>
    );
  }
  _loadServices = async () => {
    this.setState({
        loading: true,
    });   // Since we are hiding 'ALL' in Menu so we are adding '+ 1' to index  
    const langId = await AsyncStorage.getItem(Keys.langId);
    var HotelId = await AsyncStorage.getItem('HotelId');
    await this.setState({ HotelId:HotelId });
    console.log("HotelId...",HotelId)
    let overalldetails = {
        "HotelId": HotelId,
        "RoomServiceId": this.props.Details.RoomServiceId,
        "LanguageId":langId,
      }
      console.log("overalldetails",overalldetails)
      await apiCallWithUrl(APIConstants.GetViewRoomServiceAPI, 'POST', overalldetails, (callback) => { this.getServiceListResponse(callback) });
}
getServiceListResponse = async (response) => {
  if (response.IsException == null) {
      var getserviceListResponse = response.ResponseData;
      console.log('get services new', getserviceListResponse);

      await this.setState({ loading: false, serviceListArray: getserviceListResponse, IsfetchResponse: true, isRefreshing: false });

  }
  this.setState({ loading: false })
}
_onPressBoatBooking = () => {
  console.log("b4cart",this.state.serviceListArray)
  let overalldetails = []
  overalldetails.push(this.state.serviceListArray)
  Actions.Cart({cartdetails: overalldetails });
}
StatusColors(item) {
  var x = item;
  var textbgColor = "";
  if (x == Status.serviceAvailability.Available.id) {
      textbgColor = Status.serviceAvailability.Available.color;
  }
  else if (x == Status.serviceAvailability.NotAvailable.id) {
      textbgColor = Status.serviceAvailability.NotAvailable.color;
  }
  return textbgColor;
}

}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    //justifyContent: 'center',
    backgroundColor: Colors.Background_Color,
    height: '100%'
},
Restaurant: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginTop: 5
},
IconReqText: {
    color: Colors.Black,
    fontSize: 16,
    fontFamily: AppFont.Regular,
},
IconReqText1: {
  color: Colors.Black,
  fontSize: 14,
  fontFamily: AppFont.Regular,
},
DescriptionServiceList: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15
},
DescriptionServiceList1: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    backgroundColor: "#8CCED6"
},
ServiceText: {
    color: Colors.Black,
    fontSize: 13,
    // paddingHorizontal: 15,
    // paddingVertical: 7,
    fontFamily: AppFont.Regular,
},
ServiceText1: {
    color: 'white',
    fontSize: 15,
    // paddingHorizontal: 15,
    // paddingVertical: 7,
    fontFamily: AppFont.Regular,
},
containerAmount: {
    // backgroundColor: Colors.FullGray,
    // borderRadius: 5,
},
boxWithShadow: {
    borderColor: Colors.Background_Gray,
    borderWidth: 0.5,
    color: '#dbdbdb',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
    marginTop: 10,
    borderRadius: 5,
    shadowOffset: { width: 2, height: 2, },
    shadowColor: Colors.LightGray,
    shadowOpacity: 0.3,
    borderRadius: 5,
},
button: {
  alignItems: 'center',
  backgroundColor: Colors.App_Font,
  padding: 10,
  width: "100%",
  marginLeft: 8,
  marginRight: 8,
  borderRadius: 5
},
btntext: {
  color: "#FFF",
  fontSize: 14,
  letterSpacing: 1,
  fontFamily: AppFont.Regular,
},
});

module.exports = Language;