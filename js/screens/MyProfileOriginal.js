import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Button,
    ScrollView,
    TextInput,
    SegmentedControlIOS,
    AsyncStorage,
    Alert,
    StatusBar,
    Platform
} from 'react-native';
import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import BottomSheet from 'react-native-bottomsheet';

import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { Actions } from 'react-native-router-flux';
import RNRestart from 'react-native-restart';


class MyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            fullname: '',
            email: '',
            mobile: '',
            country: '',
            username: '',
            textInputDisableStatus: false,
            dropdownDisableStatus: true,
            btnText: I18n.t('edit'),
            selctedLanguage: I18n.t('chooselanguage'),
            langId: '',
            myArray: [],
            selectedLanguageCode: '',
            loading: false
        }
    }
    componentDidMount() {
        this.verifyCheckInDetails();
    }

    verifyCheckInDetails = async () => {
        await this.setState({ loading: true });
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        let overalldetails = {
            "CheckInId": checkInVal,
        }
        console.log("overalldetails", overalldetails);
        await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {

        if (Platform.OS === 'ios') {
            this.setState({ loading: false }, () => {
                setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
            });
        } else {
            this.setState({
                loading: false,
            }, () => this.funcCheckInDetailsResponse(response));
        }

        // await this.setState({ loading: false });
        // var postCheckInDetailResponse = response.ResponseData;
        // console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
        // if (postCheckInDetailResponse.InCustomer == "False") {
        //     console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
        //     Alert.alert(I18n.t('alertnotcheckin'));
        //     AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        //     AsyncStorage.removeItem(Keys.UserDetail);
        //     AsyncStorage.removeItem(Keys.roomNo);
        //     AsyncStorage.removeItem(Keys.inCustomer);
        //     AsyncStorage.removeItem(Keys.checkInId);
        //     Actions.GuestLogin();
        // } else {
        //     this._loadIntitalUserDetails();
        // }
    }

    funcCheckInDetailsResponse = async (response) => {
        var postCheckInDetailResponse = response.ResponseData;
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        if (postCheckInDetailResponse !== null) {
            console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
            if (postCheckInDetailResponse.InCustomer == "False") {
                console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
                Alert.alert(I18n.t('alertnotcheckin'));
                AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
                AsyncStorage.removeItem(Keys.UserDetail);
                AsyncStorage.removeItem(Keys.roomNo);
                AsyncStorage.removeItem(Keys.inCustomer);
                AsyncStorage.removeItem(Keys.checkInId);
                Actions.GuestLogin();
            } else {
                this._loadIntitalUserDetails();
            }
        }
    }

    updateInputEmail(strEmail) {
        this.setState({ email: strEmail.trim() });
    }
    render() {
        console.log("profile state", this.state)
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader
                    loading={this.state.loading} />
                <ScrollView>
                    <TableView>
                        <Section sectionPaddingTop={0} sectionPaddingBottom={0} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                            <Cell title="Fullname" placeholder="Fullname" id="1" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {/* {I18n.t('username')} */}
                                            {I18n.t('fullname')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={this.state.textInputDisableStatus}
                                                onChangeText={(text) => this.setState({ fullname: text })}
                                                value={this.state.fullname}
                                                placeholder={I18n.t('yourname')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            {/* <Cell title="Gender" placeholder="Gender" id="2" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10, }}>
                                    <View style={{ width: "95%" }}>
                                        <Text
                                            numberOfLines={1}
                                            style={styles.label}>
                                            Gender
                                        </Text>
                                        <View>
                                            <SegmentedControlIOS
                                                values={['Male', 'Female', 'TransGender']}
                                                tintColor={Colors.App_Font}
                                                style={{ marginLeft: 20, height: 35, fontFamily: AppFont.Regular, fontSize: 15, }}
                                                selectedIndex={this.state.selectedIndex}
                                                onChange={(event) => {
                                                    this.setState({ selectedIndex: event.nativeEvent.selectedSegmentIndex });
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                            } /> */}
                            <Cell title="Email Id" placeholder="Email Id" id="3" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('emailid')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={false}
                                                // onChangeText={(text) => this.setState({ email: text })}
                                                onChangeText={this.updateInputEmail.bind(this)}
                                                value={this.state.email}
                                                placeholder={I18n.t('enteremailid')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Mobile Number" placeholder="Mobile Number" id="4" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('mobilenumber')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                keyboardType='numeric'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={this.state.textInputDisableStatus}
                                                onChangeText={(text) => this.setState({ mobile: text })}
                                                value={this.state.mobile}
                                                placeholder={I18n.t('entermobileno')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Country" placeholder="Country" id="5" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('country')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={this.state.textInputDisableStatus}
                                                onChangeText={(text) => this.setState({ country: text })}
                                                value={this.state.country}
                                                placeholder={I18n.t('country')}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Language" placeholder="Language" id="6" cellContentView={
                                // <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', paddingHorizontal: 15 }}>
                                //     <View style={styles.loginview}>
                                //         <Text
                                //             numberOfLines={1}
                                //             style={styles.label}>
                                //             Language
                                //         </Text>
                                //         <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onButtonSheet}>
                                //             <Text style={styles.btntext}>{this.state.selctedLanguage}</Text>
                                //         </TouchableOpacity>
                                //     </View>
                                // </View>
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('language')}
                                        </Text>
                                        <View>
                                            <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onButtonSheet} disabled={this.state.dropdownDisableStatus}>
                                                <Text style={styles.picker}>{this.state.selctedLanguage}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Edit" placeholder="Edit" id="6" cellContentView={
                                <View style={{ flex: 1, marginTop: 40, justifyContent: 'center' }}>
                                    <View style={{ width: "90%", marginLeft: 20 }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressUpdateUserProfile}>
                                                {/* <Text style={styles.btntext}>{I18n.t('edit')}</Text> */}
                                                <Text style={styles.btntext}>{this.state.btnText}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            } />
                            <Cell title="Change Password" hideSeparator={true} placeholder="Change Password" id="7" cellContentView={
                                <View style={{ flex: 1, height: 50, justifyContent: "center", alignItems: "center" }}>
                                    <TouchableOpacity activeOpacity={.5} onPress={this.onPressChangePassword}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('changepassword')}</Text>
                                    </TouchableOpacity>
                                </View>
                            } />
                            {/* <Cell title="Beacon Details" hideSeparator={true} placeholder="Beacon Details" id="7" cellContentView={
                                <View style={{ flex: 1, height: 50, justifyContent: "center", alignItems: "center" }}>
                                    <TouchableOpacity activeOpacity={.5} onPress={() => Actions.BeaconDetails()}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>Beacon Details</Text>
                                    </TouchableOpacity>
                                </View>
                            } /> */}
                            {/* <View style={{ height: 100 }}></View> */}
                        </Section>
                    </TableView>
                </ScrollView>
            </View>
        )
    }

    validateEmail = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            console.log("Email is Not Correct");
            this.setState({ email: text })
            return false;
        }
        else {
            this.setState({ email: text })
            console.log("Email is Correct");
        }
    }

    _onButtonSheet = async () => {
        console.log("this.state.myArray", this.state.myArray);
        console.log("this.state.totalData", this.state.totalData);
        // this.state.myArray.unshift(I18n.t('chooselanguage'));
        // this.state.myArray.push("Cancel");
        var myArrayLen = this.state.myArray.length;
        BottomSheet.showBottomSheetWithOptions({
            title: I18n.t('chooselanguage'),
            options: this.state.myArray,
            dark: false,
            cancelButtonIndex: 8,
            // destructiveButtonIndex: 1,
        }, (value) => {
            if (value >= this.state.totalData.length - 1) {
                console.log("array", this.state.myArray)
                return;
            }
            else {
                console.log("value", value);
                console.log("this.state.totalData", this.state.totalData.length);
                console.log("this.state.totalData.value", this.state.totalData[value].LanguageCode);
                this.setState({
                    "selctedLanguage": this.state.totalData[value].NativeName,
                    "selectedLanguageCode": this.state.totalData[value].LanguageCode,
                    "langId": this.state.totalData[value].LanguageId
                })
                //I18n.locale=this.state.totalData[value].LanguageCode;
                // I18n.locale="fr-FR";
                console.log('I18n', I18n);
                 I18n.locale = this.state.totalData[value].LanguageCode;
                //alert(this.state.totalData[value].NativeName);

                // console.log('langcode bottom sheet',this.state.selectedLanguageCode)
                // console.log('languageId bottom sheet',this.state.langId)
            }
        });
    }

    _onButtonNext = async () => {
        // Alert.alert("_onButtonNext");
        console.log("_onButtonNext")
        await AsyncStorage.setItem('langcode', this.state.selectedLanguageCode);
        console.log("languageId", this.state.langId)
        // await AsyncStorage.setItem(Keys.langId, this.state.langId);
        // I18n.locale = this.state.selectedLanguageCode;

        AsyncStorage.setItem(Keys.langId, this.state.langId)
            .then((langId) => {
                console.log("setItem callback", langId)
                I18n.locale = this.state.selectedLanguageCode;
                RNRestart.Restart();
            });
    }

    _loadIntitalUserDetails = async () => {
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        await this.setState({ userId: jsonValue.UserId, });

        await apiCallWithUrl(APIConstants.GetUserProfileAPI + "?id=" + this.state.userId, 'GET', "", this.getUserProfileResponse);
        await apiCallWithUrl(APIConstants.LanguageAPI, 'POST', '', this.languageApiResponse);
    }

    getUserProfileResponse = async (response) => {
        if (response != null) {
            var getuserResponse = response.ResponseData[0];
            console.log('getuserResponse', getuserResponse.FullName);

            await this.setState({ fullname: getuserResponse.FullName, email: getuserResponse.EmailId, mobile: getuserResponse.MobileNo, country: getuserResponse.CountryName, username: getuserResponse.UserName })

        }
    }

    languageApiResponse = async (response) => {

        console.log("Language response", response);

        // this.setState({ totalData: response.ResponseData, "selctedLanguage": response.ResponseData[0].NativeName, })
        await this.setState({ totalData: response.ResponseData, langLoading: false })

        console.log("totalResponse", response.ResponseData, this.state.langLoading);
        response.ResponseData.map((userData) => {
            console.log("laaaaa...", userData);
            var joined = this.state.myArray.concat(userData.NativeName);
            this.setState({ myArray: joined })
        });
    }

    _onPressUpdateUserProfile = async () => {
        if (this.state.btnText == I18n.t('edit')) {
            await this.setState({ textInputDisableStatus: true, btnText: I18n.t('btnUpdate'), dropdownDisableStatus: false })
        }
        else {

            //alert();
            if (this.state.fullname == "") {
                Alert.alert(I18n.t('enterfullname'));
            } else {
                if (this.state.email == "") {
                    Alert.alert(I18n.t('enteremailaddr'));
                } else {
                    if (this.validateEmail(this.state.email) == false) {
                        Alert.alert(I18n.t('alertnotvalidemail'));
                    } else {
                        if (this.state.mobile == "") {
                            Alert.alert(I18n.t('entermobileno'));
                        } else {
                            let overalldetails = {
                                "UserId": this.state.userId,
                                "FullName": this.state.fullname,
                                "MobileNo": this.state.mobile,
                                "EmailId": this.state.email,
                                "CountryName": this.state.country
                            }
                            await apiCallWithUrl(APIConstants.PostUserProfileAPI, 'POST', overalldetails, this.postUserProfileResponse);
                        }
                    }
                }
            }
        }

    }

    postUserProfileResponse = async (response) => {
        if (response != null) {
            console.log("langcode", this.state.selectedLanguageCode)
            console.log("langId", this.state.langId)

            var postUserResponse = response.ResponseData;
            await this.setState({ textInputDisableStatus: false, btnText: I18n.t('edit'), dropdownDisableStatus: true })
            console.log('postUserResponse', postUserResponse);

            if (this.state.selectedLanguageCode != '') {
                this._onButtonNext();
            }
            Alert.alert(I18n.t('alertprofilesuccess'));

            // Actions.Language({ "langcode": this.state.selectedLanguageCode });
            // Actions.CustomerHome({ "langcode": this.state.selectedLanguageCode });
        }
    }

    onPressChangePassword = () => {
        Actions.ChangePassword();
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    label: {
        //flex: 1,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    picker: {
        color: Colors.PlaceholderText,
        marginLeft: 8,
        height: 28,
        padding: 0,
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        fontSize: 15,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
})

module.exports = MyProfile;