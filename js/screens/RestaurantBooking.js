import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import ListRestaurantBookings from '../components/ListRestaurantBookings';
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

class RestaurantBooking extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      
    }
  }
  
  componentWillMount() {
    //this._loadHotelDetails();
  }
  async componentDidMount() {
    console.log("componentDidMount RestaurantBooking");
     await firebase.app();
     analytics().logScreenView({
        screen_name: 'RestaurantBookingsListScreen',
        screen_class: 'RestaurantBookingsListScreen'
    });
    //analytics().setCurrentScreen('Analytics');
}
  render() {
    
    return (
      
      <View style={{ flex: 1, backgroundColor: "white" }}>
          <StatusBar
          translucent={false}
              backgroundColor={Colors.App_Font}
              barStyle='light-content'
          />
          <ListRestaurantBookings/>
          <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.createRestaurantBooking}
          style={styles.TouchableOpacityStyle}>
          <Image
             source={require('../../assets/images/Plus.png')}
            style={styles.FloatingButtonStyle}
          />
        </TouchableOpacity>
        </View>
       
    );
  }
  
createRestaurantBooking =  async() => {
  await analytics().logEvent('Restaurant_Booking', {
    id: 1,
    ClickedOption: 'Create Restaurant Booking',
  })
  Actions.CreateRestaurantBooking();
  //Actions.pop();
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column'
  },
  
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    borderRadius:50,
    backgroundColor:Colors.App_Font
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 25,
    height: 25,
    //backgroundColor:'black'
  },
});

module.exports = RestaurantBooking;