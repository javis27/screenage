import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
    StatusBar,
    Linking,
    Platform,
    RefreshControl,
    PermissionsAndroid,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Swiper from 'react-native-swiper';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl, dynamicApiCallWithUrl } from '../api/APIHandler';
import { Dimensions } from 'react-native';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const DATA = [
    {
        "Title":"Room Booking",
        "Id":7,
        "Image":require('../../assets/images/Room-booking.jpg')
    },
    {
        "Title":"Online Check In",
        "Id":8,
        "Image":require('../../assets/images/Online-Checkin.jpg')
    },
    {
        "Title":"Door Lock",
        "Id":1,
        "Image":require('../../assets/images/DLI_bg1.jpg')
    },
    {
        "Title":"Amenities",
        "Id":5,
        "Image":require('../../assets/images/A_bg1.jpg')
    },
    {
        "Title":"Watch TV",
        "Id":2,
        "Image":require('../../assets/images/WT_bg1.jpg')
    },
    {
        "Title":"Hotel Information",
        "Id":6,
        "Image":require('../../assets/images/Heritage.jpg')
    },
    {
        "Title":"Hotel Directory",
        "Id":3,
        "Image":require('../../assets/images/PD_bg1.jpg')
    },
    
];
const DATA1 = [
    {
        "Title":"Rooms",
        "Id":1,
        "Image":require('../../assets/images/sugarbeach_img10.png')
    },
    {
        "Title":"Spa",
        "Id":2,
        "Image":require('../../assets/images/sugarbeach_img11.png')
    },
    {
        "Title":"Restaurants",
        "Id":3,
        "Image":require('../../assets/images/sugarbeach_img10.png')
    }
    
];
class CheckInHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            restaurantCount: null,
            spaCount: null,
            bikeCount: null,
            bannerArray: [{
                Id:1,
                IOSImagePath:require('../../assets/images/SouthernGross_img26.png'),
                AndroidImagePath:require('../../assets/images/SouthernGross_img26.png'),
            },
            {
                Id:2,
                IOSImagePath:require('../../assets/images/SouthernGross_img24.png'),
                AndroidImagePath:require('../../assets/images/SouthernGross_img24.png'),
            },
            {
                Id:3,
                IOSImagePath:require('../../assets/images/SouthernGross_img24_1.png'),
                AndroidImagePath:require('../../assets/images/SouthernGross_img24_1.png'),
            },
            {
                Id:4,
                IOSImagePath:require('../../assets/images/SouthernGross_img25.png'),
                AndroidImagePath:require('../../assets/images/SouthernGross_img25.png'),
            }
        ],
            IsFetchBanners: false,
            bannerheight: Dimensions.get('window').height / 4,
            bannerwidth: Dimensions.get('window').width,
            imgWidth: Dimensions.get('window').width / 8.33333,
            imgHeight: Platform.OS === 'ios' ? Dimensions.get('window').height / 18.04444 : Dimensions.get('window').height / 16,
            refreshing: false,
            weatherLocationDetails: {},
            currentWeatherArray: []
            
        };
        console.log("Dimensions.get('window').height",Dimensions.get('window').height)//592
        console.log("Dimensions.get('window').width",Dimensions.get('window').width)//360
    }

    handleBackButton = () => {
        console.log("screen name", Actions.currentScene)
        if (Actions.currentScene == "_CustomerHome") {
            BackHandler.exitApp();
            return true;
        }
    }

    componentWillMount() {
        this.onLoadLocation();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    onLoadLocation = () => {
        if (Platform.OS === 'ios') {
            this.getLocation();
        } else {
            this.requestLocationPermission();
        }
    }
    async componentDidMount() {
        console.log("componentDidMount CustomerHome");
        //this._onLoadDashboardCount();
         this.verifyCheckInDetails();
         await firebase.app();
         analytics().logScreenView({
            screen_name: 'DashboardScreen',
            screen_class: 'DashboardScreen'
        });
        //analytics().setCurrentScreen('Analytics');
    }
verifyCheckInDetails = async () => {
        await this.setState({ loading: true });
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        console.log("jsonvalue...",jsonValue);
        await this.setState({ EmailId: jsonValue.EmailId });
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        let overalldetails = {
            "HotelId": HotelId,
            "email": jsonValue.EmailId ,
        }
        console.log("overalldetails", overalldetails);
        await apiCallWithUrl(APIConstants.GetVerifyLoginStatusAPI, 'POST', overalldetails, this.postCheckInDetailsResponse1);
    }

    postCheckInDetailsResponse1 = async (response) => {

        if (Platform.OS === 'ios') {
            this.setState({ loading: false }, () => {
                setTimeout(() => this.funcCheckInDetailsResponse1(response), 1000);
            });
        } else {
            this.setState({
                loading: false,
            }, () => this.funcCheckInDetailsResponse1(response));
        }
    }

    funcCheckInDetailsResponse1 = async (response) => {
        var postCheckInDetailResponse = response.ResponseData;
        console.log("postCheckInDetailResponse",postCheckInDetailResponse)
        // if (response.IsException == "True") {
        //     return Alert.alert(response.ExceptionMessage);
        // }
        if (postCheckInDetailResponse == "Logged In") {
            //await this._onLoadDashboardCount();
        }else{
            // Alert.alert(I18n.t('alertnotcheckin'));
            // AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
            // //AsyncStorage.removeItem(Keys.UserDetail);
            // AsyncStorage.removeItem(Keys.roomNo);
            // AsyncStorage.removeItem(Keys.inCustomer);
            // AsyncStorage.removeItem(Keys.checkInId);
            // Actions.GuestLogin();
        }
        // if (postCheckInDetailResponse !== null) {
        //     console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
        //     if (postCheckInDetailResponse.InCustomer == "False") {
        //         console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
        //         Alert.alert(I18n.t('alertnotcheckin'));
        //         AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        //         //AsyncStorage.removeItem(Keys.UserDetail);
        //         AsyncStorage.removeItem(Keys.roomNo);
        //         AsyncStorage.removeItem(Keys.inCustomer);
        //         AsyncStorage.removeItem(Keys.checkInId);
        //         Actions.GuestLogin();
        //     } else {
        //         this._onLoadDashboardCount();
        //     }
        // }
    }
  

    renderBannerImages(bannerData) {
        console.log("renderBannerImages")
        var base64Icon = '';
        if (Platform.OS == 'ios') {
            // base64Icon = "data:image/png;base64," + bannerData.IOSImage;
            // base64Icon = APIConstants.Image_URL + bannerData.IOSImagePath;
            base64Icon = bannerData.IOSImagePath;
        } else {

            if (bannerData.AndroidImagePath.length != 0) {
                // base64Icon = "data:image/png;base64," + bannerData.AndroidImage;
                // base64Icon = APIConstants.Image_URL + bannerData.AndroidImagePath;
                base64Icon = bannerData.AndroidImagePath;
            }
        }
        //  if (bannerData.AndroidImage != "") {
        return (
            <Image source={base64Icon} style={{ width: '100%', height: this.state.bannerheight}} />
        )

    }

    renderWeatherImage(weatherData) {
        console.log("came here", weatherData)
        var weatherLogo = '';

        if (weatherData !== null) {
            if (weatherData.length > 0) {
                if (weatherData[0].WeatherIcon < 10) {
                    console.log("we <10", "https://developer.accuweather.com/sites/default/files/0" + weatherData[0].WeatherIcon + "-s.png")
                    weatherLogo = "https://developer.accuweather.com/sites/default/files/0" + weatherData[0].WeatherIcon + "-s.png"
                }
                else {
                    console.log("we 710", "https://developer.accuweather.com/sites/default/files/" + weatherData[0].WeatherIcon + "-s.png")
                    weatherLogo = "https://developer.accuweather.com/sites/default/files/" + weatherData[0].WeatherIcon + "-s.png"
                }

                return (
                    <TouchableOpacity activeOpacity={.5} onPress={this._onPressWeather}>
                        <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                            <Text style={styles.WeatherDeg}>{Math.round(weatherData[0].Temperature.Metric.Value)}&#176;</Text><Image source={{ uri: weatherLogo }} style={{ width: 85, height: 55, padding: 5 }} />
                        </View>
                        <Text style={styles.weatherIconText}>{weatherData[0].WeatherText}</Text>
                    </TouchableOpacity>
                )
            }
            else {
                return (
                    <View>
                        <View style={{ alignItems: "center", justifyContent: "center", textAlign: "center" }}>
                            <Image source={require('../../assets/images/skyloader.gif')} style={{ width: 85, height: 55, padding: 5 }} />
                        </View>
                        <Text style={styles.weatherIconText}>Loading</Text>
                    </View>
                )
            }
        }


    }
    pressEventFunc = async(Id)=>{
        if(Id==1){
            Actions.DynamicWebpage();
        }else if (Id==2){
            Actions.SpaBooking();
        }else if (Id==3){
            Actions.RestaurantListView();
        }else if (Id==4){
            Actions.HotelInfo();
        }else if (Id==5){
            Actions.DynamicWebpage1();
        }else if (Id==6){
            Actions.DigitalKey();
        }else if (Id==7){
            Actions.AmenitiesList();
        }else if (Id==8){
            Actions.TVChannels();
        }else if (Id==9){
            Actions.PhoneDirectory();
        }
    }
    render() {
        
        console.log("render banner array", this.state.bannerArray.length)
        const Item = ({ title }) => (
            <View style={styles.item}>
              <Text style={styles.title}>{title}</Text>
            </View>
          );
          const renderItem = ({ item }) => (
            <View style={{marginTop:10,marginLeft:10,marginRight:10,marginBottom:10,width:'95%'}}>
              <TouchableOpacity onPress={this._onSelectionBooking.bind(this,item.Id)}>
                  <ImageBackground source = {item.Image} style = {{ width: '100%', height: 180, position: 'relative', borderRadius: 20,overflow: "hidden",borderWidth: 1,borderColor:'#FFF'}}>
                      
                        <Text
                        style={{
                          fontFamily: AppFont.Light,
                          fontSize:32,
                          color: 'white',
                          position: 'absolute', // child
                          bottom: 15, // position where you want
                          left: 20,
                          marginRight:10
                        }}
                      >
                        {item.Title}
                      </Text>
                </ImageBackground>
              </TouchableOpacity>
            </View>
          );
        return (
            <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>
                {/* <Loader loading={this.state.loading} /> */}
                <StatusBar
                    translucent={true}
                    backgroundColor='transparent'
                    barStyle='default'
                    hidden={true}
                />
                <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
                    <ImageBackground source ={require('../../assets/images/sc3.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flex:0.4,justifyContent:'flex-end'}}>
                        {/* <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                                <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                            </TouchableOpacity> */}
                        </View>
                        <View style={{flex:0.6,alignItems:'center',justifyContent:'center'}}>
                        <Image
                            source={require('../../assets/images/sugarbeach.png')}
                            style={{
                                width:'60%',
                                height:80,
                                position: 'absolute', // child
                                bottom: 60, // position where you want
                                // left: 30,
                                marginRight:10,
                            }}
                            />
                        </View>

                    </View>       
                    </ImageBackground>
                    </View>
                    <View style={{flex:0.75,backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                        <ScrollView>
                        <View style={{ flex: 1}}>
                            <View style={{margin:10}}>
                            <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black}}>Discover</Text>
                            <View style={{flex:1,flexDirection:'row', marginTop:10}}>
                                <View style={{flex:0.50,alignItems:'center'}}>
                                    <TouchableOpacity onPress={this.pressEventFunc.bind(this,1)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                        <Image source={require('../../assets/images/SouthernGrossbeach_img10.png')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                    </TouchableOpacity>
                                    <Text style={{textAlign:'center',marginTop:5,letterSpacing:1}}>Rooms</Text>
                                </View>
                            
                                {/* <View style={{flex:0.33,alignItems:'center'}}>
                                    <TouchableOpacity onPress={this.pressEventFunc.bind(this,2)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                        <Image source={require('../../assets/images/sugarbeach_img11.png')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                    </TouchableOpacity>
                                    <Text style={{textAlign:'center',marginTop:5,letterSpacing:1}}>Spa</Text>
                                </View> */}
                            
                                <View style={{flex:0.50,alignItems:'center'}}>
                                    <TouchableOpacity onPress={this.pressEventFunc.bind(this,3)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                        <Image source={require('../../assets/images/SouthernGross_img23.png')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                    </TouchableOpacity>
                                    <Text style={{textAlign:'center',marginTop:5,letterSpacing:1}}>Restaurants</Text>
                                </View>
                            </View>
                            </View>
                            <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,margin:10}}>Experience</Text>
                            <Swiper autoplay dotColor="rgba(255,255,255,.2)" activeDotColor="#FFFFFF" style={{ height: this.state.bannerheight - 10,width:'100%' }} showsButtons={false}>
                                {this.state.bannerArray.length > 0 ?
                                    this.state.bannerArray.map((item, index) => {
                                        return <View key={index} style={styles.slide1}>
                                            {this.renderBannerImages(item)}
                                        </View>
                                    })
                                    // <View>Hello wrong</View>
                                    : null}
                            </Swiper>
                            <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                <View style={{flex:0.5}}>
                                
                                        <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                                        <TouchableOpacity onPress={this.pressEventFunc.bind(this,4)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                            <Image source={require('../../assets/images/SouthernGrossbeach_img5.png')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                        </TouchableOpacity>
                                            
                                        </View>
                                        <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,textAlign:'center'}}>About the Hotel</Text>
                                </View>
                                <View style={{flex:0.5}}>
                                
                                    <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                                    <TouchableOpacity onPress={this.pressEventFunc.bind(this,5)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                            <Image source={require('../../assets/images/SouthernGross_img25.png')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                        </TouchableOpacity>
                                        </View>
                                        <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,textAlign:'center'}}>Check - In</Text>
                                </View>
                            </View>
                            
                            {/* <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                <View style={{flex:0.5}}>
                                
                                        <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                                        <TouchableOpacity onPress={this.pressEventFunc.bind(this,6)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                            <Image source={require('../../assets/images/DLI_bg1.jpg')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                        </TouchableOpacity>
                                        </View>
                                        <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,textAlign:'center'}}>Door Lock</Text>
                                </View>
                                <View style={{flex:0.5}}>
                               
                                    <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                                    <TouchableOpacity onPress={this.pressEventFunc.bind(this,7)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                            <Image source={require('../../assets/images/A_bg1.jpg')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                        </TouchableOpacity>
                                        </View>
                                        <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,textAlign:'center'}}>Amenities</Text>
                                </View>
                            </View> */}
                            {/* <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                <View style={{flex:0.5}}>
                                
                                        <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                                        <TouchableOpacity onPress={this.pressEventFunc.bind(this,8)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                            <Image source={require('../../assets/images/WT_bg1.jpg')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                        </TouchableOpacity>
                                        </View>
                                        <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,textAlign:'center'}}>Watch TV</Text>
                                </View>
                                <View style={{flex:0.5}}>
                                
                                    <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                                    <TouchableOpacity onPress={this.pressEventFunc.bind(this,9)} style={{width:'100%',height:100,justifyContent:'center',alignItems:'center'}}>
                                            <Image source={require('../../assets/images/PD_bg1.jpg')} style={{width:'90%',height:100,resizeMode:'cover',borderRadius:10}}/>
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={{fontSize:16,fontFamily:AppFont.Medium,color:Colors.Black,textAlign:'center'}}>Hotel Directory</Text>
                                </View>
                            </View> */}
                        </View>
                        </ScrollView>
                    </View>
                
                {/* <View style={{flex:0.25}}>
                    <Text>Discover</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:0.33}}>
                            <Image source={require('../../assets/images/logo.png')} style={{width:'90%',height:100}}>

                            </Image>
                        </View>
                    </View>
                </View> */}
            </View>
        )
    }
    
    _onSelectionBookingCount = async (BookingId) => {
        console.log("Entered")
        console.log("BookingId",BookingId)
        // await analytics().logEvent('custom_event', {
        //     id: '123123',
        //     value: 'value',
        //     variable: 'variable',
        // });
        if(BookingId==1){
            await analytics().logEvent('Dashboard', {
                id: 1,
                ClickedOption: 'Restaurant Booking',
                description: ['Booking ID',BookingId],
              })
            Actions.RestaurantBooking();
        }else if(BookingId==2){
            await analytics().logEvent('Dashboard', {
                id: 2,
                ClickedOption: 'Spa Booking',
                description: ['Booking ID',BookingId],
              })
            Actions.SpaBooking();
        }else if(BookingId==3){
            await analytics().logEvent('Dashboard', {
                id: 3,
                ClickedOption: 'Bike Booking',
                description: ['Booking ID',BookingId],
              })
            Actions.BikeBooking();
        }else if(BookingId==4){
            await analytics().logEvent('Dashboard', {
                id: 4,
                ClickedOption: 'Boat Booking',
                description: ['Booking ID',BookingId],
              })
            Actions.BoatBooking();
        }
    }

    _onSelectionBooking = async (BookingId) => {
        if(BookingId==1){
          
            await analytics().logEvent('Dashboard', {
                id: 1,
                ClickedOption: 'Door Lock',
                description: ['Booking ID',BookingId],
              })
            Actions.DigitalKey();
            //Actions.DynamicWebpage();
        }else if(BookingId==2){
            await analytics().logEvent('Dashboard', {
                id: 2,
                ClickedOption: 'Tv Channel Screen',
                description: ['Booking ID',BookingId],
              })
            Actions.TVChannels();
        }else if(BookingId==3){
            await analytics().logEvent('Dashboard', {
                id: 3,
                ClickedOption: 'Phone Directory',
                description: ['Booking ID',BookingId],
              })
            Actions.PhoneDirectory();
        }else if(BookingId==4){
            await analytics().logEvent('Dashboard', {
                id: 4,
                ClickedOption: 'Maps',
                description: ['Booking ID',BookingId],
              })
            Actions.Maps();
        }else if(BookingId==5){
            await analytics().logEvent('Dashboard', {
                id: 5,
                ClickedOption: 'Amenities Booking',
                description: ['Booking ID',BookingId],
              })
            Actions.AmenitiesList();
        }else if(BookingId==6){
            await analytics().logEvent('Dashboard', {
                id: 6,
                ClickedOption: 'Hotel Info',
                description: ['Booking ID',BookingId],
              })
            Actions.HotelInfo();
        }
        else if(BookingId==7){
            await analytics().logEvent('Dashboard', {
                id: 7,
                ClickedOption: 'Room Booking',
                description: ['Booking ID',BookingId],
              })
            //Actions.DynamicWebpage();
            //Actions.FoodMenus();
            Actions.RestaurantListView();
        }
        else if(BookingId==8){
            await analytics().logEvent('Dashboard', {
                id: 8,
                ClickedOption: 'Online Check In',
                description: ['Booking ID',BookingId],
              })
            Actions.DynamicWebpage1();
        }
    }
    _onRefresh = () => {
        this.setState({ refreshing: true })
        this._onLoadDashboardCount();
        this.onLoadLocation();
    }

    _onLoadDashboardCount = async () => {
        this.setState({
            loading: true
        })
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        console.log("roomNo", roomNo);
        var HotelId = await AsyncStorage.getItem('HotelId');
        await this.setState({ userId: userJson.UserId,HotelId:HotelId });
        console.log("HotelId...",HotelId)
        //await apiCallWithUrl(APIConstants.GetBikeBookingList + "?UserId=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        let overalldetails = {
            "HotelId": HotelId
          }
          let overalldetails1 = {
            "HotelId": HotelId,
            "UserId": this.state.userId
          }
          //await apiCallWithUrl(APIConstants.GetDashboardCountAPI, 'POST', overalldetails1, (callback) => { this.postCheckInDetailsResponse(callback) });
          await apiCallWithUrl(APIConstants.GetBannerAPI, 'POST', overalldetails, (callback) => { this.getBannerResponse(callback) });
        //apiCallWithUrl(APIConstants.GetDashboardCountAPI + "?UserId=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        //await apiCallWithUrl(APIConstants.GetBannerAPI, 'GET', "", this.getBannerResponse)
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        this.getDashboardCountResponse(response);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        }
        //  else {
        //     this.getDashboardCountResponse(response);
        // }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getDashboardCountResponse = async (response) => {
        if (response != null) {
            var dashboardCount = response.ResponseData[0];
            // console.log('dashboard', dashboardCount);
            await this.setState({ restaurantCount: dashboardCount.RestaurantBookingCount, spaCount: dashboardCount.SpaBookingCount, bikeCount: dashboardCount.BikeBookingCount, boatCount: dashboardCount.BoatBookingCount, refreshing: false })
        }
        // this.setState({ loading: false })
    }

    getBannerResponse = async (response) => {
        this.setState({ loading: false }, () => {
            if (response.ResponseData.length > 0) {
                var bannersList = response.ResponseData;
                this.setState({ bannerArray: bannersList, IsFetchBanners: true })
                console.log('bannerArray', this.state.bannerArray);
            }
        })

        // this.setState({ loading: false })
    }

    // _onGuestCheckInButton = () => {
    //     Actions.GuestCheckIn();
    // }

    _onPressDashboardBookingCount = (value) => {
        Actions.BookingsList({ BookingListIndex: value });
    }

    _onPressWatchTv = () => {
        Actions.TVChannels();
    }

    _onPressPhoneDirectory = () => {
        Actions.PhoneDirectory();
    }

    _onPressMaps = () => {
        Actions.Maps();
    }

    _onPressFlightDetails = () => {
        // var flightSearchUrl = 'https://mauritius-airport.atol.aero/passengers/flights/flight-arrival-search';
        // Linking.canOpenURL(flightSearchUrl).then(supported => {
        //     if (supported) {
        //         Linking.openURL(flightSearchUrl);
        //     } else {
        //         console.log("Don't know how to open URI: " + flightSearchUrl);
        //     }
        // });
        Actions.FlightDetails();
    }

    _onPressAmenities = async (serviceId) => {
        console.log("_onPressAmenities", serviceId);
        //  Actions.Services();
        Actions.Amenities();
    }

    // _onPressWeather = () => {
    //     var mapSearchUrl = 'http://metservice.intnet.mu/maps.php';
    //     Linking.canOpenURL(mapSearchUrl).then(supported => {
    //         if (supported) {
    //             Linking.openURL(mapSearchUrl);
    //         } else {
    //             console.log("Don't know how to open URI: " + mapSearchUrl);
    //         }
    //     });
    // }

    _onPressWeather = async () => {

        Actions.AccuWeatherForecast({ locationData: this.state.weatherLocationDetails });


        // Construct the API url to call
        //  let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=YOUR API KEY HERE';

        //  var weatheropenAPIUrl = 'http://metservice.intnet.mu/maps.php';
        // apiCallWithUrl(APIConstants.GetDashboardCountAPI + "?UserId=" + userJson.UserId, 'GET', "", this.getDashboardCountResponse)
        //   dynamicApiCallWithUrl("https://api.openweathermap.org/data/2.5/forecast?lat=12.9098899&lon=80.2260084&units=metric&appid=fa287858b83e681690f92bec06d933a4", 'GET', "", this.getWeatherResponse)
        // dynamicApiCallWithUrl("https://api.openweathermap.org/data/2.5/weather?q=Chennai,in&appid=fa287858b83e681690f92bec06d933a4", 'GET', "", this.getWeatherResponse)
    }


    requestLocationPermission = async () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then(data => {
                this.getLocation();
                // The user has accepted to enable the location services
                // data can be :
                //  - "already-enabled" if the location services has been already enabled
                //  - "enabled" if user has clicked on OK button in the popup
            }).catch(err => {
                console.log("The user has not accepted to enable the location service`")
                // The user has not accepted to enable the location services or something went wrong during the process
                // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
                // codes : 
                //  - ERR00 : The user has clicked on Cancel button in the popup
                //  - ERR01 : If the Settings change are unavailable
                //  - ERR02 : If the popup has failed to open
            });
    }



    getLocation = async () => {
        console.log("weather get location")
        await navigator.geolocation.getCurrentPosition(
            (position) => {
                //   console.log("wokeeey");
                console.log("position", position);

                //   this.setState({
                // 	geoLatLang: {
                // 	  latitude: position.coords.latitude,
                // 	  longitude: position.coords.longitude,
                // 	}
                //   });

                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }, () => {
                    this.getWeather();
                });

            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }


    getWeather() {
        console.log("hello get weather")
        // Construct the API url to call
        //let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=fa287858b83e681690f92bec06d933a4';
        //let url=APIConstants.WeatherAPI_URL + "locations/v1/cities/geoposition/search?apikey=O5ROZnWXimwNFtC1g1DmX7oUbNAbpLXG&q="+ this.state.latitude + "%2C%" + this.state.longitude;
        // below link for Mauritius weather
        //   dynamicApiCallWithUrl(APIConstants.WeatherAPI_URL + APIConstants.GeopositionSearchAPI + "?apikey=" + APIConstants.WeatherAPIKey + "&q=-20.421796%2C57.721973", 'GET', '', this.locationApiResponse);
        // below link for India weather 
        dynamicApiCallWithUrl(APIConstants.WeatherAPI_URL + APIConstants.GeopositionSearchAPI + "?apikey=" + APIConstants.WeatherAPIKey + "&q=" + this.state.latitude + "%2C" + this.state.longitude, 'GET', '', this.locationApiResponse);
    }

    locationApiResponse = (response) => {
        console.log("res", response)
        this.setState({
            weatherLocationDetails: response
        })
        if (response.Key !== "undefined" || response.Key !== null) {
            dynamicApiCallWithUrl(APIConstants.WeatherAPI_URL + "currentconditions/v1/" + response.Key + "?apikey=" + APIConstants.WeatherAPIKey, 'GET', '', this.getCurrentWeatherResponse);
        }

    }


    getCurrentWeatherResponse = (response) => {
        this.setState({
            currentWeatherArray: response
        })
        console.log("getCurrentWeatherResponse", response[0].Temperature.Metric.Value)
    }

    _onPressMoreInfo = () => {
        Actions.HotelInfo();
    }
}

const styles = StyleSheet.create({
    IconText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        paddingTop: 10,
        textAlign: 'center'
    },
    weatherIconText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        //  paddingTop: 2,
        textAlign: 'center'
    },
    rowContainer: {
        //flex: 1,
        flexDirection: 'row',
    },
    menuWrapper: {
        width: "33.3333333333%",
        // height: 120,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 15
    },
    GridView: {
        width: "33.3333333333%",
        height: 'auto',
        alignItems: 'center',
        padding: 7,
        marginBottom: 10
    },
    GridViewInner: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: "#ffffff",
        width: "100%",
        borderRadius: 5,
        ...Platform.select({
            ios: {
                shadowOffset: { width: 2, height: 2, },
                shadowColor: Colors.LightGray,
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },
    GridViewNo: {
        fontSize: 40,
        padding: 5,
        paddingVertical: 10,
        color: Colors.Black,
    },
    GridIconText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        paddingHorizontal: 15,
        paddingVertical: 10,
        textAlign: 'center',
        minHeight: 50
    },
    WeatherDeg: {
        position: "absolute",
        fontSize: 14,
        justifyContent: 'center',
        left: 15,
        top: 7,
        alignItems: 'center',
        zIndex: 1,
        fontFamily: AppFont.Medium,
        color: Colors.Black,
        textShadowColor: 'rgba(255, 255, 255, 0.75)',
        textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 7
    },



    container: {
        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        width:'100%'

    },
    bookingCount:{
        flex:0.25, 
        alignContent:'center', 
        alignItems:'center'
    },
    imageStyle:{
        marginTop:5,
        width: '98%', 
        height: Dimensions.get('window').height /9, 
        resizeMode:'contain',
        
    },
    imageStyle1:{
        width: '100%', 
        height: Dimensions.get('window').height / 9, 
        resizeMode:'cover',
        
    },
    imagebg:{
        width:'90%',
        borderWidth:0.5, 
        borderRadius: 20,
        borderColor:Colors.LightGray,  
        backgroundColor:'#F5F5F5',
        alignContent:'center', 
        alignItems:'center'
    },
    countView:{
        backgroundColor: Colors.App_Font,
        position: 'absolute', // child
        bottom:-15, // position where you want
        left:Dimensions.get('window').width / 16,
        borderRadius:50,
        alignContent:'center',
        alignItems:'center',
        width:30,
        height:30
    },
    countText:{
        fontFamily: AppFont.Medium,
        fontWeight: 'bold',
        fontSize:19,
        marginTop:3,
        color: 'white',
        position: 'absolute', // child
        alignContent:'center',
        alignItems:'center',
    }
});

module.exports = CheckInHome;