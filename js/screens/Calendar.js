import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
    SectionList,
    StatusBar,
    Alert,
    Platform
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import groupBy from '../constants/groupBy';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import Loader from '../components/Loader';
import CalendarListItem from '../components/CalendarListItem';
import Moment from 'moment';

import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

class MyCalendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            calendarEventsArray: [],
            loading: false,
            // isRefreshing: false,
            IsFetchCalendarResponse: false,
        }
    }
    componentWillMount() {
        this._onLoadCalendar();
        //  this.verifyCheckInDetails();
    }
    async componentDidMount() {
        console.log("componentDidMount Calendar");
         await firebase.app();
         analytics().logScreenView({
            screen_name: 'CalendarScreen',
            screen_class: 'CalendarScreen'
        });
    }
    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     console.log("postCheckInResponse Language", response);

    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this._onLoadCalendar();
    //         }
    //     }
    // }


    renderEmptyComponent = () => {
        console.log("empty component", this.state.calendarEventsArray)
        if (this.state.calendarEventsArray.length != 0) return null;
        return (
            // <View style={{flex:1, borderColor:'red', borderWidth: 1.0}}>
            <Text style={{ fontSize: 18, alignSelf: 'center', fontFamily: AppFont.Regular, color: 'black', textAlign: 'center' }}>{I18n.t('nodata')}</Text>
            // </View>
        );
    };
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                    hidden={true}
                />
                <Loader
                    loading={this.state.loading} />
                {
                    this.state.IsFetchCalendarResponse ?
                        <View style={{ flex: 1, alignItems: 'center', padding: 15, position: "relative", backgroundColor: Colors.Background_Color }}>
                            <View style={styles.rowContainer}>
                                <View style={[styles.menuWrapper, { borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5, borderRightColor: Colors.App_Font, borderRightWidth: 0.5, backgroundColor: Colors.App_Font }]}>
                                    <Text style={styles.TitleIconText}>{I18n.t('mycalendar')}</Text>
                                </View>
                                <View style={[styles.menuWrapper, { borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5, borderLeftColor: Colors.App_Font, borderLeftWidth: 0.5, backgroundColor: Colors.HotelCalender }]}>
                                    <Text style={styles.TitleIconText}>{I18n.t('hotelcalendar')}</Text>
                                </View>
                            </View>

                            <SectionList
                                stickySectionHeadersEnabled
                                refreshing={this.state.isRefreshing}
                                onRefresh={this._onLoadCalendar}
                                // ListEmptyComponent={this.renderEmptyComponent}
                                keyExtractor={(x, i) => i}
                                sections={this.state.calendarEventsArray}
                                renderItem={({ item }) => { return <CalendarListItem calendar={item} /> }}
                                renderSectionHeader={({ section }) =>
                                    <View style={styles.rowContainer}>
                                        <View style={{ width: "100%", backgroundColor: Colors.CalendarList, padding: 15 }}>
                                            <Text style={styles.IconText}>{section.title}</Text></View></View>}
                            />

                            {/* <View style={styles.rowContainer}>
                        <View style={{ width: "100%", backgroundColor: Colors.CalendarList, padding: 15 }}>
                            <Text style={styles.IconText}>Wednesday, 20-Mar-2019 : Today</Text>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}>Restaurant is booked for 5 at 12:30 PM</Text>
                            <Text style={{ position: "absolute", right: -6, top: 30 }}>
                                <Image source={{ uri: 'ellipse1' }} style={{ width: 10, height: 10 }} />
                            </Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.LeftText}></Text>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.LeftText}>Aqua gym at 02:00 PM</Text>
                            <Text style={{ position: "absolute", left: -5, top: 20 }}>
                                <Image source={{ uri: 'ellipse2' }} style={{ width: 10, height: 10, justifyContent: "center" }} />
                            </Text>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                    </View>

                    <View style={styles.rowContainer}>
                        <View style={{ width: "100%", backgroundColor: Colors.CalendarList, padding: 15 }}>
                            <Text style={styles.IconText}>Thursday, 21-Mar-2019 : Tomorrow</Text>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}>SPA booking at 1:00 PM</Text>
                            <Text style={{ position: "absolute", right: 0, top: 20 }}>
                                <Image source={{ uri: 'ellipse1' }} style={{ width: 10, height: 10 }} />
                            </Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.LeftText}>Live music at Mosaic</Text>
                            <Text style={{ position: "absolute", left: 0, top: 20 }}>
                                <Image source={{ uri: 'ellipse2' }} style={{ width: 10, height: 10, justifyContent: "center" }} />
                            </Text>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                    </View>

                    <View style={styles.rowContainer}>
                        <View style={{ width: "100%", backgroundColor: Colors.CalendarList, padding: 15 }}>
                            <Text style={styles.IconText}>Friday, 22-Mar-2019</Text>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={styles.rowContainer}>
                            <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                                <Text style={styles.RightText}></Text>
                            </View>
                            <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                                <Text style={styles.RightText}></Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={[styles.menuWrapper, { borderRightColor: Colors.LightGray, borderRightWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                        <View style={[styles.menuWrapper, { borderLeftColor: Colors.LightGray, borderLeftWidth: 0.5 }]}>
                            <Text style={styles.RightText}></Text>
                        </View>
                    </View> */}
                        </View>
                        : null
                }
            </View>
        )
    }

    _onLoadCalendar = async () => {

        this.setState({
            loading: true,
            isRefreshing: true
        });
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const langId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        var curdatetime = Moment().format('Y-MM-DD H:mm:ss');
        const today = Moment();
        await this.setState({ userId: userJson.UserId,HotelId:HotelId });
        console.log("HotelId...",HotelId)
        //await apiCallWithUrl(APIConstants.GetBikeBookingList + "?UserId=" + userJson.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        let overalldetails = {
            "UserId":userJson.UserId,
            "LanguageId":langId,
            "HotelId": HotelId,
            "EventToDateTime":curdatetime
          }
          await apiCallWithUrl(APIConstants.GetCalendarEvents, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
        // await apiCallWithUrl(APIConstants.GetCalendarEvents + "?UserId=" + userJson.UserId + "&&LanguageId=" + langId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        this.getCalendarResponse(response);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        }
        // else {
        //     this.getCalendarResponse(response);
        // }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getCalendarResponse = async (response) => {
        if (response.IsException == null) {

            var calendarResponse = response.ResponseData;
            console.log('calendar response', calendarResponse)

            // await this.setState({
            //     calendarEventsArray: calendarResponse === null ? '' : calendarResponse
            // })

            var groupByEventDate = groupBy(calendarResponse, 'EventDate')
            var arrayOfData = [];
            Object.keys(groupByEventDate).forEach(element => {
                arrayOfData.push({
                    title: element,
                    data: groupByEventDate[element]
                });
            });

            console.log('arrayOfData...', arrayOfData);
            this.setState({ loading: false, isRefreshing: false, calendarEventsArray: arrayOfData, IsFetchCalendarResponse: true });
        }
        // this.setState({ loading: false, isRefreshing: false, })
    }

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    TitleIconText: {
        color: Colors.Background_Color,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        textAlign: 'center',
        textTransform: "uppercase"
    },
    IconText: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'center'
    },
    rowContainer: {
        //flex: 1,
        flexDirection: 'row',
    },
    menuWrapper: {
        width: "50%",
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 10
    },
    RightText: {
        textAlign: "right",
        fontSize: 14
    },
    LeftText: {
        textAlign: "left",
        fontSize: 14
    }

})

module.exports = MyCalendar;