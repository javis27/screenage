import React from 'react';
import {
    Platform,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    BackHandler,
    ImageBackground,
    StatusBar
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Disclaimer from '../components/Disclaimer';
import Policy from '../components/Policy';
import Status from '../constants/Status';
// import PrivacyPolicy from '../screens/PrivacyPolicy';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import Dialog, { SlideAnimation, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import FCM from 'react-native-fcm';
import { SafeAreaView } from 'react-native';
// import BottomSheet from 'react-native-bottomsheet';


class GuestLogin extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            username: '',
            password: '',
            myArray: [],
            disclaimerVisibile: false,
            privacyPolicyVisible: false
        }
        //    this._onPressDisclaimer = this._onPressDisclaimer.bind(this);
        this.setDisclaimerVisibility = this.setDisclaimerVisibility.bind(this);
    }
    componentWillMount() {
        StatusBar.setHidden(true);
        FCM.getFCMToken().then(token => {
            console.log('token.....', token);
            this.setState({ deviceToken: token })
        });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        //this._loadInitialState();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name", Actions.currentScene)
        if (Actions.currentScene == "GuestLogin") {
            BackHandler.exitApp();
            return true;
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column' }}>
            <ImageBackground source={require('../../assets/images/SouthernGross_img0.png')} style={{flex: 1,resizeMode: 'cover',justifyContent: "center",flexDirection: 'column'}}>
            <View style={{ flex: 0.35,justifyContent:'center', alignItems: 'center' }}>
            
                    <Image source={require('../../assets/images/logo.png')}  style={{ width: '100%', height: '80%', resizeMode:'contain' }} />
                    {/* <Image source={{ uri: 'LaunchImage' }} style={{ width: 150, height: 150, borderColor: 'blue', borderWidth: 0.5, }} /> */}
               
            </View>
            <View style={{ flex: 0.35, justifyContent: "flex-end" }}>
                
               
            </View>
            <View style={{ flex: 0.30, justifyContent: "flex-end"}}>

            <View style={{ alignItems: "center", height: 55 }}>
                    <View style={styles.loginview}>
                        <TouchableOpacity activeOpacity={.5} style={styles.button1} onPress={this._onGuestLogin}>
                            <Text style={styles.btntext}>{I18n.t('loginbtn')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                
            <View style={{ alignItems: "center", height: 55 }}>
                    <View style={styles.loginview}>
                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onButtonSheet}>
                            <Text style={styles.btntext}>{I18n.t('registerbtn')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                
                <View style={{ alignItems: "center", height: 55 }}>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <View style={{ flex: 0.5, alignItems: "center", borderRightColor: Colors.Black, borderRightWidth: 2 }}>
                        
                        <TouchableOpacity activeOpacity={.5} onPress={this.handleOnPress}>
                            <Text style={{ paddingVertical: 25, fontSize: 14, fontFamily: AppFont.Medium, color:Colors.Black }}>{I18n.t('disclaimer')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 0.5, alignItems: "center", }}>
                        <TouchableOpacity activeOpacity={.5} onPress={this.handlePrivacyPolicy}>
                            <Text style={{ paddingVertical: 25, fontSize: 14, fontFamily: AppFont.Medium, color:Colors.Black }}>{I18n.t('privacypolicy')}</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
                {/* <View style={{ alignItems: "center", borderColor: Colors.Background_Color, borderWidth: 0.5 }}>
                    <TouchableOpacity activeOpacity={.5} onPress={this._onGuestLogin}>
                        <Text style={{ paddingVertical: 20, fontSize: 18, fontFamily: AppFont.Medium, color:Colors.Background_Color }}>{I18n.t('guest')}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", }}>
                    <View style={{ flex: 0.5, alignItems: "center", borderRightColor: Colors.Background_Color, borderWidth: 0.5 }}>
                        
                        <TouchableOpacity activeOpacity={.5} onPress={this.handleOnPress}>
                            <Text style={{ paddingVertical: 25, fontSize: 14, fontFamily: AppFont.Medium, color:Colors.Background_Color }}>{I18n.t('disclaimer')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 0.5, alignItems: "center", }}>
                        <TouchableOpacity activeOpacity={.5} onPress={this.handlePrivacyPolicy}>
                            <Text style={{ paddingVertical: 25, fontSize: 14, fontFamily: AppFont.Medium, color:Colors.Background_Color }}>{I18n.t('privacypolicy')}</Text>
                        </TouchableOpacity>
                    </View>
                </View> */}
            </View>
            {
                this.state.disclaimerVisible ?
                    <Disclaimer disclaimerVisibility={this.state.disclaimerVisible} 
                    disclaimerType={Status.disclaimerTypes.loginDisclaimer.typeId} 
                    callbackDisclaimerVisibility={this.setDisclaimerVisibility} />
                    : null
            }
            {
                this.state.privacyPolicyVisible ?
                    <Policy policyVisibility={this.state.privacyPolicyVisible}
                        policyType={Status.policyTypes.loginPrivacyPolicy.typeId}
                        callbackPolicy={this.setPrivacyPolicyVisibility} />
                    : null
            }
</ImageBackground>
        </View >
        );
    }
    _onButtonSheet = async () => {
        Actions.Login();
    }

    _onRegisterBtn = async () => {
        Actions.Register();
    }

    _onGuestLogin = async () => {
        //Actions.GuestHome();
        Actions.Hotel();
    }
    _onSampleBtn = async () => {
        Actions.Hotel();
    }

    
    handleOnPress = async () => {
        await this.setState({ disclaimerVisible: true }, () => console.log("disclaimerVisibile", this.state.disclaimerVisibile))
        
    }
    handlePrivacyPolicy = async () => {
        await this.setState({ privacyPolicyVisible: true }, () => console.log("privacyPolicyVisible", this.state.privacyPolicyVisible))
    }

    async setDisclaimerVisibility(data) {
        console.log('setAboutVisibility', data)
        await this.setState({ disclaimerVisibile: data });
    }

    setPrivacyPolicyVisibility = async (data) => {
        console.log('setPrivacyPolicyVisibility', data)
        await this.setState({ privacyPolicyVisible: data });
    }


}

const styles = StyleSheet.create({

    loginview: {
        alignItems: 'center',
        width: "90%"
    },
    button: {
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "95%",
        alignItems: "center",
        borderRadius: 25
    },
    button1: {
        backgroundColor: Colors.ButtonBlue,
        padding: 10,
        width: "95%",
        alignItems: "center",
        borderRadius: 25
    },
    btntext: {
        fontFamily: AppFont.Regular,
        color: "#FFF",
        fontSize: 18,
        alignItems: "center",
        //fontFamily: AppFont.SemiBold,
    },
    
});

module.exports = GuestLogin;