import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const DATA = [
    {
        "Title":"Rendez-Vous",
        "Id":1,
        "Image":require('../../assets/images/rendez.jpeg')
    },
    {
        "Title":"1810",
        "Id":2,
        "Image":require('../../assets/images/1810.jpeg')
    },
    {
        "Title":"Mosaic",
        "Id":3,
        "Image":require('../../assets/images/mosaic.jpeg')
    },
    {
        "Title":"Medley",
        "Id":4,
        "Image":require('../../assets/images/medley.jpeg')
    }
];

class RestaurantListView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            
        }
    }

    componentWillMount() {
        this.handleBackAddEventListener();
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }


    render() {
        const Item = ({ title }) => (
            <View style={styles.item}>
              <Text style={styles.title}>{title}</Text>
            </View>
          );
          const renderItem = ({ item }) => (
            <View style={{marginTop:10}}>
              <TouchableOpacity onPress={this._onSelectionBooking.bind(this,item.Id)}>
                  <ImageBackground source = {item.Image} style = {{ width: '100%', height: 180, position: 'relative', overflow: "hidden",borderWidth: 1,borderColor:'#FFF'}}>
                      
                        
                </ImageBackground>
                <View style={{backgroundColor:'#F4F4F4'}}><Text
                        style={{
                          fontFamily: AppFont.Medium,
                          fontSize:18,
                          color: Colors.Black,
                          marginLeft:10
                        }}
                      >
                        {item.Title}
                      </Text></View>
              </TouchableOpacity>
            </View>
          );
        return (
            <View style={styles.container}>
{/*                 
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                /> */}

                
                <View style={{ flex: 1,flexDirection:'column'}}>
                    <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
                    <ImageBackground source ={require('../../assets/images/SouthernGross_img23.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flex:0.4,justifyContent:'flex-end'}}>
                            <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                                <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:0.6}}>
                        <Image
                            source={require('../../assets/images/sugarbeach.png')}
                            style={{
                                width:'60%',
                                height:80,
                                position: 'absolute', // child
                                bottom: 60, // position where you want
                                // left: 30,
                                marginRight:10,
                            }}
                            />
                        </View>

                    </View>       
                    </ImageBackground>
                    </View>
                    <View style={{flex:0.75,backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                    
                    <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black,margin:15}}>{I18n.t('restaurants_home')}</Text>
                        <FlatList
                            data={DATA}
                            renderItem={renderItem}
                            keyExtractor={item => item.Id}
                        />
                        </View>
                    </View>
                    

            </View>
        )
    }

    _onSelectionBooking = async (BookingId) => {
        if(BookingId==1){
            Actions.RestaurantView({RestaurantId:1});
        }else if(BookingId==2){
            Actions.RestaurantView({RestaurantId:2});
        }else if(BookingId==3){
            Actions.RestaurantView({RestaurantId:3});
        }else if(BookingId==4){
            Actions.RestaurantView({RestaurantId:4});
        }
    }
    navigateBackOption=()=>{
        Actions.pop();
    }
    
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },

})

module.exports = RestaurantListView;