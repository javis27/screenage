import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground,
  ScrollView
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import Moment from 'moment';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';
import SpaBooking from '../components/SpaBooking';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      langLoading: true,
      username: '',
      password: '',
      myArray: [],
      selctedLanguage: 'Choose your Language',
      langId: '',
      totalData:[],
      boatTimeslotArray:[]
    }
  }
  
  componentWillMount() {
    this._loadboattimeDetails();
  }
  // loadtimeDetails = async () => {
  //   return (
  //     <View style={{backgroundColor:Colors.LightGray,padding:20,marginLeft:20,marginRight:20}}>
  //   {this.state.totalData.map((Data) => {
  //     <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>{Data.BoatingDateTime.toString()}</Text>
  // })};
  //  </View>
  //   );
    
  //   // return (
  //   //   <View style={{backgroundColor:Colors.LightGray,padding:20,marginLeft:20,marginRight:20}}>
  //   //     {this.state.totalData.map(n => (
          
  //   //       <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>{n.BoatingDateTime.toString()}</Text>
         
  //   //     ))}
  //   //      </View>
  //   // );
      
  // }

  loadtimeDetails() {
    return this.state.totalData.map((dataItem)=>(
      
      <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}> - {Moment(dataItem.BoatingDateTime).format('lll')}</Text>
      
    ))
}
  render() {
    
    return (
      <ScrollView style={styles.container} >
      <View style={styles.container}>
        
        <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
        />
        <View style={{ flex: 1}}>
          <View style={{height:180}}>
            <ImageBackground source = {{uri: this.props.BoatDetails.ImagePath}} style = {{ width: '100%', height: 180, position: 'relative'}}/>
          </View>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:0.8}}>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>{this.props.BoatDetails.BoatActivityName}</Text>
            </View>
            {/* <View style={{flex:0.3}}>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>MUR {this.props.BoatDetails.ChildrenPrice} onwards</Text>
            </View> */}
            
          </View>
          <View>
            <Text style={{fontSize:14,fontFamily: AppFont.Regular,padding:10}}>{this.props.BoatDetails.Location}</Text>
          </View>
          <View style={{borderTopWidth:0.5, height:10,padding:10}}></View>
          <View>
            <Text style={{fontSize:14,fontFamily: AppFont.Regular,padding:10}}>{this.props.BoatDetails.Description}</Text>
          </View>
          <View style={{borderTopWidth:0.5, height:10,padding:10}}></View>
          <View>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>AT A GLANCE</Text>
          </View>
          <View style={{backgroundColor:Colors.LightGreyAdv,padding:20,marginLeft:20,marginRight:20}}>
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Duration : {this.props.BoatDetails.Duration} {this.props.BoatDetails.DurationUnitName}</Text>
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Number of Peoples : {this.props.BoatDetails.MaxPeople}</Text>
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Maximum Adults : {this.props.BoatDetails.MaxAdult}</Text>
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Maximum Children : {this.props.BoatDetails.MaxChildren}</Text>
          {this.props.BoatDetails.AdultOfferPrice == ''?
            <View>
              <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Adult Price : MUR {this.props.BoatDetails.AdultPrice}</Text>
            </View>
            :
            <View style={{flex:1,flexDirection:'row'}}>
              <View style={{flex:0.35}}><Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Adult Price :</Text></View>
              <View style={{flex:0.32, alignItems:'flex-start'}}><Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10,textDecorationLine:'line-through'}}>MUR {this.props.BoatDetails.AdultPrice}</Text></View>
              <View style={{flex:0.33, alignItems:'flex-start'}}><Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>MUR {this.props.BoatDetails.AdultOfferPrice}</Text></View>
            </View>
          }
          {this.props.BoatDetails.ChildrenOfferPrice == ''?
            <View>
              <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Children Price : MUR {this.props.BoatDetails.ChildrenPrice}</Text>
            </View>
            :
            // <View>
            //   <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Children Price : MUR {this.props.BoatDetails.ChildrenOfferPrice}</Text>
            // </View>
            <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:0.40}}><Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Children Price :</Text></View>
            <View style={{flex:0.30, alignItems:'flex-start'}}><Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10,textDecorationLine:'line-through'}}>MUR {this.props.BoatDetails.ChildrenPrice}</Text></View>
            <View style={{flex:0.30, alignItems:'flex-start'}}><Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>MUR {this.props.BoatDetails.ChildrenOfferPrice}</Text></View>
          </View>
          }
          
          {/* <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Children Price : {this.props.BoatDetails.ChildrenPrice}</Text>
          
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- Children Offer Price : {this.props.BoatDetails.ChildrenOfferPrice}</Text> */}
          </View>
          <View>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>EXTRAS OFFERED</Text>
          </View>
          <View style={{backgroundColor:Colors.LightGreyAdv,padding:20,marginLeft:20,marginRight:20}}>
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- {this.props.BoatDetails.ExtrasOffered}</Text>
          </View>
          {/* <View>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>LANGUAGES OFFERED</Text>
          </View>
          <View style={{backgroundColor:Colors.LightGreyAdv,padding:20,marginLeft:20,marginRight:20}}>
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>- {this.props.BoatDetails.LanguageOffered}</Text>
          </View> */}
          <View>
            <Text style={{fontSize:18,fontFamily: AppFont.Regular,padding:10}}>AVAILABLE TIME SLOTS</Text>
          </View>
          <View style={{backgroundColor:Colors.LightGreyAdv,padding:20,marginLeft:20,marginRight:20}}>
          {this.state.boatTimeslotArray.length > 0 ? this.loadtimeDetails() : 
          <View >
          <Text style={{fontSize:14,fontFamily: AppFont.Regular,marginLeft:10}}>No Available Time Slots</Text>
          </View>
          }
          </View>
          <View style={{ flex: 1, marginTop: 25,marginBottom:30, justifyContent: 'center' }}>
                                    <View style={{ width: "90%", marginLeft: 15 }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressBoatBooking}>
                                                <Text style={styles.btntext}>{I18n.t('bookbtn')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
        </View>
        
      </View>
      </ScrollView>
    );
  }
  
  _loadboattimeDetails = async () => {
    await this.setState({ spaloading: true });
    const details = await AsyncStorage.getItem(Keys.UserDetail);
    var jsonValue = JSON.parse(details);
    console.log("json", jsonValue)
    await this.setState({ userId: jsonValue.UserId });
    var languageId = await AsyncStorage.getItem(Keys.langId);
    const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    var HotelId = await AsyncStorage.getItem('HotelId');
    console.log("HotelId...",HotelId)
    
    let overalldetails = {
        "HotelId": HotelId,
        "BoatActivityId":this.props.BoatDetails.BoatActivityId
      }
      await apiCallWithUrl(APIConstants.GetBoatTimeSlotListAPI, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
    //await apiCallWithUrl(APIConstants.GetSpaMastersAPI + "?FullName=" + jsonValue.Fullname + "&&LanguageId=" + languageId + "&&CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
}

postCheckInDetailsResponse = async (response) => {
    if (response.IsException == "True") {
        return Alert.alert(response.ExceptionMessage);
    }
    const timeElapsed = Date.now();
    const today = new Date(timeElapsed);
    var currentdate=today.toISOString();
    this.getDropdownsResponse(response);
    this.setState({ minBookingDate: currentdate, maxBookingDate: currentdate })
    console.log("postCheckInResponse", response.InCustomer);
    
}


getDropdownsResponse = async (response) => {
    console.log("response.ResponseData.length", response.ResponseData.length)
    await this.setState({ spaloading: false, })
    if (response.ResponseData.length > 0) {
        this.setState({ totalData: response.ResponseData, refreshing: false })

        console.log("totalResponse", response.ResponseData);
        console.log("haaa...", response.ResponseData[0]);
        var list1 = [];
        response.ResponseData.map((spacategory, index) => {
            console.log("loop", index, spacategory.BoatingDateTime);
            list1.push(spacategory.BoatingDateTime);
        });
        this.setState({ boatTimeslotArray: list1 })

        

        console.log("boatTimeslotArray", this.state.boatTimeslotArray);
        // console.log("timeslotArray", this.state.spaTimeslotArray);
        
    }
}
_onPressBoatBooking = () => {
  Actions.CreateBoatBooking2({BoatDetails: this.props.BoatDetails });
}

}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    //justifyContent: 'center',
    backgroundColor: Colors.Background_Color,
    height: '100%'
},
Restaurant: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginTop: 5
},
IconReqText: {
    color: Colors.Black,
    fontSize: 16,
    fontFamily: AppFont.Regular,
},
IconReqText1: {
  color: Colors.Black,
  fontSize: 14,
  fontFamily: AppFont.Regular,
},
DescriptionServiceList: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15
},
DescriptionServiceList1: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    backgroundColor: "#8CCED6"
},
ServiceText: {
    color: Colors.Black,
    fontSize: 13,
    // paddingHorizontal: 15,
    // paddingVertical: 7,
    fontFamily: AppFont.Regular,
},
ServiceText1: {
    color: 'white',
    fontSize: 15,
    // paddingHorizontal: 15,
    // paddingVertical: 7,
    fontFamily: AppFont.Regular,
},
containerAmount: {
    // backgroundColor: Colors.FullGray,
    // borderRadius: 5,
},
boxWithShadow: {
    borderColor: Colors.Background_Gray,
    borderWidth: 0.5,
    color: '#dbdbdb',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
    marginTop: 10,
    borderRadius: 5,
    shadowOffset: { width: 2, height: 2, },
    shadowColor: Colors.LightGray,
    shadowOpacity: 0.3,
    borderRadius: 5,
},
button: {
  alignItems: 'center',
  backgroundColor: Colors.App_Font,
  padding: 10,
  width: "100%",
  marginLeft: 8,
  marginRight: 8,
  borderRadius: 5
},
btntext: {
  color: "#FFF",
  fontSize: 14,
  letterSpacing: 1,
  fontFamily: AppFont.Regular,
},
});

module.exports = Language;