import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    FlatList,
    StatusBar,
    Dimensions,
    Platform,
    Alert
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import ServiceListItem from '../components/ServiceListItem';
import AllServiceListItem from '../components/AllServiceListItem';
// import RoomServiceListItem from '../components/RoomServiceListItem';
import SegmentedControlTab from "react-native-segmented-control-tab";
import Loader from '../components/Loader';

import { apiCallWithUrl, dynamicApiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';
import ActionButton from 'react-native-action-button';
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            serviceListArray: [],
            IsfetchResponse: false,
            selectedIndex: 0,
            isRefreshing: false,
            serviceMenuNamesArray: [],
            restaurantId: 1,
            selectedans:[],
            totalamountCart:0
        };
        //console.log("service props", this.props.restaurantDetails);
    }

    async componentDidMount() {
        console.log("componentDidMount Create RoomServiceBooking");
         await firebase.app();
         analytics().logScreenView({
            screen_name: 'CreateRoomServiceScreen',
            screen_class: 'CreateRoomServiceScreen'
        });
        this._loadServices();
        // this.verifyCheckInDetails();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {

    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this._loadServices();
    //         }
    //     }
    // }

    handleSubMenuIndexChange = async (childindex) => {
        await this.setState({
            ...this.state,
            selectedIndex: childindex,
            IsfetchResponse: false,
            loading: true,

        });

        const typeId = this.state.selectedIndex + 1;  // Since we are hiding 'ALL' in Menu so we are adding '+ 1' to index
        const langId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);

        console.log("service type", typeId);
        //await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=" + this.state.restaurantId + "&&ServiceTypeId=" + typeId + "&&LanguageId=" + langId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
        
            let overalldetails1 = {
                "HotelId": this.state.HotelId,
                "RestaurantId":"1",
                "LanguageId":langId,
                "ServiceTypeId":typeId,
              }
              await apiCallWithUrl(APIConstants.GetServiceList, 'POST', overalldetails1, (callback) => { this.postCheckInDetailsResponse(callback) });  
    };

    renderEmptyComponent = () => {
        if (this.state.serviceListArray.length != 0) return null;
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center", height: Dimensions.get('window').height - 200, width: Dimensions.get('window').width, }}>
                <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', fontFamily: AppFont.Regular }}>{I18n.t('nodata')}</Text>
            </View>
        );
    };

    updateTabBar = () => {
        //let bgListColors = [Colors.Background_Color, Colors.ShadowGray];

        // if (this.state.selectedIndex == 0) {
        //     return (<View style={{ marginBottom: 100 }}>{this.state.IsfetchResponse ? <FlatList
        //         refreshing={this.state.isRefreshing}
        //         onRefresh={this._onRefreshServices}
        //         data={this.state.serviceListArray}
        //         keyExtractor={(x, i) => i}
        //         ListEmptyComponent={this.renderEmptyComponent}
        //         renderItem={({ item, index }) => {
        //             return (
        //                 // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
        //                 <View style={styles.boxWithShadow}><AllServiceListItem services={item} /></View>
        //                 // </View>
        //             )
        //         }}
        //     /> : null}</View>);
        // }
        //if (this.state.selectedIndex == 0) {
            return (<View style={{ marginBottom: 100 }}>{this.state.IsfetchResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshServices}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} callbackfunc={this.getselecteditem}/></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        //}
        // else if (this.state.selectedIndex == 1) {
        //     return (<View style={{ marginBottom: 100 }}>{this.state.IsfetchResponse ? <FlatList
        //         refreshing={this.state.isRefreshing}
        //         onRefresh={this._onRefreshServices}
        //         data={this.state.serviceListArray}
        //         keyExtractor={(x, i) => i}
        //         ListEmptyComponent={this.renderEmptyComponent}
        //         renderItem={({ item, index }) => {
        //             return (
        //                 //<View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
        //                 <View style={styles.boxWithShadow}><ServiceListItem services={item} callbackfunc={this.getselecteditem}/></View>
        //                 // </View>
        //             )
        //         }}
        //     /> : null}</View>);
        // }
        // else if (this.state.selectedIndex == 2) {
        //     return (<View style={{ marginBottom: 100 }}>{this.state.IsfetchResponse ? <FlatList
        //         refreshing={this.state.isRefreshing}
        //         onRefresh={this._onRefreshServices}
        //         data={this.state.serviceListArray}
        //         keyExtractor={(x, i) => i}
        //         ListEmptyComponent={this.renderEmptyComponent}
        //         renderItem={({ item, index }) => {
        //             return (
        //                 // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
        //                 <View style={styles.boxWithShadow}><ServiceListItem services={item} callbackfunc={this.getselecteditem}/></View>
        //                 // </View>
        //             )
        //         }}
        //     /> : null}</View>);
        // }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader
                    loading={this.state.loading} />

                {this.state.serviceMenuNamesArray.length > 0 ?
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flex:0.90}}>
                        <SegmentedControlTab
                            tabsContainerStyle={styles.tabsContainerStyle}
                            tabStyle={styles.tabStyle}
                            firstTabStyle={styles.firstTabStyle}
                            lastTabStyle={styles.lastTabStyle}
                            tabTextStyle={styles.tabTextStyle}
                            activeTabStyle={styles.activeTabStyle}
                            activeTabTextStyle={styles.activeTabTextStyle}
                            // values={[I18n.t("all"), I18n.t("breakfast"), I18n.t("lunch"), I18n.t("dinner")]}
                            values={this.state.serviceMenuNamesArray}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={this.handleSubMenuIndexChange}
                        />
                        {this.updateTabBar()}
                        </View>
                        <View style={{ flex:0.10 }}>
                            {this.state.selectedans.length>0?
                                        <View style={{ width: "90%", marginLeft: 20,alignItems: 'center', marginBottom:10 }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.gotocart}>
                                                <Text style={styles.btntext}>{I18n.t('view_cart')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        :
                                        <View></View>
                            }
                        </View>
                    </View>
                    
                    : null}




            </View>
        )
    }
    getselecteditem = async (item,qty) => {
        console.log("item..",item)
        Alert.alert(qty + I18n.t('items_added_to_card'))
        const seldata= {
            "RoomServiceId":item.RoomServiceId,
            "ServiceTypeId":item.ServiceTypeId,
            "ItemName":item.ItemName,
            "ItemTypeId":item.ItemTypeId,
            "ItemType":item.ItemType,
            "SpicyLevel":item.SpicyLevel,
            "Price":item.Price,
            "Availability":item.Availability,
            "AvailabilityId":item.AvailabilityId,
            "ItemImagePath":item.ItemImagePath,
            "Remarks":item.Remarks,
            "Qty":qty
        }
        console.log("seldata..",seldata)
        var joined1 = this.state.selectedans.concat(seldata);
        var totalamount = parseInt(this.state.totalamountCart) + parseInt(qty )*parseInt(item.Price )
        await this.setState({ selectedans: joined1 , totalamountCart:totalamount})
        console.log("selectedans..",this.state.selectedans)
        console.log("totalamountCart..",this.state.totalamountCart)
        await analytics().logEvent('RoomService_Booking', {
            id: 1,
            ClickedOption: 'Cart In Room Service',
            item:this.state.selectedans,
            Amount: this.state.totalamountCart
          })
        //Actions.FoodDetails({"Details":item});
    }
    gotocart=()=>{
        var values = this.state.selectedans
//         var valueArr = values.map(function(item){ return item.ItemName });
// var isDuplicate = valueArr.some(function(item, idx){ 
//     return valueArr.indexOf(item) != idx 
// });
// console.log(isDuplicate);
//unique = [...new Set(values.map(a => a.name))];
        //alert('entered');
        Actions.Cart({"cartdetails":this.state.selectedans,"totalAmountInCart":this.state.totalamountCart});
    }
    _loadServices = async () => {
        this.setState({
            loading: true,
        });
        const typeId = this.state.selectedIndex + 1;    // Since we are hiding 'ALL' in Menu so we are adding '+ 1' to index  
        const langId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        await this.setState({ HotelId:HotelId });
        console.log("HotelId...",HotelId)

        console.log("service type", typeId);
        let overalldetails = {
            "HotelId": HotelId,
          }
        //   let overalldetails1 = {
        //     "HotelId": HotelId,
        //     "RestaurantId":"1",
        //     "LanguageId":langId,
        //     "ServiceTypeId":typeId,
        //   }
          await apiCallWithUrl(APIConstants.GetServiceTabbarAPI, 'POST', overalldetails, (callback) => { this.getServiceTabbarResponse(callback) });
        // await apiCallWithUrl(APIConstants.GetServiceTabbarAPI + "?LanguageId=" + langId, 'GET', "", this.getServiceTabbarResponse)
        //await apiCallWithUrl(APIConstants.GetServiceList, 'POST', overalldetails1, (callback) => { this.postCheckInDetailsResponse(callback) });

        // await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=" + this.state.restaurantId + "&&ServiceTypeId=" + typeId + "&&LanguageId=" + langId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
    }

    _onRefreshServices = () => {
        this.setState({ isRefreshing: true });
        this._loadServices();
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getServiceListResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getServiceTabbarResponse = async (response) => {
        console.log("tabbarresponse", response.ResponseData)
        if (response.ResponseData.length > 0) {
            var getserviceMenuListResponse = response.ResponseData;
            var list1 = [];
            getserviceMenuListResponse.map((menus, index) => {
                list1.push(menus.ServiceTabbarName)
            });
            console.log('list...',list1);
            await this.setState({ loading: false, serviceMenuNamesArray: list1 });
            const typeId = this.state.selectedIndex + 1;
            const langId = await AsyncStorage.getItem(Keys.langId);
            let overalldetails1 = {
                "HotelId": this.state.HotelId,
                "RestaurantId":"1",
                "LanguageId":langId,
                "ServiceTypeId":typeId,
              }
              await apiCallWithUrl(APIConstants.GetServiceList, 'POST', overalldetails1, (callback) => { this.postCheckInDetailsResponse(callback) });  
        }
    }

    getServiceListResponse = async (response) => {
        if (response.IsException == null) {
            var getserviceListResponse = response.ResponseData;
            // var getserviceMenuListResponse = [{ id: "1", name: "Any" }, { id: "2", name: "Break" }, { id: "3", name: "Lun" }, { id: "4", name: "Dinn" }];
            // var list1 = [];
            // getserviceMenuListResponse.map((menus, index) => {
            //     list1.push(menus.name)
            // })
            console.log('get services', getserviceListResponse);

            await this.setState({ loading: false, serviceListArray: getserviceListResponse, IsfetchResponse: true, isRefreshing: false });
            // await this.setState({ loading: false, serviceListArray: getserviceListResponse, IsfetchResponse: true, isRefreshing: false, serviceMenuNamesArray: list1 });

            //console.log('get service length', this.state.serviceListArray.length);
            // console.log('get service menus', this.state.serviceMenuNamesArray);
            // console.log('get service menu length', this.state.serviceMenuNamesArray.length);

        }
        this.setState({ loading: false })
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    tabStyle: {
        //backgroundColor: "#FFFFFF",
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabStyle: {
        // backgroundColor: Colors.TabActive,
        backgroundColor: Colors.App_Font,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabTextStyle: {
        // color: Colors.Black,
        color: Colors.Background_Color,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    AvailableText: {
        color: Colors.AvailableColor,
        fontSize: 13
    },
    NotAvailableText: {
        color: Colors.NotavailableColor,
        fontSize: 13
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
      },
      btntext: {
        color: "#FFF",
        fontSize: 14,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
      },

})

module.exports = Services;