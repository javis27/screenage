import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    StatusBar
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import CancellationPolicy from '../components/CancellationPolicy';
import ListRestaurantBookings from '../components/ListRestaurantBookings';
import ListSpaBookings from '../components/ListSpaBookings';
import ListBikeBookings from '../components/ListBikeBookings';
import SegmentedControlTab from "react-native-segmented-control-tab";
import Loader from '../components/Loader';

import { apiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';

class BookingsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            restaurantListArray: [],
            cancelPolicyVisible: false,
            selectedIndex: 0
        };

        // this.setCancelPolicyVisibility = this.setCancelPolicyVisibility.bind(this);
    }

    componentDidMount() {
        console.log("bookinglist", this.props);
        // this._loadRestaurantBookings();
        if (this.props.BookingListIndex !== undefined) {
            this.setState({
                selectedIndex: this.props.BookingListIndex
            });
            console.log("props", this.props.BookingListIndex)
        }
    }

    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };

    updateTabBar = () => {

        if (this.state.selectedIndex == 0) {
            return (<View style={{ marginBottom: 175 }}><ListRestaurantBookings /></View>);
        }
        else if (this.state.selectedIndex == 1) {
            return (
                <View style={{ marginBottom: 175 }}><ListSpaBookings /></View>
            );
        }
        else if (this.state.selectedIndex == 2) {
            return (
                <View style={{ marginBottom: 175 }}><ListBikeBookings /></View>
            );
        }
    }

    updateImage = () => {
        if (this.state.selectedIndex == 0) {
            return (<View>
                <Image source={require('../../assets/images/restaurant.jpg')} style={{ width: '100%', height: 120 }} />
            </View>);
        }
        else if (this.state.selectedIndex == 1) {
            return (<View>
                <Image source={require('../../assets/images/spa.jpg')} style={{ width: '100%', height: 120 }} />
            </View>);
        }
        else if (this.state.selectedIndex == 2) {
            return (<View>
                <Image source={require('../../assets/images/bike.jpg')} style={{ width: '100%', height: 120 }} />
            </View>);
        }
    }
    render() {
        // let statusVal = this.StatusColors(this.state.restaurantListArray.StatusName);
        return (
            <View style={styles.container}>
                {this.updateImage()}

                <View>
                    <StatusBar
                        translucent
                        backgroundColor='transparent'
                        barStyle='default'
                    />
                    {/* <StatusBar
                        translucent={false}
                        backgroundColor={Colors.App_Font}
                        barStyle="light-content"
                    /> */}
                    <Loader
                        loading={this.state.loading} />
                    <SegmentedControlTab
                        tabsContainerStyle={styles.tabsContainerStyle}
                        tabStyle={styles.tabStyle}
                        firstTabStyle={styles.firstTabStyle}
                        lastTabStyle={styles.lastTabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                        values={["Restaurant", "SPA", "Bike"]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />
                </View>

                {this.updateTabBar()}

            </View>
        )
    }

    StatusColors(item) {
        var x = item;
        var textbgColor = "";
        if (x == "Requested") {
            textbgColor = "orangered";
        }
        else if (x == "Confirmed") {
            textbgColor = "green";
        }
        else if (x == "Rejected") {
            textbgColor = "red";
        }

        return textbgColor;
    }

    handleCancelPolicy = async () => {
        await this.setState({ cancelPolicyVisible: true }, () => console.log("cancelPolicyVisible", this.state.cancelPolicyVisible))
    }

    async setCancelPolicyVisibility(data) {
        console.log('setAboutVisibility', data)
        await this.setState({ cancelPolicyVisible: data });
    }

    // _loadRestaurantBookings = async () => {
    //     if (this.props.BookingListIndex !== undefined) {
    //         this.setState({
    //             loading: true,
    //             selectedIndex: this.props.BookingListIndex
    //         });
    //         console.log("props", this.props.BookingListIndex)
    //     }

    //     const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
    //     var userJson = JSON.parse(userdetails);

    //     await apiCallWithUrl(APIConstants.GetRestaurantBookingsList + "?id=" + userJson.UserId, 'GET', "", this.getRestaurantBookingsResponse)
    // }

    // getRestaurantBookingsResponse = async (response) => {
    //     if (response != null) {
    //         var getrestaurantListResponse = response.ResponseData;
    //         console.log('getuserResponse', getrestaurantListResponse);

    //         await this.setState({ loading: false, restaurantListArray: getrestaurantListResponse });

    //         console.log('getuserResponse', this.state.restaurantListArray.length);

    //     }
    //     this.setState({ loading: false })
    // }

    _onPressButton = () => {
        Actions.Booking();
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    tabStyle: {
        // backgroundColor: "#FFFFFF",
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabStyle: {
        backgroundColor: Colors.App_Font,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabTextStyle: {
        color: Colors.Background_Color,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },

})

module.exports = BookingsList;