import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      langLoading: true,
      username: '',
      password: '',
      myArray: [],
      selctedLanguage: 'Choose your Language',
      langId: '',
    }
  }
  
  componentWillMount() {
    this._loadHotelDetails();
  }
  render() {
    const Item = ({ title }) => (
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
    const renderItem = ({ item }) => (
      <View style={{marginTop:10,marginLeft:10,marginRight:10,marginBottom:10,borderRadius:30}}>
        <TouchableOpacity onPress={this._onSelectedHotel.bind(this,1)}>
          {/* <Item title={item.HotelName} /> */}
            <ImageBackground source = {{uri:item.Image}} style = {{ width: '100%', height: 180, position: 'relative', borderRadius:20,borderWidth: 1,borderColor:'#FFF'}}>
                <Text
                  style={{
                    fontFamily:AppFont.Light,
                    fontSize:32,
                    color: '#FFFFFF',
                    position: 'absolute', // child
                    bottom: 25, // position where you want
                    left: 10,
                    marginRight:10
                  }}
                >
                  {item.HotelName}
                </Text>
                <Text
                  style={{
                    fontFamily:AppFont.Light,
                    
                    fontSize:22,
                    color: '#FFFFFF',
                    position: 'absolute', // child
                    bottom: 3, // position where you want
                    left: 10
                  }}
                >
                  {item.Location}
                </Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    );
    return (
      <View style={styles.container}>
        <Loader
          loading={this.state.langLoading} />
        <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
        />
        <View style={{ flex: 1}}>
          <View style={{marginTop:30}}></View>
          <FlatList
            data={this.state.totalData}
            renderItem={renderItem}
            keyExtractor={item => item.HotelId}
          />
        </View>
        
      </View>
    );
  }
  
  _loadHotelDetails = async () => {
      await apiCallWithUrl(APIConstants.HotelSelectionAPI, 'POST', '', this.hotelApiResponse);
  }
  _onSelectedHotel = async (HotelId) => {
    console.log("AllNewHotelId",HotelId)
    await AsyncStorage.setItem('HotelId',  HotelId.toString());
    Actions.UserWelcome();
    //Actions.pop();
}
  hotelApiResponse = async (response) => {
    console.log("Language response", response);

    // this.setState({ totalData: response.ResponseData, "selctedLanguage": response.ResponseData[0].NativeName, })
    await this.setState({ totalData: response.ResponseData, langLoading: false })

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column'
  },
  loginview: {
    flex: 0.8,
    alignItems: 'center',
    //width: "40%"
  },
  button: {
    backgroundColor: Colors.App_Font,
    padding: 10,
    width: "95%",
    borderRadius: 5
  },
  btntext: {
    fontFamily: AppFont.Regular,
    color: Colors.Background_Color,
    fontSize: 15,
    textAlign: 'center',
    //fontFamily: AppFont.SemiBold,
  },
  copyrightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  copyrightText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    color: Colors.LightGray
  },
  versionText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    paddingVertical: 10,
    color: '#dbdbdb'
  }
});

module.exports = Language;