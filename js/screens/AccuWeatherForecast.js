import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	StatusBar,
	TouchableOpacity,
	AsyncStorage,
	Alert,
	Platform
} from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import { FlatList } from 'react-native';
import AccuForecastCard from '../components/AccuForecastCard';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';
import I18n from '../constants/i18n';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl, dynamicApiCallWithUrl } from '../api/APIHandler';

import BottomSheet from 'react-native-bottomsheet';

export default class AccuWeatherForecast extends React.Component {

	constructor(props) {
		super(props);

		// this.state = {
		// 	latitude: this.props.positions.latitude,
		// 	longitude: this.props.positions.longitude,
		// 	forecast: [],
		// 	location:{},
		// 	error: '',
		// };

		this.state = {
			locationDetails: this.props.locationData,
			forecast12Hours: [],
			loading: false,

			//newly added state
			selectedCity: I18n.t('chooselocation'),
			selectedLat: '',
			selectedLong: '',
			cityArray: [],
		}

		console.log("accuweather", this.props)
	}

	componentWillMount() {
		this.loadDropdowns();
		//	this.verifyCheckInDetails();
	}

	// verifyCheckInDetails = async () => {
	// 	await this.setState({ loading: true });
	// 	const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
	// 	let overalldetails = {
	// 		"CheckInId": checkInVal,
	// 	}
	// 	console.log("overalldetails", overalldetails);
	// 	await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
	// }

	// postCheckInDetailsResponse = async (response) => {
	// 	if (Platform.OS === 'ios') {
	// 		this.setState({ loading: false }, () => {
	// 			setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
	// 		});
	// 	} else {
	// 		this.setState({
	// 			loading: false,
	// 		}, () => this.funcCheckInDetailsResponse(response));
	// 	}
	// }

	// funcCheckInDetailsResponse = async (response) => {
	// 	var postCheckInDetailResponse = response.ResponseData;
	// 	if (response.IsException == "True") {
	// 		return Alert.alert(response.ExceptionMessage);
	// 	}

	// 	if (postCheckInDetailResponse !== null) {
	// 		console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
	// 		if (postCheckInDetailResponse.InCustomer == "False") {
	// 			console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
	// 			Alert.alert(I18n.t('alertnotcheckin'));
	// 			AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
	// 			AsyncStorage.removeItem(Keys.UserDetail);
	// 			AsyncStorage.removeItem(Keys.roomNo);
	// 			AsyncStorage.removeItem(Keys.inCustomer);
	// 			AsyncStorage.removeItem(Keys.checkInId);
	// 			Actions.GuestLogin();
	// 		} else {
	// 			// Get the user's location
	// 			this.get12HoursForecastByLocationkey();
	// 			// this.loadDropdowns();
	// 		}
	// 	}
	// }

	loadDropdowns = async () => {
		await this.setState({ loading: true });
		const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
		await apiCallWithUrl(APIConstants.GetWeatherLocations + "?CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
	}

	postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getDropdownsResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

	getDropdownsResponse = async (response) => {
		await this.setState({ loading: false, totalData: response.ResponseData })

		console.log("totalResponse", response.ResponseData);
		var list1 = [];
		response.ResponseData.map((data, index) => {
			console.log("loop", index, data.LocationName);
			list1.push(data.LocationName);
		});
		this.setState({ cityArray: list1 })

		console.log("cityArray", this.state.cityArray);
	}

	get12HoursForecastByLocationkey = () => {
		this.setState({
			loading: true,
		});
		console.log("dynamicApiCallWithUrl", APIConstants.WeatherAPI_URL + "forecasts/v1/hourly/12hour/" + this.state.locationDetails.Key + "?apikey=" + APIConstants.WeatherAPIKey + "&metric=true")
		dynamicApiCallWithUrl(APIConstants.WeatherAPI_URL + "forecasts/v1/hourly/12hour/" + this.state.locationDetails.Key + "?apikey=" + APIConstants.WeatherAPIKey + "&metric=true", 'GET', '', this.Hours12ForecastApiResponse);

	}

	Hours12ForecastApiResponse = (response) => {
		this.setState({
			loading: false,
		});
		console.log("res", response)
		this.setState({
			forecast12Hours: response
		})
	}

	render() {
		return (
			<View style={styles.container}>
				<Loader loading={this.state.loading} />
				<StatusBar
					translucent={false}
					backgroundColor={Colors.App_Font}
					barStyle='light-content'
				/>
				<View>
					<View style={{ paddingHorizontal: 15 }}>
						<Text
							allowFontScaling
							numberOfLines={1}
							style={styles.label} >
							{I18n.t('chooselocation')}
						</Text>
						<View >
							<TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetCities}>
								<Text style={styles.picker}>{this.state.selectedCity}</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.textBorderLine}></View>
					</View>
					<View style={{ paddingTop: 10 }}>
						<FlatList data={this.state.forecast12Hours} keyExtractor={item => item.DateTime} renderItem={({ item }) => <AccuForecastCard detail={item} location={this.state.locationDetails.EnglishName} />} />
					</View>
				</View>

			</View>
		);
	}

	//newly added methods
	_onBtnSheetCities = async () => {

		// var totalData = [
		// 	{
		// 		"id": "",
		// 		"value": "<Select>",
		// 		// "lat":"12.8373",
		// 		// "long":"79.7042"
		// 	},
		// 	{
		// 		"id": "1",
		// 		"value": "Kancheepuram",
		// 		"lat": "12.817",
		// 		"long": "79.702"
		// 	},
		// 	{
		// 		"id": "2",
		// 		"value": "Tambaram",
		// 		"lat": "12.897",
		// 		"long": "80.1"
		// 	},
		// 	{
		// 		"id": "5",
		// 		"value": "Mylapore",
		// 		"lat": "13.249",
		// 		"long": "79.898"
		// 	},
		// 	{
		// 		"id": "6",
		// 		"value": "Madurai",
		// 		"lat": "9.926",
		// 		"long": "78.127"
		// 	},
		// 	{
		// 		"id": "",
		// 		"value": "Cancel"
		// 	}
		// ]
		// var cityArray = ["<Select>", "Kanceepuram", "Tambaram", "Mylapore", "Madurai", "Cancel"]

		// var restaurantArrayLen = this.state.restaurantArray.length;
		BottomSheet.showBottomSheetWithOptions({
			// title: "Choose Restaurant",
			options: this.state.cityArray,
			dark: false,
			cancelButtonIndex: 12,
			// destructiveButtonIndex: 1,
		}, (value) => {
			// console.log("totalData.value", totalData[value].value);
			// this.setState({
			// 	"selectedCity": totalData[value].value,
			// 	"cityId": totalData[value].id,
			// 	"selectedLat": totalData[value].lat,
			// 	"selectedLong": totalData[value].long

			// }, () => {
			// 	this.getSelectedCityWeather();
			// })
			// console.log("cityId", this.state.cityId);
			// console.log("selectedCity", this.state.selectedCity);
			console.log("totalData.value", this.state.totalData[value].LocationName);
			this.setState({
				"selectedCity": this.state.totalData[value].LocationName,
				"cityId": this.state.totalData[value].LocationId,
				"selectedLat": this.state.totalData[value].Latitude,
				"selectedLong": this.state.totalData[value].Longitude

			}, () => {
				this.getSelectedCityWeather();
			})
			console.log("cityId", this.state.cityId);
			console.log("selectedCity", this.state.selectedCity);
		});
	}

	getSelectedCityWeather() {
		console.log("accu weather selected")
		dynamicApiCallWithUrl(APIConstants.WeatherAPI_URL + APIConstants.GeopositionSearchAPI + "?apikey=" + APIConstants.WeatherAPIKey + "&q=" + this.state.selectedLat + "%2C" + this.state.selectedLong, 'GET', '', this.locationApiResponse);
	}

	locationApiResponse = (response) => {
		console.log("res", response)
		this.setState({
			locationDetails: response
		})
		if (response.Key !== "undefined" || response.Key !== null) {
			this.get12HoursForecastByLocationkey();
		}
	}


}

const styles = StyleSheet.create({
	container: {
		// flex: 1,
		//justifyContent: 'center',
		backgroundColor: Colors.Background_Color,
		height: '100%',
		//paddingHorizontal: 15
	},
	label: {
		marginTop: 10,
		marginLeft: 8,
		marginRight: 8,
		fontSize: 13,
		fontFamily: AppFont.Regular,
		color: Colors.Black
	},
	picker: {
		color: Colors.PlaceholderText,
		marginLeft: 8,
		height: 28,
		padding: 0,
		textAlign: 'left',
		fontFamily: AppFont.Regular,
		fontSize: 15,
	},
	textBorderLine: {
		marginLeft: 8,
		marginRight: 8,
		borderBottomColor: Colors.App_Font,
		borderBottomWidth: 0.8
	},
})

module.exports = AccuWeatherForecast;