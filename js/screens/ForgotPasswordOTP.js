import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    AsyncStorage,
    Alert,
    Image,
    KeyboardAvoidingView,
    ImageBackground,
    Dimensions,
    StatusBar
} from 'react-native';

import TextBoxWithLink from '../components/TextBoxWithLink';
import CodeInput from 'react-native-confirmation-code-input';
import Keys from '../constants/Keys';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import I18n from '../constants/i18n';

import { apiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';


class ForgotPasswordOTP extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentStep: 1,
            // email: "",
            passRef: "abc",
            userEmailAddress: '',
            userVerificationCode: '',
            newPassword: '',
            checkOTPResponse: {},
            textboxDisableStatus: true,
            btnVerify: I18n.t('verify')  //'Verify'
        }

        // this.passRef = "";

        this.moveToStepTwo = this.moveToStepTwo.bind(this);
    }


    moveToStepTwo() {
        this.setState({ currentStep: 2 });
        console.log("Email Confirm: ", this.state.currentStep);
    }

    validateEmail = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            console.log("Email is Not Correct");
            this.setState({ email: text })
            return false;
        }
        else {
            this.setState({ email: text })
            console.log("Email is Correct");
        }
    }

    changePassword = async () => {
        const { userEmailAddress, currentStep } = this.state;
        // moveToStepTwo = () => {
        //     this.setState({ currentStep: 2 });
        //     this.refs.ForgetEmail.setStep(2);
        // }

        console.log("Email Confirm: ", userEmailAddress, currentStep);

        // await this.moveToStepTwo();
        await apiCallWithUrl(APIConstants.VerifyEmailIdAPI + "?EmailId=" + userEmailAddress, 'GET', "", this.getOTPResponse);
    }

    getOTPResponse = async (response) => {
        console.log('OTP Response', response)
        if (response.ResponseData !== undefined) {
            if (response.ResponseData.OTP !== "" || response.ResponseData.OTP != null) {
                // Alert.alert("OTP send to your register email address");
                this.moveToStepTwo();

                var getOtpResult = response.ResponseData;
                await this.setState({ checkOTPResponse: getOtpResult });
            }
        } else
            Alert.alert(I18n.t('alertsmthngwrng'));
    }

    setEmailAndSubmit = async (email) => {
        console.log("email", email)
        console.log("verify btn click", this.state)
        if (email == "") {
            Alert.alert(I18n.t('enteremailaddr'));
        } else {
            if (this.validateEmail(email) == false) {
                Alert.alert(I18n.t('alertnotvalidemail'));
            } else {
                if (this.state.btnVerify == I18n.t('verify')) {
                    console.log("verify")
                    //console.log("ABC : ", email);
                    await this.setState({ userEmailAddress: email, textboxDisableStatus: false, btnVerify: I18n.t('change') });
                    //console.log("ABC : ", this.state.userEmailAddress);
                    await this.changePassword();
                } else {
                    console.log("change")
                    Actions.refresh({ key: Math.random() });
                }
            }

        }



        // if (email == "") {
        //     if (this.state.btnVerify == 'Verify') {
        //         console.log("verify")
        //         //console.log("ABC : ", email);
        //         await this.setState({ userEmailAddress: email, textboxDisableStatus: false, btnVerify: 'Change' });
        //         //console.log("ABC : ", this.state.userEmailAddress);
        //         await this.changePassword();
        //     } else {
        //         console.log("change")
        //         Actions.refresh({ key: Math.random() });
        //     }
        // }
        // else {
        //     Alert.alert(I18n.t('enteremailaddr'));
        // }
    }

    onPressForgotPassword = () => {
        console.log('Email Confirm', this.state.userVerificationCode, this.state.checkOTPResponse.OTP)
        if (this.state.userVerificationCode == "") {
            Alert.alert(I18n.t('enterverifycode'));
        } else {
            if (this.state.userVerificationCode === this.state.checkOTPResponse.OTP) {
                Actions.ForgotPassword({ user_id: this.state.checkOTPResponse.UserId });
            } else {
                Alert.alert(I18n.t('otpnotmatch'));
                this.refs.codeInputRef2.clear();
            }
        }

    }

    _onPressForgotLogin = () => {
        Actions.Login();
    }

    _onFulfill(code) {
        // TODO: call API to check code here
        // If code does not match, clear input with: this.refs.codeInputRef1.clear()
        console.log("code", code)
        if (code != '') {
            this.setState({ userVerificationCode: code })
        } else {
            Alert.alert(
                'Preskil',
                'Please try again',
                [{ text: 'OK' }],
                { cancelable: false }
            );

            this.refs.codeInputRef2.clear();
        }

    }

    render() {
        currentStep = this.state.currentStep;
        //  passRefrence = "";

        console.log('currentstep', this.state.currentStep, this.state.textboxDisableStatus);

        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle="light-content"
                />
                <View style={{ justifyContent: "center", marginTop: 0 }}>
                    <TextBoxWithLink placeholder={I18n.t('emailid')} linkHandler={this.setEmailAndSubmit} textboxDisableStatus={this.state.textboxDisableStatus} verifybtnText={this.state.btnVerify} ref="ForgetEmail" />
                </View>
                {currentStep > 1 ?
                    <View style={{ marginTop: 0 }}>
                        {currentStep == 2 ?
                            <Text style={{ fontSize: 14, fontFamily: AppFont.Regular, textAlign: 'center', marginTop: 15, paddingLeft:15, paddingRight:15 }}>
                                {I18n.t('emailvericationcode')}
                            </Text>
                            :
                            <View></View>
                        }
                        <View style={{ fontSize: 14, fontFamily: AppFont.Regular, textAlign: 'center', marginTop: 15 }}>
                            <CodeInput
                                ref="codeInputRef2"
                                keyboardType="numeric"
                                // secureTextEntry
                                //compareWithCode='AsDW2'
                                activeColor='rgba(108, 211, 213, 1)'
                                inactiveColor='rgba(108, 211, 213, 1.3)'
                                // autoFocus={false}
                                ignoreCase={true}
                                inputPosition='center'
                                size={50}
                                onFulfill={(code) => this._onFulfill(code)}
                                //onFulfill={(code) => { this.setState({ currentStep: 3, userVerificationCode: code }); }}
                                containerStyle={{ marginTop: 10 }}
                                codeInputStyle={{ borderWidth: 1.5 }}
                            />
                        </View>
                        <View style={styles.loginview}>
                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.onPressForgotPassword}>
                                <Text style={styles.btntext}>{I18n.t('verify')}</Text>
                            </TouchableOpacity>
                        </View>
                        {/* {currentStep == 2 ?
                            <Text style={{ fontSize: 14, fontFamily: AppFont.Regular, textAlign: 'center', marginTop: 15 }}>
                                Enter the verification code to validate
                            </Text>
                            :
                            <View></View>
                        } */}
                        {/* {currentStep == 3 ?
                            <View style={{ flexDirection: "column", justifyContent: "center", marginTop: 15 }}>
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "center" }}>

                                </View>
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", marginTop: "15%" }}>
                                    <TouchableOpacity style={styles.update} onPress={() => { this.ConfirmChangePassword() }}>
                                        <Text style={{ color: "white", justifyContent: "center", fontSize: 18 }}>
                                            Update
                                    </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            :
                            <View>
                            </View>
                        } */}
                    </View>
                    :
                    <View></View>
                }

                <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", marginTop: 15 }}>
                    <TouchableOpacity style={styles.newUser} onPress={this._onPressForgotLogin}>
                        <Text style={{ color: "white", justifyContent: "center", fontSize: 14, textDecorationLine: 'underline' }}>
                            {I18n.t('backtologin')}
                    </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: Colors.Background_Color,
        flex: 1
    },
    newUser: {
        height: 30
    },
    loginview: {
        alignItems: 'center',
        marginTop: 80,
        marginBottom: 15
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "75%",
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
});

export default ForgotPasswordOTP;