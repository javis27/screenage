import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import ListRoomServiceBookings from '../components/ListRoomServiceBookings';
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

class RoomServiceBooking extends React.Component {

  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    //this._loadHotelDetails();
  }
  async componentDidMount() {
    console.log("componentDidMount RoomServiceBooking");
     await firebase.app();
     analytics().logScreenView({
        screen_name: 'RoomServiceListScreen',
        screen_class: 'RoomServiceListScreen'
    });
}
  render() {
    
    return (
      
      <View style={{ flex: 1, backgroundColor: "white" }}>
          {/* <StatusBar
              backgroundColor={Colors.App_Font}
              barStyle='light-content'
          /> */}
          <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
          <ListRoomServiceBookings/>
          <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.createRoomServiceBooking}
          style={styles.TouchableOpacityStyle}>
          <Image
             source={require('../../assets/images/Plus.png')}
            style={styles.FloatingButtonStyle}
          />
        </TouchableOpacity>
        </View>
       
    );
  }
  
  createRoomServiceBooking =  async() => {
    await analytics().logEvent('RoomService_Booking', {
      id: 1,
      ClickedOption: 'Create RoomService Booking',
    })
  Actions.Services();
  //Actions.pop();
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column'
  },
  
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    borderRadius:50,
    backgroundColor:Colors.App_Font
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 25,
    height: 25,
    //backgroundColor:'black'
  },
});

module.exports = RoomServiceBooking;