import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Moment from 'moment';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import Swiper from 'react-native-swiper';
import CartListItem from '../components/CartListItem';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

// const data = [
//     {
//         "ItemName":"Restaurant",
//         "Id":1,
//         "ItemImagePath":require('../../assets/images/Restaurant_bg1.jpg')
//     },
//     {
//         "ItemName":"SPA",
//         "Id":2,
//         "ItemImagePath":require('../../assets/images/SPA_bg1.jpg')
//     },
//     {
//         "ItemName":"Bike",
//         "Id":3,
//         "ItemImagePath":require('../../assets/images/Bike_bg1.jpg')
//     },
//     {
//         "ItemName":"Boat",
//         "Id":4,
//         "ItemImagePath":require('../../assets/images/Boat_bg1.jpg')
//     }

// ];

class GuestHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            totalamountCartnew:this.props.totalAmountInCart,
            minBookingDate: '',
      maxBookingDate: '',
            
        }
    }

    componentWillMount() {
        //this.handleBackAddEventListener();
        console.log("enteringcart",this.props.cartdetails)
        this.setState({
            "totalDetails": this.props.cartdetails
        })
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        //BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }


    render() {
        
        return (
            <View style={styles.container}>
                <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
                />
                {/* <Loader loading={this.state.loadingMenu} /> */}

{/*                 
                <View style={{ flex: 1}}>
                    <View style={{marginTop:30}}></View>
                    <FlatList
                        data={this.props.cartdetails}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxWithShadow}><CartListItem services={item} servicesIndex={index} callbackfunc={this.getselecteditem}/></View>
                            )
                        }}
                       /> 
                </View> */}



                <View style={{flexDirection:"column",flex:1}}>
                    <ScrollView>
                        <View style={{flex:0.60}}>
                        <FlatList
                        data={this.props.cartdetails}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxWithShadow}><CartListItem services={item} servicesIndex={index} callbackfunc={this.getselecteditem}/></View>
                            )
                        }}
                       /> 
                        </View>
                        {/* <View  style={{flex:0.05}}>
                            <Text style={{color: Colors.Black,fontSize: 14,letterSpacing: 1,fontFamily: AppFont.Regular,marginLeft:5}}>Preferred Delivery Date & Time</Text>
                        </View> */}
                        <View  style={{flex:0.10,flexDirection:'row'}}>
                            <View style={{flex:0.40}}>
                            <DatePicker
                        style={{ width: null, marginLeft: 8, }}
                        date={this.state.bookingDate}
                        mode="date"
                        placeholder={I18n.t('date_alone')}
                        // format="LL LT"
                        format="DD/MM/YYYY"
                        // maxDate={this.state.maximumDate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={{ uri: 'date' }}
                        customStyles={{
                          dateInput: DatePickerAttributes.dateInput,
                          dateIcon: DatePickerAttributes.dateIcon,
                          placeholderText: DatePickerAttributes.placeholderText,
                          dateText: DatePickerAttributes.dateText,
                          btnTextCancel: DatePickerAttributes.btnTextCancel,
                          btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                        }}
                        onDateChange={(date) => {
                          this.setState({
                            bookingDate: date,
                          })
                        }}
                      />
                            </View>
                            <View style={{flex:0.35}}>
                            <DatePicker
                        style={{ width: null, marginLeft: 8, }}
                        date={this.state.bookingTime}
                        mode="time"
                        placeholder={I18n.t('time')}
                        // format="LL LT"
                        format="HH:mm"
                        minDate="10:30"
                        maxDate="19:30"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={{ uri: 'date' }}
                        is24Hour={false}
                        androidMode="spinner"
                        customStyles={{
                          dateInput: DatePickerAttributes.dateInput,
                          dateIcon: DatePickerAttributes.dateIcon,
                          placeholderText: DatePickerAttributes.placeholderText,
                          dateText: DatePickerAttributes.dateText,
                          btnTextCancel: DatePickerAttributes.btnTextCancel,
                          btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                        }}
                        onDateChange={(time) => {
                          this.setState({
                            bookingTime: time,
                          })
                        }}
                      />
                            </View>
                        
                        </View>
                        <View style={{flex:0.15}}>
                        <TextInput
                            multiline={true}
                            underlineColorAndroid="transparent"
                            style={styles.textInput}
                            numberOfLines={4}
                            placeholder={I18n.t('add_your_comments_here')}
                            onChangeText={(text) => this.setState({comments:text})}
                            value={this.state.comments}/>
                        </View>
                        <View style={{flex:0.15,marginTop:20}}>
                            
                        <View style={{ width: "90%", marginBottom: 35, marginLeft: 20}}>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressConfirm}>
                                <Text style={styles.btntext}>{I18n.t('confirm_order')} {I18n.t('mur')} {this.props.totalAmountInCart}</Text>
                                    {/* <Text style={styles.btntext}>Confirm Order</Text> */}
                                </TouchableOpacity>
                            </View>
                         </View>
                         </View>
                         </ScrollView>
                         </View>

            </View>
        )
    }
    _onPressConfirm = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        await this.setState({ userId: userJson.UserId,HotelId:HotelId });
        console.log("HotelId...",HotelId)
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        var today = new Date();
        var todayDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var todayTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var requestedDateTime = todayDate + ' ' + todayTime;
        var copyItems =[]
        this.props.cartdetails.forEach(function(item){
            console.log("itemname..",item.ItemName);
            let servicelist = {
            "RoomServiceId": item.RoomServiceId,
            "Quantity":item.Qty,
            "Price":item.Price
          }
            copyItems.push(servicelist)
          })
          console.log("copyitems..",copyItems);
          console.log("this.state.bookingDate..",this.state.bookingDate);
          console.log("this.state.bookingTime..",this.state.bookingTime);
          var res = this.state.bookingDate.split("/");
            var bookingdt=res[2]+'-'+res[1]+'-'+res[0];
            var bookingdatetime=bookingdt+' '+this.state.bookingTime;
        let overalldetails = {
            "DeliveryDateTime": bookingdatetime,
            "RoomServiceList":copyItems,
            "UserId":this.state.userId,
            "RoomNo":roomNo,
            "Remarks":this.state.comments,
            "HotelId":this.state.HotelId
          }
          console.log("overalldetails..",overalldetails);

          await apiCallWithUrl(APIConstants.CreateRoomServiceAPI, 'POST', overalldetails, (callback) => { this.getafterCartResponse(callback) });
    }
    getafterCartResponse = async (response) => {
        this.setState({
            loading: false,
        });
        if (response != null) {
            Alert.alert('Thank You, Your order is placed!');
            Actions.pop();
        }
    }
    getselecteditem = async (item) => {
        console.log("item..",item)
        this.props.cartdetails.splice(item, 1)
        console.log("this.props.cartdetails..",this.props.cartdetails)
        var landSum = this.props.cartdetails.reduce(function(sum, d) {
            return parseInt(sum) + parseInt(d.Price);
          }, 0);
          console.log("landSum",landSum);
        
             this.props.totalAmountInCart = landSum
            console.log("this.props.totalAmountInCart",this.props.totalAmountInCart);
            //await this.setState({  totalamountCart:landSum})
            await this.setState({ totalamountCartnew: landSum }, () => console.log("totalamountCart", this.state.totalamountCartnew))
        Actions.refresh({ key: Math.random() * 1000000,totalAmountInCart:landSum}) 
        console.log("propslength",this.props.cartdetails.length)
        if(this.props.cartdetails.length == 0){
            Actions.pop();
        }
    }
    
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
      },
      btntext: {
        color: "#FFF",
        fontSize: 16,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
      },
      textInput: {
        fontFamily: AppFont.Regular,
        textAlignVertical:'top',
        textAlign: 'left',
        height: 35,
        fontSize: 17,
        height:100,
        padding: 0,
        includeFontPadding: false,
        borderWidth:0.5,
        borderColor:Colors.LightGray,
        marginLeft:10,
        marginRight:10,

      },
})

module.exports = GuestHome;