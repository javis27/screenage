import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    StatusBar
} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import ServiceListItem from '../components/ServiceListItem';
import AllServiceListItem from '../components/AllServiceListItem';
import SegmentedControlTab from "react-native-segmented-control-tab";
import Loader from '../components/Loader';

import { apiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            serviceListArray: [],
            IsfetchResponse: false,
            mainSelectedIndex:0,
            selectedIndex: 0,
            IsFoodMenu: true,
        };

        // this.setCancelPolicyVisibility = this.setCancelPolicyVisibility.bind(this);
    }

    componentDidMount() {
        this._loadServices();
    }

    handleIndexChange = async (index) => {
        console.log("handleIndexChange",index)
        await this.setState({
            ...this.state,
            selectedIndex: index,
            IsfetchResponse: false,
            loading: true,
        });

        console.log('handleIndexState',this.state)

        const typeId = this.state.selectedIndex;

        console.log("service type", typeId);
        await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=1&&ServiceTypeId=" + typeId, 'GET', "", this.getServiceListResponse)
    };

    handleMainMenuIndexChange = async (index) => {
        console.log("handleMainMenuIndexChange",index)
        await this.setState({
            ...this.state,
            mainSelectedIndex: index,
            IsfetchResponse: false,
            //  loading: true,
            IsFoodMenu: false
        });

        const typeId = this.state.selectedIndex;
        console.log('handleMainMenuIndexChange',this.state)
        console.log("service type", typeId);
        await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=1&&ServiceTypeId=" + typeId, 'GET', "", this.getServiceListResponse)


    };

    renderEmptyComponent = () => {
        console.log("empty component", this.state.serviceListArray.length)
        if (this.state.serviceListArray.length != 0) return null;
        return (
            <View style={{ flex: 1, borderColor: 'red', borderWidth: 1.0 }}>
                <Text style={{ fontSize: 18, alignSelf: 'center', fontFamily: AppFont.Light, textAlign: 'center' }}>{I18n.t('nodata')}</Text>
            </View>
        );
    };

    updateTabBar = () => {
        //let bgListColors = [Colors.Background_Color, Colors.ShadowGray];

        if (this.state.selectedIndex == 0) {
            return (<View>{this.state.IsfetchResponse ? <FlatList
                // refreshing={this.state.isRefreshing}
                // onRefresh={this.AnnouncementItemNotif}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><AllServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 1) {
            return (<View>{this.state.IsfetchResponse ? <FlatList
                // refreshing={this.state.isRefreshing}
                // onRefresh={this.AnnouncementItemNotif}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 2) {
            return (<View>{this.state.IsfetchResponse ? <FlatList
                // refreshing={this.state.isRefreshing}
                // onRefresh={this.AnnouncementItemNotif}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        //<View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 3) {
            return (<View>{this.state.IsfetchResponse ? <FlatList
                // refreshing={this.state.isRefreshing}
                // onRefresh={this.AnnouncementItemNotif}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
    }

    updateRoomTabBar = () => {
        //let bgListColors = [Colors.Background_Color, Colors.ShadowGray];

        if (this.state.selectedIndex == 0) {
            return (<View>{this.state.IsfetchResponse ? <FlatList
                // refreshing={this.state.isRefreshing}
                // onRefresh={this.AnnouncementItemNotif}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><AllServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
        else if (this.state.selectedIndex == 1) {
            return (<View>{this.state.IsfetchResponse ? <FlatList
                // refreshing={this.state.isRefreshing}
                // onRefresh={this.AnnouncementItemNotif}
                data={this.state.serviceListArray}
                keyExtractor={(x, i) => i}
                // ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                        <View style={styles.boxWithShadow}><ServiceListItem services={item} /></View>
                        // </View>
                    )
                }}
            /> : null}</View>);
        }
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <View>
                    <Image source={require('../../assets/images/AboutUs.jpg')} style={{ width: '100%', height: 200 }} />
                </View> */}
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader
                    loading={this.state.loading} />
                <View>
                    <SegmentedControlTab
                        tabsContainerStyle={styles.tabsContainerStyle}
                        tabStyle={styles.tabStyle}
                        firstTabStyle={styles.firstTabStyle}
                        lastTabStyle={styles.lastTabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                        values={['Food', 'Room','Three']}
                        mainSelectedIndex={this.state.mainSelectedIndex}
                        onTabPress={this.handleMainMenuIndexChange}
                    />
                </View>



{/* 
                {this.state.IsFoodMenu ?
                    <View>
                        <SegmentedControlTab
                            tabsContainerStyle={styles.tabsContainerStyle}
                            tabStyle={styles.tabStyle}
                            firstTabStyle={styles.firstTabStyle}
                            lastTabStyle={styles.lastTabStyle}
                            tabTextStyle={styles.tabTextStyle}
                            activeTabStyle={styles.activeTabStyle}
                            activeTabTextStyle={styles.activeTabTextStyle}
                            values={[I18n.t('all'), I18n.t('breakfast'), I18n.t('lunch'), I18n.t('dinner')]}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={this.handleIndexChange}
                        />
                        {this.updateTabBar()}
                    </View>
                    :
                    <View>
                        <SegmentedControlTab
                            tabsContainerStyle={styles.tabsContainerStyle}
                            tabStyle={styles.tabStyle}
                            firstTabStyle={styles.firstTabStyle}
                            lastTabStyle={styles.lastTabStyle}
                            tabTextStyle={styles.tabTextStyle}
                            activeTabStyle={styles.activeTabStyle}
                            activeTabTextStyle={styles.activeTabTextStyle}
                            values={[I18n.t('all'), I18n.t('breakfast')]}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={this.handleIndexChange}
                        />
                        {this.updateRoomTabBar()}
                    </View>

                } */}

            </View>
        )
    }

    _loadServices = async () => {
        // const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        // var userJson = JSON.parse(userdetails);

        this.setState({
            loading: true,
        });
        const typeId = this.state.selectedIndex;

        console.log("service type", typeId);

        await apiCallWithUrl(APIConstants.GetServiceList + "?RestaurantId=1&&ServiceTypeId=" + this.state.selectedIndex, 'GET', "", this.getServiceListResponse)
    }

    getServiceListResponse = async (response) => {
        if (response.IsException == null) {
            var getserviceListResponse = response.ResponseData;
            console.log('get services', getserviceListResponse);

            await this.setState({ loading: false, serviceListArray: getserviceListResponse, IsfetchResponse: true });

            console.log('get service length', this.state.serviceListArray.length);

        }
        this.setState({ loading: false })
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    tabStyle: {
        backgroundColor: "#FFFFFF",
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabStyle: {
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    activeTabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        textTransform: "uppercase",
        fontSize: 13,
    },
    AvailableText: {
        color: Colors.AvailableColor,
        fontSize: 13
    },
    NotAvailableText: {
        color: Colors.NotavailableColor,
        fontSize: 13
    }

})

module.exports = Services;