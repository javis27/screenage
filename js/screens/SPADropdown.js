import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    TouchableOpacity
} from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import { Actions } from 'react-native-router-flux';
class SPADropdown extends Component {
    constructor(props) {
        super(props);
        console.log("totalprops..", this.props.totalStateData);
    }

    componentDidMount() {
        //this._loadDropdowns();
    }
    getValues = (values) => {
        console.log("selectedvalues..", values.SPAPackageName);
        this.props.callbackSpaFunc(values);
        Actions.pop();
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                    <FlatList
                        data={this.props.totalStateData}
                        keyExtractor={(x, i) => i}
                        style={{ marginBottom: 35 }}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxWithShadow}>
                                    <TouchableOpacity onPress={this.getValues.bind(this, item)}>
                                        <View style={styles.Restaurant}>
                                            <View style={{ alignItems: "flex-start", flex: 1 }}>
                                                <Text style={styles.IconReqText}>{item.SpaPackageName}</Text>
                                                <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, fontFamily: AppFont.Regular }}>{item.Description}</Text>
                                            </View>
                                        </View>

                                        {/* <View style={{ flex: 1, paddingHorizontal: 15 }}>
                                            <Text style={{ color: Colors.Black, marginTop: 5, fontSize: 13, fontFamily: AppFont.Regular }}>Select the Package</Text>
                                        </View> */}

                                        {item.Duration != "" ?
                                            <View style={{ flex: 1, flexDirection: "row", paddingHorizontal: 15 }}>
                                                <View style={{ flex: 0.2, }}>
                                                    <Text style={[styles.ServiceText, { color: Colors.LightGray, }]}>{I18n.t('duration')} </Text>
                                                </View>
                                                <View style={{ flex: 0.8 }}>
                                                    <Text style={styles.ServiceText}>{item.Duration} minutes</Text>
                                                </View>
                                            </View>
                                            : null}
                                        {item.SingleOfferAmt != "" || item.DuoOfferAmt != "" ?
                                        <View style={styles.DescriptionServiceList}>
                                            {item.Amount != "" ?
                                                <View style={{ flex: 0.5, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}>
                                                    <View style={{ flex: 3 }}>
                                                        <Text style={[styles.ServiceText, { color: Colors.LightGray, textDecorationLine: 'line-through'}]}>Solo - </Text>
                                                        {/* <View><Text style={styles.ServiceText}>Solo - MUR {item.Amount}</Text></View> */}
                                                    </View>
                                                    <View style={{ flex: 7, }}><Text style={[styles.ServiceText,{textDecorationLine: 'line-through'}]}>MUR {item.Amount}</Text></View>
                                                </View>
                                                : null}
                                            {item.DuoAmount != "" ?
                                                <View style={{ flex: 0.5, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}>
                                                    <View style={{ flex: 3, }}>
                                                        <Text style={[styles.ServiceText, { color: Colors.LightGray, textDecorationLine: 'line-through'}]}>Duo - </Text>
                                                    </View>
                                                    <View style={{ flex: 7 }}><Text style={[styles.ServiceText,{textDecorationLine: 'line-through'}]}>MUR {item.DuoAmount}</Text></View>
                                                    {/* <View style={{ flex: 1, borderColor: "black", borderWidth: 1 }}>
                                                <View><Text style={styles.ServiceText}>Duo - MUR {item.DuoAmount}</Text></View>
                                            </View> */}
                                                </View>
                                                : null}

                                        </View> :
                                        <View style={styles.DescriptionServiceList}>
                                        {item.Amount != "" ?
                                            <View style={{ flex: 0.5, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}>
                                                <View style={{ flex: 3 }}>
                                                    <Text style={[styles.ServiceText, { color: Colors.LightGray, }]}>Solo - </Text>
                                                    {/* <View><Text style={styles.ServiceText}>Solo - MUR {item.Amount}</Text></View> */}
                                                </View>
                                                <View style={{ flex: 7, }}><Text style={styles.ServiceText}>MUR {item.Amount}</Text></View>
                                            </View>
                                            : null}
                                        {item.DuoAmount != "" ?
                                            <View style={{ flex: 0.5, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}>
                                                <View style={{ flex: 3, }}>
                                                    <Text style={[styles.ServiceText, { color: Colors.LightGray, }]}>Duo - </Text>
                                                </View>
                                                <View style={{ flex: 7 }}><Text style={styles.ServiceText}>MUR {item.DuoAmount}</Text></View>
                                                {/* <View style={{ flex: 1, borderColor: "black", borderWidth: 1 }}>
                                            <View><Text style={styles.ServiceText}>Duo - MUR {item.DuoAmount}</Text></View>
                                        </View> */}
                                            </View>
                                            : null}

                                    </View>
                                        }

                                        {item.SingleOfferAmt != "" || item.DuoOfferAmt != "" ?
                                        <View style={styles.DescriptionServiceList1}>
                                            <View style={{ flex: 0.2, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}><Text style={[styles.ServiceText1, { color: 'white', }]}>OFFER </Text></View>
                                            {item.SingleOfferAmt != "" ?
                                            
                                                <View style={{ flex: 0.4, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}>
                                                    <View style={{ flex: 3 }}>
                                                        <Text style={[styles.ServiceText1, { color: 'white', }]}>Solo - </Text>
                                                        {/* <View><Text style={styles.ServiceText}>Solo - MUR {item.Amount}</Text></View> */}
                                                    </View>
                                                    <View style={{ flex: 7, }}><Text style={styles.ServiceText1}>MUR {item.SingleOfferAmt}</Text></View>
                                                </View>
                                                : null}
                                            {item.DuoOfferAmt != "" ?
                                                <View style={{ flex: 0.4, flexDirection: "row", paddingTop: 7, paddingBottom: 15 }}>
                                                    <View style={{ flex: 3, }}>
                                                        <Text style={[styles.ServiceText1, { color: 'white', }]}>Duo - </Text>
                                                    </View>
                                                    <View style={{ flex: 7 }}><Text style={styles.ServiceText1}>MUR {item.DuoOfferAmt}</Text></View>
                                                    {/* <View style={{ flex: 1, borderColor: "black", borderWidth: 1 }}>
                                                <View><Text style={styles.ServiceText}>Duo - MUR {item.DuoAmount}</Text></View>
                                            </View> */}
                                                </View>
                                                : null}

                                        </View>
                                        : null}
                                    </TouchableOpacity>
                                </View>
                            )
                        }}
                    />

                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        paddingVertical: 8,
        paddingHorizontal: 15,
        marginTop: 10
    },
    IconReqText: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 15
    },
    DescriptionServiceList1: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 15,
        backgroundColor: "#8CCED6"
    },
    ServiceText: {
        color: Colors.Black,
        fontSize: 13,
        // paddingHorizontal: 15,
        // paddingVertical: 7,
        fontFamily: AppFont.Regular,
    },
    ServiceText1: {
        color: 'white',
        fontSize: 15,
        // paddingHorizontal: 15,
        // paddingVertical: 7,
        fontFamily: AppFont.Regular,
    },
    containerAmount: {
        // backgroundColor: Colors.FullGray,
        // borderRadius: 5,
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
        borderRadius: 5,
    },

})

module.exports = SPADropdown;