import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { Button } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;



class RestaurantView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            
        }
    }

    componentWillMount() {
        this.handleBackAddEventListener();
        console.log("RestaurantId",this.props.RestaurantId);
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    
    


    render() {
        
        return (
            <View style={styles.container}>
                <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
                    <ImageBackground source ={require('../../assets/images/SouthernGross_img23-2.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flex:0.4,justifyContent:'flex-end'}}>
                        <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                                <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:0.6}}>
                        <Image
                            source={require('../../assets/images/sugarbeach.png')}
                            style={{
                                width:'60%',
                                height:80,
                                position: 'absolute', // child
                                bottom: 60, // position where you want
                                // left: 30,
                                marginRight:10,
                            }}
                            />
                        </View>

                    </View>       
                    </ImageBackground>
                    </View>
                    <View style={{flex:0.75,backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                        <ScrollView>
                        {/* <View style={{ flex: 1,margin:10}}> */}
                        {/* <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black,margin:10}}>{this.props.Details.title}</Text>
                        <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>{this.props.Details.Desc}</Text>
                        <View style={{margin:10,height:200}}>
                        <Image source={this.props.Details.Img} style={{width:'100%',height:200,resizeMode:'cover',borderRadius:15}}></Image>
                        </View>*/}
                        {/* {this.renderNewContent()} */}
                        {/* </View>  */}
                        {
                            this.props.Details.Category==3 && this.props.Details.id==1?
                            <View>
                <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black,margin:10}}>{this.props.Details.title}</Text>
                        <Text style={{fontSize:16,fontFamily:AppFont.Bold,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Tasting Notes​</Text>
                        <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Pétrus Pomerol offers intense aromas of chocolate, truffle, minerality, earth, blackberry essence, plum liqueur, blueberry, flowers and black cherry liqueur with kirsch accents.​ Offering a beautiful sensation of lift and purity along with decadent textures, polished, dark berries, plums, bitter chocolate, fresh cherries on the Taste.​</Text>
                        <Text style={{fontSize:16,fontFamily:AppFont.Bold,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Food Pairing</Text>
                        <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Petrus is best served at 15.5 degrees Celsius, 60 degrees Fahrenheit. The cool, almost cellar temperature gives the wine more freshness and lift.​ Petrus is best paired with all types of classic meat dishes, veal, pork, beef, lamb, duck, game, roast chicken, roasted, braised and grilled dishes. Petrus is also good when matched with Asian dishes, hearty fish courses like tuna, mushrooms and pasta.​​</Text>
                        <View style={{margin:10,height:200}}>
                        <Image source={require('../../assets/images/redwine.jpg')} style={{width:'100%',height:200,resizeMode:'cover',borderRadius:15}}></Image>
                            
                        </View>
                        <View  style={{alignItems:'center',marginTop:20,marginBottom:20,justifyContent:'center',alignContent:'center'}}>
                            <View style={{width:'50%',alignItems:'center'}}>
                                    <TouchableOpacity activeOpacity={.5} style={styles.button1}>
                                        <Text style={styles.btntext}>Add to Cart</Text>
                                    </TouchableOpacity>
                            </View>
                        </View>
            </View>:this.props.Details.Category==3 && this.props.Details.id==2?
            <View>
            <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black,margin:10}}>{this.props.Details.title}</Text>
                    <Text style={{fontSize:16,fontFamily:AppFont.Bold,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>More Information</Text>
                    <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>The Cabernet Sauvignon Pays d 'OC IGP Terroir Littoral by Fortant de France is revealed in the glass in a purple red and with an intense bouquet. This is dominated by the wonderful berry aromas of blueberries and is complemented by gentle floral notes of violets. This red wine from France is balanced, powerful and pleasant on the palate with distinct notes of dark berries </Text>
                    <Text style={{fontSize:16,fontFamily:AppFont.Bold,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Vinification of the Cabernet Sauvignon Terroir Littoral by Fortant de France</Text>
                    <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>
                     After harvesting, the grapes were cold macerated for 4 days, favouring the intensity of colour and fruit notes of this pure Cabernet Sauvignon. The musts of the various individual layers are then vinified separately from one another, which ensures that the character of the individual layers is retained. The fermentation processes take place at low temperatures in order to preserve the varietal character and freshness of the aroma. After fermentation, a warm maceration takes place for 7 days, which serves to refine the tannins and gives the wine its body. The wine is then aged for 3 months on the yeast and carefully stirred. After tasting the base wines, this varietal cuvée is then mixed​</Text>
                    <Text style={{fontSize:16,fontFamily:AppFont.Bold,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Food recommendation for the Cabernet Sauvignon Terroir Littoral by Fortant de France </Text>
                    <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>Enjoy this dry red wine with pasta with strong sauces or with a rib-eye steak with oven vegetables.​​</Text>
                    <View style={{margin:10,height:200}}>
                    <Image source={require('../../assets/images/redwine.jpg')} style={{width:'100%',height:200,resizeMode:'cover',borderRadius:15}}></Image>
                    </View>
                    {/* <View  style={{alignItems:'center',marginTop:20,marginBottom:20,justifyContent:'center',alignContent:'center'}}>
                            <View style={{width:'50%',alignItems:'center'}}>
                                    <TouchableOpacity activeOpacity={.5} style={styles.button}>
                                        <Text style={styles.btntext}>Add to Cart</Text>
                                    </TouchableOpacity>
                            </View>
                        </View>
                        <View  style={{alignItems:'center',marginTop:20,marginBottom:20,justifyContent:'center',alignContent:'center'}}>
                            <View style={{width:'50%',alignItems:'center'}}>
                                    <TouchableOpacity activeOpacity={.5} style={styles.button1}>
                                        <Text style={styles.btntext}>View Cart</Text>
                                    </TouchableOpacity>
                            </View>
                        </View> */}
                        <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressAddToCart}>
                                            <Text style={styles.btntext}>Add to Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            {/* <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button1} onPress={this._onPressViewCart}>
                                            <Text style={styles.btntext}>View Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View> */}
        </View>:<View>
                <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black,margin:10}}>{this.props.Details.title}</Text>
                        <Text style={{fontSize:14,fontFamily:AppFont.Regular,color:Colors.Black,margin:10,lineHeight:20,letterSpacing:1}}>{this.props.Details.Desc}</Text>
                        <View style={{margin:10,height:200}}>
                        <Image source={this.props.Details.Img} style={{width:'100%',height:200,resizeMode:'cover',borderRadius:15}}></Image>
                        </View>
            </View>
                        }
                        
                        </ScrollView>
                    </View>
                </View>
        )
    }
    
    _onSelectionBooking = async (BookingId) => {
        if(BookingId==1){
           // Actions.RestaurantBooking();
        }else if(BookingId==2){
            //Actions.SpaBooking();
        }else if(BookingId==3){
            //Actions.BikeBooking();
        }
    }
    openMenu = ()=>{
        Actions.FoodMenus();
    }
    navigateBackOption=()=>{
        Actions.pop();
    }
    _onPressAddToCart = async () => {
        Alert.alert('Package Added to cart')
      }
      _onPressViewCart = async () => {
        let passingDetails=[{
            // spaBookingDate: this.state.spaBookingDate,
            // selectedSpaPackage: this.state.selectedSpaPackage,
            // selectedTimeSlot: this.state.selectedTimeSlot,
            // selectedVisitor: this.state.selectedVisitor,
            // selectedSpaCategory: this.state.selectedSpaCategory,
            // spaPackageId: this.state.spaPackageId,
            // spaVisitorId: this.state.spaVisitorId,
            // spaCategoryId: this.state.spaCategoryId,
            // preferenceNote: this.state.preferenceNote,
            // userId: this.state.userId,
            // selectthepackage: this.state.selectthepackage,
            // Amount: this.state.Amount,
            // spaTime: this.state.spaTime,
            // selectedPackageType: this.state.selectedPackageType,
            // selectedDuration: this.state.selectedDuration,
            // therapist: this.state.therapist,
            // internalGuests:this.state.internalGuests,
            // externalGuests:this.state.externalGuests
        }]
        Actions.BookingCart({'PassingDetails':passingDetails});
      }
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    button1: {
        backgroundColor: Colors.ButtonBlue,
        padding: 10,
        width: "95%",
        alignItems: "center",
        borderRadius: 25
    },
    btntext: {
        fontFamily: AppFont.Regular,
        color: "#FFF",
        fontSize: 18,
        alignItems: "center",
        //fontFamily: AppFont.SemiBold,
    },
      txtstyle:{
        letterSpacing:1, 
        lineHeight:20,
        color: Colors.Black,
        fontSize: 13,
        fontFamily: AppFont.Regular,
      }

})

module.exports = RestaurantView;