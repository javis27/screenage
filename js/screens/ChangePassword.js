import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    AsyncStorage,
    Alert
} from 'react-native';
import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';

import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';
import Keys from '../constants/Keys';
import I18n from '../constants/i18n';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            currentPassword: '',
            newPassword: '',
            confirmPassword: '',
            oldPassword: '',
        }
    }
    componentDidMount() {
        this.getUserDetails();
    }

    render() {
        console.log("profile state", this.state)
        return (
            <View style={styles.container}>
                <ScrollView>
                    <TableView>
                        <Section sectionPaddingTop={0} sectionPaddingBottom={100} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                            <Cell title="Current Password" placeholder="Current Password" id="1" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t("curpassword")}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                secureTextEntry={true}
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={this.state.textInputDisableStatus}
                                                onChangeText={(text) => this.setState({ currentPassword: text })}
                                                value={this.state.currentPassword}
                                                placeholder={I18n.t('pcCurPassword')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="New Password" placeholder="New Password" id="3" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t("NPassword")}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                secureTextEntry={true}
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={this.state.textInputDisableStatus}
                                                onChangeText={(text) => this.setState({ newPassword: text })}
                                                value={this.state.newPassword}
                                                placeholder={I18n.t('pcNPassword')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Confirm Password" placeholder="Confirm Password" id="4" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t("CPassword")}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                secureTextEntry={true}
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={this.state.textInputDisableStatus}
                                                onChangeText={(text) => this.setState({ confirmPassword: text })}
                                                value={this.state.confirmPassword}
                                                placeholder={I18n.t('pcCPassword')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Edit" placeholder="Edit" id="6" cellContentView={
                                <View style={{ flex: 1, marginTop: 40, justifyContent: 'center' }}>
                                    <View style={{ width: "90%", marginLeft: 20 }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressChangePassword}>
                                                {/* <Text style={styles.btntext}>{I18n.t('edit')}</Text> */}
                                                <Text style={styles.btntext}>{I18n.t('btnsave')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            } />
                        </Section>
                    </TableView>
                </ScrollView>
            </View>
        )
    }

    getUserDetails = async () => {
        const userDetails = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValues = JSON.parse(userDetails);
        console.log("jsonValues", jsonValues)
        //let originalPassword = jsonValues.Password;

        await this.setState({ userId: jsonValues.UserId, oldPassword: jsonValues.Password });
    }

    validateCurrentPassword = async (currentPassword) => {
        console.log("currentPassword", currentPassword);
        console.log("old password", this.state.oldPassword)


        //console.log("current password", jsonValues.Password)
        if (this.state.oldPassword === currentPassword) {
            console.log("Password is matching");
            // this.setState({ email: text }) 
        }
        else {
            // this.setState({ email: text })
            console.log("Password is not matching");
            return false;
        }
    }

    _onPressChangePassword = async () => {

        //console.log("validatecurrentpassword",this.validateCurrentPassword(this.state.currentPassword))

        if (this.state.currentPassword == "") {
            Alert.alert(I18n.t('pcCurPassword'));
        } else {
            if (await this.validateCurrentPassword(this.state.currentPassword) == false) {
                Alert.alert(I18n.t('alertpwdnotmatching'));
            } else {
                if (this.state.newPassword == "") {
                    Alert.alert(I18n.t('pcNPassword'));
                } else {
                    if (this.state.confirmPassword == "") {
                        Alert.alert(I18n.t('pcCPassword'));
                    } else {
                        if (this.state.newPassword === this.state.confirmPassword) {
                            let overalldetails = {
                                "UserId": this.state.userId,
                                "OldPassword": this.state.oldPassword,
                                "NewPassword": this.state.newPassword,
                            }
                            console.log("overalldetails", overalldetails);
                            await apiCallWithUrl(APIConstants.ChangePasswordAPI, 'POST', overalldetails, this.postChangePasswordResponse);
                        } else {
                            Alert.alert(I18n.t('alertnewPwdnotmatch'));
                        }
                    }
                }
            }
        }
    }

    postChangePasswordResponse = async (response) => {
        console.log('response', response);
        if (response.ResponseData != null) {
            var postUserResponse = response.ResponseData;
            console.log('postUserResponse', postUserResponse);
            if (postUserResponse == true) {
                Alert.alert(I18n.t('alertpwdsuccess'));

                AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
                AsyncStorage.removeItem(Keys.UserDetail);
                AsyncStorage.removeItem(Keys.roomNo);
                AsyncStorage.removeItem(Keys.inCustomer);
                AsyncStorage.removeItem(Keys.checkInId);
                
                Actions.GuestLogin();
            }
            else {
                Alert.alert(I18n.t('alertpwdnotupdated'))
            }
        }
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background_Color,
    },
    label: {
        //flex: 1,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    cellrow: {
        marginTop: 10,
        marginBottom: 5,
        width: null
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
})

module.exports = ChangePassword;