import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { Button } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const DATA = [
    {
        "Title":"Buddha Bar Beach",
        "Id":1,
        "Image":require('../../assets/images/Restaurant_bg1.jpg')
    },
    {
        "Title":"Citronellas Beach Lounge Restaurant",
        "Id":2,
        "Image":require('../../assets/images/2.jpg')
    },
    {
        "Title":"Mon Plaisir",
        "Id":3,
        "Image":require('../../assets/images/3.jpg')
    }
];

class RestaurantView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            
        }
    }

    componentWillMount() {
        this.handleBackAddEventListener();
        console.log("RestaurantId",this.props.RestaurantId);
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    
handleContent=()=>{
    if(this.props.RestaurantId==1){
        return(<View>
            <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black}}>Rendez-Vous</Text>
            <View style={{flexDirection:'row',margin:10}}>
                <View style={{flex:0.1}}>
                    <Image source={require('../../assets/images/Restaurants_icon.png')} style={{width:40,height:40}}/>
                </View>
                <View style={{flex:0.9}}>
                <View style={{backgroundColor:'#29B398',width:80,height:40,borderRadius:5,justifyContent:'center',alignContent:'center',alignItems:'center',marginLeft:20,marginBottom:10}}>
                        <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:'#FFFFFF'}}>OPEN</Text>
                    </View>
                </View>
            </View>
                    
                    {/* <Text style={{fontSize:14,fontFamily:AppFont.Medium,color:Colors.Black,letterSpacing:1,lineHeight:30}}>MONDAY TO THURSDAY</Text>
                    <Text style={{fontSize:12,fontFamily:AppFont.Medium,letterSpacing:1,lineHeight:30}}>Dinner 7 pm to 10 pm</Text>
                    <Text style={{fontSize:14,fontFamily:AppFont.Medium,color:Colors.Black,letterSpacing:1,lineHeight:30}}>FRIDAY TO SUNDAY</Text>
                    <Text style={{fontSize:12,fontFamily:AppFont.Medium,letterSpacing:1,lineHeight:30}}>Lunch 12.30 pm to 3 pm</Text>
                    <Text style={{fontSize:12,fontFamily:AppFont.Medium,letterSpacing:1,lineHeight:30}}>Dinner 7 pm to 10 pm</Text> */}
                    <View style={{marginTop:10}}>
                        <Text style={styles.txtstyle}>Rendez-Vous is the hotel’s meeting point. This is where your day starts, with a breakfast buffet offering a variety of classic and exclusive flavours, complete with a selection of colourful exotic fruits. Experience the delights of a creative cuisine made from the freshest local ingredients and best imported products, taste different dishes from around the world and observe our chefs in action while interacting with them.</Text>
                    </View>
                    {/* <View style={{ width: "90%", marginLeft: 20,alignItems: 'center', marginBottom:10,marginTop:20 }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.openMenu()}>
                                                <Text style={styles.btntext}>Open Menu</Text>
                                            </TouchableOpacity>
                                        </View> */}
        </View>);
    }else if(this.props.RestaurantId==2){
        return(<View>
            <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black}}>1810</Text>
            <View style={{flexDirection:'row',margin:10}}>
                <View style={{flex:0.1}}>
                    <Image source={require('../../assets/images/Restaurants_icon.png')} style={{width:40,height:40}}/>
                </View>
                <View style={{flex:0.9}}>
                <View style={{backgroundColor:'#29B398',width:80,height:40,borderRadius:5,justifyContent:'center',alignContent:'center',alignItems:'center',marginLeft:20,marginBottom:10}}>
                        <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:'#FFFFFF'}}>OPEN</Text>
                    </View>
                </View>
            </View>
                    
                    {/* <Text style={{fontSize:14,fontFamily:AppFont.Medium,color:Colors.Black,letterSpacing:1,lineHeight:30}}>OPEN EVERYDAY</Text>
                    <Text style={{fontSize:12,fontFamily:AppFont.Medium,letterSpacing:1,lineHeight:30}}>A la carte Lunch - 12.30pm to 3pm</Text>
                    <Text style={{fontSize:12,fontFamily:AppFont.Medium,letterSpacing:1,lineHeight:30}}>A la carte Dinner- 7pm to 10pm </Text> */}
                    <View style={{marginTop:10}}>
                        <Text style={styles.txtstyle}>Located on the beach, facing the turquoise lagoon and beautiful île aux Aigrettes, the 1810 offers a chic and refined cuisine. Taste revisited Mauritian culinary specialities for lunch and let our chefs impress you at supper, with beautiful dishes mainly made from seafood.</Text>
                        {/* <Text style={styles.txtstyle}>Dress-code: Gentlemen are requested to wear long trousers for dinner.</Text> */}
                    </View>
                    {/* <View style={{ width: "90%", marginLeft: 20,alignItems: 'center', marginBottom:10,marginTop:20 }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button}  onPress={this.openMenu()}>
                                                <Text style={styles.btntext}>Open Menu</Text>
                                            </TouchableOpacity>
                                        </View> */}
        </View>);
    }
    else if(this.props.RestaurantId==3){
        return(<View>
            <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black}}>Mosaic</Text>
            <View style={{flexDirection:'row',margin:10}}>
                <View style={{flex:0.1}}>
                    <Image source={require('../../assets/images/Restaurants_icon.png')} style={{width:40,height:40}}/>
                </View>
                <View style={{flex:0.9}}>
                <View style={{backgroundColor:'#29B398',width:80,height:40,borderRadius:5,justifyContent:'center',alignContent:'center',alignItems:'center',marginLeft:20,marginBottom:10}}>
                        <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:'#FFFFFF'}}>OPEN</Text>
                    </View>
                </View>
            </View>
                    
                    <View style={{marginTop:10}}>
                        <Text style={styles.txtstyle}>The Mediterranean region is rich is traditional flavours, fruit of the different cultures it brings together… Go for Mosaïc’s convivial atmosphere and its selection of dishes to share, for a memorable culinary experience in the company of your loved ones. Travel across this region and let yourself be impressed by the talents of our chefs, tasting a variety of typical dishes made from fresh Western ingredients or arranged with delicious oriental spices.</Text>
                        {/* <Text style={styles.txtstyle}>A continental buffet is served from 4:00 to 7:00 followed by an English breakfast buffet from 7:00 to 10:30 after which a late continental breakfast is served until 11:30. The thematic dinner buffets are served from 19:00 to 22:00.</Text>
                        <Text style={styles.txtstyle}>Dress-code: Gentlemen are requested to wear long trousers for dinner.</Text> */}
                    </View>
                    
        </View>);
    }else if(this.props.RestaurantId==4){
        return(<View>
            <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black}}>Medley</Text>
            <View style={{flexDirection:'row',margin:10}}>
                <View style={{flex:0.1}}>
                    <Image source={require('../../assets/images/Restaurants_icon.png')} style={{width:40,height:40}}/>
                </View>
                <View style={{flex:0.9}}>
                <View style={{backgroundColor:'#29B398',width:80,height:40,borderRadius:5,justifyContent:'center',alignContent:'center',alignItems:'center',marginLeft:20,marginBottom:10}}>
                        <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:'#FFFFFF'}}>OPEN</Text>
                    </View>
                </View>
            </View>
                    
                    <View style={{marginTop:10}}>
                        <Text style={styles.txtstyle}>Cocktails are in fashion and best enjoyed after a long day under the hot Mauritian sun! Our mixologist offers a selection of creative cocktails for all tastes, served in modern or somewhat eccentric glasses. Thanks to our “Best Brews” concept, you can try one (or more!) of 12 different beers from 12 different countries. If you are more of a wine enthusiast, we invite you to choose from our list of 50 carefully selected references.</Text>
                        {/* <Text style={styles.txtstyle}>A continental buffet is served from 4:00 to 7:00 followed by an English breakfast buffet from 7:00 to 10:30 after which a late continental breakfast is served until 11:30. The thematic dinner buffets are served from 19:00 to 22:00.</Text>
                        <Text style={styles.txtstyle}>Dress-code: Gentlemen are requested to wear long trousers for dinner.</Text> */}
                    </View>
                    
        </View>);
    }
}

    render() {
        
        return (
            <View style={styles.container}>
                <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
                    <ImageBackground source ={require('../../assets/images/SouthernGrossbeach_img6.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flex:0.4,justifyContent:'flex-end'}}>
                        <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                                <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:0.6}}>
                        <Image
                            source={require('../../assets/images/sugarbeach.png')}
                            style={{
                                width:'60%',
                                height:80,
                                position: 'absolute', // child
                                bottom: 60, // position where you want
                                // left: 30,
                                marginRight:10,
                            }}
                            />
                        </View>

                    </View>       
                    </ImageBackground>
                    </View>
                    <View style={{flex:0.75,backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                        <ScrollView>
                        <View style={{ flex: 1,margin:10}}>
                            {this.handleContent()}
                            <View style={{ width: "90%", marginLeft: 20,alignItems: 'center', marginBottom:10,marginTop:20 }}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.openMenu}>
                                                <Text style={styles.btntext}>{I18n.t('open_menu')}</Text>
                                            </TouchableOpacity>
                                        </View>
                        </View>
                        </ScrollView>
                    </View>
                

            </View>
        )
    }

    _onSelectionBooking = async (BookingId) => {
        if(BookingId==1){
           // Actions.RestaurantBooking();
        }else if(BookingId==2){
            //Actions.SpaBooking();
        }else if(BookingId==3){
            //Actions.BikeBooking();
        }
    }
    openMenu = ()=>{
        Actions.FoodMenus();
    }
    navigateBackOption=()=>{
        Actions.pop();
    }
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 20
      },
      btntext: {
        color: "#FFF",
        fontSize: 14,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
      },
      txtstyle:{
        letterSpacing:1, 
        lineHeight:20,
        color: Colors.Black,
        fontSize: 13,
        fontFamily: AppFont.Regular,
      }

})

module.exports = RestaurantView;