import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Dimensions,
} from 'react-native';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Keys from '../constants/Keys';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class DynamicSubMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            submenuList: this.props.subMenus,
            loadingSubMenu: false
        }
        console.log('props', this.props.subMenus)
        console.log('props', this.state.submenuList)
        console.log(this.state.submenuList[0].ParentPageContent)
        // console.log("navigation", this.props.navigation)
        // console.log("navigation", this.props.navigation.state.params.routeName)
    }
    componentDidMount() {
        this.props.navigation.setParams({ title: this.props.subMenus[0].ParentPageName });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader loading={this.state.loadingSubMenu} />
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                    {
                        this.state.submenuList[0].ParentPageContent ?
                            <View style={{ paddingLeft: 10, paddingRight: 10, fontFamily: AppFont.Regular }}>
                                <HTML
                                    html={this.state.submenuList[0].ParentPageContent}
                                    tagsStyles={{ fontFamily: AppFont.Regular, }}
                                    baseFontStyle={{ fontFamily: AppFont.Regular,letterSpacing:0.5,lineHeight:30}}
                                    ignoredStyles={["font-family", "letter-spacing"]}
                                    imagesMaxWidth={Dimensions.get('window').width}
                                    
                                //style={{ paddingBottom: 10, fontFamily: AppFont.Light, color: '#717171', fontFamily: AppFont.Regular, textAlign: 'justify' }} 
                                />
                            </View>
                            : null
                    }

                    <View style={{ borderTopColor: Colors.App_Font, borderTopWidth: 0.5 }}>
                        {this.state.submenuList.length > 0 ?
                            this.state.submenuList.map((submenu, index) => {
                                return <TouchableOpacity key={index} activeOpacity={.5} onPress={this._onPressDynamicSubMenu.bind(this, submenu.PageId)}>
                                    <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                        <View style={{ width: "82%" }}>
                                            <Text style={styles.mainmenu}>{submenu.PageName}{console.log(submenu.PageName)}</Text>
                                        </View>
                                        <View style={{ width: "18%", padding: 12 }}>
                                            <Cell accessory="DisclosureIndicator" />
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            })
                            :
                            null
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }

    _onPressDynamicSubMenu = async (pageId) => {
        this.setState({
            loadingSubMenu: true
        })
        console.log("id", pageId)
        var langId = await AsyncStorage.getItem(Keys.langId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        let overalldetails = {
            "HotelId": HotelId,
            "PageId": pageId,
            "LanguageId": langId
          }
        await apiCallWithUrl(APIConstants.GetPagesContentAPI, 'POST', overalldetails, (callback) => { this.submenuContentAPIResponse(callback) });
        //await apiCallWithUrl(APIConstants.GetPagesContentAPI + "?LanguageId=" + langId + "&&PageId=" + pageId, 'GET', "", this.submenuContentAPIResponse);
        //  await apiCallWithUrl(APIConstants.GetSubMenuAPI + "?LanguageId=" + this.state.languageId + "&&PageId=" + pageId, 'GET', "", (callback) => { this.subMenuAPIResponse(callback, pageId) });
    }

    submenuContentAPIResponse = async (response) => {
        console.log("content", response);
        if (response.ResponseData != null) {
            console.log('content response', response.ResponseData[0])
            Actions.DynamicMenuContent({ menuContent: response.ResponseData[0] })
        }
        this.setState({ loadingSubMenu: false })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background_Color,
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
        
    },
});

module.exports = DynamicSubMenu;