import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  ScrollView,
  WebView

} from 'react-native';

class DynamicWebpage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      
    }
  }
  

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
        <WebView
        source={{ uri: 'https://southerncrosshotels.mu/preskil/' }}
        style={{ marginTop: 20 }}
      />
      </View>
    );
  }

  
}


module.exports = DynamicWebpage;