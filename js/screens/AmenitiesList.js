import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    FlatList,
    AsyncStorage,
    Dimensions,
    Platform,
    Alert,
    TouchableOpacity,
    Image
} from 'react-native';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Loader from '../components/Loader';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import AmenitiesListItem from '../components/AmenitiesListItem';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import { Actions } from 'react-native-router-flux';

class AmenitiesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isRefreshing: false,
            amenitiesArray: [],
            IsAmenitiesResponse: false,
        }
    }
    componentWillMount() {
        //this.verifyCheckInDetails();
        this.loadAmenitiesList();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = (response) => {
    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this.loadAmenitiesList();
    //         }
    //     }
    // }

    renderEmptyComponent = () => {
        if (this.state.amenitiesArray.length != 0) return null;
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center", height: Dimensions.get('window').height - 150, width: Dimensions.get('window').width, }}>
                <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', fontFamily: AppFont.Regular }}>{I18n.t('nodata')}</Text>
            </View>
        );
    };
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader
                    loading={this.state.loading} />
                <View style={{flex:1}}>
                    {/* {this.state.IsAmenitiesResponse ? */}
                    <FlatList
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefreshAmenities}
                        data={this.state.amenitiesArray}
                        keyExtractor={(x, i) => i}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxWithShadow}><AmenitiesListItem amenityDetails={item} /></View>
                            )
                        }}
                    />
                    {/* : null} */}
                    <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.createAmenities}
          style={styles.TouchableOpacityStyle}>
          <Image
             source={require('../../assets/images/Plus.png')}
            style={styles.FloatingButtonStyle}
          />
        </TouchableOpacity>
                </View>

                {/* <View>{this.state.IsRoomServiceResponse ? <FlatList
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefreshRoomServices}
                data={this.state.roomServiceArray}
                keyExtractor={(x, i) => i}
                ListEmptyComponent={this.renderEmptyComponent}
                renderItem={({ item, index }) => {
                    return (
                        <View style={styles.boxWithShadow}><RoomServiceListItem roomServices={item} /></View>
                    )
                }}
            /> : null}</View> */}
            </View>
        );
    }

    onRefreshAmenities = () => {
        this.setState({ isRefreshing: true });
        this.loadAmenitiesList();
    }
    createAmenities =()=>{
        Actions.Amenities();
    }
    loadAmenitiesList = async () => {
        this.setState({
            loading: true
        })
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);

        console.log("userId", jsonValue.UserId);

        var HotelId = await AsyncStorage.getItem('HotelId');
       
        let overalldetails = {
            "HotelId": HotelId,
            "UserId": jsonValue.UserId
          }
          await apiCallWithUrl(APIConstants.GetAmenitiesList, 'POST', overalldetails, this.postCheckInDetailsResponse);

        //await apiCallWithUrl(APIConstants.GetAmenitiesList + "?UserId=" + jsonValue.UserId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse)
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getAmenitiesListResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getAmenitiesListResponse = async (response) => {
        console.log("get amenities response", response);
        if (response.ResponseData.length > 0) {
            console.log("get amenities response", response.ResponseData);
            var amenitiesList = response.ResponseData;
            await this.setState({ amenitiesArray: amenitiesList, IsAmenitiesResponse: true, isRefreshing: false })
        }
        this.setState({ loading: false })
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background_Color,
        height: '100%',
        flex:1
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    TouchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
        borderRadius:50,
    backgroundColor:Colors.App_Font
      },
    
      FloatingButtonStyle: {
        resizeMode: 'contain',
        width: 25,
        height: 25,
        //backgroundColor:'black'
      },
})

module.exports = AmenitiesList;
