import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  ScrollView,
  StatusBar

} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import FCM from 'react-native-fcm';
class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      emailId: '',
      password: '',
      deviceToken: '',
      languageId: '',
    }
  }
  componentWillMount() {
    
    FCM.getFCMToken().then(token => {
      console.log('token.....', token);
      this.setState({ deviceToken: token })
    });
    this._loadInitialState();
  }

  updateInputEmail(email) {
    // console.log("email", email)
    // console.log("email length", email.length)
    this.setState({ emailId: email.trim() });
  }

  render() {
    const lcode = this.props.langcode;
    return (
      <View style={{ flex: 1, backgroundColor:Colors.App_Font, flexDirection: 'column' }}>
        <Loader loading={this.state.loading} />
        <View style={{flex:0.40,backgroundColor:Colors.App_Font}}>
            <Image source={require('../../assets/images/Logo_white.png')}  style={{ width: '100%', height: '80%', resizeMode:'contain' }} />
        </View>
        <View style={{flex:0.60,backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
        <View style={{ width: '100%', height: 80, alignItems: 'center',marginTop:20 }}>
              <Text
                allowFontScaling
                numberOfLines={1}
                style={styles.label}>
                {I18n.t('emailid')}
              </Text>
              <View style={styles.SectionStyle}>

                <TextInput
                  value={this.state.emailId}
                  placeholderTextColor={Colors.PlaceholderText}
                  placeholder={I18n.t('emailid')}
                  underlineColorAndroid='transparent'
                  style={styles.textInput}
                  onChangeText={this.updateInputEmail.bind(this)}
                //onChangeText={(emailId) => this.setState({ emailId })}
                />
              </View>
            </View>
            <View style={{ width: '100%', height: 80, alignItems: 'center' }}>
              <Text
                allowFontScaling
                numberOfLines={1}
                style={styles.label}>
                {I18n.t('password')}
              </Text>
              <View style={styles.SectionStyle}>
                <TextInput
                  value={this.state.password}
                  secureTextEntry={true}
                  placeholderTextColor={Colors.PlaceholderText}
                  placeholder={I18n.t('password')}
                  underlineColorAndroid='transparent'
                  style={styles.textInput}
                  onChangeText={(password) => this.setState({ password })}
                />
              </View>
            </View>
            

            <View style={styles.loginview}>
              <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressButton}>
                <Text style={styles.btntext}> {I18n.t('loginbtn')} </Text>
              </TouchableOpacity>
            </View>

            <View style={{ width: '100%', paddingBottom: 20, paddingTop: 20, alignItems: 'center' }}>
              <TouchableOpacity onPress={this._onPressRegister}>
                <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color:Colors.Black }}>{I18n.t('register')}</Text>
              </TouchableOpacity>
            </View>

            <View style={{ width: '100%', paddingBottom: 25, paddingTop: 10, alignItems: 'center' }}>
              <TouchableOpacity activeOpacity={.5} onPress={this._onPressForgot}>
                <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color:Colors.Black }}>{I18n.t('forgot')}</Text>
              </TouchableOpacity>
            </View>
        </View>
                
      </View>
    );
  }

  _onPressForgot = () => {
    Actions.ForgotPasswordOTP();
  }
  _loadInitialState = async () => {
    const value = await AsyncStorage.getItem('langcode');
    const langId = await AsyncStorage.getItem(Keys.langId);
    this.setState({
      "langCode": value,
      "languageId": langId
    })

    // const mapdetails = await AsyncStorage.getItem(Keys.mapDetail);
    // console.log("mapDetails",mapdetails)
  }

  validateEmail = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text })
      return false;
    }
    else {
      this.setState({ email: text })
      console.log("Email is Correct");
    }
  }

  _onPressButton = async () => {

    if (this.state.emailId == "") {
      Alert.alert(I18n.t('enteremailaddr'));
    } else {
      if (this.validateEmail(this.state.emailId) == false) {
        Alert.alert(I18n.t('alertnotvalidemail'));
      } else {
        if (this.state.password == "") {
          Alert.alert(I18n.t('enterpasswordplaceholder'));
        } else {
          this.setState({ loading: true })
          let overalldetails = {
            "Fullname": "",
            "EmailId": this.state.emailId,
            "Password": this.state.password,
            "IsSocialLogin": "0",
            "fcmToken": this.state.deviceToken,
            "LanguageId": this.state.languageId,
          }
          console.log("overalldetails", overalldetails)
          await apiCallWithUrl(APIConstants.GuestLoginAPI, 'POST', overalldetails, this.postUserProfileResponse);
        }
      }
    }
  }

  postUserProfileResponse = async (response) => {
    console.log("response", response);

    if (Platform.OS === 'ios') {
      this.setState({ loading: false }, () => {
        setTimeout(() => this.funcUserProfileResponse(response), 1000);
      });
    } else {
      this.setState({
        loading: false,
      }, () => this.funcUserProfileResponse(response));
    }

    // this.setState({ loading: false }, () => {
    //   setTimeout(() => {
    //     if (response.IsException != "True") {
    //       console.log("response", response, roomNo);
    //       if (response.ResponseData == null) {
    //         return Alert.alert(I18n.t('alertsmthngwrng'))
    //       }
    //       console.log('response', response);
    //       if (response.ResponseData != "Invalid Username or Password") {
    //         // alert("Login Success!!");
    //         AsyncStorage.setItem(Keys.isLogin, JSON.stringify(true));
    //         AsyncStorage.setItem(Keys.UserDetail, JSON.stringify(response.ResponseData));


    //         console.log("roomNo", roomNo)
    //         if (roomNo != null) {
    //           Actions.tabbar();
    //         } else {
    //           Actions.GuestCheckIn();
    //         }

    //       } else {
    //         Alert.alert(I18n.t('alertchknamepwd'));
    //       }
    //     }
    //     else {
    //       Alert.alert(I18n.t('alerttryagain'));
    //     }
    //   }, 1000)
    // });
  }

  funcUserProfileResponse = async (response) => {
    const roomNo = await AsyncStorage.getItem(Keys.roomNo);
    if (response.IsException != "True") {
      console.log("response", response, roomNo);
      if (response.ResponseData == null) {
        return Alert.alert(I18n.t('alertsmthngwrng'))
      }
      console.log('response', response);
      if (response.ResponseData != "Invalid Username or Password") {
        // alert("Login Success!!");
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(true));
        AsyncStorage.setItem(Keys.UserDetail, JSON.stringify(response.ResponseData));


        console.log("roomNo", roomNo)
        if (roomNo != null) {
          Actions.tabbar();
        } else {
          // Actions.GuestCheckIn();
          //Actions.GuestHome();
          Actions.Hotel();
        }

      } else {
        Alert.alert(I18n.t('alertchknamepwd'));
      }
    }
    else {
      Alert.alert(I18n.t('alerttryagain'));
    }
  }

  _onPressRegister = () => {
    Actions.Register();
    // Actions.RegisterEmailOTP();
  }
}

const styles = StyleSheet.create({
  textInput: {
    marginLeft: 0,
    height: '100%',
    width: '85%',
    fontSize: 15,
    fontFamily: AppFont.Regular,
    marginTop: 8
  },
  SectionStyle: {
    height: 50,
    width: '85%',
    borderBottomWidth: 1,
    borderColor: Colors.App_Font,
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden',
    marginBottom: 10
  },
  loginview: {
    alignItems: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: Colors.App_Font,
    padding: 10,
    width: "75%",
    borderRadius: 5
  },
  btntext: {
    color: "#FFF",
    fontSize: 18,
    letterSpacing: 1,
    fontFamily: AppFont.Regular,
  },
  label: {
    alignItems: "flex-start",
    width: '85%',
    fontFamily: AppFont.Regular,
    color: Colors.Black,
    ...Platform.select({
      ios: {
        marginLeft: 0,
      },
      android: {
        marginLeft: 8,
      },
    }),
  },

});

module.exports = Login;