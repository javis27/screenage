import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Alert,
    StatusBar,
    AsyncStorage,
    Platform
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';

import { Cell, Section, TableView } from "react-native-tableview-simple";
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class GuestCheckIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            fullname: '',
            roomno: '',
            emailId: '',
        }
    }

    componentDidMount() {
        this.loadInitialData();
    }

    loadInitialData = async () => {
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        console.log("json", jsonValue)
        await this.setState({ emailId: jsonValue.EmailId });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='default'
                />
                <Loader loading={this.state.loading} />
                <ScrollView>
                    <TableView>
                        <Section sectionPaddingTop={0} sectionPaddingBottom={100} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                            {/* <Cell title="Fullname" placeholder="Fullname" id="1" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('fullname')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={(text) => this.setState({ fullname: text })}
                                                value={this.state.fullname}
                                                placeholder={I18n.t('yourname')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Login" hideSeparator={true} placeholder="Login" id="7" cellContentView={
                                <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop: 10 }}>
                                    <Text style={{ fontSize: 15, fontFamily: AppFont.Regular }}>{I18n.t('or')}</Text>
                                </View>
                            } /> */}
                            <Cell title="Email" placeholder="Email" id="2" cellContentView={
                                <View style={{ flex: 1, paddingTop: 7 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('emailid')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                editable={false}
                                                onChangeText={(text) => this.setState({ emailId: text.trim() })}
                                                value={this.state.emailId}
                                                placeholder={I18n.t('emailid')} />
                                            {/* <Text
                                                allowFontScaling
                                                numberOfLines={1}
                                                style={[{ paddingTop: 7 }, styles.textInput]}>
                                                {this.state.emailId}
                                            </Text> */}
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />

                            <Cell title="Room No" placeholder="Room No" id="2" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('roomno')}
                                        </Text>
                                        <View>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={(text) => this.setState({ roomno: text.trim() })}
                                                value={this.state.roomno}
                                                placeholder={I18n.t('roomno')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />

                            <View style={styles.loginview}>
                                <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onGuestHomeButton}>
                                    <Text style={styles.btntext}> Check-In </Text>
                                </TouchableOpacity>
                            </View>


                        </Section>
                    </TableView>
                </ScrollView>
            </View>
        )
    }

    validateEmail = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            this.setState({ email: text })
            return false;
        }
        else {
            this.setState({ email: text })
        }
    }

    _onGuestHomeButton = async () => {
        if (this.state.emailId == "") {
            Alert.alert(I18n.t('enteremailaddr'));
        } else {
            if (this.validateEmail(this.state.emailId) == false) {
                Alert.alert(I18n.t('alertnotvalidemail'));
            } else {
                if (this.state.roomno == "") {
                    Alert.alert("Enter room number");
                } else {
                    this.setState({ loading: true });
                    var HotelId = await AsyncStorage.getItem('HotelId');
                    let overalldetails = {
                        "Email": this.state.emailId,
                        "RoomNo": this.state.roomno,
                        "HotelId": HotelId,
                    }
                    console.log("overalldetails", overalldetails);
                    await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInResponse);
                }
            }
        }
        //  Actions.CheckInDetails();
    }

    postCheckInResponse = async (response) => {
        if (Platform.OS === 'ios') {
            this.setState({ loading: false }, () => {
                setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
            });
        } else {
            this.setState({
                loading: false,
            }, () => this.funcCheckInDetailsResponse(response));
        }
    }

    funcCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }
        if (response.IsException == null) {
            var postCheckInResponse = response.ResponseData;
            console.log('postCheckInResponse', postCheckInResponse);
            console.log('room no', postCheckInResponse.RoomNo);


            if (postCheckInResponse.InCustomer != null) {
                if (postCheckInResponse.InCustomer == "True") {
                    AsyncStorage.setItem(Keys.roomNo, postCheckInResponse.RoomNo);
                    AsyncStorage.setItem(Keys.inCustomer, postCheckInResponse.InCustomer);
                    AsyncStorage.setItem(Keys.checkInId, postCheckInResponse.CheckInId);
                    Actions.CheckInDetails({ checkInDetails: postCheckInResponse });
                } else {
                    Alert.alert(I18n.t('alertnotcheckin'));
                }
            }
            else {
                Alert.alert(I18n.t('alertchkemailroomno'));
            }
        }
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        //  height: '100%'
    },
    loginview: {
        alignItems: 'center',
        paddingTop: 20
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "85%",
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    label: {
        //flex: 1,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },

})

module.exports = GuestCheckIn;