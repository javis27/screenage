import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const DATA = [
    {
        "Title":"Door Lock Integration",
        "Id":1,
        "Image":require('../../assets/images/Doorlock.jpg')
    },
    {
        "Title":"Watch TV",
        "Id":2,
        "Image":require('../../assets/images/watchtv.jpg')
    },
    {
        "Title":"Phone Directory",
        "Id":3,
        "Image":require('../../assets/images/phonedirectory.jpg')
    },
    {
        "Title":"Offline Maps",
        "Id":4,
        "Image":require('../../assets/images/offline.jpg')
    },
    {
        "Title":"Amenities",
        "Id":5,
        "Image":require('../../assets/images/amenities.jpg')
    }

];

class GuestHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            
        }
    }

    componentWillMount() {
        this.handleBackAddEventListener();
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }


    render() {
        const Item = ({ title }) => (
            <View style={styles.item}>
              <Text style={styles.title}>{title}</Text>
            </View>
          );
          const renderItem = ({ item }) => (
            <View style={{marginTop:10,marginLeft:10,marginRight:10,marginBottom:10}}>
              <TouchableOpacity onPress={this._onSelectionBooking.bind(this,item.Id)}>
                  <ImageBackground source = {item.Image} style = {{ width: '100%', height: 180, position: 'relative'}}>
                      
                        <Text
                        style={{
                          backgroundColor: '#66CACD',
                          fontFamily: AppFont.Medium,
                          fontWeight: 'bold',
                          fontSize:22,
                          color: 'white',
                          position: 'absolute', // child
                          top: 30, // position where you want
                          left: 10
                        }}
                      >
                        {item.Title}
                      </Text>
                </ImageBackground>
              </TouchableOpacity>
            </View>
          );
        return (
            <View style={styles.container}> 
                <View style={{height:180}}>
                    <ImageBackground source={{uri: 'http://sun.smartpoint.in/uploads/Hotel/3_Hotel.png'}} style = {{ width: '100%', height: 180, position: 'relative'}}/>
                </View>
                <View  style={{height:80,flexDirection:'row', marginTop:25}}>
                        <View style={styles.bookingCount}><View style={styles.imagebg}><Image source={require('../../assets/images/Restaurants.png') } style={styles.imageStyle} /></View></View>
                        <View style={styles.bookingCount}><View style={styles.imagebg}><Image source={require('../../assets/images/Wellness-Center.png') } style={styles.imageStyle} /></View></View>
                        <View style={styles.bookingCount}><View style={styles.imagebg}><Image source={require('../../assets/images/Cycle.png') } style={styles.imageStyle} /></View></View>
                        <View style={styles.bookingCount}><View style={styles.imagebg}><Image source={require('../../assets/images/Boat.png') } style={styles.imageStyle} /></View></View>
                </View>
                    <FlatList
                        data={DATA}
                        renderItem={renderItem}
                        keyExtractor={item => item.Id}
                    />
            </View>
        )
    }

    _onSelectionBooking = async (BookingId) => {
        if(BookingId==1){
            //Actions.RestaurantBooking();
        }else if(BookingId==2){
            Actions.TVChannels();
        }else if(BookingId==3){
            Actions.PhoneDirectory();
        }else if(BookingId==4){
            //Actions.BoatBooking();
        }else if(BookingId==5){
            Actions.AmenitiesList();
        }
    }
    
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    bookingCount:{
        flex:0.25, 
        alignContent:'center', 
        alignItems:'center'
    },
    imageStyle:{
        width: 65, 
        height: 80, 
        position: 'relative'
    },
    imagebg:{
        width:90,
        borderWidth:0.5, 
        borderColor:Colors.LightGray,  
        backgroundColor:'#F5F5F5',
        alignContent:'center', 
        alignItems:'center'
    }

})

module.exports = GuestHome;