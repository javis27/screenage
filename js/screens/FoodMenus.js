import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    FlatList,
    StatusBar,
    Dimensions,
    Platform,
    Alert,
    ImageBackground
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
// import Keys from '../constants/Keys';
// import ServiceListItem from '../components/ServiceListItem';
// import AllServiceListItem from '../components/AllServiceListItem';
// // import RoomServiceListItem from '../components/RoomServiceListItem';
import SegmentedControlTab from "react-native-segmented-control-tab";
import Loader from '../components/Loader';

import { apiCallWithUrl, dynamicApiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';
import ActionButton from 'react-native-action-button';
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';
const DATA = [
  {
    id: '1',
    title: 'Salads',
  },
  {
    id: '2',
    title: 'Sandwiches',
  },
  {
    id: '3',
    title: 'Non-Veg',
  },
{  id: '4',
  title: 'Beverages',
},
];
const DATA1 = [
  {
    id: '1',
    title: 'Cheesy Veg Salad',
    Price:'200',
    Category:0,
    Img: require('../../assets/images/Dishes/Salads1.jpg'),
    Desc:''
  },
  {
    id: '2',
    title: 'Spicy Chicken Salad',
    Price:'250',
    Category:0,
    Img: require('../../assets/images/Dishes/Salads2.jpg'),
    Desc:''
  },
  {
    id: '3',
    title: 'Brocoli crushed Salad',
    Price:'200',
    Category:0,
    Img: require('../../assets/images/Dishes/Salads3.jpg'),
    Desc:''
  },
  {  id: '4',
    title: 'Marine Mixed Salad',
    Price:'350',
    Category:0,
    Img: require('../../assets/images/Dishes/Salads4.jpg'),
    Desc:''
  },
  {  id: '5',
    title: 'Veggie Salad',
    Price:'150',
    Category:0,
    Img: require('../../assets/images/Dishes/Salads5.jpg'),
    Desc:''
  },
];
const DATA2 = [
  {
    id: '1',
    title: 'Italian Club Sandwich',
    Price:'200',
    Category:1,
    Img: require('../../assets/images/Dishes/Sandwiches1.jpg'),
    Desc:''
  },
  {
    id: '2',
    title: 'Veg Shammi Club Sandwich',
    Price:'250',
    Category:1,
    Img: require('../../assets/images/Dishes/Sandwiches2.jpg'),
    Desc:''
  },
  {
    id: '3',
    title: 'Veggies & Chicken Wrap',
    Price:'200',
    Category:1,
    Img: require('../../assets/images/Dishes/Sandwiches3.jpg'),
    Desc:''
  },
  {  id: '4',
    title: 'Turkey Club Sandwich',
    Price:'350',
    Category:1,
    Img: require('../../assets/images/Dishes/Sandwiches4.jpg'),
    Desc:''
  },
  {  id: '5',
    title: 'Veggie Club Sandwich',
    Price:'150',
    Category:1,
    Img: require('../../assets/images/Dishes/Sandwiches5.jpg'),
    Desc:''
  },
];
const DATA3 = [
  {
    id: '1',
    title: 'Pepper BBQ ',
    Price:'200',
    Category:2,
    Img: require('../../assets/images/Dishes/Non-veg1.jpg'),
    Desc:''
  },
  {
    id: '2',
    title: 'Grill Roasted Chicken',
    Price:'250',
    Category:2,
    Img: require('../../assets/images/Dishes/Non-veg2.jpg'),
    Desc:''
  },
  {
    id: '3',
    title: 'Italian Mixed',
    Price:'200',
    Category:2,
    Img: require('../../assets/images/Dishes/Non-veg3.jpg'),
    Desc:''
  },
  {  id: '4',
    title: 'Roasted Beef',
    Price:'350',
    Category:2,
    Img: require('../../assets/images/Dishes/Non-veg4.jpg'),
    Desc:''
  },
  {  id: '5',
    title: 'Bacon Meat',
    Price:'150',
    Category:2,
    Img: require('../../assets/images/Dishes/Non-veg5.jpg'),
    Desc:''
  },
];
const DATA4 = [
  {
    id: '1',
    title: 'PétrusPomerol 1982',
    Price:'200',
    Category:3,
    Img: require('../../assets/images/petrus.jpg'),
    Desc:'PétrusPomerol offers intense aromas of chocolate, truffle, minerality and earth, blackberry essence, plum liquery, blueberry, flowers and black cherry liquer with kirsch accents. Offering a beautifull sensation of lift and purity along with decadent textures, polished, dark berries, plums, bitter chocolates, fresh cherries on the taste.'
  },
  {
    id: '2',
    title: 'Fortant de France - Cabernet Sauvignon',
    Price:'250',
    Category:3,
    Img: require('../../assets/images/fortant.jpg'),
    Desc:'Der Château Latour 2011 leuchtet in intensivem Purpurrot. Im Bukett zeigen sich feine Nuancen von Rauch und Oliven kombiniert mit Beeren. Ein fruchtiger und zugleich floraler Duft verleiht der Nase Tiefgang. Der Gaumen entdeckt einen fruchtigen Rotwein mit schwarzer Pflaume und Brombeere. Der Château Latour 2011 bleibt im langen Nachhall kraftvoll und strukturiert.'
  },
  {
    id: '3',
    title: 'Natural Drinks & Shakes',
    Price:'200',
    Category:3,
    Img: require('../../assets/images/Dishes/Beverages3.jpg'),
    Desc:''
  },
  {  id: '4',
    title: 'Red Wine',
    Price:'350',
    Category:3,
    Img: require('../../assets/images/Dishes/Beverages4.jpg'),
    Desc:''
  },
  {  id: '5',
    title: 'Grape Wine',
    Price:'150',
    Category:3,
    Img: require('../../assets/images/Dishes/Beverages5.jpg'),
    Desc:''
  },
];
const DATA5 = [
  {
    id: '1',
    title: 'Red'
  },
  {
    id: '2',
    title: 'Rose',
  },
  {
    id: '3',
    title: 'White'
  },
  {  id: '4',
    title: 'Sparkling'
  },
  {  id: '5',
    title: 'Sommeliers Choice'
  },
];
const renderItem1 = ({ item }) => (
  <View style={{flex:1,flexDirection:'column',marginTop:5}}>
    <TouchableOpacity onPress={this.getItemDetails.bind(this,item)}>
    <View style={{flex:0.8,}}><Image  source = {item.Img} style = {{ width: '100%', height: 180, resizeMode:'contain'}}/></View>
    <View style={{flex:0.2,flexDirection:'row',backgroundColor:'#F4F4F4',height:40,justifyContent:'center'}}>
      <View style={{flex:0.80,justifyContent:'center'}}><Text style={styles.title1}>{item.title}</Text></View>
      <View style={{flex:0.20,justifyContent:'center'}}><Text style={styles.title1}>MUR {item.Price}</Text></View>
    </View>
    </TouchableOpacity>
  </View>
);
const renderItem2 = ({ item }) => (
  <View style={{flex:1,flexDirection:'row',margin:10}}>
    <Text style={{fontFamily:AppFont.Regular,fontSize:14,color:Colors.Black,textDecorationLine:item.id==1?'underline':'none'}}>{item.title}</Text>
  </View>
);
getItemDetails=(item)=>{
  console.log('item..',item)
  Actions.FoodDetailView({Details:item})
}
class FoodMenus extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      serviceMenuNamesArray: [I18n.t('salads'),I18n.t('sandwiches'),I18n.t('non_veg'),I18n.t('beverages')],
      selectedIndex:0
      
    }
  }
  componentDidMount(){
    console.log("selectedIndex",this.state.selectedIndex);
  }
  handleSubMenuIndexChange = async (childindex) => {
    await this.setState({
        ...this.state,
        selectedIndex: childindex,
        IsfetchResponse: false,
        loading: true,

    });
    console.log("selectedIndex",this.state.selectedIndex);
    //const typeId = this.state.selectedIndex + 1;  // Since we are hiding 'ALL' in Menu so we are adding '+ 1' to index
    
};

displayItems=()=>{
  if(this.state.selectedIndex==0){
    return(<View style={{flex:1}}>
      <FlatList
                            data={DATA1}
                            renderItem={renderItem1}
                            keyExtractor={item => item.id}
                          />
    </View>);
  }else if(this.state.selectedIndex==1){
    return(<View style={{flex:1}}>
      <FlatList
                            data={DATA2}
                            renderItem={renderItem1}
                            keyExtractor={item => item.id}
                          />
      </View>);
  }else if(this.state.selectedIndex==2){
    return(<View style={{flex:1}}>
      <FlatList
                            data={DATA3}
                            renderItem={renderItem1}
                            keyExtractor={item => item.id}
                          />
      </View>);
  }else if(this.state.selectedIndex==3){
    return(<View style={{flex:1}}>
      <View style={{margin:10}}>
      <FlatList
      horizontal={true}
                            data={DATA5}
                            renderItem={renderItem2}
                            keyExtractor={item => item.id}
                          />
      </View>
      <FlatList
                            data={DATA4}
                            renderItem={renderItem1}
                            keyExtractor={item => item.id}
                          />
      </View>);
  }
}


  render() {
    
    const Item = ({ title }) => (
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
    
    
      const renderItem = ({ item }) => (
        <Item title={item.title} />
      );

      // const renderItem1 = ({ item }) => (
      //   <View style={{flex:1,flexDirection:'column',marginTop:5}}>
      //     <View style={{flex:0.8,}}><Image  source = {item.Img} style = {{ width: '100%', height: 180}}/></View>
      //     <View style={{flex:0.2,flexDirection:'row',backgroundColor:'#F4F4F4',height:40,justifyContent:'center'}}>
      //     <View style={{flex:0.70,justifyContent:'center'}}><Text style={styles.title1}>{item.title}</Text></View>
      //     <View style={{flex:0.30,justifyContent:'center'}}><Text style={styles.title1}>INR {item.Price}</Text></View>
      //     </View>
 
      //   </View>
      // );
    return (
      <View style={styles.container}>
      <View style={{ flex: 1,flexDirection:'column'}}>
            <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
              <ImageBackground source ={require('../../assets/images/SouthernGross_img23-1.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
              <View style={{flex:1,flexDirection:'column'}}>
                  <View style={{flex:0.4,justifyContent:'flex-end'}}>
                      <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                          <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                      </TouchableOpacity>
                  </View>
                  <View style={{flex:0.6}}>
                  <Image
                      source={require('../../assets/images/sugarbeach.png')}
                      style={{
                        width:'60%',
                        height:80,
                        position: 'absolute', // child
                        bottom: 60, // position where you want
                        // left: 30,
                        marginRight:10,
                    }}
                      />
                  </View>
              </View>       
            </ImageBackground>
            </View>
            <View style={{flex:0.75,flexDirection:'column',backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                    {/* <View style={{ flex: 1, backgroundColor: "#FFFFFF" ,flexDirection:'column'}}> */}
                <View style={{flex:0.20}}>
                  <Text style={{fontFamily:AppFont.Bold,fontSize:18,color:Colors.App_Font,marginLeft:10,marginTop:10}}>Preskil Island Restaurant</Text>
                  <Text style={{fontFamily:AppFont.Medium,fontSize:12,color:Colors.Black,marginLeft:10}}>{I18n.t('breakfast_lunch_dinner')}</Text>
                  <Text style={{fontFamily:AppFont.Medium,fontSize:11,color:Colors.Black,marginLeft:10}}>{I18n.t('open_closes')}</Text>
                </View>
                <View style={{flex:0.70}}>
                <SegmentedControlTab
                                    tabsContainerStyle={styles.tabsContainerStyle}
                                    tabStyle={styles.tabStyle}
                                    firstTabStyle={styles.firstTabStyle}
                                    lastTabStyle={styles.lastTabStyle}
                                    tabTextStyle={styles.tabTextStyle}
                                    activeTabStyle={styles.activeTabStyle}
                                    activeTabTextStyle={styles.activeTabTextStyle}
                                    // values={[I18n.t("all"), I18n.t("breakfast"), I18n.t("lunch"), I18n.t("dinner")]}
                                    values={this.state.serviceMenuNamesArray}
                                    selectedIndex={this.state.selectedIndex}
                                    onTabPress={this.handleSubMenuIndexChange}
                                />
                                {this.displayItems()}
                                
                </View>
                <View style={{flex:0.10}}></View>
                    
                    </View>
                    </View>
      
      </View>
    );
  }

  navigateBackOption=()=>{
    Actions.pop();
}
}
const styles = StyleSheet.create({
    container: {
         flex: 1,
         
    },
    boxWithShadow: {
        borderColor: Colors.Background_Gray,
        borderWidth: 0.5,
        color: '#dbdbdb',
        backgroundColor: '#FFFFFF',
        marginHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        shadowOffset: { width: 2, height: 2, },
        shadowColor: Colors.LightGray,
        shadowOpacity: 0.3,
    },
    tabStyle: {
        //backgroundColor: "#FFFFFF",
        backgroundColor: Colors.TabActive,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        height: 50,
        color: Colors.Black,
        fontSize: 13,
    },
    activeTabStyle: {
        // backgroundColor: Colors.TabActive,
        backgroundColor: Colors.App_Font,
        borderColor: Colors.App_Font,
        borderWidth: 1,
        color: Colors.Black,
        fontSize: 13,
    },
    activeTabTextStyle: {
        // color: Colors.Black,
        color: Colors.Background_Color,
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    tabsContainerStyle: {
        color: Colors.Black,
        fontSize: 13,
    },
    tabTextStyle: {
        color: Colors.Black,
        fontSize: 13,
        fontFamily: AppFont.Regular,
    },
    firstTabStyle: {
        color: Colors.Black,
        fontSize: 13,
    },
    lastTabStyle: {
        color: Colors.Black,
        fontSize: 13,
    },
    AvailableText: {
        color: Colors.AvailableColor,
        fontSize: 13
    },
    NotAvailableText: {
        color: Colors.NotavailableColor,
        fontSize: 13
    },
  item: {
    padding: 15,
  },
  title: {
    fontSize: 16,
    fontFamily: AppFont.Regular,
    color: Colors.Black,
  },
  title1: {
    fontSize: 13,
    fontFamily: AppFont.Bold,
    color: Colors.Black,
    textTransform:'uppercase',
    marginLeft:10
  },

})

module.exports = FoodMenus;