import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
    FlatList,
    ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const DATA = [
    {
        "Title":I18n.t('restaurant_book'),
        "Id":1,
        "Image":require('../../assets/images/SouthernGrossbeach_img6.png')
    },
    {
        "Title":I18n.t('spa_book'),
        "Id":2,
        "Image":require('../../assets/images/SouthernGrossbeach_img15_1.png')
    },
    {
        "Title":I18n.t('bike_book'),
        "Id":3,
        "Image":require('../../assets/images/SouthernGross_img24_1.png')
    },
    {
        "Title":I18n.t('boat_book'),
        "Id":4,
        "Image":require('../../assets/images/sugarbeach_img2.png')
    }

];

class GuestHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            
        }
    }

    componentWillMount() {
        this.handleBackAddEventListener();
    }

    componentWillUnmount() {
       // this.handleBackRemoveEventListener();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        console.log("screen name handleBackButton", Actions.currentScene);
        console.log("guest home props", this.props);

        
    }

    handleBackAddEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackAddEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }
    handleBackRemoveEventListener = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        console.log("guest home backhandler", userJson);
        if (userJson != null) {
            console.log("handleBackRemoveEventListener")
            this.props.navigation.setParams({ left: null });
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    }


    render() {
        const Item = ({ title }) => (
            <View style={styles.item}>
              <Text style={styles.title}>{title}</Text>
            </View>
          );
          const renderItem = ({ item }) => (
            <View style={{marginTop:10, borderRadius:30}}>
              <TouchableOpacity onPress={this._onSelectionBooking.bind(this,item.Id)}>
                  {/* <ImageBackground source = {item.Image} style = {{ width: '100%', height: 180, position: 'relative', borderRadius: 20,overflow: "hidden",borderWidth: 1,borderColor:'#FFF'}}>
                      
                        <Text
                        style={{
                          fontFamily: AppFont.Light,
                          fontSize:35,
                          color: 'white',
                          position: 'absolute', // child
                          bottom: 15, // position where you want
                          left: 20,
                          marginRight:10
                        }}
                      >
                        {item.Title}
                      </Text>
                </ImageBackground> */}
                <ImageBackground source = {item.Image} style = {{ width: '100%', height: 180, position: 'relative', overflow: "hidden",borderWidth: 1,borderColor:'#FFF'}}>
                      
                        
                </ImageBackground>
                <View style={{backgroundColor:'#F4F4F4'}}><Text
                        style={{
                          fontFamily: AppFont.Medium,
                          fontSize:18,
                          color: Colors.Black,
                          marginLeft:10
                        }}
                      >
                        {item.Title}
                      </Text></View>
              </TouchableOpacity>
            </View>
          );
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                    hidden={true}/>
                <View style={{ flex: 1,flexDirection:'column'}}>
                    <View style={{flex:0.25,backgroundColor:Colors.App_Font}}>
                    <ImageBackground source ={require('../../assets/images/sc1.png')} style = {{ width: '100%', height: '100%', position: 'relative',overflow: "hidden"}}>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flex:0.4,justifyContent:'flex-end'}}>
                        {/* <TouchableOpacity onPress={()=>this.navigateBackOption()}>
                                <Image source={require('../../assets/images/left-arrow.png')} style={{width:35,height:25,bottom:5}}/>
                            </TouchableOpacity> */}
                        </View>
                        <View style={{flex:0.6,alignItems:'center',justifyContent:'center'}}>
                        <Image
                            source={require('../../assets/images/sugarbeach.png')}
                            style={{
                                width:'60%',
                                height:80,
                                position: 'absolute', // child
                                bottom: 60, // position where you want
                                // left: 30,
                                marginRight:10,
                            }}
                            />
                        </View>

                    </View>      
                    </ImageBackground>
                    </View>
                    <View style={{flex:0.75,flexDirection:'column',backgroundColor:'#FFF',border:1,borderTopLeftRadius:20,borderTopRightRadius:20,marginTop:-20}}>
                    <Text style={{fontSize:18,fontFamily:AppFont.Medium,color:Colors.Black,margin:15}}>{I18n.t('bookings')}</Text>
                        <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                            
                            <FlatList
                                data={DATA}
                                renderItem={renderItem}
                                keyExtractor={item => item.Id}
                            />
                        </ScrollView>
                    </View>
                </View>

                
                {/* <View style={{ flex: 1}}>
                    <View style={{marginTop:30}}></View>
                    <FlatList
                        data={DATA}
                        renderItem={renderItem}
                        keyExtractor={item => item.Id}
                    />
                    </View> */}

            </View>
        )
    }

    _onSelectionBooking = async (BookingId) => {
        if(BookingId==1){
            Actions.RestaurantBooking();
        }else if(BookingId==2){
            Actions.SpaBooking();
        }else if(BookingId==3){
            Actions.BikeBooking();
        }else if(BookingId==4){
            Actions.BoatBooking();
        }
    }
    
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },

})

module.exports = GuestHome;