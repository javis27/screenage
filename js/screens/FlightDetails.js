import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    Image,
    Platform,
    TouchableOpacity,
    StatusBar
} from 'react-native';

import {
    Cell,
    TableView,
    Section
} from 'react-native-tableview-simple';

import FlightsListCard from '../components/FlightsListCard';
// import FlightListData from '../translations/FlightList.json';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import NumericInput from 'react-native-numeric-input';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';

import { dynamicApiCallWithUrl } from '../api/APIHandler';
import APIConstants from '../api/APIConstants';

class FlightDetails extends Component {

    constructor(props) {
        super(props);
        // this.state = {
        //     flightDetailsResponse: FlightListData,
        //     scheduledFlightsDetails: FlightListData.scheduledFlights,
        //     flightDetails: [],
        //     bookingDate: '',
        //     selectedTime: '',
        //     show: false,
        //     loading: false,
        // }
        this.state = {
            flightDetailsResponse: [],
            scheduledFlightsDetails: [],
            flightDetails: [],
            bookingDate: '',
            selectedTime: '',
            show: false,
            loading: false,
        }
    }
    componentWillMount() {
        console.log("scheduledFlightsDetails", this.state.scheduledFlightsDetails);
        this.loadApiFlightDetails();
        var today = new Date();
        var date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        var time = today.getHours();
        this.setState({ bookingDate: date, selectedTime: time });

        // this.testjsonListBind();

    }

    testjsonListBind = () => {
        var tempArray = [];
        this.state.scheduledFlightsDetails.map((response) => {
            var splitDepartureDateTime = this.convertDateTime(response.departureTime);
            var splitArrivalDateTime = this.convertDateTime(response.arrivalTime);

            tempArray.push({
                FlightNumber: response.carrierFsCode + '-' + response.flightNumber,
                airlineName: this.getAirlineName(response.carrierFsCode),
                ScheduledDepartureDate: splitDepartureDateTime.date,
                ScheduledDepartureTime: splitDepartureDateTime.time,
                departureAirport: response.departureAirportFsCode,
                departureAirportName: this.getAirportName(response.departureAirportFsCode),
                arrivalDate: splitArrivalDateTime.date,
                arrivalTime: splitArrivalDateTime.time,
                arrivalAirport: response.arrivalAirportFsCode,
                arrivalAirportName: this.getAirportName(response.arrivalAirportFsCode),
            })
        });

        // this.state.flightDetails.push(tempArray);
        this.setState({ flightDetails: tempArray })


        console.log("tempArray", tempArray);
        console.log("this.state.flightDetails", this.state.flightDetails)
    }


    loadApiFlightDetails = async () => {

        var today = new Date();
        var cur_year = today.getFullYear();
        var cur_month = today.getMonth() + 1;
        var cur_day = today.getDate();
        var cur_hourOfDay = today.getHours();
        var default_airport_code = "MRU";


        console.log('year', cur_year);
        console.log('month', cur_month);
        console.log('day', cur_day);
        console.log('hourOfDay', cur_hourOfDay)

        console.log(APIConstants.FlightAPI_URL + 'v1/json/from/' + default_airport_code + '/departing/' + cur_year + '/' + cur_month + '/' + cur_day + '/' + cur_hourOfDay + '?appId=' + APIConstants.FlightAppId + '&appKey=' + APIConstants.FlightAppKey)
        this.setState({ loading: true })
        var api_url = APIConstants.FlightAPI_URL + 'v1/json/from/' + default_airport_code + '/departing/' + cur_year + '/' + cur_month + '/' + cur_day + '/' + cur_hourOfDay + '?appId=' + APIConstants.FlightAppId + '&appKey=' + APIConstants.FlightAppKey;
        //https://api.flightstats.com/flex/schedules/rest/v1/json/from/MAA/departing/2019/7/19/11
        //https://api.flightstats.com/flex/schedules/rest//v1/json/from/MRU/departing/2019/7/30/15?appId=63b67db3&appKey=a5757d18b4d832c9c3fafdc45d49bad7
        //  await dynamicApiCallWithUrl('https://api.flightstats.com/flex/schedules/rest/v1/json/from/MAA/departing/2019/7/29/22?appId=63b67db3&appKey=a5757d18b4d832c9c3fafdc45d49bad7', 'GET', "", this.getFlightResponse)
        await dynamicApiCallWithUrl(api_url, 'GET', "", this.getFlightResponse)
    }

    getFlightResponse = async (response) => {
        console.log("response", response);
        var tempArray = [];
        await this.setState({ flightDetailsResponse: response, scheduledFlightsDetails: response.scheduledFlights })
        this.state.scheduledFlightsDetails.map((response) => {
            var splitDepartureDateTime = this.convertDateTime(response.departureTime);
            var splitArrivalDateTime = this.convertDateTime(response.arrivalTime);

            tempArray.push({
                FlightNumber: response.carrierFsCode + '-' + response.flightNumber,
                airlineName: this.getAirlineName(response.carrierFsCode),
                ScheduledDepartureDate: splitDepartureDateTime.date,
                ScheduledDepartureTime: splitDepartureDateTime.time,
                departureAirport: response.departureAirportFsCode,
                departureAirportName: this.getAirportName(response.departureAirportFsCode),
                arrivalDate: splitArrivalDateTime.date,
                arrivalTime: splitArrivalDateTime.time,
                arrivalAirport: response.arrivalAirportFsCode,
                arrivalAirportName: this.getAirportName(response.arrivalAirportFsCode),
            })

        });

        // this.state.flightDetails.push(tempArray);
        // this.setState({ flightDetails: tempArray })
        this.setState({ flightDetails: tempArray })
        console.log("tempArray", tempArray);
        console.log("this.state.flightDetails", this.state.flightDetails);
        this.setState({
            loading: false,
        });
    }
    /* To get airline name for the carrierfscode */

    getAirlineName(condition) {
        let item;
        this.state.flightDetailsResponse.appendix.airlines.find(i => {
            if (i.fs === condition) {
                item = i;
                //return item.name;
            }
        });
        return item.name
        console.log("sss", item)
    }

    /* To get airport name for the airport code */

    getAirportName(code) {
        let obj;
        this.state.flightDetailsResponse.appendix.airports.find(i => {
            if (i.fs === code) {
                obj = i;
                //return item.name;
            }
        });
        return obj.name
        console.log("aa", obj)
    }

    /* To display current date in 19 Jul 2019, Friday */

    displayDepartureDate(currentDateTime) {
        console.log("currentDateTime", currentDateTime)
        var dat = new Date(currentDateTime);
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var month_names = ["Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"];
        var date = dat.getDate();
        var month_index = dat.getMonth();
        var year = dat.getFullYear();
        var dayName = days[dat.getDay()];
        return "" + date + " " + month_names[month_index] + " " + year + ", " + dayName;
    }

    arrangeScheduledFlights = async () => {
        var arr1 = this.state.scheduledFlightsDetails;
        var arr2 = { date: "d", time: "e" };
        var arr3 = [];
        console.log("flights before", arr1)
        await arr1.map((res) => {
            var values = this.convertDateTime(res.departureTime);
            console.log('values', values);
            var date = values.date;
            var time = values.time;
            console.log('date n time', date, time);

            arr3 = arr1.push(arr2)
        })

        console.log("flights after", arr1.concat(arr2))
    }

    /* */
    convertDateTime = (yourString) => {
        //  var yourString = '2019-07-18T11:55:00.000';
        var date = yourString.split("T");
        var p = this.formatDate(date[0]);
        var q = this.formatTime(date[1]);
        return {
            date: p,
            time: q
        }
    }

    /* */

    formatDate = (input) => {
        var datePart = input.match(/\d+/g),
            year = datePart[0].substring(2), // get only two digits
            month = datePart[1],
            day = datePart[2];

        return day + '/' + month + '/' + year;
    }

    /* */
    formatTime = (input) => {
        var time = input.substring(0, 5);
        return time;
    }

    _onPressSearch = async () => {
        this.setState({ loading: true })
        var selectedDate = this.state.bookingDate;
        var selectedHour = this.state.selectedTime;
        var default_airport_code = "MRU";

        var datePart = selectedDate.match(/\d+/g),
            day = datePart[0],
            month = datePart[1],
            year = datePart[2];

        console.log("day", datePart, day, month, year)
        console.log('onSearch', this.state.bookingDate, this.state.selectedTime);
        console.log(APIConstants.FlightAPI_URL + 'v1/json/from/' + default_airport_code + '/departing/' + year
            + '/' + month + '/' + day + '/' + selectedHour + '?appId=' + APIConstants.FlightAppId + '&appKey=' + APIConstants.FlightAppKey);

        var searchApi_url = APIConstants.FlightAPI_URL + 'v1/json/from/' + default_airport_code + '/departing/' + year
            + '/' + month + '/' + day + '/' + selectedHour + '?appId=' + APIConstants.FlightAppId + '&appKey=' + APIConstants.FlightAppKey;

        await dynamicApiCallWithUrl(searchApi_url, 'GET', "", this.getFlightResponse)
    }

    showHide = () => {
        console.log("show", this.state.show)
        this.setState({ show: !this.state.show })
        console.log("show", this.state.show)
    }

    render() {
        console.log("this.state.flightDetails", this.state.flightDetails)
        console.log("this.state.scheduledFlightsDetails", this.state.scheduledFlightsDetails)
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle="light-content"
                />
                <Loader loading={this.state.loading} />
                <ScrollView contentContainerStyle={{ paddingHorizontal: 15 }}>
                    <View>

                        {this.state.scheduledFlightsDetails.length > 0 ?
                            this.state.flightDetails.length > 0 ?
                                <View>
                                    <View style={styles.flighthead}>
                                        <View style={{ flex: 0.1, alignItems: "flex-start" }}>
                                            <Image source={{ uri: 'departure' }} style={{ width: 24, height: 24, marginTop: 3 }} />
                                        </View>
                                        <View style={{ flex: 0.9, alignItems: "flex-start" }}>
                                            <Text style={styles.flighttitle}>{this.state.flightDetails[0].departureAirportName}</Text>
                                        </View>
                                        <View style={{ flex: 0.1, alignItems: "flex-end" }}>
                                            <TouchableOpacity activeOpacity={.5} onPress={this.showHide}>
                                                <Image source={{ uri: 'magnifier' }} style={{ width: 24, height: 24, marginTop: 5 }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <Text style={styles.flightsubtitle}>{this.displayDepartureDate(this.state.scheduledFlightsDetails[0].departureTime)}</Text>

                                    {this.state.show ?
                                        <View>
                                            <View style={styles.flightSearch}>
                                                <View style={{ flex: 0.5, alignItems: "flex-start" }}>
                                                    <View style={{ flex: 1 }}>
                                                        <View style={{ width: "100%" }}>
                                                            <Text
                                                                allowFontScaling
                                                                numberOfLines={1}
                                                                style={styles.label} >
                                                                {I18n.t('date')}
                                                            </Text>
                                                            <View style={styles.cellrow}>
                                                                <DatePicker
                                                                    style={{ width: 180, marginLeft: 0, }}
                                                                    date={this.state.bookingDate}
                                                                    mode="date"
                                                                    placeholder="Date"
                                                                    format="DD/MM/YYYY"
                                                                    minDate={new Date()}
                                                                    maxDate={this.state.maximumDate}
                                                                    confirmBtnText="Confirm"
                                                                    cancelBtnText="Cancel"
                                                                    iconSource={{ uri: 'date' }}
                                                                    customStyles={{
                                                                        dateInput: DatePickerAttributes.dateInput,
                                                                        dateIcon: DatePickerAttributes.dateIcon,
                                                                        placeholderText: DatePickerAttributes.placeholderText,
                                                                        dateText: DatePickerAttributes.dateText,
                                                                        btnTextCancel: DatePickerAttributes.btnTextCancel,
                                                                        btnTextConfirm: DatePickerAttributes.btnTextConfirm,
                                                                    }}
                                                                    onDateChange={(date) => {
                                                                        this.setState({
                                                                            bookingDate: date,
                                                                        })
                                                                    }}
                                                                />
                                                            </View>
                                                        </View>
                                                        <View style={styles.textBorderLine}></View>
                                                    </View>
                                                </View>
                                                <View style={{ flex: 0.5, alignItems: "flex-end" }}>
                                                    <View style={{ flex: 1, paddingBottom: 10 }}>
                                                        <View style={{ width: "100%" }}>
                                                            <Text
                                                                allowFontScaling
                                                                numberOfLines={1}
                                                                style={styles.label}>
                                                                Hour of day (0-23)
                                            </Text>
                                                            <View style={{ marginTop: 10, marginLeft: 0 }}>
                                                                <NumericInput
                                                                    value={this.state.selectedTime}
                                                                    onChange={selectedTime => this.setState({ selectedTime })}
                                                                    onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                                                                    minValue={0}
                                                                    maxValue={23}
                                                                    totalWidth={155}
                                                                    totalHeight={35}
                                                                    iconSize={14}
                                                                    step={1}
                                                                    valueType='real'
                                                                    rounded
                                                                    textColor='#000000'
                                                                    borderColor={Colors.App_Font}
                                                                    iconStyle={{ color: 'white' }}
                                                                    rightButtonBackgroundColor={Colors.App_Font}
                                                                    leftButtonBackgroundColor={Colors.App_Font} />
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>

                                            <View style={{ flex: 1, marginTop: 7, marginBottom: 25, justifyContent: 'center' }}>
                                                <View style={{ width: "100%", marginLeft: 0 }}>
                                                    <View style={{ alignItems: 'center' }}>
                                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressSearch}>
                                                            <Text style={styles.btntext}>Search</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        : null
                                    }

                                    <FlatList data={this.state.flightDetails} keyExtractor={item => item.FlightNumber}
                                        // renderItem={({ item }) => return ( <FlightsListCard detail={item} />)}
                                        renderItem={({ item, index }) => {
                                            return (
                                                // <View style={{ backgroundColor: bgListColors[index % bgListColors.length] }}>
                                                <View>

                                                    <FlightsListCard detail={item} /></View>
                                                // </View>
                                            )
                                        }}
                                    />
                                </View>
                                : <View style={{ flex: 1, marginTop: "50%" }}><Text style={{ textAlign: 'center', justifyContent: 'center', alignItems: 'center', fontFamily: AppFont.Regular }}>Finding records</Text></View>
                            : <View style={{ flex: 1, marginTop: "50%" }}><Text style={{ textAlign: 'center', justifyContent: 'center', alignItems: 'center', fontFamily: AppFont.Regular }}>No records found</Text></View>}
                    </View>
                </ScrollView>

            </View >
        );
    }
}
const styles = StyleSheet.create({
    flighttitle: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Bold,
        paddingHorizontal: 10,
        backgroundColor: Colors.Background_Color,
    },
    flightsubtitle: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular,
        backgroundColor: Colors.Background_Color,
        paddingBottom: 10,
    },
    flighthead: {
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                // paddingHorizontal: 20,
                paddingVertical: 10,
                marginTop: 10,
            },
            android: {
                // paddingHorizontal: 20,
                paddingVertical: 10,
                marginTop: 10,
            },
        }),
    },
    flightSearch: {
        // flex: 1,
        flexDirection: 'row',
        backgroundColor: Colors.Background_Color,
    },
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    label: {
        marginTop: 10,
        marginLeft: 0,
        marginRight: 0,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textBorderLine: {
        marginLeft: 0,
        marginRight: 0,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
})

module.exports = FlightDetails;