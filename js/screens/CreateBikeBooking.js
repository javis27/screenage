import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';
import BikeBooking from '../components/BikeBooking';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      langLoading: true,
      username: '',
      password: '',
      myArray: [],
      selctedLanguage: 'Choose your Language',
      langId: '',
    }
  }
  
  componentWillMount() {
    //this._loadHotelDetails();
  }
  render() {
   
    return (
      <View style={styles.container}>
        
        <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
        />
        <View style={{ flex: 1}}>
          <BikeBooking/>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column'
  },
  loginview: {
    flex: 0.8,
    alignItems: 'center',
    //width: "40%"
  },
  button: {
    backgroundColor: Colors.App_Font,
    padding: 10,
    width: "95%",
    borderRadius: 5
  },
  btntext: {
    fontFamily: AppFont.Regular,
    color: Colors.Background_Color,
    fontSize: 15,
    textAlign: 'center',
    //fontFamily: AppFont.SemiBold,
  },
  copyrightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  copyrightText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    color: Colors.LightGray
  },
  versionText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    paddingVertical: 10,
    color: '#dbdbdb'
  }
});

module.exports = Language;