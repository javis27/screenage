import React from 'react';
import {
    Platform,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    ScrollView,
    StatusBar
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

class ForgotPassword extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            password: '',
            confirmpassword: '',
           // userId:'',
        }
    }
    componentWillMount() {
        this._loadInitialState();
    }

    render() {
        const lcode = this.props.langcode;
        return (
            <View style={{ flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'row' }}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle="light-content"
                />
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <View style={{ alignItems: 'center', paddingTop: 25 }}>
                            <Text style={{ fontSize: 22, fontFamily: AppFont.Medium, marginBottom: 20 }}>{I18n.t('resetyourpassword')}</Text>
                        </View>

                        <Text style={{ fontSize: 14, fontFamily: AppFont.Regular, marginBottom: 35, textAlign: 'center', paddingHorizontal: 20 }}>
                        {I18n.t('accountverifysuccess')}</Text>

                        <View style={{ width: '100%', height: 80, alignItems: 'center' }}>
                            <Text
                                allowFontScaling
                                numberOfLines={1}
                                style={styles.label}>
                                {I18n.t('NPassword')}
                            </Text>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    value={this.state.emailId}
                                    secureTextEntry={true}
                                    placeholderTextColor={Colors.PlaceholderText}
                                    placeholder={I18n.t('NPassword')}
                                    underlineColorAndroid='transparent'
                                    style={styles.textInput}
                                    onChangeText={(text) => this.setState({ password: text })}
                                    value={this.state.password}
                                />
                            </View>
                        </View>
                        <View style={{ width: '100%', height: 80, alignItems: 'center' }}>
                            <Text
                                allowFontScaling
                                numberOfLines={1}
                                style={styles.label}>
                                {I18n.t('CPassword')}
                            </Text>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    value={this.state.password}
                                    secureTextEntry={true}
                                    placeholderTextColor={Colors.PlaceholderText}
                                    placeholder={I18n.t('CPassword')}
                                    underlineColorAndroid='transparent'
                                    style={styles.textInput}
                                    onChangeText={(text) => this.setState({ confirmpassword: text })}
                                    value={this.state.confirmpassword}
                                />
                            </View>
                        </View>
                        <View style={styles.loginview}>
                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressButton}>
                                <Text style={styles.btntext}> {I18n.t('RPassword')} </Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ width: '100%', paddingBottom: 20, paddingTop: 20, alignItems: 'center' }}>
                            <TouchableOpacity onPress={this._onPressLogin}>
                                <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('backtologin')}</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </View>
        );
    }
    _loadInitialState = async () => {
        const value = await AsyncStorage.getItem('langcode');
        const forgetpwd = Locale.lCode.forgot;
        this.setState({
            "langCode": value,
            "forgot": forgetpwd
        })
    }

    validateEmail = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            console.log("Email is Not Correct");
            this.setState({ email: text })
            return false;
        }
        else {
            this.setState({ email: text })
            console.log("Email is Correct");
        }
    }

    _onPressButton = async () => {
        // const details = await AsyncStorage.getItem(Keys.UserDetail);
        // var jsonValue = JSON.parse(details);
        // await this.setState({ userId: jsonValue.UserId });

        if (this.state.password == "") {
            Alert.alert(I18n.t('pcNPassword'));
        } else {
            if (this.state.confirmpassword == "") {
                Alert.alert(I18n.t('pcCPassword'));
            } else {
                if (this.state.password === this.state.confirmpassword) {
                    this.setState({ registerLoading: true })
                    let overalldetails = {
                        "UserId": this.props.user_id,
                        "NewPassword": this.state.password,
                    }
                    await apiCallWithUrl(APIConstants.NewPasswordAPI, 'POST', overalldetails, this.newPasswordApiResponse);
                } else {
                    Alert.alert(I18n.t('alertnewPwdnotmatch'));
                }
            }
        }


    }

    newPasswordApiResponse = async(response) => {
        console.log('New Password Response', response)
        var passwordResponse=response.ResponseData
        if(passwordResponse){
            Alert.alert(I18n.t('alertpwdsuccess'));
            Actions.Login();
        }
    }

    _onPressRegister = () => {
        Actions.Register();
    }

    _onPressLogin = () => {
        Actions.Login();
    }
}

const styles = StyleSheet.create({
    textInput: {
        marginLeft: 0,
        height: '100%',
        width: '85%',
        fontSize: 15,
        fontFamily: AppFont.Regular,
        marginTop: 8
    },
    SectionStyle: {
        height: 50,
        width: '85%',
        borderBottomWidth: 1,
        borderColor: Colors.App_Font,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
        marginBottom: 10
    },
    loginview: {
        alignItems: 'center',
        marginTop: 20
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "75%",
        borderRadius:5,
        justifyContent:'center'
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
        textAlign: 'center'
    },
    label: {
        alignItems: "flex-start",
        width: '85%',
        fontFamily: AppFont.Regular,
        color: Colors.Black,
        ...Platform.select({
            ios: {
                marginLeft: 0,
            },
            android: {
                marginLeft: 8,
            },
        }),
    },

});

module.exports = ForgotPassword;