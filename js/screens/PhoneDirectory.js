import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    SectionList,
    StatusBar,
    Dimensions,
    AsyncStorage,
    Alert,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import Keys from '../constants/Keys';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import groupBy from '../constants/groupBy';
import PhoneDirectoryItem from '../components/PhoneDirectoryItem';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class PhoneDirectory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneDirectoryArray: [],
            loading: false,
            isRefreshing: false,
            isPhoneDirectory: false,
        }
    }
    componentWillMount() {
        this._onLoadPhoneDirectoryDetails();
        // this.verifyCheckInDetails();
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = (response) => {
    //     console.log("postCheckInResponse Language", response);
    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             console.log("postCheckInResponse Calendar else", response);
    //             this._onLoadPhoneDirectoryDetails();
    //         }
    //     }
    // }

    renderEmptyComponent = () => {
        if (this.state.phoneDirectoryArray.length != 0) return null;
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', fontFamily: AppFont.Regular }}>{I18n.t('nodata')}</Text>
            </View>
        );
    };
    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.isPhoneDirectory ?
                        <View style={styles.FlightDetailsOuter}>
                            <StatusBar
                                translucent={false}
                                backgroundColor={Colors.App_Font}
                                barStyle='light-content'
                            />
                            <Loader
                                loading={this.state.loading} />
                            <View>
                                <SectionList
                                    stickySectionHeadersEnabled
                                    refreshing={this.state.isRefreshing}
                                    onRefresh={this._onLoadPhoneDirectoryDetails}
                                    ListEmptyComponent={this.renderEmptyComponent}
                                    keyExtractor={(x, i) => i}
                                    sections={this.state.phoneDirectoryArray}
                                    renderItem={({ item }) => { return <PhoneDirectoryItem phonedirectory={item} /> }}
                                    renderSectionHeader={({ section }) => <View style={styles.FlightDetailsOuterActive}><Text style={styles.title}>{section.title}</Text></View>}
                                />
                            </View>

                        </View>

                        : null
                }
            </View>
        )
    }

    _onLoadPhoneDirectoryDetails = async () => {
        this.setState({
            loading: true,
            isRefreshing: true
        });
        var languageId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        console.log("languageId phonedirectory", languageId)
        var HotelId = await AsyncStorage.getItem('HotelId');
       
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId": languageId
          }
          await apiCallWithUrl(APIConstants.GetPhoneDirectoryAPI, 'POST', overalldetails, this.postCheckInPhoneDirectoryResponse);
        //apiCallWithUrl(APIConstants.GetPhoneDirectoryAPI + "?LanguageId=" + languageId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInPhoneDirectoryResponse);
    }

    postCheckInPhoneDirectoryResponse = async (response) => {
        // var postCheckInDetailResponse = response.ResponseData;
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getPhoneDirectoryResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getPhoneDirectoryResponse = async (response) => {
        if (response.IsException == null) {
            var phoneDirectoryResponse = response.ResponseData;
            console.log('phonedirectory response', phoneDirectoryResponse)

            // await this.setState({
            //     phoneDirectoryArray: phoneDirectoryResponse === null ? '' : phoneDirectoryResponse
            // })

            // console.log('state phonedirectory', this.state.phoneDirectoryArray)

            var groupByCategoryId = groupBy(phoneDirectoryResponse, 'Category')
            var arrayOfData = [];
            Object.keys(groupByCategoryId).forEach(element => {
                arrayOfData.push({
                    title: element,
                    data: groupByCategoryId[element]
                });
            });

            console.log('arrayOfData...', arrayOfData);
            this.setState({ loading: false, isRefreshing: false, phoneDirectoryArray: arrayOfData, isPhoneDirectory: true });
        }
    }

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    title: {
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.SemiBold,
        padding: 13,
    },
    FlightDetailsOuter: {
        borderColor: Colors.Gray,
        borderWidth: 1,
        flex: 1,
        padding: 25
        //flexDirection: "row",
    },
    FlightDetailsOuterActive: {
        backgroundColor: Colors.FullGray,
        borderColor: Colors.Gray,
        borderWidth: 0.5,
        flex: 1,
        //flexDirection: "row",
    },

})

module.exports = PhoneDirectory;