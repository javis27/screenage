import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Dimensions,
    ScrollView,
    Image,
    StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';
import AppFont from '../constants/AppFont';
import Colors from '../constants/Colors';

const SCREEN_HEIGHT = Dimensions.get('window').height;


export default class DynamicMenuContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageMenuData: this.props.menuContent
        }
        console.log('props', this.props.menuContent)
    }
    componentDidMount() {
        this.props.navigation.setParams({ title: this.props.menuContent.PageName });
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                     translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <View style={{ backgroundColor: Colors.Background_Color }}>
                    {this.state.pageMenuData.Content != "" ?
                        <View>
                            <ScrollView>
                                <View style={{ padding: 0, fontFamily: AppFont.Regular }}>
                                    <HTML
                                        html={this.state.pageMenuData.Content}
                                        tagsStyles={{fontFamily: AppFont.Regular,}}
                                        baseFontStyle={{ fontFamily: AppFont.Regular,letterSpacing:0.5,lineHeight:30}}
                                        // emSize={10}
                                        // ptSize={5}
                                        ignoredStyles={["font-family", "letter-spacing"]}
                                        imagesMaxWidth={Dimensions.get('window').width}
                                        //style={{ paddingBottom: 20, fontFamily: AppFont.Regular, color: '#717171' }} 
                                        />
                                </View>
                            </ScrollView>
                        </View>
                        : null
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%',
       paddingLeft: 10,
       paddingRight:10,
       paddingTop:10,
    },
})