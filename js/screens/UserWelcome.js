import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      langLoading: true,
      username: '',
      password: '',
      myArray: [],
      langId: '',
      totalData:[]
    }
  }
  
  componentWillMount() {
    //this._loadHotelDetails();
  }
  render() {
    
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../../assets/images/SouthernGrossbeach_img1.png')} style={{flex: 1,resizeMode: 'cover',justifyContent: "center",flexDirection: 'column'}}>
            <View style={{ flex: 0.50,justifyContent:'center', alignItems: 'center' }}></View>
            <View style={{ flex: 0.50, justifyContent: "center" }}>
                <View style={{flex:1,backgroundColor:'#00000070',margin:40,borderRadius:25,justifyContent:'center',alignItems:'center',height:200}}>
                <Text style={{fontFamily:AppFont.Light, fontSize:16, color:'#FFF',letterSpacing:3,marginTop:5}}> Welcome To</Text>
                <Image
                            source={require('../../assets/images/sugarbeach.png')}
                            style={{
                                width:'80%',
                                height:120,
                                marginTop:5,
                                resizeMode:'cover'
                            }}
                            />
                  <View style={styles.loginview}>
                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onSelectedHotel.bind(this,1)}>
                            <Text style={styles.btntext}>LET'S START</Text>
                        </TouchableOpacity>
                    </View>
                </View>
               
            </View>
            </ImageBackground>
        
      </View>
    );
  }
  
 
_onSelectedHotel = async (HotelId) => {
  console.log("AllNewHotelId",HotelId)
  Actions.GuestHome();
  //Actions.pop();
}


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column'
  },
  loginview: {
    alignItems: 'center',
    width: "85%",
    marginTop:15,
    marginBottom:20
},
button: {
    backgroundColor: Colors.Background_Color,
    padding: 5,
    width: "90%",
    alignItems: "center",
    borderRadius: 25,
    height:35
},
btntext: {
    fontFamily: AppFont.Medium,
    color: "#000",
    fontSize: 18,
    alignItems: "center",
    //fontFamily: AppFont.SemiBold,
},
  copyrightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  copyrightText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    color: Colors.LightGray
  },
  versionText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    paddingVertical: 10,
    color: '#dbdbdb'
  }
});

module.exports = Language;