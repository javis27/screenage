import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
    Image,
    TextInput,
    RefreshControl,
    Platform,
    StatusBar
} from 'react-native';

import {
    Cell,
    TableView,
    Section

} from 'react-native-tableview-simple';

import { Actions } from 'react-native-router-flux';
import BottomSheet from 'react-native-bottomsheet';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import Icon from 'react-native-vector-icons/Fontisto';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import DatePicker from 'react-native-datepicker';
import DatePickerAttributes from '../constants/DatePickerAttributes';
import Status from '../constants/Status';
// import CancellationPolicy from '../components/CancellationPolicy';
import Policy from '../components/Policy';
import NumericInput from 'react-native-numeric-input';
import Moment from 'moment';
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

class SpaBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
            selectedTimeSlot: 'Time Slot',
            selectedTimeSlot: I18n.t('choosetimeslot'),
            // selectedVisitor: I18n.t('chooseguest'),
            selectedVisitor: '',
            selectedSpaCategory: I18n.t('choosespacategory'),
            spaPackageArray: [],
            boatTimeslotArray: [],
            selectedBoatingDateTime:[],
            SessionId:[],
            spaVisitorArray: [],
            boatTimeArray: [],
            spaPackageId: '',
            // spaTimeslotId: '',
            spaVisitorId: 1,
            spaCategoryId: '',
            preferenceNote: '',
            userId: '',
            selectthepackage: 'Select the Package',
            displayAmountState: false,
            selectedColor1: Colors.App_Font,
            selectedColor2: Colors.FullGray,
            selectedFontColor1: "#FFFFFF",
            selectedFontColor2: Colors.Black,
            Amount: '',
            spaloading: false,
            boatCancelPolicyVisible: false,
            spaTime: '',
            refreshing: false,
            noofadults:'',
            noofchilds:'',
            Notes:'',
            isSelectedBoatDisclaimer:false
            
        };
    }

    async componentDidMount() {
        console.log("componentDidMount BoatBookingCart");
     await firebase.app();
     analytics().logScreenView({
        screen_name: 'BoatBookingcartScreen',
        screen_class: 'BoatBookingcartScreen'
    });

        this._loadDropdowns();
        // this.verifyCheckInDetails();
        // console.log("seleee..", this.props.selectedVals);
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ spaloading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     console.log("postCheckInResponse Language", response);

    //     if (Platform.OS === 'ios') {
    //         this.setState({ spaloading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             spaloading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             this.setState({ minBookingDate: postCheckInDetailResponse.currentDate, maxBookingDate: postCheckInDetailResponse.checkOutDate })
    //             // this._loadDropdowns();
    //         }
    //     }
    // }

    render() {
        return (
            <View style={{flex:1,backgroundColor: "#FFFFFF"}}>
            <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
        />
                <TableView>
                    <Loader loading={this.state.spaloading} />
                    <Section sectionPaddingTop={0} sectionPaddingBottom={0} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                        
                        <Cell title="Boat" placeholder="Boat" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        Boat Activity
                                    </Text>
                                    <View style={{ paddingTop: 7 }}>
                                            <Text style={styles.picker}>{this.props.BoatDetails.BoatActivityName}</Text>
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="Time Slot" placeholder="Time Slot" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label}>
                                        Time Slot
                                    </Text>

                                    <View>
                                        <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtmSheettimeslot}>
                                            <Text style={styles.picker}>{this.state.selectedBoatingDateTime}</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
              <Cell title="Number of People" placeholder="Number of People" id="" cellContentView={
                <View style={{ flex: 1,flexDirection:'row', paddingTop: 10, paddingBottom: 10 }}>
                  <View style={{flex:0.5}}> 
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      No of Adults
                    </Text>
                    {/* <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetNoofPeople}>
                        <Text style={styles.picker}>{this.state.numberofPeople}</Text>
                      </TouchableOpacity>
                    </View> */}
                    <View style={{ marginTop: 10, marginLeft: 7 }}>
                      <NumericInput
                        value={this.state.noofadults}
                        onChange={noofadults => this.setState({ noofadults })}
                        onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                        minValue={0}
                        maxValue={this.props.BoatDetails.MaxAdult}
                        totalWidth={120}
                        totalHeight={40}
                        iconSize={16}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#000000'
                        borderColor={Colors.App_Font}
                        iconStyle={{ color: 'white' }}
                        rightButtonBackgroundColor={Colors.App_Font}
                        leftButtonBackgroundColor={Colors.App_Font} />
                    </View>
                  </View>
                  </View>
                  <View style={{flex:0.5}}> 
                  <View style={{ width: "90%" }}>
                    <Text
                      allowFontScaling
                      numberOfLines={1}
                      style={styles.label}>
                      No of Children
                    </Text>
                    {/* <View>
                      <TouchableOpacity style={{ paddingTop: 7 }} activeOpacity={.5} onPress={this._onBtnSheetNoofPeople}>
                        <Text style={styles.picker}>{this.state.numberofPeople}</Text>
                      </TouchableOpacity>
                    </View> */}
                    <View style={{ marginTop: 10, marginLeft: 7 }}>
                      <NumericInput
                        value={this.state.noofchilds}
                        onChange={noofchilds => this.setState({ noofchilds })}
                        onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                        minValue={0}
                        maxValue={this.props.BoatDetails.MaxChildren}
                        totalWidth={120}
                        totalHeight={40}
                        iconSize={16}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#000000'
                        borderColor={Colors.App_Font}
                        iconStyle={{ color: 'white' }}
                        rightButtonBackgroundColor={Colors.App_Font}
                        leftButtonBackgroundColor={Colors.App_Font} />
                    </View>
                  </View>
                  </View>
                  
                  {/* <View style={styles.textBorderLine}></View> */}

                </View>
              } />
                        
                        <Cell title="Notes" placeholder="Notes" id="" cellContentView={
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ width: "90%" }}>
                                    <Text
                                        allowFontScaling
                                        numberOfLines={1}
                                        style={styles.label} >
                                        Notes
                                    </Text>
                                    <View>
                                        <TextInput
                                            underlineColorAndroid="transparent"
                                            autoCapitalize='none'
                                            placeholderTextColor={Colors.PlaceholderText}
                                            style={styles.textInput}
                                            placeholder='Notes'
                                            // placeholder={I18n.t('preference')}
                                            onChangeText={(Notes) => this.setState({ Notes })}
                                            value={this.state.Notes} />
                                    </View>
                                </View>
                                <View style={styles.textBorderLine}></View>
                            </View>
                        } />
                        <Cell title="Disclaimer" hideSeparator={true} placeholder="Disclaimer" id="6" cellContentView={
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingVertical: 20, }}>
                                    {/* <View style={{ flex: 1, alignItems: "flex-start" }}> */}
                                    <View>
                                        <TouchableOpacity onPress={() => this.onPressAgree()} style={{ paddingHorizontal: 10 }}>
                                            <Icon name={this.state.isSelectedBoatDisclaimer ? "checkbox-active" : "checkbox-passive"} size={18} color={Colors.Black} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexWrap: 'wrap' }}>
                                        <Text style={styles.AgreeText}>{I18n.t('agreebikedisclaimer')}</Text>
                                    </View>
                                    {/* <TouchableOpacity activeOpacity={.5} style={{ paddingHorizontal: 5 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeIndividualDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('individualDisclaimer')}</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.AgreeText}> or </Text>
                                    <TouchableOpacity activeOpacity={.5} style={{ paddingLeft: 40, paddingVertical:10 }} onPress={() => this._onPressSafetyPolicy(Status.disclaimerTypes.bikeGroupDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('groupbikeDisclaimer')}</Text>
                                    </TouchableOpacity> */}
                                </View>
                            } />
                        <Cell title="See Cancellation Policy" hideSeparator={true} placeholder="See Cancellation Policy" id="7" cellContentView={
                            <View style={{ flex: 1, height: 50, justifyContent: "center", alignItems: "center" }}>
                                <TouchableOpacity activeOpacity={.5} onPress={this.handleBoatCancelPolicy}>
                                    <Text style={{ fontSize: 15, marginTop: 20, fontFamily: AppFont.Regular, textDecorationLine: 'underline' }}>{I18n.t('cancellaitionpolicy')}</Text>
                                </TouchableOpacity>
                            </View>
                        } />
                        <Cell title="Book" placeholder="Book" id="" cellContentView={
                            <View style={{ flex: 1, marginTop: 25, justifyContent: 'center' }}>
                                <View style={{ width: "90%", marginLeft: 15 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressBoatBooking}>
                                            <Text style={styles.btntext}>{I18n.t('bookbtn')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        } />
                    </Section>
                    {
                        this.state.boatCancelPolicyVisible ?
                            <Policy policyVisibility={this.state.boatCancelPolicyVisible}
                                policyType={Status.policyTypes.boatCancellationPolicy.typeId}
                                bookPageIndex={1}
                                callbackPolicy={this.setBoatCancelPolicyVisibility} />
                            : null
                    }
                </TableView>
          
                </View>

        );
    }

    _onSpaRefresh = () => {
        this.setState({ refreshing: true })
        this._loadDropdowns();
    }

    _onBtmSheettimeslot = async () => {
        console.log("this.state.myArray", this.state.boatTimeslotArray);
        console.log("this.state.totaldata", this.state.totalData);

        BottomSheet.showBottomSheetWithOptions({
            // title: "Choose Restaurant",
            options: this.state.boatTimeslotArray,
            dark: false,
            cancelButtonIndex: 60,
            // destructiveButtonIndex: 1,
        }, (value) => {
            console.log("selectedBoatingDateTime", this.state.totalData[value].BoatingDateTime);
            this.setState({
                "selectedBoatingDateTime": Moment(this.state.totalData[value].BoatingDateTime).format('lll'),
                "SessionId": this.state.totalData[value].SessionId
            })
            
            console.log("SessionId", this.state.SessionId);
            console.log("selectedBoatingDateTime", this.state.BoatingDateTime);
        });
    }
    onPressAgree = () => {
        console.log("isSelected", this.state.isSelectedBoatDisclaimer)
        this.setState({ isSelectedBoatDisclaimer: !this.state.isSelectedBoatDisclaimer })
    }
    handleBoatCancelPolicy = async () => {
        await this.setState({ boatCancelPolicyVisible: true }, () => console.log("boatCancelPolicyVisible", this.state.boatCancelPolicyVisible))
    }
    setBoatCancelPolicyVisibility = async (data) => {
        console.log('setAboutVisibility', data)
        await this.setState({ boatCancelPolicyVisible: data });
    }
    

    _loadDropdowns = async () => {
        await this.setState({ spaloading: true });
        const details = await AsyncStorage.getItem(Keys.UserDetail);
        var jsonValue = JSON.parse(details);
        console.log("json", jsonValue)
        await this.setState({ userId: jsonValue.UserId });
        var languageId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        
        let overalldetails = {
            "HotelId": HotelId,
            "BoatActivityId":this.props.BoatDetails.BoatActivityId
          }
          await apiCallWithUrl(APIConstants.GetBoatTimeSlotListAPI, 'POST', overalldetails, (callback) => { this.postCheckInDetailsResponse(callback) });
        //await apiCallWithUrl(APIConstants.GetSpaMastersAPI + "?FullName=" + jsonValue.Fullname + "&&LanguageId=" + languageId + "&&CheckInId=" + checkInVal, 'GET', '', this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }
        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        var currentdate=today.toISOString();
        this.getDropdownsResponse(response);
        this.setState({ minBookingDate: currentdate, maxBookingDate: currentdate })
        console.log("postCheckInResponse", response.InCustomer);
        
    }


    getDropdownsResponse = async (response) => {
        console.log("response.ResponseData.length", response.ResponseData.length)
        await this.setState({ spaloading: false, })
        if (response.ResponseData.length > 0) {
            this.setState({ totalData: response.ResponseData, refreshing: false })

            console.log("totalResponse", response.ResponseData);
            console.log("haaa...", response.ResponseData[0]);
            var list1 = [];
            response.ResponseData.map((spacategory, index) => {
                console.log("loop", index, spacategory.BoatingDateTime);
                list1.push(Moment(spacategory.BoatingDateTime).format('lll'));
            });
            this.setState({ boatTimeslotArray: list1 })

            

            console.log("boatTimeslotArray", this.state.boatTimeslotArray);
            // console.log("timeslotArray", this.state.spaTimeslotArray);
            
        }
    }

   


    _onPressBoatBooking = async () => {
        console.log("SPA Time", this.state.spaTime);
        console.log("spaCategoryId", this.state.spaCategoryId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log("HotelId...",HotelId)
        const InCustomer = await AsyncStorage.getItem(Keys.inCustomer);
        console.log("InCustomer", InCustomer);
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        console.log("roomNo", roomNo);
        if (InCustomer == "True") {
            let RoomNo=roomNo
          } else {
            let RoomNo=""
          }
        
                    if (this.state.SessionId == "") {
                        Alert.alert(I18n.t('selectTime'));
                    } else {
                        if (this.state.noofadults == "") {
                            Alert.alert('Choose No of Peoples');
                        } else {
                            this.setState({ spaloading: true })
                            if (!this.state.isSelectedBoatDisclaimer) {
                                Alert.alert(I18n.t('agreedisclaimer'));
                                return false;
                            }
                        //    if(this.props.BoatDetails.Amount!="" &&  this.props.BoatDetails.Amount!=null){
                        //        var amount_passed=this.props.BoatDetails.Amount
                        //    }else{
                        //     var amount_passed=1000
                        //    }

                            if(this.props.BoatDetails.AdultOfferPrice!="" && this.props.BoatDetails.AdultOfferPrice!=null){
                                var adult_price = this.props.BoatDetails.AdultOfferPrice
                            }else{
                                var adult_price = this.props.BoatDetails.AdultPrice
                            }
                            if(this.props.BoatDetails.ChildrenOfferPrice!="" && this.props.BoatDetails.ChildrenOfferPrice!=null){
                                var child_price = this.props.BoatDetails.ChildrenOfferPrice
                            }else{
                                var child_price = this.props.BoatDetails.ChildrenPrice
                            }
                            var totaladultprice = adult_price * this.state.noofadults
                            var totalchildprice = child_price * this.state.noofchilds
                            var amount_passed = totaladultprice + totalchildprice
                            let overalldetails = {
                              
                                "BookingDateTime": this.state.selectedBoatingDateTime,
                                "BoatActivityId": this.props.BoatDetails.BoatActivityId,
                                "BoatSessionId": this.state.SessionId,
                                "Amount": amount_passed,
                                "UserId": this.state.userId,
                                "NoOfAdult": this.state.noofadults,
                                "NoOfChildren": this.state.noofchilds,
                                "Remarks":this.state.Notes,         
                                "HotelId":HotelId,
                                "UserType":0,
                                "RoomNo":roomNo
                            }
                            await analytics().logEvent('BoatBooking_Cart', {
                                id: 1,
                                ClickedOption: 'BoatBooking Cart',
                                Details:overalldetails
                              })
                            console.log('overalldetails', overalldetails)
                            await apiCallWithUrl(APIConstants.PostBoatBookingAPI, 'POST', overalldetails, this.postBoatResponse);
                        }
                    }
                
            }
        

    
      postBoatResponse = async (response) => {
        if (Platform.OS === 'ios') {
            await this.setState({
                spaloading: false,
            }, () => {
                setTimeout(() => this.funcBoatResponse(response), 1000);
            });
        } else {
            this.setState({
                spaloading: false,
            }, () => this.funcBoatResponse(response));
        }


    }

    funcBoatResponse = (response) => {
        if (response.IsException == null) {
            // if (response.ResponseData.length > 0) {
                var funcBoatResponse = response.ResponseData;
                console.log('funcBoatResponse', funcBoatResponse);
                Alert.alert('Booking Successful');
                Actions.pop({ refresh: true });
            //     setTimeout(() => {
            //         Actions.refresh({ key: Math.random() * 1000000 })
            //   }, 5)
            // }
        }

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        // flexDirection: 'column'
    },
    label: {
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        includeFontPadding: false,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    picker: {
        color: Colors.PlaceholderText,
        marginLeft: 8,
        // height: 28,
        padding: 0,
        textAlign: 'left',
        fontFamily: AppFont.Regular,
        fontSize: 15,
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 10
    },

});

module.exports = SpaBooking;

