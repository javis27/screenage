import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      langLoading: false,
      username: '',
      password: '',
      myArray: [],
      selctedLanguage: 'Choose your Language',
      langId: '',
    }
  }
  // componentDidMount() {
  //   console.log("componentDidMount Language")
  // }

  componentWillMount() {
    console.log("componentWillMount Language", this.props)
    this._loadLanguageDetails();
    console.disableYellowBox = true;
    I18n.locale = "en-US";
  }

  render() {
    return (
      <View style={styles.container}>
        <Loader
          loading={this.state.langLoading} />
        <StatusBar
          backgroundColor={Colors.Background_Color}
          barStyle='light-content'
        />
        <View style={{ flex: 0.70 }}>
          <View style={{ height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <Image source={require('../../assets/images/logo.png')} style={{ width: '70%', height: '60%' }} />
          </View>
        </View>
        <View style={{ flex: 0.30, justifyContent: 'flex-end' }}>
          {/* <View style={{ height: 60, alignItems: 'center' }}>
            <Text style={{ fontSize: 18, paddingVertical: 15, fontFamily: AppFont.Regular }}>{I18n.t('chooselanguage')}</Text>
          </View> */}

          <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', paddingHorizontal: 15 }}>
            <View style={styles.loginview}>
              <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onButtonSheet}>
                <Text style={styles.btntext}>{this.state.selctedLanguage}</Text>
                {/* <Text style={styles.btntext}>{I18n.t('chooselanguage')}</Text> */}
              </TouchableOpacity>
            </View>
            {/* <View style={{ flex: 0.2, alignItems: 'center' }}>
              <TouchableOpacity activeOpacity={.5} style={[styles.buttonNext, { padding: 6 }]} onPress={this._onButtonNext}>
                <Text style={[styles.btntext, { fontSize: 25 }]}>></Text>
              </TouchableOpacity>
            </View> */}
          </View>
          <View style={{ height: 50 }}>
          </View>
          {/* <View style={{ borderBottomColor: Colors.App_Font, borderBottomWidth: .5, height: 20 }}></View> */}
          <View style={styles.copyrightContainer}>
            <View style={{ alignItems: 'center', flex: 0.7, }}>
              <Text style={styles.copyrightText}>Copyright @2019 Southern Cross Limited.</Text>
              <Text style={styles.copyrightText}> All rights reserved.</Text>
            </View>
          </View>
          <View style={styles.copyrightContainer}>
            <View style={{ alignItems: 'center', flex: 0.5 }}>
              <Text style={styles.versionText}>Version: 1.0.1</Text>
            </View>
          </View>

        </View>
      </View>
    );
  }
  _onButtonSheet = async () => {
    console.log("this.state.myArray", this.state.myArray);
    console.log("this.state.totalData", this.state.totalData);
    // this.state.myArray.unshift(I18n.t('chooselanguage'));
    // this.state.myArray.push("Cancel");
    var myArrayLen = this.state.myArray.length;
    BottomSheet.showBottomSheetWithOptions({
      title: I18n.t('chooselanguage'),
      options: this.state.myArray,
      dark: false,
      cancelButtonIndex: 8,
      // destructiveButtonIndex: 1,
    }, (value) => {
      if (value >= this.state.totalData.length - 1) {
        console.log("array", this.state.myArray)
        return;
      }
      else {
        console.log("value", value);
        console.log("this.state.totalData", this.state.totalData.length);
        console.log("this.state.totalData.value", this.state.totalData[value].LanguageCode);
        this.setState({
          "selctedLanguage": this.state.totalData[value].NativeName,
          "selectedLanguageCode": this.state.totalData[value].LanguageCode,
          "langId": this.state.totalData[value].LanguageId
        })
        //I18n.locale=this.state.totalData[value].LanguageCode;
        // I18n.locale="fr-FR";
        console.log('I18n', I18n);
        I18n.locale = this.state.totalData[value].LanguageCode;
        this._onButtonNext();
        //alert(this.state.totalData[value].NativeName);
      }
    });
  }
  _onButtonNext = async () => {
    // Alert.alert("_onButtonNext");
    console.log("_onButtonNext")
    await AsyncStorage.setItem('langcode', this.state.selectedLanguageCode);
    console.log("languageId", this.state.langId)
    await AsyncStorage.setItem(Keys.langId, this.state.langId);

    Actions.GuestLogin({ "langcode": this.state.selectedLanguageCode });
  }
  _loadLanguageDetails = async () => {
    // Alert.alert("_loadInitialState");
    console.log("_loadInitialState")
    const value = await AsyncStorage.getItem('langcode');
    const isLoggedIn = await AsyncStorage.getItem(Keys.isLogin);
    const InCustomer = await AsyncStorage.getItem(Keys.inCustomer);
    console.log("isLoggedIn", JSON.parse(isLoggedIn))
    console.log("InCustomer", InCustomer);
    if (value != null) {
      console.log("I18n value", I18n.locale)
      I18n.locale = value;

      if (JSON.parse(isLoggedIn) === true) {
        console.log("userJson I18n value", I18n.locale)
        if (InCustomer == "True") {
          Actions.CustomerHome();
        } else {
          Actions.GuestHome();
        }

      }
      else {
        console.log("guest login")
        Actions.GuestLogin({ "langcode": value });
      }
    }
    else {

      console.log(" else condition _loadInitialState")
      // this.setState({
      //   langLoading: true,
      // });
      await apiCallWithUrl(APIConstants.LanguageAPI, 'POST', '', this.languageApiResponse);
    }
    //await apiCallWithUrl(APIConstants.LanguageAPI, 'POST', '', this.languageApiResponse);
  }
  languageApiResponse = async (response) => {

    console.log("Language response", response);

    // this.setState({ totalData: response.ResponseData, "selctedLanguage": response.ResponseData[0].NativeName, })
    await this.setState({ totalData: response.ResponseData, langLoading: false })

    console.log("totalResponse", response.ResponseData, this.state.langLoading);
    response.ResponseData.map((userData) => {
      console.log("laaaaa...", userData);
      var joined = this.state.myArray.concat(userData.NativeName);
      this.setState({ myArray: joined })
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: 'column'
  },
  loginview: {
    flex: 0.8,
    alignItems: 'center',
    //width: "40%"
  },
  button: {
    backgroundColor: Colors.App_Font,
    padding: 10,
    width: "95%",
    borderRadius: 5
  },
  btntext: {
    fontFamily: AppFont.Regular,
    color: Colors.Background_Color,
    fontSize: 15,
    textAlign: 'center',
    //fontFamily: AppFont.SemiBold,
  },
  copyrightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  copyrightText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    color: Colors.LightGray
  },
  versionText: {
    fontFamily: AppFont.Regular,
    fontSize: 13,
    paddingVertical: 10,
    color: '#dbdbdb'
  }
});

module.exports = Language;