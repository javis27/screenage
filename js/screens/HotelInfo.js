import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    AppRegistry,
    AsyncStorage,
    StatusBar,
    Platform,
    Dimensions,
    BackHandler,
    Alert
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Swiper from 'react-native-swiper';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


class HotelInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // userId: '',
            languageId: '',
            // hasroomNo: false,
            dynamicMenus: [],
            loadingMenu: false,
            bannerArray: [],
            IsFetchBanners: false,
            bannerheight: Dimensions.get('window').height / 4,
            bannerwidth: Dimensions.get('window').width,
        }
    }

    componentWillMount() {
        this._loadInitialState();
        // this.verifyCheckInDetails();

    }

    // componentWillUnmount() {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // }

    // handleBackButton = () => {
    //     console.log("screen name", Actions.currentScene);
    //     console.log("guest home props", this.props);
    //     if (Actions.currentScene == "GuestHome") {
    //         BackHandler.exitApp();
    //         return true;
    //     }
    // }


    // verifyCheckInDetails = async () => {
    //     await this.setState({ loadingMenu: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = (response) => {
    //     console.log("postCheckInResponse Language", response);
    //     if (Platform.OS === 'ios') {
    //         this.setState({ loadingMenu: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loadingMenu: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             console.log("postCheckInResponse Calendar else", response);
    //             this._loadInitialState();
    //         }
    //     }
    // }


    renderBannerImages(bannerData) {
        var base64Icon = '';
        if (Platform.OS == 'ios') {
            console.log("ios")
            // base64Icon = "data:image/png;base64," + bannerData.IOSImage;
            base64Icon = bannerData.IOSImagePath;
        } else {

            if (bannerData.AndroidImagePath.length != 0) {
                console.log("android")
                //base64Icon = "data:image/png;base64," + bannerData.AndroidImage;
                base64Icon = bannerData.AndroidImagePath;
            }
        }
        //  if (bannerData.AndroidImage != "") {
        return (
            <Image source={{ uri: base64Icon }} style={{ width: this.state.bannerwidth, height: this.state.bannerheight }} />
        )
        // }
        // else
        //     return null

    }

    render() {
        //const lid = this.props.userid;
        // console.log('hasroomNo', this.state.hasroomNo)
        return (
            <View style={styles.container}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                    barStyle='default'
                />
                <Loader loading={this.state.loadingMenu} />

                <View style={{ flex: 0.25, justifyContent: "flex-end" }}>
                    {
                        this.state.IsFetchBanners ?

                            <Swiper autoplay dotColor="rgba(255,255,255,.2)" activeDotColor="#FFFFFF" style={{ height: this.state.bannerheight - 10 }} showsButtons={false}>
                                {this.state.bannerArray.length > 0 ?
                                    this.state.bannerArray.map((item, index) => {
                                        return <View key={index} style={styles.slide1}>
                                            {this.renderBannerImages(item)}
                                            {/* <Image source={require('../../assets/images/1.jpg')} style={{ width: this.state.bannerwidth, height: this.state.bannerheight }} /> */}
                                        </View>
                                    })
                                    : null}
                            </Swiper>
                            :
                            <View><Image source={require('../../assets/images/logo.png')} style={{ width: this.state.bannerwidth, height: this.state.bannerheight, resizeMode:'contain' }} /></View>}

                </View>
                <View style={{ flex: 0.75, justifyContent: "flex-end" }}>
                    <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                        {this.state.dynamicMenus.length > 0 ?
                            this.state.dynamicMenus.map((menus, index) => {
                                return <TouchableOpacity key={index} activeOpacity={.5} onPress={this._onPressDynamicMenu.bind(this, menus.PageId)}>
                                    <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                        <View style={{ width: "12%", padding:20 }}>
                                            <Image style={{ width: 28, height: 28 }} source={require('../../assets/images/Green-Tick.png') } />
                                        </View>
                                        <View style={{ width: "70%" }}>
                                            <Text style={styles.mainmenu}>{menus.Title}</Text>
                                        </View>
                                        <View style={{ width: "18%", padding: 12 }}>
                                            <Cell accessory="DisclosureIndicator" />
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            })
                            :
                            null
                        }
                        {/* {!this.state.hasroomNo ?
                            <TouchableOpacity activeOpacity={.5} onPress={this._onGuestCheckInButton}>
                                <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.App_Font, borderBottomWidth: 0.5 }}>
                                    <View style={{ width: "82%" }}>
                                        <Text style={styles.mainmenu}>{I18n.t('checkin')}</Text>
                                    </View>
                                    <View style={{ width: "18%", padding: 12 }}>
                                        <Cell accessory="DisclosureIndicator" />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : null} */}

                    </ScrollView>
                </View>

            </View>
        )
    }

    // API call while clicking on dynamic Menus 
    _onPressDynamicMenu = async (pageId) => {
        console.log("id", pageId);
        this.setState({
            loadingMenu: true
        })
        let overalldetails = {
            "HotelId": this.state.HotelId,
            "PageId": pageId,
            "LanguageId": this.state.languageId
          }
          await apiCallWithUrl(APIConstants.GetSubMenuAPI, 'POST', overalldetails, (callback) => { this.subMenuAPIResponse(callback, pageId) });
        //await apiCallWithUrl(APIConstants.GetSubMenuAPI + "?LanguageId=" + this.state.languageId + "&&PageId=" + pageId, 'GET', "", (callback) => { this.subMenuAPIResponse(callback, pageId) });
    }

    // Response for dynamic menus api call

    subMenuAPIResponse = async (response, selectedPageId) => {
        console.log("res", response)
        console.log("selectedPageId", selectedPageId)
        if (response.ResponseData != null) {
            var submenuApiResponse = response.ResponseData;
            var submenuResponselength = submenuApiResponse.length;
            console.log('submenu response', submenuApiResponse.length)
            console.log('submenu response', selectedPageId)
            if (submenuResponselength == 0) {
                await this.setState({
                    loadingMenu: true
                })
                let overalldetails = {
                    "HotelId": this.state.HotelId,
                    "PageId": selectedPageId,
                    "LanguageId": this.state.languageId
                  }
                await apiCallWithUrl(APIConstants.GetPagesContentAPI, 'POST', overalldetails, (callback) => { this.menuContentAPIResponse(callback) });
                //await apiCallWithUrl(APIConstants.GetPagesContentAPI + "?LanguageId=" + this.state.languageId + "&&PageId=" + selectedPageId, 'GET', "", this.menuContentAPIResponse);
            } else {
                await this.setState({ loadingMenu: false })
                Actions.DynamicSubMenu({ subMenus: submenuApiResponse })
            }
        }else{
            let overalldetails = {
                "HotelId": this.state.HotelId,
                "PageId": selectedPageId,
                "LanguageId": this.state.languageId
              }
            await apiCallWithUrl(APIConstants.GetPagesContentAPI, 'POST', overalldetails, (callback) => { this.menuContentAPIResponse(callback) });
        }

    }

    menuContentAPIResponse = async (response) => {
        await this.setState({ loadingMenu: false })
        console.log("content", response);
        if (response.ResponseData != null) {
            console.log('content response', response.ResponseData[0])
            Actions.DynamicMenuContent({ menuContent: response.ResponseData[0] })
        }


    }

    // Page load API call
    _loadInitialState = async () => {
        this.setState({
            loadingMenu: true
        })
        var langId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        console.log('details', langId)
        // AsyncStorage.getItem(Keys.roomNo)
        //     .then((roomNumber) => {
        //         console.log('guesthome', roomNumber)
        //         this.setState({ hasroomNo: roomNumber ? true : false })
        //     });
        await this.setState({
            languageId: langId,
            HotelId: HotelId
        })

        console.log("state", this.state)
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId": this.state.languageId
          }
          await apiCallWithUrl(APIConstants.MobilePagesAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
        //await apiCallWithUrl(APIConstants.MobilePagesAPI + "?LanguageId=" + this.state.languageId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse);
        //await apiCallWithUrl(APIConstants.GetBannerAPI, 'GET', "", this.getBannerResponse)
        await apiCallWithUrl(APIConstants.GetBannerAPI, 'POST', overalldetails, (callback) => { this.getBannerResponse(callback) });

    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loadingMenu: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loadingMenu: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.menuApiResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    // Page load API call response

    menuApiResponse = async (response) => {
        if (response.ResponseData != null) {
            var menuApiResponse = response.ResponseData;
            console.log('dynamic response', menuApiResponse)
            console.log('dynamic response', menuApiResponse.Title)

            this.setState({
                dynamicMenus: menuApiResponse === null ? [] : menuApiResponse
            })
        }
         this.setState({ loadingMenu: false })
    }

    getBannerResponse = async (response) => {
        if (response.ResponseData.length > 0) {
            var bannersList = response.ResponseData;
            console.log('bannerArray', bannersList);
            await this.setState({ bannerArray: bannersList, IsFetchBanners: true, loadingMenu: false })
            console.log('bannerArray', this.state.bannerArray);
        }
        this.setState({ loadingMenu: false })
    }

    // _onAboutButton = () => {
    //     Actions.About();
    // }

    _onGuestCheckInButton = async () => {
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        console.log("userdetails", userdetails);
        if (userdetails == null) {
            // If user is not logged in then redirect to login 
            Actions.Login();
            // var userJson = JSON.parse(userdetails);
            // Actions.GuestCheckIn();
        }
        else {
            Actions.GuestCheckIn();

        }



    }

    // _onGuestMaps = () => {
    //     Actions.Maps();
    // }

    // _onGuestHomeButton = () => {
    //     Actions.GuestHome();
    // }
}

AppRegistry.registerComponent('myproject', () => Swiper);

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: Colors.Background_Color,
        // height: '100%'

        flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
    },
    mainmenu: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
        lineHeight: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },

})

module.exports = HotelInfo;