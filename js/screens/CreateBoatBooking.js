import React from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
  Image,
  TextInput,
  StatusBar,
  BackHandler,
  AppState,
  FlatList,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import Layout from '../constants/Layout';
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Keys from '../constants/Keys';
import Loader from '../components/Loader';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
import BottomSheet from 'react-native-bottomsheet';
import SpaBooking from '../components/SpaBooking';
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

class Language extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      langLoading: true,
      username: '',
      password: '',
      myArray: [],
      selctedLanguage: 'Choose your Language',
      langId: '',
      totalData:[]
    }
  }
  
  componentWillMount() {
    this._loadboatDetails();
  }
  async componentDidMount() {
    console.log("componentDidMount BoatBooking");
     await firebase.app();
     analytics().logScreenView({
        screen_name: 'BoatListScreen',
        screen_class: 'BoatListScreen'
    });
}
//   renderEmptyComponent = () => {
//     console.log("empty component", this.state.totalData)
//     if (this.state.totalData.length != 0) return null;
//     return (
//         // <View style={{flex:1, borderColor:'red', borderWidth: 1.0}}>
//         <Text style={{ fontSize: 18, alignSelf: 'center', fontFamily: AppFont.Regular, color: 'black', textAlign: 'center' }}>{I18n.t('nodata')}</Text>
//         // </View>
//     );
// };
  render() {
    const renderItem = ({ item }) => (
      <View  style={styles.boxWithShadow}>
                                    <TouchableOpacity onPress={this.getValues.bind(this, item)}>
                                      <View style={{flexDirection:"row"}}>
                                        <View style={{flex:0.25}}>
                                          <View style={{height:120}}>
                                        <Image source={{uri: item.ImagePath}} style={{ width: '100%', height: '100%', marginTop: 3}} />
                                        </View>
                                        </View>
                                        <View style={{flex:0.75}}>
                                        <View style={styles.Restaurant}>
                                            <View style={{ alignItems: "flex-start"}}>
                                                <Text style={styles.IconReqText}>{item.BoatActivityName}</Text>
                                                <Text numberOfLines={2} style={{ marginTop: 5, fontSize: 13, fontFamily: AppFont.Regular}}>{item.Description}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.Restaurant}>
                                            <View style={{ alignItems: "flex-start",  flexDirection:'row' }}>
                                              <View style={{flex:0.3}}><Text style={styles.IconReqText1}>{I18n.t('time_boat')}{item.Duration} {item.DurationUnitName}</Text></View>
                                              <View style={{flex:0.4}}><Text style={styles.IconReqText1}>{I18n.t('from_mur')} {item.AdultOfferPrice!=""?item.AdultPrice:item.AdultOfferPrice} {I18n.t('onwards')}</Text></View>
                                              <View style={{flex:0.3}}>

                                                  <View style={{ width: "90%", marginLeft: 15 }}>
                                                    <View style={{ alignItems: 'center' }}>
                                                        <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this.getValues.bind(this, item)}>
                                                            <Text style={styles.btntext}>{I18n.t('bookbtn')}</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                  </View>
                                              </View>
                                                
                                            </View>
                                        </View>
                                        </View>

                                      </View>
                                        

                                    </TouchableOpacity>
                                </View>
    );
    return (
      <View style={styles.container}>
        
        <StatusBar
          backgroundColor={Colors.App_Font}
          barStyle='light-content'
        />
        <View style={{ flex: 1}}>
          {this.state.totalData.length>0?
          <FlatList
            data={this.state.totalData}
            keyExtractor={(x, i) => i}
            style={{ marginBottom: 10 }}
            // ListEmptyComponent={this.renderEmptyComponent}
            renderItem={renderItem}
          />
          :
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ fontSize: 18, alignSelf: 'center', fontFamily: AppFont.Regular, color: 'black', textAlign: 'center' }}>{I18n.t('sorry_currently_not_Available')}</Text>
          </View>
  }
          </View>
        
      </View>
    );
  }
  _loadboatDetails = async () => {
    this.setState({ loading: true })
        const userdetails = await AsyncStorage.getItem(Keys.UserDetail);
        var userJson = JSON.parse(userdetails);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
        await this.setState({ userId: userJson.UserId });
        console.log("HotelId...",HotelId)
        const languageId = await AsyncStorage.getItem(Keys.langId);
    let overalldetails = {
      "HotelId": HotelId,
      "LanguageId": languageId
    }
    await apiCallWithUrl(APIConstants.GetBoatListAPI, 'POST', overalldetails, this.getboatApiResponse);
}
getValues = async (selectedDetails) => {
  console.log("selectedDetails",selectedDetails)
  // await AsyncStorage.setItem('HotelId', HotelId);
  Actions.CreateBoatBooking1({BoatDetails: selectedDetails });
  //Actions.pop();
}
getboatApiResponse = async (response) => {
  console.log("boatlist response", response);
if(response.Status)
  // this.setState({ totalData: response.ResponseData, "selctedLanguage": response.ResponseData[0].NativeName, })
  await this.setState({ totalData: response.ResponseData,langLoading: false })

}
}

const styles = StyleSheet.create({
  container: {
    flex: 1, backgroundColor: "#FFFFFF", flexDirection: 'column'
},
Restaurant: {
    marginTop: 5,
    paddingVertical: 8,
        paddingHorizontal: 15,
},
IconReqText: {
    color: Colors.Black,
    fontSize: 16,
    fontFamily: AppFont.Regular,
},
IconReqText1: {
  color: Colors.Black,
  fontSize: 14,
  fontFamily: AppFont.Regular,
},
DescriptionServiceList: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15
},
DescriptionServiceList1: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    backgroundColor: "#8CCED6"
},
ServiceText: {
    color: Colors.Black,
    fontSize: 13,
    // paddingHorizontal: 15,
    // paddingVertical: 7,
    fontFamily: AppFont.Regular,
},
ServiceText1: {
    color: 'white',
    fontSize: 15,
    // paddingHorizontal: 15,
    // paddingVertical: 7,
    fontFamily: AppFont.Regular,
},
containerAmount: {
    // backgroundColor: Colors.FullGray,
    // borderRadius: 5,
},
boxWithShadow: {
    borderColor: Colors.Background_Gray,
    borderWidth: 0.5,
    color: '#dbdbdb',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    shadowOffset: { width: 2, height: 2, },
    shadowColor: Colors.LightGray,
    shadowOpacity: 0.3,
    borderRadius: 5,
    marginTop:15,
    marginLeft:5,
    marginRight:5
},
button: {
  alignItems: 'center',
  backgroundColor: Colors.App_Font,
  padding: 10,
  width: "100%",
  marginLeft: 8,
  marginRight: 8,
  borderRadius: 5
},
btntext: {
  color: "#FFF",
  fontSize: 14,
  letterSpacing: 1,
  fontFamily: AppFont.Regular,
},
});

module.exports = Language;