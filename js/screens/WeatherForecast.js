import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	StatusBar
} from 'react-native';
import Colors from '../constants/Colors';
import { FlatList } from 'react-native';
import ForecastCard from '../components/ForecastCardItem';


export default class WeatherForecast extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			latitude: 0,
			longitude: 0,
			forecast: [],
			error: ''
		};
	}

	componentDidMount() {
		// Get the user's location
		this.getLocation();
	}

	getLocation() {
		console.log("weather get location")
		// Get the current position of the user
		navigator.geolocation.getCurrentPosition(
			(position) => {
				console.log("position",position)
				// this.setState(
				// 	(prevState) => ({
				// 	latitude: position.coords.latitude, 
				// 	longitude: position.coords.longitude
				// 	}), () => { this.getWeather(); }
				// );

				this.setState({
					latitude: position.coords.latitude,
					longitude: position.coords.longitude
				}, () => {
					this.getWeather();
				})
			},
			(error) => this.setState({ forecast: error.message }),
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
		);
	}

	getWeather() {

		// Construct the API url to call
		let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=fa287858b83e681690f92bec06d933a4';

		console.log("url get weather", url)

		// Call the API, and set the state of the weather forecast
		fetch(url)
			.then(response => response.json())
			.then(data => {
				this.setState((prevState, props) => ({
					forecast: data
				}));
			})
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					translucent={false}
					backgroundColor={Colors.App_Font}
					barStyle='light-content'
				/>
				<FlatList data={this.state.forecast.list} keyExtractor={item => item.dt_txt} renderItem={({ item }) => <ForecastCard detail={item} location={this.state.forecast.city.name} />} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		// flex: 1,
		//justifyContent: 'center',
		backgroundColor: Colors.Background_Color,
		height: '100%',
		paddingHorizontal: 15
	},
})

module.exports = WeatherForecast;