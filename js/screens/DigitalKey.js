import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    StatusBar,
    Platform,
    Dimensions,
    TouchableWithoutFeedback,
    Slider,
    BackHandler,
    RefreshControl,
    AsyncStorage,
    Alert
} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';

import { Actions } from 'react-native-router-flux';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const screenWidth = Dimensions.get('window').width;



class DigitalKey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            roomsNo:''
        };
        this.handleBackButton = this.handleBackButton.bind(this);
        console.log('key props', this.props)
        //const roomNo =  AsyncStorage.getItem(Keys.roomNo);
    }

    handleBackButton = () => {
        console.log("screen name", Actions.currentScene)
        if (Actions.currentScene == "TVChannels") {
            console.log('isfullscreen before', this.state.isFullScreen)
            if (this.state.isFullScreen) {
                this.onControlShrinkPress();
            } else {
                Actions.CustomerHome();
            }
            // Orientation.unlockAllOrientations();
            return true;
        }
    }

    componentWillMount() {
        this.onLoadLocation();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    onLoadLocation = async() => {
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        console.log("roomNo", roomNo);
        this.setState({roomsNo:roomNo})
    }

    render() {
        return (
            <View style={styles.container} onLayout={this._onLayout}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader loading={this.state.loading} />
                <View style={{flex:1,flexDirection:'column',padding: 10}}>
                    <View style={{flex:0.1}}><Text style={styles.text1}>{I18n.t('your_room')}</Text></View>
                    <View style={{flex:0.1,borderBottomWidth:0.5}}><Text style={styles.text2}>{I18n.t('room_no')} {this.state.roomsNo}</Text></View>
                    <View style={{flex:0.4}}></View>
                    <View style={{flex:0.4, alignItems:'center',alignContent:'center'}}><Image source={require('../../assets/images/unlock.png')}  style={{ width: '90%', height: '90%', resizeMode:'contain' }} /></View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    // container: {
    //     // flex: 1,
    //     //justifyContent: 'center',
    //     backgroundColor: Colors.Background_Color,
    //     height: '100%'
    // },
    container: {
        flex: 1,
        backgroundColor: '#f0f0f0'
    },
    text1:{
        color: Colors.Black,
        fontSize: 14,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
    },
    text2:{
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        textAlign: 'justify',
    }


})

module.exports = DigitalKey;

