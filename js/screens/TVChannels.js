import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    StatusBar,
    Platform,
    Dimensions,
    TouchableWithoutFeedback,
    Slider,
    BackHandler,
    RefreshControl,
    AsyncStorage,
    Alert
} from 'react-native';
import Video from 'react-native-video';
import Orientation from 'react-native-orientation';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';

import { Actions } from 'react-native-router-flux';
import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';

const screenWidth = Dimensions.get('window').width;

function formatTime(second) {
    let h = 0, i = 0, s = parseInt(second);
    if (s > 60) {
        i = parseInt(s / 60);
        s = parseInt(s % 60);
    }
    // Zero padding
    let zero = function (v) {
        return (v >> 0) < 10 ? "0" + v : v;
    };
    return [zero(h), zero(i), zero(s)].join(":");
}

class TVChannels extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            tvChannelsArray: [],
            videoUrl: '',
            headerText: '',
            headerLogo: '',
            IsFetchTVResponse: false,
            isRefreshing: false,

            videoWidth: screenWidth,
            videoHeight: screenWidth * 9 / 16, // Default 16:9 aspect ratio
            showVideoControl: false, // Whether to display the video control component
            isPlaying: false,        // Whether the video is playing
            currentTime: 0,        // The current time the video is playing
            duration: 0,           // Total length of video
            isFullScreen: false,     // Is it currently full screen?
            playFromBeginning: false, // Whether to play from the beginning     
        };
        this.handleBackButton = this.handleBackButton.bind(this);
        console.log('tv props', this.props)
    }

    handleBackButton = () => {
        console.log("screen name", Actions.currentScene)
        if (Actions.currentScene == "TVChannels") {
            console.log('isfullscreen before', this.state.isFullScreen)
            if (this.state.isFullScreen) {
                this.onControlShrinkPress();
            } else {
                Actions.CustomerHome();
            }
            // Orientation.unlockAllOrientations();
            return true;
        }
    }

    componentWillMount() {
        this._onLoadTVChannels();
        //this.verifyCheckInDetails();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    // verifyCheckInDetails = async () => {
    //     await this.setState({ loading: true });
    //     const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
    //     let overalldetails = {
    //         "CheckInId": checkInVal,
    //     }
    //     console.log("overalldetails", overalldetails);
    //     await apiCallWithUrl(APIConstants.GuestCheckInAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);
    // }

    // postCheckInDetailsResponse = async (response) => {
    //     console.log("postCheckInResponse Language", response);
    //     if (Platform.OS === 'ios') {
    //         this.setState({ loading: false }, () => {
    //             setTimeout(() => this.funcCheckInDetailsResponse(response), 1000);
    //         });
    //     } else {
    //         this.setState({
    //             loading: false,
    //         }, () => this.funcCheckInDetailsResponse(response));
    //     }
    // }

    // funcCheckInDetailsResponse = async (response) => {
    //     var postCheckInDetailResponse = response.ResponseData;
    //     if (response.IsException == "True") {
    //         return Alert.alert(response.ExceptionMessage);
    //     }

    //     if (postCheckInDetailResponse !== null) {
    //         console.log("postCheckInResponse", postCheckInDetailResponse.InCustomer);
    //         if (postCheckInDetailResponse.InCustomer == "False") {
    //             console.log("postCheckInResponse Language", postCheckInDetailResponse.InCustomer);
    //             Alert.alert(I18n.t('alertnotcheckin'));
    //             AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
    //             AsyncStorage.removeItem(Keys.UserDetail);
    //             AsyncStorage.removeItem(Keys.roomNo);
    //             AsyncStorage.removeItem(Keys.inCustomer);
    //             AsyncStorage.removeItem(Keys.checkInId);
    //             Actions.GuestLogin();
    //         } else {
    //             console.log("postCheckInResponse Calendar else", response);
    //             this._onLoadTVChannels();
    //         }
    //     }
    // }


    formatDate = () => {
        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        var today = new Date();

        return today.toLocaleDateString("en-US", options);

        console.log(today.toLocaleDateString("en-US")); // 9/17/2016
        console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016
        console.log(today.toLocaleDateString("hi-IN", options));
    }

    renderImage(tvChannelData) {
        //  console.log("came here", tvChannelData.ChannelLogoPath)
        // const encodedData = logo;
        var base64tvChannelIcon = '';
        if (tvChannelData.ChannelLogoPath != "") {
            // base64tvChannelIcon = "data:image/png;base64," + tvChannelData.ChannelLogo;
            base64tvChannelIcon = tvChannelData.ChannelLogoPath;
        }
        // if (Platform.OS == 'ios') {
        //     console.log("ios")
        //     base64Icon = "data:image/png;base64," + tvChannelData.IOSImage;
        // } else {
        //     if (bannerData.AndroidImage.length != 0) {
        //         console.log("android")
        //         base64Icon = "data:image/png;base64," + tvChannelData.AndroidImage;
        //     }
        // }
        if (base64tvChannelIcon != "") {
            return (
                <TouchableOpacity activeOpacity={.5} onPress={() => this._onLiveTv(tvChannelData)}>
                    {/* <Image source={require('../../assets/images/Live_1.png')} style={{ width: 85, height: 38 }} /> */}
                    <Image source={{ uri: base64tvChannelIcon }} style={{ width: 50, height: 90 , resizeMode:'contain'}} />
                </TouchableOpacity>
            )
        }
        else
            return null

    }
    renderChannelHeaderImage() {
        console.log("came here", this.state.headerLogo)
        // const encodedData = logo;
        var tvChannelIconUrl = '';
        if (this.state.headerLogo != "") {
            tvChannelIconUrl = this.state.headerLogo;
        }
        if (tvChannelIconUrl != "") {
            return (
                <Image source={{ uri: tvChannelIconUrl }} style={{ width: 50, height: 50, resizeMode:'contain' }} />
            )
        }
        else
            return null
    }
    renderChannel(channelData) {
        console.log("item", channelData)
        console.log("came here", channelData.ChannelName)
        if (channelData.ChannelName != "") {
            return (
                <TouchableOpacity activeOpacity={.5} onPress={() => this._onLiveTv(channelData)}>
                    <Text>{channelData.ChannelName}</Text>
                    {/* <Image source={require('../../assets/images/Live_1.png')} style={{ width: 85, height: 38 }} /> */}
                </TouchableOpacity>
            )
        }
        else
            return null

    }

    _onLiveTv = async (channelItem) => {
        console.log("onclick", channelItem)
        await this.setState({ videoUrl: channelItem.ChannelURL, headerText: channelItem.ChannelName, headerLogo: channelItem.ChannelLogoPath });
        console.log("onclick", this.state.videoUrl, this.state.headerText)
    }


    render() {
        return (
            <View style={styles.container} onLayout={this._onLayout}>
                <StatusBar
                    translucent={false}
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader loading={this.state.loading} />
                <View>
                    {this.state.IsFetchTVResponse ?
                        Platform.OS === 'android' ?

                            <View style={{ width: this.state.videoWidth, height: this.state.videoHeight, backgroundColor: '#000000' }}>
                                <Video
                                    source={{
                                        uri: this.state.videoUrl,
                                        headers: {
                                            Authorization: 'Base64 YWRtaW46YWRtaW5AMzIx',
                                        },
                                        // type: 'm3u8'
                                    }}
                                    ref={(ref) => this.videoPlayer = ref}
                                    source={{ uri: this.state.videoUrl, headers: { Authorization: 'Base64 YWRtaW46YWRtaW5AMzIx' } }}
                                    rate={1.0}
                                    volume={1.0}
                                    muted={false}
                                    paused={!this.state.isPlaying}
                                    resizeMode={'contain'}
                                    playWhenInactive={false}
                                    playInBackground={false}
                                    ignoreSilentSwitch={'ignore'}
                                    progressUpdateInterval={250.0}
                                    onLoadStart={this._onLoadStart}
                                    onLoad={this._onLoaded}
                                    onProgress={this._onProgressChanged}
                                    onEnd={this._onPlayEnd}
                                    onError={this._onPlayError}
                                    onBuffer={this._onBuffering}
                                    style={{ width: this.state.videoWidth, height: this.state.videoHeight }}
                                />
                                {/* {
                                    this.state.showVideoControl ?
                                <TouchableWithoutFeedback onPress={() => { this.handleBackButton() }}>
                                    <View
                                        style={{
                                            position: 'absolute',
                                            top: 0,
                                            // left: 0,
                                            width: this.state.videoWidth,
                                            height: this.state.videoHeight,
                                           // backgroundColor: this.state.isPlaying ? 'transparent' : 'rgba(0, 0, 0, 0.2)',
                                            alignItems: 'flex-start',
                                            justifyContent: 'flex-start',
                                            // borderColor:'red',
                                            // borderWidth:1
                                        }}>
                                                <TouchableWithoutFeedback onPress={() => {this.handleBackButton()}}>
                                                    <Image
                                                        style={[styles.playButton, { color: 'white', borderColor:'green',borderWidth:1 }]}
                                                        //source={{ uri: 'close' }}
                                                        source={require('../../assets/images/icon_video_play.png')}
                                                    />
                                                </TouchableWithoutFeedback>
                                    </View>
                                </TouchableWithoutFeedback>
                                : null
                            } */}
                                <TouchableWithoutFeedback onPress={() => { this.hideControl() }}>
                                    <View
                                        style={{
                                            position: 'absolute',
                                            top: 0,
                                            left: 0,
                                            width: this.state.videoWidth,
                                            height: this.state.videoHeight,
                                            backgroundColor: this.state.isPlaying ? 'transparent' : 'rgba(0, 0, 0, 0.2)',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                        {
                                            this.state.isPlaying ? null :
                                                <TouchableWithoutFeedback onPress={() => { this.onPressPlayButton() }}>
                                                    <Image
                                                        style={styles.playButton}
                                                        source={require('../../assets/images/icon_video_play.png')}
                                                    />
                                                </TouchableWithoutFeedback>
                                        }
                                    </View>
                                </TouchableWithoutFeedback>

                                {
                                    this.state.showVideoControl ?
                                        <View style={[styles.control, { width: this.state.videoWidth }]}>
                                            <TouchableOpacity activeOpacity={0.3} onPress={() => { this.handleBackButton() }}>
                                                <Image
                                                    style={styles.playControl}
                                                    source={{ uri: 'arrow' }}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlPlayPress() }}>
                                                <Image
                                                    style={styles.playControl}
                                                    source={this.state.isPlaying ? require('../../assets/images/icon_control_pause.png') : require('../../assets/images/icon_control_play.png')}
                                                />
                                            </TouchableOpacity>
                                            <Text style={styles.time}>{formatTime(this.state.currentTime)}</Text>
                                            <Slider
                                                style={{ flex: 1 }}
                                                maximumTrackTintColor={'#999999'}
                                                minimumTrackTintColor={'#00c06d'}
                                                thumbImage={require('../../assets/images/icon_control_slider.png')}
                                                value={this.state.currentTime}
                                                minimumValue={0}
                                                maximumValue={this.state.duration}
                                                onValueChange={(currentTime) => { this.onSliderValueChanged(currentTime) }}
                                            />
                                            <Text style={styles.time}>{formatTime(this.state.duration)}</Text>
                                            <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlShrinkPress() }}>
                                                <Image
                                                    style={styles.shrinkControl}
                                                    source={this.state.isFullScreen ? require('../../assets/images/icon_control_shrink_screen.png') : require('../../assets/images/icon_control_full_screen.png')}
                                                />
                                            </TouchableOpacity>
                                        </View> : null
                                }
                            </View>
                            :
                            <View style={{ width: this.state.videoWidth, height: this.state.videoHeight, backgroundColor: '#000000' }}>
                                <Video source={{
                                    uri: this.state.videoUrl, headers: { Authorization: 'Base64 YWRtaW46YWRtaW5AMzIx' }
                                }}
                                    ref={(ref) => {
                                        this.player = ref
                                    }}
                                    fullscreen={true}
                                    controls={true}
                                    // Store reference
                                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                                    onError={this.videoError}               // Callback when video cannot be loaded
                                    resizeMode="cover"
                                    style={{ width: this.state.videoWidth, height: this.state.videoHeight }} />
                            </View>

                        : null}

                </View>

                <View style={{ backgroundColor: Colors.ShadowGray, height: 'auto', flexDirection: 'row', justifyContent: 'center', padding: 15, marginBottom: 15 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ alignItems: "flex-start", flex: 0.8, marginTop: 12 }}>
                            <Text style={styles.IconReqText}>{this.state.headerText}</Text>
                            <Text style={styles.IconReq}>{this.formatDate()}</Text>
                        </View>
                        <View style={{ alignItems: "center", flex: 0.2, backgroundColor: '#FFFFFF' }}>
                            <View style={{ padding: 7, borderRadius: 3, alignItems: 'center' }}>
                                {/* <Image source={require('../../assets/images/Live_2.png')} style={{ width: 25, height: 25 }} /> */}
                                {this.renderChannelHeaderImage()}
                            </View>
                        </View>
                    </View>
                </View>

                <ScrollView contentContainerStyle={{ alignItems: 'center', paddingHorizontal: 5, paddingVertical: 10, position: 'absolute' }} refreshControl={
                    <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={this._onRefreshTVChannels}
                    />
                }>

                    <View style={{ flexDirection: "row", width: "100%", flex: 1, flexWrap: 'wrap' }}>
                        {this.state.tvChannelsArray.length > 1 ?
                            this.state.tvChannelsArray.map((item, index) => {
                                return <View style={{ width: "33.333333333333333%", height: "auto" }}>
                                    <View key={index} style={{
                                        borderColor: Colors.FullGray, borderWidth: 1, alignItems: 'center', paddingVertical: 10, paddingHorizontal: 10, margin: 3, borderRadius: 5, justifyContent: 'center', marginRight:20,
                                        color: '#dbdbdb',
                                        backgroundColor: Colors.Background_Color,
                                        borderRadius: 3,
                                        ...Platform.select({
                                            ios: {
                                                shadowOffset: { width: 2, height: 2, },
                                                shadowColor: Colors.LightGray,
                                                shadowOpacity: 0.3,
                                            },
                                            android: {
                                                elevation: 2
                                            },
                                        }),
                                    }}>
                                        {this.renderImage(item)}
                                        {/* {this.renderChannel(item)} */}

                                    </View>
                                </View>
                            })
                            : null}
                    </View>

                </ScrollView>
            </View>
        )
    }

    _onRefreshTVChannels = () => {
        this.setState({ isRefreshing: true })
        this._onLoadTVChannels();
    }


    /// -------Video Component callback event-------

    _onLoadStart = () => {
        console.log('Video starts loading');
    };

    _onBuffering = () => {
        console.log('Video buffering...')
    };

    _onLoaded = (data) => {
        console.log('Video loading completed');
        this.setState({
            duration: data.duration,
        });
    };

    _onProgressChanged = (data) => {
        console.log('Video progress update');
        if (this.state.isPlaying) {
            this.setState({
                currentTime: data.currentTime,
            })
        }
    };

    _onPlayEnd = () => {
        console.log('End of video playback');
        this.setState({
            currentTime: 0,
            isPlaying: false,
            playFromBeginning: true
        });
    };

    _onPlayError = (error) => {
        console.log('error', error)
        console.log('Video playback failed');
    };

    ///-------Control click event-------

    /// Control the display and hide of the player toolbar
    hideControl() {
        if (this.state.showVideoControl) {
            this.setState({
                showVideoControl: false,
            })
        } else {
            this.setState(
                {
                    showVideoControl: true,
                },
                // Automatically hide the toolbar after 5 seconds
                () => {
                    setTimeout(
                        () => {
                            this.setState({
                                showVideoControl: false
                            })
                        }, 5000
                    )
                }
            )
        }
    }

    /// Clicked the play button in the middle of the player
    onPressPlayButton = async () => {
        let isPlay = !this.state.isPlaying;
        console.log('isPlay', isPlay, this.state.playFromBeginning);
        this.setState({
            isPlaying: isPlay,
            showVideoCover: false
        });
        if (this.state.playFromBeginning) {
            console.log("playFromBeginning")
            this.videoPlayer.seek(0);
            this.setState({
                playFromBeginning: false,
            })
        }
    }

    /// Clicked the play button on the toolbar
    onControlPlayPress() {
        this.onPressPlayButton();
    }

    /// Clicked the full screen button on the toolbar
    onControlShrinkPress = async () => {
        console.log('isfullscreen before', this.state.isFullScreen)
        await this.setState({ isFullScreen: !this.state.isFullScreen })
        console.log('isfullscreen after', this.state.isFullScreen)
        if (this.state.isFullScreen) {
            Orientation.lockToLandscape();
            StatusBar.setHidden(true);
            //  Actions.refresh({key: 'TVChannels', hideNavBar: false});
        } else {
            Orientation.lockToPortrait();
            StatusBar.setHidden(false);
            // Actions.refresh({key: 'TVChannels', hideNavBar: true});
        }
    }

    /// Progress bar value change
    onSliderValueChanged(currentTime) {
        console.log("onSliderValueChanged", currentTime)
        this.videoPlayer.seek(currentTime);
        if (this.state.isPlaying) {
            this.setState({
                currentTime: currentTime
            })
        } else {
            this.setState({
                currentTime: currentTime,
                isPlaying: true,
                showVideoCover: false
            })
        }
    }

    /// When the screen is rotated, the width and height will change. It can be processed in the onLayout method, and the width and height can be changed more timely than the monitor screen rotation.
    _onLayout = (event) => {
        //Get the width and height of the root view
        let { width, height } = event.nativeEvent.layout;
        console.log('Width obtained by onLayout：' + width);
        console.log('The height obtained by onLayout :' + height);

        // Generally, the device is wider than the height under the horizontal screen. Here you can use this to judge the horizontal and vertical screen.
        let isLandscape = (width > height);
        if (isLandscape) {
            this.setState({
                videoWidth: width,
                videoHeight: height,
                // isFullScreen: true,
            })
        } else {
            this.setState({
                videoWidth: width,
                videoHeight: width * 9 / 16,
                // isFullScreen: false,
            })
        }
        //   Orientation.unlockAllOrientations();
    };

    _onLoadTVChannels = async () => {
        this.setState({
            loading: true,
        });
        const langId = await AsyncStorage.getItem(Keys.langId);
        const checkInVal = await AsyncStorage.getItem(Keys.checkInId);
        var HotelId = await AsyncStorage.getItem('HotelId');
       
        let overalldetails = {
            "HotelId": HotelId,
            "LanguageId": langId
          }
          await apiCallWithUrl(APIConstants.GetTvChannelAPI, 'POST', overalldetails, this.postCheckInDetailsResponse);


        //apiCallWithUrl(APIConstants.GetTvChannelAPI + "?LanguageId=" + langId + "&&CheckInId=" + checkInVal, 'GET', "", this.postCheckInDetailsResponse);
    }

    postCheckInDetailsResponse = async (response) => {
        if (response.IsException == "True") {
            return Alert.alert(response.ExceptionMessage);
        }

        console.log("postCheckInResponse", response.InCustomer);
        if (response.InCustomer == "False") {
            console.log("postCheckInResponse Language", response.InCustomer);
            if (Platform.OS === 'ios') {
                console.log("ios postCheckInResponse")
                this.setState({ loading: false }, () => {
                    setTimeout(() => this.funcCheckOutResponse(response), 1000);
                });
            } else {
                console.log("android postCheckInResponse")
                this.setState({
                    loading: false,
                }, () => this.funcCheckOutResponse(response));
            }

        } else {
            this.getTvChannelsResponse(response);
        }
    }

    funcCheckOutResponse = async (response) => {
        Alert.alert(I18n.t('alertnotcheckin'));
        AsyncStorage.setItem(Keys.isLogin, JSON.stringify(false));
        AsyncStorage.removeItem(Keys.UserDetail);
        AsyncStorage.removeItem(Keys.roomNo);
        AsyncStorage.removeItem(Keys.inCustomer);
        AsyncStorage.removeItem(Keys.checkInId);
        Actions.GuestLogin();
    }

    getTvChannelsResponse = async (response) => {
        console.log("tv", response.ResponseData)
        if (response.ResponseData != null) {
            var tvChannelResponse = response.ResponseData;
            console.log('tvchannel response', tvChannelResponse)

            await this.setState({
                tvChannelsArray: tvChannelResponse === null ? '' : tvChannelResponse,
                headerText: tvChannelResponse[0].ChannelName,
                headerLogo: tvChannelResponse[0].ChannelLogoPath,
                videoUrl: tvChannelResponse[0].ChannelURL,
                IsFetchTVResponse: true,
                loading: false,
                isRefreshing: false
            })
            this.setState({ loading: false })

            console.log('state tvchannel', this.state.tvChannelsArray)
        }

        // var tvChannelResponse = response.ResponseData;
        // console.log('tvchannel response', tvChannelResponse)

        // await this.setState({ loading: false }, () => setTimeout(() => {
        //     this.setState({
        //         tvChannelsArray: tvChannelResponse === null ? '' : tvChannelResponse
        //     });
        //   }, 200))

        //   console.log('state tvchannel', this.state.tvChannelsArray)

    }
}

const styles = StyleSheet.create({
    // container: {
    //     // flex: 1,
    //     //justifyContent: 'center',
    //     backgroundColor: Colors.Background_Color,
    //     height: '100%'
    // },
    container: {
        flex: 1,
        backgroundColor: '#f0f0f0'
    },
    playButton: {
        width: 50,
        height: 50,
    },
    playControl: {
        width: 24,
        height: 24,
        marginLeft: 15,
    },
    shrinkControl: {
        width: 15,
        height: 15,
        marginRight: 15,
    },
    time: {
        fontSize: 12,
        color: 'white',
        marginLeft: 10,
        marginRight: 10
    },
    control: {
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        position: 'absolute',
        bottom: 0,
        left: 0
    },
    backgroundVideo: {
        position: 'relative',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height: 250,
        alignSelf: "stretch",
    },
    // backgroundVideo: {
    //     position: 'relative',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    //     height: 250,
    //     alignSelf: "stretch",
    // },
    IconReqText: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
    },
    IconReq: {
        color: Colors.DarkGray,
        fontSize: 11,
        fontFamily: AppFont.Regular,
        marginTop: 5
    },


})

module.exports = TVChannels;



{/* <Video source={{uri: 'https://www.youtube.com/watch?v=1z7Kg2bONVk' }}
                        ref={(ref) => {
                            this.player = ref
                        }}    
                        onBuffer={this.onBuffer}           
                        onError={this.videoError}
                        resizeMode="cover"     
                        style={styles.backgroundVideo} /> */}


{/* <Video source={{
    uri: this.state.videoUrl,
    headers: {
        Authorization: 'Base64 YWRtaW46YWRtaW5AMzIx',
    }
}}
    ref={(ref) => {
        this.player = ref
    }}
    fullscreen={true}
    controls={true}
    // Store reference
    onBuffer={this.onBuffer}                // Callback when remote video is buffering
    onError={this.videoError}               // Callback when video cannot be loaded
    resizeMode="cover"
    style={styles.backgroundVideo} /> */}