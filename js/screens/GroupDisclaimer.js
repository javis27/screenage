import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Image

} from 'react-native';

import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import I18n from '../constants/i18n';
import { Actions } from 'react-native-router-flux';

class GroupDisclaimer extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ paddingHorizontal: 0 }}>
                    <View style={styles.Restaurant}>
                        <View style={{ alignItems: "flex-start", flex: 1 }}>
                            <Text style={styles.IconReqText}>Group Disclaimer</Text>
                            <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, marginBottom: 10, textAlign: 'justify',fontFamily:AppFont.Regular }}>
                            Preskil Island resort inform the participants that individual Bike Tour can be a risky activity and cause injury or damage as long as the participants do not take the necessary precautions or  if participants do not follow the traffic laws.​
                            </Text>
                            <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, marginBottom: 10, textAlign: 'justify',fontFamily:AppFont.Regular }}>
                            I agree not to drink alcohol or use drugs before or during  bike Tours, activities or events.​
                            </Text>
                            <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, marginBottom: 10, textAlign: 'justify',fontFamily:AppFont.Regular }}>
                            All participants must wear an helmet and should not use iPod, musical devices or telephones while riding.​
                            </Text>
                            <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, marginBottom: 10, textAlign: 'justify',fontFamily:AppFont.Regular }}>
                            All participants must follow the basic rules for safe cycling: When on the road – ride to the left, single file. Stop at all stop signs and red signals. Protect and warn one another about road hazards and traffic. ​
                            </Text>
                            <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, marginBottom: 10, textAlign: 'justify',fontFamily:AppFont.Regular }}>
                            I declare that I do not have any fitness, medical or physical conditions that would affect my individual Bike Tour. I authorise the hotel to arrange medical or hospital treatment if necessary and I agree to pay for all associated costs.​
                            </Text>
                            <Text style={{ color: Colors.LightGray, marginTop: 5, fontSize: 13, marginBottom: 10, textAlign: 'justify',fontFamily:AppFont.Regular }}>
                            Participants under the age of fifteen MUST be accompanied by a responsible party.
                            </Text>

                        </View>
                    </View>

                    <View style={{ backgroundColor: 'white', margin: 10, flex: 0.5, flexDirection: 'row', alignItems: 'center' }}>                    
                        {/* <Image style={{
                        resizeMode: 'contain',
                        width: 16,
                        height: 16,
                        marginHorizontal: 5,
                        }} source={require('../../assets/images/check-box-empty.png')} /> */}

                        <Text style={styles.AgreeText}> * I agree terms and conditions apply here </Text>
                    </View>

                    {/* <View style={{ backgroundColor: 'white', margin: 10, flex: 0.5, flexDirection: 'row', alignItems: 'center' }}>                    
                        <Image style={{
                        resizeMode: 'contain',
                        width: 16,
                        height: 16,
                        marginHorizontal: 5,
                        }} source={require('../../assets/images/check.png')} />

                        <Text style={styles.AgreeText}> * I agree terms and conditions apply here </Text>
                    </View> */}

                    <View style={styles.DescriptionServiceList}>
                        <View style={{ flex: 0.5, paddingTop: 7, paddingBottom: 15 }}>
                            <View style={{ flex: 1, alignItems: 'center', fontFamily:AppFont.Regular }}>
                                <View style={styles.ServiceTextActive}><Text style={{ fontFamily:AppFont.Regular }}>Agree</Text></View>
                            </View>
                        </View>
                        <View style={{ flex: 0.5, paddingTop: 7, paddingBottom: 15 }}>
                            <View style={{ flex: 1, alignItems: 'center', fontFamily:AppFont.Regular }}>
                                <View style={styles.ServiceText}><Text style={{ fontFamily:AppFont.Regular }}>Cancel</Text></View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        //justifyContent: 'center',
        backgroundColor: Colors.Background_Color,
        height: '100%'
    },
    Restaurant: {
        flex: 1,
        flexDirection: "row",
        paddingVertical: 8,
        paddingHorizontal: 15,
        marginTop: 10
    },
    IconReqText: {
        color: Colors.Black,
        fontSize: 16,
        fontFamily: AppFont.Regular,
        marginBottom: 10
    },
    DescriptionServiceList: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 15,
        marginTop: 10,
        marginBottom: 25
    },
    AgreeText: {
        color: Colors.Black,
        fontSize: 15,
        fontFamily:AppFont.Regular
    },
    ServiceText: {
        color: Colors.Black,
        fontSize: 15,
        backgroundColor: Colors.FullGray,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        width: '75%',
        fontFamily:AppFont.Regular
    },
    ServiceTextActive: {
        color: "#FFFFFF",
        fontSize: 15,
        backgroundColor: Colors.App_Font,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        width: '75%',
        fontFamily:AppFont.Regular
    },

})

module.exports = GroupDisclaimer;