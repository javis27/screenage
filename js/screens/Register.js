import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    ScrollView,
    Image,
    AsyncStorage,
    Alert,
    StatusBar,
    ActivityIndicator
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import AppFont from '../constants/AppFont';
import { Cell, Section, TableView } from "react-native-tableview-simple";
import Locale from '../constants/Locale';
import I18n from '../constants/i18n';
import Loader from '../components/Loader';
import Keys from '../constants/Keys';
import Status from '../constants/Status';
import Icon from 'react-native-vector-icons/Fontisto';

import APIConstants from '../api/APIConstants';
import { apiCallWithUrl } from '../api/APIHandler';
// var { FBLoginManager } = require('react-native-facebook-login');
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import FCM from 'react-native-fcm';

class Register extends Component {
    constructor(props) {
        super(props);
        // GoogleSignin.configure({
        //     //It is mandatory to call this method before attempting to call signIn()
        //     scopes: ['https://www.googleapis.com/auth/drive.readonly'],
        //     // Repleace with your webClientId generated from Firebase console
        //     webClientId:
        //       '913406409388-j545s60mkkhqs2dju92rhe2613cb9j21.apps.googleusercontent.com',
        //   });
         //GoogleSignin.configure();
        //  GoogleSignin.configure({
        //     webClientId: '147560102777-6826febjf525eiifnn9f499gngft1s93.apps.googleusercontent.com', // client ID of type WEB for your server(needed to verify user ID and offline access)
        //     offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        //     forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
        //     accountName: '', // [Android] specifies an account name on the device that should be used
        //        });
        

        this.state = {
            username: '',
            email: '',
            mobile: '',
            pwd: '',
            cnfpwd: '',
            loading: false,
            deviceToken: '',
            isSelectedTerms: false,
        }
    }

    componentWillMount() {
        FCM.getFCMToken().then(token => {
            console.log('devicetoken.....', token);
            this.setState({ deviceToken: token })
        });
    }
    updateInputEmail(strEmail) {
        // console.log("email", email)
        // console.log("email length", email.length)
        this.setState({ email: strEmail.trim() });
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white" }}>
                <StatusBar
                    backgroundColor={Colors.App_Font}
                    barStyle='light-content'
                />
                <Loader loading={this.state.loading} />
                {/* <ActivityIndicator
                    color={Colors.App_Font}
                    animating={this.state.loading} /> */}
                <ScrollView>
                    <TableView>
                        <Section sectionPaddingTop={0} sectionPaddingBottom={100} sectionTintColor='white' hideSeparator={true} separatorTintColor='white'>
                            <Cell title="Username" placeholder="Username" id="1" cellContentView={
                                <View style={{ flex: 1, paddingTop: 10 }}>
                                    <View style={{ width: "90%", }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('fullname')}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={(text) => this.setState({ username: text })}
                                                value={this.state.username}
                                                placeholder={I18n.t('yourname')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Email Id" hideSeparator={true} placeholder="Email Id" id="2" cellContentView={
                                <View style={{ flex: 1 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('emailid')}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={this.updateInputEmail.bind(this)}
                                                //onChangeText={(text) => this.setState({ email: text })}
                                                // onBlur={(event) => this.validateEmail(event.nativeEvent.text)}
                                                value={this.state.email}
                                                placeholder={I18n.t('enteremailid')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Mobile Number" hideSeparator={true} placeholder="Mobile Number" id="3" cellContentView={
                                <View style={{ flex: 1 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('mobilenumber')}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                keyboardType='numeric'
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={(text) => this.setState({ mobile: text.trim() })}
                                                value={this.state.mobile}
                                                placeholder={I18n.t('entermobileno')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Password" hideSeparator={true} placeholder="Password" id="4" cellContentView={
                                <View style={{ flex: 1 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('password')}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                secureTextEntry
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={(text) => this.setState({ pwd: text })}
                                                value={this.state.pwd}
                                                placeholder={I18n.t('enterpasswordplaceholder')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Confirm Password" hideSeparator={true} placeholder="Confirm Password" id="5" cellContentView={
                                <View style={{ flex: 1 }}>
                                    <View style={{ width: "90%" }}>
                                        <Text
                                            allowFontScaling
                                            numberOfLines={1}
                                            style={styles.label}>
                                            {I18n.t('confirmpassword')}
                                        </Text>
                                        <View style={styles.cellrow}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCapitalize='none'
                                                secureTextEntry
                                                placeholderTextColor={Colors.PlaceholderText}
                                                style={styles.textInput}
                                                onChangeText={(text) => this.setState({ cnfpwd: text })}
                                                value={this.state.cnfpwd}
                                                placeholder={I18n.t('enterconfirmpwdplaceholder')} />
                                        </View>
                                    </View>
                                    <View style={styles.textBorderLine}></View>
                                </View>
                            } />
                            <Cell title="Disclaimer" hideSeparator={true} placeholder="Disclaimer" id="6" cellContentView={
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingVertical: 20 }}>
                                    {/* <View style={{ flex: 1, alignItems: "flex-start" }}> */}
                                    <TouchableOpacity onPress={() => this.onPressAgree()} style={{ paddingHorizontal: 10 }}>
                                        {/* <Image style={{
                                            resizeMode: 'contain',
                                            width: 16,
                                            height: 16,
                                            marginHorizontal: 5,
                                        }} source={{ uri: this.state.isSelectedTerms ? 'check_on' : 'check_off' }} /> */}
                                        <Icon name={this.state.isSelectedTerms ? "checkbox-active" : "checkbox-passive"} size={18} color={Colors.Black} />
                                    </TouchableOpacity>
                                    <Text style={styles.AgreeText}>{I18n.t('iagreeto')}</Text>
                                    <TouchableOpacity activeOpacity={.5} style={{ paddingHorizontal: 5 }} onPress={() => this._onPressRegisterDiscliamer(Status.disclaimerTypes.loginDisclaimer.typeId)}>
                                        <Text style={{ fontSize: 12, fontFamily: AppFont.Regular, textDecorationLine: 'underline', color: Colors.App_Font }}>{I18n.t('termsandconditions')}</Text>
                                    </TouchableOpacity>
                                    {/* </View> */}
                                </View>
                            } />
                            <Cell title="Button" hideSeparator={true} placeholder="Button" id="7" cellContentView={
                                <View style={{ flex: 1, height: 80, justifyContent: "center" }}>
                                    <View style={{ width: "90%", marginLeft: 20 }}>
                                        <View style={styles.loginview}>
                                            <TouchableOpacity activeOpacity={.5} style={styles.button} onPress={this._onPressButton}>
                                                <Text style={styles.btntext}> {I18n.t('registerbtn')} </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            } />
                            <Cell title="Login" hideSeparator={true} placeholder="Login" id="8" cellContentView={
                                <View style={{ flex: 1, height: 30, justifyContent: "center", alignItems: "center" }}>
                                    <TouchableOpacity activeOpacity={.5} onPress={this._onPressLogin}>
                                        <Text style={{ fontSize: 15, fontFamily: AppFont.Regular }}>{I18n.t('alreadyhaveaccount')}</Text>
                                    </TouchableOpacity>
                                </View>
                            } />
                            <Cell title="Login" hideSeparator={true} placeholder="Login" id="9" cellContentView={
                                <View style={{ flex: 1, height: 20, justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ fontSize: 15, fontFamily: AppFont.Regular }}>--- {I18n.t('or')} ---</Text>
                                </View>
                            } />
                            <Cell title="Login" hideSeparator={true} placeholder="Login" id="10" cellContentView={
                                <View style={{ flex: 1, flexDirection: "row", height: 50, justifyContent: "center", alignItems: "center" }}>
                                    <View style={{ flex: 0.5, alignItems: "flex-end" }}>
                                        <TouchableOpacity activeOpacity={.5} onPress={this.onFbAccess}>
                                            <Image source={require('../../assets/images/fb.png')} style={{ width: 50, height: 50, paddingRight: 10 }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 0.5, alignItems: "flex-start", marginLeft: 10, marginTop: 3 }}>
                                        <TouchableOpacity activeOpacity={.5} onPress={this.googleSignIn}>
                                            <Image source={require('../../assets/images/google.png')} style={{ width: 50, height: 50 }} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            } />
                        </Section>
                    </TableView>
                </ScrollView>
            </View>
        )
    }

    onPressAgree = () => {
        console.log("isSelected", this.state.isSelectedTerms)
        this.setState({ isSelectedTerms: !this.state.isSelectedTerms })
    }

    _onPressRegisterDiscliamer = async (typeId) => {
        console.log("typeId", typeId)
        Actions.TermsandConditions({
            disclaimerType: typeId,
            // callbackBikeDisclaimerFunc: this.getcallbackLoginDisclaimerFunc
        });
    }

    // getcallbackLoginDisclaimerFunc = async (data) => {
    //     console.log("selecteddata..", data);
    //     if (data.typeId == Status.disclaimerTypes.loginDisclaimer.typeId) {
    //         await this.setState({ isSelectedLoginDisclaimer: data.isSelected })
    //     }

    //     console.log("isSelectedLoginDisclaimer..", this.state.isSelectedLoginDisclaimer);
    // }

    validateEmail = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            this.setState({ email: text })
            return false;
        }
        else {
            this.setState({ email: text })
        }
    }
    _onPressButton = async () => {
        if (this.state.username == "") {
            Alert.alert(I18n.t('enterfullname'));
        } else {
            if (this.state.email == "") {
                Alert.alert(I18n.t('enteremailaddr'));
            } else {
                if (this.validateEmail(this.state.email) == false) {
                    Alert.alert(I18n.t('alertnotvalidemail'));
                } else {
                    if (this.state.mobile == "") {
                        Alert.alert(I18n.t('entermobileno'));
                    } else {
                        if (this.state.pwd == "") {
                            Alert.alert(I18n.t('enterpasswordplaceholder'));
                        } else {
                            if (this.state.cnfpwd == "") {
                                Alert.alert(I18n.t('pcCPassword'));
                            } else {
                                if (this.state.pwd === this.state.cnfpwd) {
                                    if (!this.state.isSelectedTerms) {
                                        Alert.alert(I18n.t('agreetermsconds'));
                                        return false;
                                    }
                                    let overalldetails = {
                                        "Username": this.state.username,
                                        "Password": this.state.pwd,
                                        "Fullname": this.state.username,
                                        "MobileNo": this.state.mobile,
                                        "EmailId": this.state.email
                                    }
                                    this.setState({ loading: true }, () => console.log("loading before API", this.state.loading))
                                    // console.log("loading before API", this.state.loading)
                                    await apiCallWithUrl(APIConstants.Register, 'POST', overalldetails, this.registerApiResponse)
                                    this.setState({ loading: false });
                                    console.log("loading in button press after false", this.state.loading)
                                } else {
                                    Alert.alert(I18n.t("alertpwdnotmatch"));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    _onPressLogin = async () => {
        Actions.Login();
    }
    registerApiResponse = async (response) => {

        this.setState({ loading: false }, () => {

            setTimeout(() => {
                if (response.ResponseData.UserId == "Already exist") {
                    Alert.alert(I18n.t("alertregistrationexist"));
                    var userid = response.ResponseData;
                } else {
                    Alert.alert(I18n.t("alertregistrationsuccess"));
                    Actions.Login();
                }
            }, 1000)
        });
        console.log("method loading", this.state.loading)

    }

    updateLoading = async () => {
        console.log("updateloading")
        await this.setState({ loading: false }, () => console.log("updateloading", this.state.loading));
        console.log("status??", this.state.loading)
    }

    googleSignIn = async () => {
        try {
            //console.log("isSignedIn")
            this.setState({
                loading: true
            })
            const isSignedIn = await GoogleSignin.isSignedIn();
            // console.log("isSignedIn", isSignedIn)

            if (isSignedIn) {
                await GoogleSignin.revokeAccess();
                await GoogleSignin.signOut();
            }


            await GoogleSignin.hasPlayServices({
                showPlayServicesUpdateDialog: true,
            });
            const userInfo = await GoogleSignin.signIn();
            GoogleSignin.signIn()
                .then((googleSignIndata) => {
                    console.log("data", googleSignIndata)
                    this.responseUserCall(googleSignIndata.user);
                })
                .then((user) => {
                    console.log("user", user)
                    // If you need to do anything with the user, do it here
                    // The user will be logged in automatically by the
                    // `onAuthStateChanged` listener we set up in App.js earlier
                })
                .catch((error) => {
                    const { code, message } = error;
                    console.log("error", error);
                    that.setState({
                        loading: false
                    })
                    // For details of error codes, see the docs
                    // The message contains the default Firebase string
                    // representation of the error
                });




            // console.log(userInfo.user.email, userInfo.user.name);
            // this.setState({ userInfo });
            // console.log(userInfo.user.email, userInfo.user.name);

            // this.setState({ email: userInfo.user.email, username: userInfo.user.name })
            // if (userInfo.user.email != "") {
            //     Actions.tabbar();
            // }


        } catch (error) {
            this.updateLoading();
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                console.log("user cancelled the login flow");
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (f.e. sign in) is in progress already
                console.log("operation (f.e. sign in) is in progress already");
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                console.log("play services not available or outdated");
            } else {
                // some other error happened
                console.log("some other error happened", error);
            }
        }
    };

    onFbAccess = async () => {
        that = this;
        this.setState({
            loading: true
        })

        //*****************************************Facebook Integrated code */
        // FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Native); // defaults to Native
        // FBLoginManager.logout((data) => {
        //     //do something
        // })
        console.log("did enter fb login***");
        //alert("fbstart");
        // FBLoginManager.loginWithPermissions(["public_profile", "email"], function (error, data) {
        //     if (!error) {
        //         console.log("Login data: " + data);
        //         //alert(JSON.stringify(data));
        //         that.getUserDetailsFb(data.credentials.token);

        //     } else {
        //         console.log("Error: ", error);
        //         that.setState({
        //             loading: false
        //         })
        //         //alert(error);
        //     }
        // })

        LoginManager.logInWithPermissions(["public_profile", "email"]).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                    that.updateLoading();
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            console.log(data.accessToken.toString())
                            that.getUserDetailsFb(data.accessToken.toString());
                        }
                    )
                }
            },
            function (error) {
                console.log("Login fail with error: " + error);
            }
        );
    }

    getUserDetailsFb = async (token) => {
        that = this;
        console.log("token....fb", token);
        var api = 'https://graph.facebook.com/v2.8/me?fields=name,email&access_token=' + token;
        //alert("apipart..",api);
        return await fetch(api, {
            method: 'get',
            headers: {
                'Accept': 'application/json'
            },
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.responseUserCall(responseJson);
            })
            .catch((error) => {
                console.error(error);
                that.setState({
                    loading: false
                })
            });
    }
    responseUserCall = async (response) => {
        that = this;
        // alert("Did enter fb response");
        console.log("responseJson..", response);

        let overalldetails = {
            "Fullname": response.name,
            "EmailId": response.email,
            "Password": "",
            "IsSocialLogin": "1",
            "fcmToken": this.state.deviceToken,

        }

        console.log("overalldetails..", overalldetails);
        await apiCallWithUrl(APIConstants.GuestLoginAPI, 'POST', overalldetails, this.postUserProfileResponse);
        //await apiCallWithUrl(APIConstants.Register, 'POST', overalldetails, this.registerApiResponse);

        // Actions.CustomerHome();


        //alert(JSON.stringify(responseJson));
        // alert("responseJson.."+responseJson);
        //     that.setState({email:responseJson.email,username:responseJson.name,password:responseJson.id})
        //   var setVal=0,firebaseRef = firebase.database().ref("users");
        //   firebaseRef
        //     .once("value")
        //     .then(items => {
        //       const userref=items.val();console.log("");
        //       _.forIn(userref, (value, key) => {
        //         if(responseJson.email==value["email"]){setVal=1;}
        //       })
        //       if(setVal==1){console.log("Email ID Exists!!!");that.getFbUserId(responseJson.id,'1');}else{console.log("No Email ID!!!");that.getFbUserId(responseJson.id,'0');}
        //       that.setState({
        //         loading:false
        //       })
        //     })
    }

    postUserProfileResponse = async (response) => {
        await this.setState({ loading: false });
        const roomNo = await AsyncStorage.getItem(Keys.roomNo);
        if (response.IsException != "True") {

            if (response.ResponseData == null) {
                return Alert.alert(I18n.t('alertsmthngwrng'))
            }
            console.log('response', response);
            if (response.ResponseData != "Invalid Username or Password") {
                // alert("Login Success!!");
                console.log('res', response.ResponseData)

                AsyncStorage.setItem(Keys.isLogin, JSON.stringify(true));
                AsyncStorage.setItem(Keys.UserDetail, JSON.stringify(response.ResponseData));
                
                console.log("roomNo", roomNo)
                if (roomNo != null) {
                    Actions.tabbar();
                } else {
                    Actions.GuestCheckIn();
                }
                // Actions.tabbar();
            } else {
                Alert.alert(I18n.t('alertchknamepwd'));
            }
        }
        else {
            Alert.alert(I18n.t('alerttryagain'));
        }
    }
    getFbUserId = async (userid, val) => {
        await AsyncStorage.setItem('fbUserId', userid);
        this.setState({
            loading: false
        })
        if (val == 1) { this.authenticateLogin(); } else { this.createUser(); }
    }
    //   getFbUserId =async(userid,val)=>{
    //     await AsyncStorage.setItem('fbUserId', userid);
    //     this.setState({
    //       loading:false
    //     })
    //     if(val==1){this.authenticateLogin();}else{this.createUser();}
    //   }
}

const styles = StyleSheet.create({
    label: {
        flex: 1,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8,
        fontSize: 13,
        fontFamily: AppFont.Regular,
        color: Colors.Black
    },
    textInput: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        height: 35,
        fontSize: 15,
        padding: 0,
        marginLeft: 8,
        // marginRight: 8,
        includeFontPadding: false,
    },
    cellrow: {
        marginTop: 10,
        marginBottom: 5,
        width: null
    },
    loginview: {
        alignItems: 'center',
    },
    button: {
        alignItems: 'center',
        backgroundColor: Colors.App_Font,
        padding: 10,
        width: "100%",
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 5
    },
    btntext: {
        color: "#FFF",
        fontSize: 18,
        letterSpacing: 1,
        fontFamily: AppFont.Regular,
    },
    textBorderLine: {
        marginLeft: 8,
        marginRight: 8,
        borderBottomColor: Colors.App_Font,
        borderBottomWidth: 0.8
    },
    AgreeText: {
        color: Colors.Black,
        fontSize: 12,
        fontFamily: AppFont.Regular
    },
})

module.exports = Register;