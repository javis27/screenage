import AppFont from './AppFont';
import Colors from './Colors';
export default {
    dateInput: {
        borderColor: 'transparent',
        marginLeft: 0,
        alignItems: 'flex-start',
        height: 35,
       // marginTop: 15,
        marginBottom: 5,
    },
    dateIcon: {
        resizeMode: 'contain'
    },
    placeholderText: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        fontSize: 15,
        padding: 0,
        includeFontPadding: false,
        color: Colors.PlaceholderText
    },
    dateText: {
        fontFamily: AppFont.Regular,
        textAlign: 'left',
        fontSize: 15,
        padding: 0,
        includeFontPadding: false,
    },
    btnTextCancel: {
        fontFamily: AppFont.Regular,
    },
    btnTextConfirm: {
        fontFamily: AppFont.Regular,
        color: Colors.App_Font
    }

};