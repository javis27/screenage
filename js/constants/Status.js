import Colors from './Colors';

export default {

  serviceItemType: {
    Veg: {
      type: 'Veg',
      image: 'green_dot'
    },
    Nonveg: {
      type: 'Non-Veg',
      image: 'red_dot'
    }
  },

  SpiceLevelStatus: {
    Low: {
      spiceLevel: 'Low',
      image: 'mild',
      imgWidth: 54,
      imgHeight: 18,
      imgFlex: 0.2,
      textFlex: 0.8,
    },
    Medium: {
      spiceLevel: 'Medium',
      image: 'spicy',
      imgWidth: 54,
      imgHeight: 18,
      imgFlex: 0.4,
      textFlex: 0.6,
    },
    High: {
      spiceLevel: 'High',
      image: 'hot',
      imgWidth: 54,
      imgHeight: 18,
      imgFlex: 0.5,
      textFlex: 0.5,
    },
  },

  serviceAvailability: {
    Available: {
      id: '1',
      color: Colors.AvailableColor
    },
    NotAvailable: {
      id: '2',
      color: Colors.NotavailableColor
    }
  },

  policyTypes: {
    loginPrivacyPolicy: {
      typeId: 1
    },
    restaurantCancellationPolicy: {
      typeId: 2
    },
    spaCancellationPolicy: {
      typeId: 3
    },
    bikeSafetyPolicy: {
      typeId: 4
    },
    bikeCancellationPolicy: {
      typeId: 5
    },
    boatCancellationPolicy: {
      typeId: 6
    },
    roomserviceCancellationPolicy: {
      typeId: 7
    }

    //     Login Page Privacy policy     - 1
    // Restaurant Cancellation Policy    - 2
    // Spa Cancellation Policy        - 3 
    // Bike Safety Policy         - 4
    // Bike Cancellation Policy       - 5

  },

  disclaimerTypes: {
    loginDisclaimer: {
      typeId: 1
    },
    visitorWelcomeDisclaimer: {
      typeId: 2
    },
    customerWelcomeDisclaimer: {
      typeId: 3
    },
    bikeIndividualDisclaimer: {
      typeId: 4
    },
    bikeGroupDisclaimer: {
      typeId: 5
    }

    //     Login Page Disclaimer     - 1
    // Visitor Welcome Disclaimer    - 2
    // Customer welcome Disclaimer        - 3 
    // Bike Individual Disclaimer         - 4
    // Bike Group Disclaimer       - 5

  },

  BookingStatusTypes: {
    requested: {
      name: 'Requested',
      textColor: 'blue'
    },
    confirmed: {
      name: 'Confirmed',
      textColor: 'green'
    },
    rejected: {
      name: 'Rejected',
      textColor: 'red'
    }

  },

  RoomServiceStatusType: {
    requested: {
      id: '0',
      name: 'Requested',
      textColor: 'orangered'
    },
    completed: {
      id: '1',
      name: 'Pending',
      textColor: 'green'
    },
    pending: {
      id: '2',
      name: 'Completed',
      textColor: 'blue'
    },
    notavailable: {
      id: '3',
      name: 'Not Available',
      textColor: 'red'
    }
  },

  amenityRequestStatus: {
    requested: {
      id: '1',
      name: 'Requested',
      textColor: 'orangered'
    },
    completed: {
      id: '2',
      name: 'Pending',
      textColor: 'blue'
    },
    pending: {
      id: '3',
      name: 'Completed',
      textColor: 'green'
    },
    notavailable: {
      id: '4',
      name: 'Not Available',
      textColor: "#929495"
    }
  },

  Currency_ISO_Code: {
    Mauritius: {
      code: 'MUR'
    }
  },

  MapTypes: {
    restaurants: {
      typeId: 1
    },
    routes: {
      typeId: 2
    },
    viewPoints: {
      typeId: 3
    }
  },

  bookingTypes: {
    restaurant: {
      typeId: 1
    },
    spa: {
      typeId: 2
    },
    bike: {
      typeId: 3
    },
    boat: {
      typeId: 4
    },
    roomservice: {
      typeId: 7
    }
  },

  amenitiesTypes: {
    water: {
      id: 1,
      name: "Water Bottle"
    },
    towel: {
      id: 2,
      name: "Towel"
    },
    soap: {
      id: 3,
      name: "Soap"
    },
    minibar: {
      id: 4,
      name: "Mini Bar"
    },
    waterFrench: {
      id: 5,
      name: "Bouteille d'eau"
    },
    towelFrench: {
      id: 6,
      name: "Serviette"
    },
    soapFrench: {
      id: 7,
      name: "Savon"
    },
    minibarFrench: {
      id: 8,
      name: "Mini Bar"
    },

  }


  // NonVegSpiceLevelStatus: {
  //   Low: {
  //     spiceLevel: 'Low',
  //     image: 'red_chili',
  //     imgWidth: '18',
  //     imgFlex: 0.2,
  //     textFlex:0.8,
  //   },
  //   Medium: {
  //     spiceLevel: 'Medium',
  //     image: 'red_chili2',
  //     imgWidth: 'auto',
  //     imgFlex: 0.3,
  //     textFlex:0.7,
  //   },
  //   High: {
  //     spiceLevel: 'High',
  //     image: 'tick',
  //     imgWidth: 'auto',
  //     imgFlex: 0.5,
  //     textFlex:0.5,
  //   },
  // }


};