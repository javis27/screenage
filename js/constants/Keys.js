
export default {
    isLogin: 'isLogin',
    UserDetail: 'userDetail',
    langId: 'langId',
    mapDetail: 'mapDetail',
    roomNo: 'roomNo',
    inCustomer: 'inCustomer',
    checkInId: 'checkInId',
    language: 'language',

    noInternetTitle: 'No Internet connection',
    noInternetMessage: 'Please check your connection or try again later',
    timedOutTitle: 'Request timed out',
    timedOutTitleMessage: 'Something went wrong, please try again after some time.',

}