
export default {


  // BASE_URL: 'http://preskil.fortiddns.com:8082/api/',

   BASE_URL: 'http://sun.smartpoint.in/api/v2/mobile/',

  //BASE_URL: 'http://192.168.24.23:8021/api/v2/mobile/',

  Image_URL: 'http://preskil.fortiddns.com:8082/',

  BeaconAPI_URL: 'https://edit.meridianapps.com/api/',

  BeaconAccessToken: 'Token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsIjo0NzQ4NDk3MDY3OTY2NDY0LCJ0IjoxNTU3ODIzMzg3fQ.K--UtYWOC1yd698iy6UoWPiIwKb04Mwdj-GUhmeEM_o',

  WeatherAPI_URL: 'http://dataservice.accuweather.com/',

  GeopositionSearchAPI: 'locations/v1/cities/geoposition/search',

  WeatherAPIKey: 'YjFjxXToLgrcalAI5Y6S311Wm5JEgGUW',     // Screenage account
  //WeatherAPIKey: 'e9xaZ60xPOgIxgr4Xov2k48XiMAFYYs4',        // Swathi account

  FlightAPI_URL: 'https://api.flightstats.com/flex/schedules/rest/',

  FlightAppId: '63b67db3',

  FlightAppKey: 'a5757d18b4d832c9c3fafdc45d49bad7',

  // LanguageAPI: 'LanguageAPI/GetLanguage',

  LanguageAPI: 'language/language_list',

  HotelSelectionAPI: 'hotel/hotel_list', //new API

  // GuestLoginAPI: 'GuestLoginAPI/GuestLogin',

  GuestLoginAPI: 'MobileUserLogin/GuestLogin',

  // Register: 'GuestLoginAPI/GuestRegister',

  Register: 'MobileUserLogin/GuestRegister',

  //MobilePagesAPI: 'MobilePages/GetMobilePages',
  
  MobilePagesAPI: 'pages/menus_list',

  // GetUserProfileAPI: 'ProfileAPI/GetUserById', //ok

  GetUserProfileAPI: 'Profile/GetProfileBy_UserIdHotelId', //ok

  // PostUserProfileAPI: 'ProfileAPI/SaveUser', //ok

  PostUserProfileAPI: 'Profile/Update_User', //ok

  // GetRestaurantMasters: 'RestaurantAPI/GetRestautrantMastersAPI',
  
  GetRestaurantMasters: 'restaurant/restaurant_list',

  // PostRestaurantBooking: 'RestaurantAPI',

  PostRestaurantBooking: 'restaurant/create_restaurant_booking',
  
  //GetRestaurantBookingsList: 'RestaurantAPI/GetRestaurantBookingsList',

  GetRestaurantBookingsList: 'restaurant/restaurant_booking_list',

  GetDisclaimerAPI: 'Disclaimer/GetDisclaimer', //ok

  // GetPolicyAPI: 'PolicyAPI/GetPolicy', //ok

  GetPolicyAPI: 'policy/GetPolicy',

  // GetPhoneDirectoryAPI: 'PhoneDirectoryAPI/GetPhoneDirectory', //ok

  GetPhoneDirectoryAPI: 'PhoneDirectory/GetPhoneDirectory', //ok

  // GetTvChannelAPI: 'TVChannelAPI/GetTVChannel', //ok

  GetTvChannelAPI: 'TvChannel/GetTvChannel', //ok

  // GetBikesMastersAPI: '/BikeAPI/GetBikeMastersAPI',

  GetBikesMastersAPI: 'Bike_Booking/Bike_list',

  // GetSpaPackagesAPI: 'SPAPackageAPI/GetSpaPackageAPI',

  GetSpaPackagesAPI: 'spapackage/spa_package_list',

  // GetSpaMastersAPI: 'SPAPackageAPI/GetSPAMastersAPI',

  GetSpaMastersAPI: 'spapackage/spaPackage_category_list',

  // PostSpaBookingAPI: 'SPAAPI/BookSPA',

  PostSpaBookingAPI: 'spa_booking/create_SpaBooking',

  // PostBikeBookingAPI: 'BikeAPI/BikeBooking',

  PostBikeBookingAPI: 'bike_booking/Create_Bike_Booking',

  // GetSpaBookingList: 'SPAAPI/GetSpaBookingsList',

  GetSpaBookingList: 'spa_booking/spa_booking_list',

  // GetBikeBookingList: 'BikeAPI/GetBikeBookingsList',

  GetBikeBookingList: 'bike_booking/bike_booking_list',

  // GetServiceList: 'ServiceAPI/GetService',

  GetServiceList: 'RoomService/GetRoomService',

  // GetCalendarEvents: 'EventAPI/GetEvents', //ok

  GetCalendarEvents: 'Event/GetEvent', //ok

  // GetDashboardCountAPI: 'DashboardAPI/GetBookingCount',

  GetDashboardCountAPI: 'restaurant/all_typeof_booking_count',

  // GetSubMenuAPI: 'MobilePages/GetSubMenu',

  GetSubMenuAPI: 'pages/sub_menus_list',

  // GetPagesContentAPI: 'MobilePages/GetContent',

  GetPagesContentAPI: 'pages/page_content',

  GetOfflineMapsAPI: 'Map/GetMap',

  // ChangePasswordAPI: 'GuestLoginAPI/ChangePassword',ForgotPassword/UpdateNewPassword

  ChangePasswordAPI: 'ForgotPassword/UpdateNewPassword',

  GeoLocationAPI: 'GeoLocationAPI/GetGeoLocations',

  // GetBannerAPI: 'BannerAPI/GetBanner',

  GetBannerAPI: 'banner/banner_list',

  GetCancelAPI: "CancelAPI/CancelBooking",

  VerifyEmailIdAPI: 'ForgotPasswordAPI/CheckEmail',

  NewPasswordAPI: 'ForgotPasswordAPI/UpdateNewPassword',

  // GetRoomServiceList: 'RoomServiceAPI/ListRoomService',

  PostServiceRequest: 'RoomServiceAPI/RequestRoomService', //ok

  GetRoomCategoryMasters: 'RoomServiceAPI/ServiceRequestDropdown',

  // PostAmenities: 'AmenitiesAPI/RequestAmenities',

  PostAmenities: 'AmenityBooking/Create_AmenityBooking',

  // GetAmenitiesList: 'AmenitiesAPI/ListAmenities',
  
  GetAmenitiesMasterAPI: 'Amenity/amenity_list',

  GetAmenitiesList: 'AmenityBooking/AmenityBooking_list',    

  GetWeatherLocations: 'WeatherAPI/ListLocations', //ok

  // GetServiceTabbarAPI: 'ServiceTabbarAPI/ListTabbar',

  GetServiceTabbarAPI: 'service_tabbar/service_tabbar_list',

  GuestCheckInAPI: 'CheckIn/VerifyCheckInDetails', //ok

  GetRestaurantListAPI: 'RestaurantAPI/GetRestautrantsAPI',

  BoatBookingListAPI: 'BoatBooking/BoatBooking_list',

  GetBoatListAPI: 'BoatBooking/boat_list',

  GetBoatTimeSlotListAPI: 'BoatBooking/load_time_slot',

  PostBoatBookingAPI: 'BoatBooking/create_boat_booking',
  
  GetRestaurantCancelAPI: "restaurant/cancel_restaurant_booking",
  
  GetSpaCancelAPI: "spa_booking/cancel_spa_booking",
  
  GetBikeCancelAPI: "Bike_Booking/Cancel_Bike_Booking",

  GetBoatCancelAPI: "BoatBooking/cancel_BoatBooking",

  GetAmenityCancelAPI: "AmenityBooking/cancel_AmenityBooking",

  GetRatingQuestionAPI: "ratingquestions/rating_questions_list",

  CreateRatingAPI: "user_review/create_user_review_ratings",

  CreateRoomServiceAPI: "room_service_booking/create_RoomServiceBooking",

  GetRoomServiceBookingListAPI: "room_service_booking/RoomServiceBooking_list",

  GetRoomServiceCancelAPI: "room_service_booking/Cancel_RoomServiceBooking",

  GetViewRoomServiceAPI: "RoomService/view_room_service",

  GetVerifyLoginStatusAPI: "CheckIn/VerifyLoggedInOrOut"
};
