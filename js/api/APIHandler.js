
import APIConstants from './APIConstants';
import { NetInfo, Alert } from 'react-native';
import Keys from '../constants/Keys';

export async function apiCallWithUrl(url, APIMethod, requestParams, callback) {

  NetInfo.isConnected.addEventListener('connectionChange', (isConnected) => {
    console.log('IsInternetconnected? ', isConnected)
    if (!isConnected) {
      NetInfo.isConnected.removeEventListener('connectionChange', isConnected);
      return Alert.alert(Keys.noInternetTitle, Keys.noInternetMessage);
    }
  })

  let baseUrl = APIConstants.BASE_URL
  //   let baseUrl ="http://jeppiaar.smartams.co/api/";
  console.log(baseUrl + url, '..', requestParams)

  let requestOptions;
  if (APIMethod == "POST") {
    requestOptions = {
      method: APIMethod,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Base64 YWRtaW46YWRtaW5AMzIx'
      },
      body: JSON.stringify(requestParams)
    };
  }
  else {
    requestOptions = {
      method: APIMethod,
      headers: {
        'Authorization': 'Base64 YWRtaW46YWRtaW5AMzIx'
      }
    };
  }


  return await fetch(baseUrl + url, requestOptions)
    .then((response) => response.json())
    .then((responseJson) => {
      console.log("responsejson..", responseJson);
      NetInfo.isConnected.removeEventListener('connectionChange', this);
      callback(responseJson);

    })
    .catch((error) => {
      callback(null);
      console.log('error...', error);
      NetInfo.isConnected.removeEventListener('connectionChange', this);
    });

}

export async function beaconApiCallWithUrl(url, APIMethod, requestParams, callback) {
  NetInfo.isConnected.addEventListener('connectionChange', (isConnected) => {
    if (!isConnected) {
      NetInfo.isConnected.removeEventListener('connectionChange', isConnected);
      return Alert.alert(Keys.noInternetTitle, Keys.noInternetMessage);
    }
  })

  let beaconBaseUrl = APIConstants.BeaconAPI_URL
  console.log(beaconBaseUrl + url, '..', requestParams)

  let requestOptions;
  if (APIMethod == "POST") {
    requestOptions = {
      method: APIMethod,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': APIConstants.BeaconAccessToken
      },
      body: JSON.stringify(requestParams)
    };
  }
  else {
    requestOptions = {
      method: APIMethod,
      headers: {
        'Authorization': APIConstants.BeaconAccessToken
      }
    };
  }

  return await fetch(beaconBaseUrl+url,requestOptions)
    .then((response)=>response.json())
    .then((responseJson)=>{
      console.log("responsejson..", responseJson);
      NetInfo.isConnected.removeEventListener('connectionChange',this);
      callback(responseJson);
    })
      .catch((error)=>{
        callback(null);
        console.error('error..',error);
        NetInfo.isConnected.removeEventListener('connectionChange',this);
      })

}

export async function dynamicApiCallWithUrl(dynamicBaseUrl, APIMethod, requestParams, callback) {
  NetInfo.isConnected.addEventListener('connectionChange', (isConnected) => {
    if (!isConnected) {
      NetInfo.isConnected.removeEventListener('connectionChange', isConnected);
      return Alert.alert(Keys.noInternetTitle, Keys.noInternetMessage);
    }
  })

  console.log(dynamicBaseUrl, '..', requestParams)

  let requestOptions;
  if (APIMethod == "POST") {
    requestOptions = {
      method: APIMethod,
      // headers: {
      //   Accept: 'application/json',
      //   'Content-Type': 'application/json',
      //   'Authorization': APIConstants.BeaconAccessToken
      // },
      body: JSON.stringify(requestParams)
    };
  }
  else {
    requestOptions = {
      method: APIMethod,
      // headers: {
      //   'Authorization': APIConstants.BeaconAccessToken
      // }
    };
  }

  return await fetch(dynamicBaseUrl,requestOptions)
    .then((response)=>response.json())
    .then((responseJson)=>{
      console.log("dynamic responsejson..", responseJson);
      NetInfo.isConnected.removeEventListener('connectionChange',this);
      callback(responseJson);
    })
      .catch((error)=>{
        callback(null);
        console.error('error..',error);
        NetInfo.isConnected.removeEventListener('connectionChange',this);
      })

}

