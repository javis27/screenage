//
//  MRDLoadingIndicator.h
//  MeridianiOSControls
//
//  Created by Christen Hubbard on 3/14/17.
//  Copyright © 2017 Aruba Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MRDLoadingIndicatorStyle) {
    MRDLoadingIndicatorSmallBlue,
    MRDLoadingIndicatorLargeBlue,
    MRDLoadingIndicatorLargeWhite,
};

@interface MRDLoadingIndicator : UIView
- (nonnull instancetype)initWithStyle:(MRDLoadingIndicatorStyle)style;
- (void)startAnimating;
- (void)stopAnimating;
// setting these properties will override default style-derived colors
@property (nonnull, nonatomic, strong) UIColor *color;
@property (nonatomic, assign) BOOL applyGradations;
@end

