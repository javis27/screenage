//
//  MRPlacemark.h
//  Meridian
//
//  Copyright (c) 2016 Aruba Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Meridian/MRPointAnnotation.h>

NS_ASSUME_NONNULL_BEGIN


/// `MRPlacemark` NSUserActivity domain identifier
extern NSString * const kMRPlacemarkUserActivityDomainIdentifier;

/**
 * Represents a Placemark, possibly created by the Meridian Editor.
 */

@interface MRPlacemark : MRPointAnnotation

/// Uniquely identifies this placemark. The parent of this key should identify the map containing this placemark.
@property (nonatomic, copy) MREditorKey *key;

/// If not nil, indicates this placemark leads to another map.
@property (nonatomic, copy) MREditorKey *relatedMapKey;

/// The name given to this placemark.
@property (nullable, nonatomic, copy) NSString *name;

/// The placemark's type, such as "cafe" or "water_fountain".
@property (nullable, nonatomic, copy) NSString *type;

/// A form of the placemark's type intended for display to users, such as "Cafe" or "Water Fountain".
@property (nullable, nonatomic, copy) NSString *typeName;

/// The color to use when drawing this placemark's map annotation. You can set this to override the default color.
@property (nullable, nonatomic, strong) UIColor *color;

/// This placemark's coordinates relative to its parent map.
@property (nonatomic, assign) CGPoint point;

/// If YES, this placemark will not be shown on its map.
@property (nonatomic, assign) BOOL hideOnMap;

/// An image representing this placemark.
@property (nullable, nonatomic, strong) NSURL *imageURL;

/// A path describing the placemark's area, if one was defined.
@property (nonatomic, strong) UIBezierPath *area;

/// This property changes the the front-to-back ordering of annotations onscreen. Default will be some value between MRZPositionPlacemarkMin and MRZPositionPlacemarkMax
@property (nonatomic, assign) CGFloat zPosition;

/// If YES this annotation collides with other annotations. Default is YES.
@property (nonatomic, assign) BOOL collides;

/// A unique identifier to link this placemark to objects outside of the Meridian Editor
@property (nullable, nonatomic, copy) NSString *uid;


/**
 * Programmatically create an MRPlacemark that may not exist in the Meridian Editor, but can be used for other APIs like directions.
 *
 * @param mapKey  The map that this placemark belongs to.
 * @param point  The coordinates of this placemark.
 */
- (instancetype)initWithMap:(MREditorKey *)mapKey point:(CGPoint)point;

@end

NS_ASSUME_NONNULL_END
