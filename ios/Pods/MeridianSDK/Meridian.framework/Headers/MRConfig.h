//
//  MRConfig.h
//  Meridian
//
//  Copyright (c) 2016 Aruba Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRDSpotlightConfiguration;

/**
 * Constants for setting logging verbosity.
 */

typedef NS_ENUM(NSUInteger, MRLogLevel) {
    /// Will not log at all
    MRLogLevelOff,
    /// Error Level - Something went very wrong
    MRLogLevelError,
    /// Warning Level - Not fatal but we need to know
    MRLogLevelWarn,
    /// Info Level - Nice to know. For checkpoints
    MRLogLevelInfo,
    /// Debug Level - for fixing potential issues
    MRLogLevelDebug,
    /// Verbose Level - Lots and lots of info going to come out here
    MRLogLevelVerbose,
    /// Info Level - Nice to know
    MRLogLevelNormal = MRLogLevelInfo, //DEPRECATED_MSG_ATTRIBUTE("Please use MRLogLevelInfo instead") = MRLogLevelInfo,
};

/**
 *  Constants for the currently supported Domain Regions
 */
typedef NS_ENUM(NSUInteger, MRDomainRegion) {
    /// Default region, most likely in the US, but not restricted
    MRDomainRegionDefault,
    /// Domain region is restricted to US-compliant servers
    ///    Currently unused
    MRDomainRegionUS,
    /// Domain Region is restricted to EU-compliant servers
    MRDomainRegionEU
};

/**
 *  Holds Meridian domain/host configuration data for your application.
 */
@interface MRDomainConfig : NSObject <NSCopying>
NS_ASSUME_NONNULL_BEGIN

/**
 *  Flag to control the Meridian Editor data storage restrictions [MRDomainRegion (ex: US vs EU)]
 */
@property (nonatomic) MRDomainRegion domainRegion;

/**
 *  Reset domainConfig to default values
 */
- (void)reset;

/**
 *  Reset domainConfig using a domainName
 *
 * @param domainName domain name used to reset the domainConfig(ex: "staging-edit.meridianapps.com")
 */
- (void)resetWithDomainName:(NSString *)domainName;

/**
 *  Reset domainConfig using a well formed url
 *    badly formed URLS will be silently ignored
 *
 *  @param urlString editor URL used to reset the domainConfig (ex: "https://edit-eu.meridianapps.com")
 */
- (void)resetWithEditorURL:(NSString *)urlString;

/**
 *  Full URL to the Meridian Editor, adjusted by the settings and override
 *
 *  @return The editor URL (ex: "https://edit-eu.meridianapps.com")
 */
-(NSString *) editorURL;

NS_ASSUME_NONNULL_END
@end



/**
 *  Holds Meridian configuration data for your application.
 */

@interface MRConfig : NSObject <NSCopying>
NS_ASSUME_NONNULL_BEGIN

/**
 *  A convience method to retrieve the current editor URL from the domainConfig
 *
 *  @return The editor URL (ex: "https://edit-eu.meridianapps.com")
 */
-(NSString *) editorURL;

/**
 *  Class to encapsulate domainName configuration data
 */
@property (nonatomic, readonly) MRDomainConfig *domainConfig;

/// Spotlight configuration for sharing location data such as placemarks with CoreSpotlight.
/// If `nil` (the default) no indexing with CoreSpotlight will be performed
@property (nonatomic, nullable, strong) MRDSpotlightConfiguration *spotlightConfiguration;

/// Disable CoreSpotlight integration for Placemarks. `NO` by default
@property (nonatomic, assign) BOOL disableCoreSpotlightIndexing;

/**
 *  Google Analytics tracking code to use for sending events and page views.
 */
@property (nonatomic, copy) NSString *googleAnalyticsTrackingCode;

/**
 * Disable Meridian Google Analytics collection
 */
@property (nonatomic, assign) BOOL disableMeridianAnalytics;

/**
 *  Flag to override the server specified cache timeout.
 */
@property (nonatomic, assign) BOOL cacheOverrideEnabled;

/**
 *  Duration in milliseconds to hold on to cached requests when overriding the server defaults
 */
@property (nonatomic, assign) long cacheOverrideTimeout;

/**
 *  Flag to enable using simulated location on devices. Defaults to NO.
 */
@property (nonatomic, assign) BOOL useSimulatedLocation;

/**
 *  When using system-provided services for outdoor location, readings will only be considered valid 
 *  with an accuracy of at least this value in meters. Defaults to 5.
 */
@property (nonatomic, assign) CGFloat outdoorLocationAccuracyThreshold;

/**
 *  Threshold (in degrees) used to determine whether the compass is aligned with the route
 *  Valid range is 45 - 90 degrees.
 *  Default value is 90.
 */
@property (nonatomic, assign) NSInteger orientationWarningThreshold;

/**
 *  When the compass is poorly aligned to the routing line, a warning is displayed after a period of time (in seconds)
 *  Valid range is 2.0 to 10.0 seconds.
 *  Default value is 5.0 seconds
 */
@property (nonatomic, assign) NSTimeInterval orientationWarningDelay;

/**
 *  Distance in meters a user's location must diverge from the route before the route can be recalculated. 
 *  Valid range is 5 - 30 meters. Default value is 10.
 */
@property (nonatomic, assign) CGFloat rerouteDistance;

/**
 *  Time in seconds that a user's location must remain in divergence from the route before the route can be
 *  recalculated. Valid range is 5 - 15 seconds. Default value is 10.
 */
@property (nonatomic, assign) NSTimeInterval rerouteDelay;

/**
 *  Logging level. Defaults to MRLogLevelNormal
 */
@property (nonatomic, assign) MRLogLevel logLevel;

/**
 *  Application Token. Can be generated at either
 *       edit.meridianapps.com > `your location` > Permissions > Application Token.
 *       edit-eu.meridianapps.com > `your location` > Permissions > Application Token.
 */
@property (nonatomic, strong) NSString *applicationToken;

NS_ASSUME_NONNULL_END
@end


/**
 The heart of the SDK, this helps start with a configuration and use simulated
 locations.
 */
@interface Meridian : NSObject

/**
 *  You must call this once inside -application:didFinishLaunching. The passed MRConfig object
 *  will be cloned, so making changes to if after this call will have no effect. You can
 *  only call this method once for the lifetime of your application.
 *  @param config The MRConfig to be cloned.
 */
+ (void)configure:(nonnull MRConfig *)config;

/**
 * The shared MRConfig instance. Use this if you need to update any of the configuration
 * properties.
 */
+ (nullable MRConfig *)sharedConfig;


@end

