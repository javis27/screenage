//
// NSError+Helpers.h
// Meridianapps
//
// THIS FILE IS GENERATED
// DO NOT EDIT DIRECTLY.
// TO ADD AN ERROR PLEASE ADD IT TO `MRDErrors.plist`
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MeridianErrorCode) {
    /// Bluetooth is currently powered off
MeridianErrorCodeBluetoothTurnedOff = 7001,
/// Directions are currently calculating
MeridianErrorCodeDirectionsCalculating = 1001,
/// No source provided for directions request
MeridianErrorCodeDirectionsNoSource = 1002,
/// Directions Request was cancelled
MeridianErrorCodeDirectionsRequestCancelled = 1003,
/// No prefix on location sharing request
MeridianErrorCodeFriendsMissingInvitationPrefix = 2001,
/// No friend location
MeridianErrorCodeFriendsMissingLocation = 2002,
/// Multiple Monitoring Managers
MeridianErrorCodeInternalMultipleMonitoringManagers = 8001,
/// No application token found for Meridian data requests
MeridianErrorCodeInternalNoApplicationToken = 8002,
/// Access to Location Services have been denied
MeridianErrorCodeLocationAccessDenied = 3001,
/// Access to Location Services are restricted
MeridianErrorCodeLocationAccessRestricted = 3002,
/// Background location has been denied
MeridianErrorCodeLocationBackgroundLocationDenied = 3003,
/// Usage description is missing from Info.plist
MeridianErrorCodeLocationMissingLocationDescription = 3004,
/// No location providers available
MeridianErrorCodeLocationNoProvidersAvailable = 3005,
/// Location request is missing from Info.plist
MeridianErrorCodeLocationRequestMissing = 3006,
/// Location fetch timout
MeridianErrorCodeLocationTimeout = 3007,
/// Unable to determine current location
MeridianErrorCodeLocationUnableToDetermineLocation = 3008,
/// Error loading the map
MeridianErrorCodeMapLoadError = 4001,
/// Invalid map size provided
MeridianErrorCodeMapSizeInvalid = 4002,
/// Error loading the map surface
MeridianErrorCodeMapSurfaceError = 4003,
/// Incomplete data loaded
MeridianErrorCodeNetworkIncompleteDataFound = 5001,
/// Maximum number of request retries reached
MeridianErrorCodeNetworkMaxmimumRetriesReached = 5002,
/// 
MeridianErrorCodeNetworkOperationCancelled = 5003,
/// Request is already in progress
MeridianErrorCodeNetworkRequestInProgress = 5004,
/// 
MeridianErrorCodeRoutingCalculating = 6001,
/// No route found
MeridianErrorCodeRoutingNoRoute = 6002,
/// 
MeridianErrorCodeRoutingNoSource = 6003,
/// Request operation was cancelled
MeridianErrorCodeRoutingRequestCancelled = 6004,
};

@interface NSError (Helpers)

/// Bluetooth is currently powered off
+ (nonnull instancetype)_bluetoothTurnedOff;
/// Directions are currently calculating
+ (nonnull instancetype)_directionsCalculating;
/// No source provided for directions request
+ (nonnull instancetype)_directionsNoSource;
/// Directions Request was cancelled
+ (nonnull instancetype)_directionsRequestCancelled;
/// No prefix on location sharing request
+ (nonnull instancetype)_friendsMissingInvitationPrefix;
/// No friend location
+ (nonnull instancetype)_friendsMissingLocation;
/// Multiple Monitoring Managers
+ (nonnull instancetype)_internalMultipleMonitoringManagers;
/// No application token found for Meridian data requests
+ (nonnull instancetype)_internalNoApplicationToken;
/// Access to Location Services have been denied
+ (nonnull instancetype)_locationAccessDenied;
/// Access to Location Services are restricted
+ (nonnull instancetype)_locationAccessRestricted;
/// Background location has been denied
+ (nonnull instancetype)_locationBackgroundLocationDenied;
/// Usage description is missing from Info.plist
+ (nonnull instancetype)_locationMissingLocationDescription;
/// No location providers available
+ (nonnull instancetype)_locationNoProvidersAvailable;
/// Location request is missing from Info.plist
+ (nonnull instancetype)_locationRequestMissing;
/// Location fetch timout
+ (nonnull instancetype)_locationTimeout;
/// Unable to determine current location
+ (nonnull instancetype)_locationUnableToDetermineLocation;
/// Error loading the map
+ (nonnull instancetype)_mapLoadError;
/// Invalid map size provided
+ (nonnull instancetype)_mapSizeInvalid;
/// Error loading the map surface
+ (nonnull instancetype)_mapSurfaceError;
/// Incomplete data loaded
+ (nonnull instancetype)_networkIncompleteDataFound;
/// Maximum number of request retries reached
+ (nonnull instancetype)_networkMaxmimumRetriesReached;
/// 
+ (nonnull instancetype)_networkOperationCancelled;
/// Request is already in progress
+ (nonnull instancetype)_networkRequestInProgress;
/// 
+ (nonnull instancetype)_routingCalculating;
/// No route found
+ (nonnull instancetype)_routingNoRoute;
/// 
+ (nonnull instancetype)_routingNoSource;
/// Request operation was cancelled
+ (nonnull instancetype)_routingRequestCancelled;

/// Create a NSError with a given error code and specified userInfo dictionary
/// @param code     `MeridianErrorCode` of the error
/// @param userInfo Optional UserInfo dictionary to associate with the error
+ (nonnull instancetype)mrd_errorWithMeridianErrorCode:(MeridianErrorCode)code userInfo:(nullable NSDictionary *)userInfo;

@end

// Call this empty method to include this category-only object file in your binary without needed the -ObjC flag.
void NSErrorHelpersInclude(void);