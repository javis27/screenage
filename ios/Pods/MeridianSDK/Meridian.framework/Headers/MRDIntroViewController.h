//
//  MRIntroViewController.h
//  MeridianiOSControls
//
//  Created by Daniel Miedema on 7/20/17.
//  Copyright © 2017 Aruba Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MRDIntroPage.h"

@protocol MRDIntroViewControllerDelegate;

/// View Controller set to present a set of pages for introducing something.
/// Any number of `MRDIntroPage` instances can be passed however
/// it is assumed that one and only one will have a button set.
/// When that button is pressed an alert can be shown by setting the `alertController`
/// property or whatever the thing being introduced is can begin after that
/// button press if `alertController` is `nil`.
@interface MRDIntroViewController : UIViewController

/// If we should use our custom transition manager.
/// Only applies to presentation on iPad. Default is `YES`
@property (nonatomic, assign) BOOL useCustomTransitionManager;

/// Our pages to present
@property (nonatomic, nonnull, strong) NSArray <MRDIntroPage *> *pages;

/// Delegate to inform of actions
@property (nonatomic, nullable, weak) id<MRDIntroViewControllerDelegate> delegate;

/// Alert Controller to present after a button is pressed on one of the pages
@property (nonatomic, nullable, strong) UIAlertController *alertController;

/// Create an `MRDIntroViewController` nested in a `UINavigationController` for presenting
/// @param pages    array of `MRIntroPage` instances to present
/// @param delegate optional delegate to set
+ (nonnull UINavigationController *)introViewControllerWithPages:(nonnull NSArray <MRDIntroPage *> *)pages delegate:(nullable id<MRDIntroViewControllerDelegate>)delegate;
@end

@protocol MRDIntroViewControllerDelegate <NSObject>
/// Our `MRDIntroViewController` will been dismissed via a button action
/// on one of our pages
/// @param introViewController the `MRDIntroViewController` instance that generated this call
- (void)introViewControllerWillDismiss:(nonnull MRDIntroViewController *)introViewController;

@optional
/// Our `MRDIntroViewController` did press the `done` bar button to cancel
/// @param introViewController the `MRDIntroViewController` instance that generated this call
- (void)introViewControllerDidCancel:(nonnull MRDIntroViewController *)introViewController;

@end
