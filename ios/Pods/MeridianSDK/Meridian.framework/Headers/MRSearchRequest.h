//
//  MRSearchRequest.h
//  Meridian
//
//  Copyright © 2017 Aruba Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Describes a search to be performed on Meridian servers.
 */

@interface MRSearchRequest : NSObject
NS_ASSUME_NONNULL_BEGIN

/// The search term to use when filtering results.
@property (nonatomic, copy, nullable) NSString *naturalLanguageQuery;

/// The Meridian app whose data should be searched.
@property (nonatomic, copy) MREditorKey *app;

/// Limits the number of results. Limit results to 20 or less for best performance.
@property (nonatomic, assign) NSInteger limit;

NS_ASSUME_NONNULL_END
@end
