//
//  MRDBottomSheetViewController.h
//  MRDBottomSheet
//
//  Created by Cody Garvin on 6/7/17.
//  Copyright © 2017 Aruba Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MRDBottomSheetProtocols.h"

extern CGFloat MRBottomSheetCornerRadius;
extern CGFloat MRBottomDropShadowThickness;
extern CGFloat MRDBottomStopHeightThreshold;

@class MRDBottomSheetAbstractController;

extern CGFloat MRDBottomSheetCornerRadius;

typedef NS_ENUM(NSInteger, MRDBottomSheetState) {
    MRDBottomSheetStateHidden,
    MRDBottomSheetStateMinimum,
    MRDBottomSheetStateMiddle,
    MRDBottomSheetStateMaximum,
};

typedef NS_ENUM(NSInteger, MRDBottomSheetAllowance) {
    MRDBottomSheetAllowanceThreeState,
    MRDBottomSheetAlloanceTwoState,
    MRDBottomSheetAllowanceNone,
};

extern CGFloat MRDBottomDropShadowThickness;

@class MRDBottomSheetAbstractController;


/**
 A sheet that holds interactive content, usually in a table view. This has a 
 blurry / drop shadow view that is useful for on screen interaction at the same 
 time keeping the view it relates to in perspective.
 */
@interface MRDBottomSheetViewController : UIViewController

/// Delegate mediator allows anyone to register / unregister for delegation calls
/// as there may be more than one party that needs to know about events
@property (nonatomic, nullable, strong) id<MRDBottomSheetMediator> mediator;

/// Whether the child content controller should be able to scroll.
@property (nonatomic, readonly) BOOL scrollEnabled;


/// Whether the sheet view is able to be brought to a state beyond minimum.
/// eg: middle or maximum.
@property (nonatomic, assign) BOOL expandable;


/**
 Convenience method to add a bottom sheet with a detail view to a main view 
 controller. Create the childSheetController that lives inside the sheet and pass
 along your main parent controller to win at life. This control takes the 
 appearance of a sheet found in Apple Maps. This is usually the recommended path
 for most use cases.

 @param childSheetController An instance of MRDBottomSheetAbstractController that will
 contain the main functionality of the sheet inner views.
 @param parentHostController The main view controller that will host the sheet.
 @param expandable If NO, the default height is where it will always be. Still 
 determined by the child if it can be completely dismissed however.
 @return An instance of the sheet that was added to the view controller passed. 
 nil if one was not successfully created.
 */
+ (nullable MRDBottomSheetViewController *)addSheetController:(nonnull MRDBottomSheetAbstractController *)childSheetController
                                                 toController:(nonnull UIViewController *)parentHostController
                                                   expandable:(BOOL)expandable;

/**
 Convenience method to add a bottom sheet with a detail view to a main view
 controller. Create the childSheetController that lives inside the sheet and pass
 along your main parent controller to win at life. This control takes the
 appearance of a sheet found in Apple Maps. This is reserved for instances where
 the mapview is in a constrained subview. These sheets expand to the max size
 of the subview.
 
 @param childSheetController An instance of MRDBottomSheetAbstractController that will
 contain the main functionality of the sheet inner views.
 @param parentHostController The main view controller that will host the sheet.
 @return An instance of the sheet that was added to the view controller passed.
 nil if one was not successfully created.
 */
+ (nullable MRDBottomSheetViewController *)addEmbeddedSheetController:(nonnull MRDBottomSheetAbstractController *)childSheetController
                                                         toController:(nonnull UIViewController *)parentHostController;

/**
 Convenience method to add a bottom sheet with a detail view to a main view
 controller.

 @see + (nullable MRDBottomSheetViewController *)addSheetController:(nonnull MRDBottomSheetAbstractController *)childSheetController
 toController:(nonnull UIViewController *)parentHostController
 expandable:(BOOL)expandable

 @param childSheetController An instance of MRDBottomSheetAbstractController that will
 contain the main functionality of the sheet inner views.
 @param parentHostController The main view controller that will host the sheet.
 @return An instance of the sheet that was added to the view controller passed.
 nil if one was not successfully created.
 */
+ (nullable MRDBottomSheetViewController *)addSheetController:(nonnull MRDBottomSheetAbstractController *)childSheetController
                                                 toController:(nonnull UIViewController *)parentHostController;

/**
 Convenience method to quickly remove a bottom sheet from a view controller.

 @param childSheetController An instance of MRDBottomSheetViewController that will be
 removed from your view controller.
 */
+ (void)removeSheetController:(nonnull MRDBottomSheetViewController *)childSheetController;

/**
 Creates an instance of MRDBottomSheetViewController with a view controller in 
 the content of such. The content controller should be of instance 
 MRDBottomSheetAbstractController.

 @param controller MRDBottomSheetAbstractController that tightly integrates with 
 MRDBottomSheetViewController via scrolling ability and dismissing. This is an 
 area that should be improved when migrated to Swift 3.2 / 4.0, protocol 
 extensions with default implementations instead of aggregation.
 @param minimumHeight The height the control should be from the bottom when 
 completely minimized.
 @return An instance of MRDBottomSheetViewController.
 */
- (nonnull instancetype)initWithViewController:(nonnull MRDBottomSheetAbstractController *)controller
                   withMinimumHeightFromBottom:(CGFloat)minimumHeight;

/**
 Creates an instance of MRDBottomSheetViewController with a view controller in 
 the content of such. The content controller should be of instance 
 MRDBottomSheetAbstractController.
 
 @param controller MRDBottomSheetAbstractController that tightly integrates with 
 MRDBottomSheetViewController via scrolling ability and dismissing. This is an 
 area that should be improved when migrated to Swift 3.2 / 4.0, protocol extensions 
 with default implementations instead of aggregation.
 @param minimumHeight The height the control should be from the bottom when 
 completely minimized.
 @param expandable If NO, the default height is where it will always be. Still 
 determined by the
 child if it can be completely dismissed however.
 @return An instance of MRDBottomSheetViewController.
 */
- (nonnull instancetype)initWithViewController:(nonnull MRDBottomSheetAbstractController *)controller
                   withMinimumHeightFromBottom:(CGFloat)minimumHeight
                                    expandable:(BOOL)expandable;

/**
 If the sheet is hidden show and animate it to the minimum height specified. 
 If the sheet is already displayed, it will move to the default position 
 specified by minimumHeight. 
 @see initWithViewController:withMinimumHeightFromBottom:

 @param animated Whether the sheet will animate to that position or snap to it 
 instantly.
 @param completionHandler The block to be executed when animation completes, or
 if no animation is enabled executed immediately.

 */
- (void)showAtState:(MRDBottomSheetState)state
           animated:(BOOL)animated
         completion:(void(^_Nullable)(void))completionHandler;

/**
 Moves the sheet below the bottom of the screen and hides it. Animates to the 
 bottom if specified. Removes it from the parent controller after.
 
 @param animated Whether the sheet will animate to the bottom of the screen or 
 instantly disappear.
 */
- (void)closeAndRemove:(BOOL)animated;

/**
 Moves the sheet below the bottom of the screen and hides it. Animates to the
 bottom if specified. Removes it from the parent controller after.

 @param animated Whether the sheet will animate to the bottom of the screen or
 instantly disappear.
 @param forced YES if we should force removal even if the sheet is sticky. NO if not
 */
- (void)closeAndRemoveAnimated:(BOOL)animated forceRemoval:(BOOL)forced;


/**
 Enable forwarding gestures by exposing the method that is used to move the 
 sheet up and down.

 @param recognizer UIPanGestureRecognizer that contains translation values.
 */
- (void)dragGesture:(nonnull UIPanGestureRecognizer *)recognizer;
@end
